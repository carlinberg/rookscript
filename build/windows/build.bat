@echo off

:: initialize msvc to be callable from command line, %2 is x86 or x64
call vcvarsall.bat %2

:: create or clean bin and temp directories
if exist "bin" (
	del bin\* /q
)else (
	mkdir bin
)
if exist "temp" (
	del temp\* /q
)else (
	mkdir temp
)

mkdir temp\base
mkdir temp\terminal
mkdir temp\tests


:: set compiler flags, 4013=undeclared function, 4133=pointer type mismatch, 4047=pointer differs in level of indirection, 4100=unreferenced formal parameter
set flags=/W4 /we"4013" /we"4133" /we"4047" /we"4552" /wd"4100" /FC /I .\rook

if %1 == d ( set flags=%flags% /Zi /Od )
if %1 == r ( set flags=%flags% /O2 /MT )

if %2 == x64 ( set flags=%flags% /DROOK_64 )
if %2 == x32 ( set flags=%flags% /DROOK_32 )


:: compile utilities
cl %flags% /Fd.\temp\base\utility.pdb /FS /c /Fo.\temp\base\ .\rook\utility\*.c

:: compile core files
cl %flags% /Fd.\temp\base\compiler.pdb /FS /c /Fo.\temp\base\ .\rook\core\compiler\*.c
cl %flags% /Fd.\temp\base\runtime.pdb /FS /c /Fo.\temp\base\ .\rook\core\runtime\*.c

:: compile library files
cl %flags% /Fd.\temp\base\library.pdb /FS /c /Fo.\temp\base\ .\rook\library\*.c

:: compile terminal files
cl %flags% /Fd.\temp\terminal\terminal.pdb /FS /c /Fo.\temp\terminal\ .\rook\terminal\*.c

:: compile test files
cl %flags% /Fd.\temp\tests\tests.pdb /FS /c /Fo.\temp\tests\ .\rook\tests\*.c

:: compile terminal executable from obj files
cl %flags% /Fe.\bin\rook.exe .\temp\base\*.obj .\temp\terminal\*.obj user32.lib Shell32.lib Comdlg32.lib Kernel32.lib

:: compile test executable from obj files
cl %flags% /Fe.\bin\rook_tests.exe .\temp\base\*.obj .\temp\tests\*.obj user32.lib Shell32.lib Comdlg32.lib Kernel32.lib

echo ============ Rook build is done! ============