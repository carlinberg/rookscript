# compiler
compiler=clang


# create or clean bin and temp directories

if [ ! -d "bin" ]
then
	mkdir bin
else
    rm -f bin/*
fi

if [ ! -d "temp" ]
then
	mkdir temp
else
    rm -rf temp/*
fi

mkdir temp/base
mkdir temp/terminal
mkdir temp/tests


# set $compiler flags

flags="-Wall -Werror=implicit -Irook"

if [ $1 = d ]
then
	flags="$flags -g -O0"
else
	flags="$flags -O2"
fi

if [ $2 = x64 ]
then
	flags="$flags -m64 -DROOK_64"
else
	flags="$flags -m32 -DROOK_32"
fi


# compile utilities
for f in ./rook/utility/*.c; do
	echo "${f##*/}"
	$compiler -c "$f" $flags -o temp/base/"${f##*/}".o
done

# compile core files
for f in ./rook/core/compiler/*.c; do
	echo "${f##*/}"
	$compiler -c "$f" $flags -o temp/base/"${f##*/}".o
done

for f in ./rook/core/runtime/*.c; do
	echo "${f##*/}"
	$compiler -c "$f" $flags -o temp/base/"${f##*/}".o
done

# compile library files
for f in ./rook/library/*.c; do
	echo "${f##*/}"
	$compiler -c "$f" $flags -o temp/base/"${f##*/}".o
done

# compile terminal files
for f in ./rook/terminal/*.c; do
	echo "${f##*/}"
	$compiler -c "$f" $flags -o temp/terminal/"${f##*/}".o
done

# compile test files
for f in ./rook/tests/*.c; do
	echo "${f##*/}"
	$compiler -c "$f" $flags -o temp/tests/"${f##*/}".o
done


# compile terminal executable from obj files
$compiler temp/base/*.o temp/terminal/*.o -o bin/rook -lm

# compile test executable from obj files
$compiler temp/base/*.o temp/tests/*.o -o bin/rook_tests -lm

echo ============ Rook build is done! ============