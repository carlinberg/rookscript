#!/bin/bash

mkdir bin
mkdir lib
mkdir temp/rookscript
mkdir temp/rookscripttest

cd source/rookscript
gcc -c *.c -o ../../temp/rookscript/

cd ../../temp/rookscript
ar -rc ../../lib/rookscript.a *.obj

cd ../../source/rookscripttest
gcc *.c -o ../../bin/rookscripttest_d64 -L../../lib -lrookscript

cd ../../bin
./rookscripttest_d64

read -p "Press enter to continue"
