#ifndef ROOK_BYTECODE_GENERATOR
#define ROOK_BYTECODE_GENERATOR

#include <utility/rook_allocators.h>
#include <core/runtime/rook_bytecode_chunk.h>

#include "rook_callframe.h"
#include "rook_scope.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_stmt_base rook_stmt_base;
typedef struct rook_error rook_error;

typedef struct rook_bytecode_generator
{
	rook_callframe callframe;
	rook_scope scope;
	rook_bytecode_chunk* chunk;
	rook_allocator* runtime_allocator;
	rook_cstring file;
	rook_cstring code;
	rook_error** errors;
} rook_bytecode_generator;

void rook_bytecode_generator_init(rook_bytecode_generator* g, rook_allocator* runtime_allocator, rook_cstring file, rook_cstring code, rook_error** errors, rook_callframe* parent_callframe);
void rook_bytecode_generator_free(rook_bytecode_generator* g);
int rook_bytecode_generator_run(rook_bytecode_generator* g, rook_stmt_base** stmts, int stmt_count, rook_bytecode_chunk* chunk);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_BYTECODE_GENERATOR
