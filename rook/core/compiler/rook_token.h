#ifndef ROOK_TOKEN
#define ROOK_TOKEN

#include <utility/rook_string.h>
#include "rook_token_type.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_token
{
	rook_token_type type;

	rook_cstring lexeme;

	int line;
	int column;
} rook_token;

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_TOKEN
