#ifndef ROOK_STMT
#define ROOK_STMT

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "rook_token_type.h"

typedef struct rook_token rook_token;
typedef struct rook_expr_base rook_expr_base;
typedef struct rook_cstring rook_cstring;

typedef enum rook_stmt_type
{
	rook_stmt_type_empty,
	rook_stmt_type_local,
	rook_stmt_type_global,
	rook_stmt_type_assignment,
	rook_stmt_type_assignment_local,
	rook_stmt_type_assignment_captured_value,
	rook_stmt_type_assignment_global,
	rook_stmt_type_set,
	rook_stmt_type_subscr_set,
	rook_stmt_type_set_comp_assign,

	rook_stmt_type_exprstmt,
	rook_stmt_type_block,
	rook_stmt_type_return,
	//rook_stmt_type_defer,

	rook_stmt_type_if,
	rook_stmt_type_while,
	rook_stmt_type_for,
	rook_stmt_type_for_each,
	rook_stmt_type_break,
	rook_stmt_type_continue
} rook_stmt_type;

typedef struct rook_stmt_base
{
	rook_stmt_type type;
	rook_token* begin;
	rook_token* end;
} rook_stmt_base;

typedef struct rook_stmt_empty
{
	rook_stmt_base base;
} rook_stmt_empty;

typedef struct rook_stmt_assignment //or decl
{
	rook_stmt_base base;

	rook_token** names;
	int name_count;
	rook_expr_base** vals;
	int val_count;
	rook_cstring* attrs;
	int attr_count;
	int* idxs; //idx_count=name_count
	rook_stmt_type* types; //type_count=name_count
} rook_stmt_assignment;

typedef struct rook_stmt_set
{
	rook_stmt_base base;

	rook_expr_base* map;
	rook_token* name;
	rook_expr_base* val;
} rook_stmt_set;

typedef struct rook_stmt_subscr_set
{
	rook_stmt_base base;

	rook_expr_base* collection;
	rook_expr_base* key;
	rook_expr_base* val;
} rook_stmt_subscr_set;

typedef struct rook_stmt_set_comp_assign
{
	rook_stmt_base base;

	rook_expr_base* collection;
	rook_expr_base* key;
	rook_expr_base* val;
	rook_token_type op;
} rook_stmt_set_comp_assign;

typedef struct rook_stmt_exprstmt
{
	rook_stmt_base base;

	rook_expr_base* expr;
} rook_stmt_exprstmt;

typedef struct rook_stmt_block
{
	rook_stmt_base base;

	int stmt_count;
	rook_stmt_base* stmts[1];
} rook_stmt_block;

typedef struct rook_stmt_return
{
	rook_stmt_base base;

	int expr_count;
	rook_expr_base* exprs[1];
} rook_stmt_return;

typedef struct rook_stmt_if
{
	rook_stmt_base base;

	rook_expr_base* cond;
	rook_stmt_base* then;
	rook_stmt_base* else_;
} rook_stmt_if;

typedef struct rook_stmt_while
{
	rook_stmt_base base;

	rook_expr_base* cond;
	rook_stmt_base* body;
} rook_stmt_while;

typedef struct rook_stmt_for
{
	rook_stmt_base base;

	rook_stmt_base* init;
	rook_expr_base* cond;
	rook_expr_base* incr;
	rook_stmt_base* body;
} rook_stmt_for;

typedef struct rook_stmt_for_each
{
	rook_stmt_base base;
	
	rook_token* key_name;
	rook_token* value_name;
	rook_expr_base* container;
	rook_stmt_base* body;
} rook_stmt_for_each;

typedef struct rook_stmt_break
{
	rook_stmt_base base;
} rook_stmt_break;

typedef struct rook_stmt_continue
{
	rook_stmt_base base;
} rook_stmt_continue;

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_STMT
