#ifndef ROOK_SCOPE
#define ROOK_SCOPE

#include <utility/rook_string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_obj rook_obj;
typedef struct rook_allocator rook_allocator;

typedef struct rook_local_var
{
	rook_cstring name;
	int stack_idx;
} rook_local_var;

typedef struct rook_scope
{
	struct rook_scope* parent;
	rook_local_var* locals;
} rook_scope;

void rook_scope_init(rook_scope* scope, rook_scope* parent);
void rook_scope_free(rook_scope* scope);

int rook_scope_add_local(rook_scope* scope, rook_cstring name, int stack_idx);
void rook_scope_add_unnamed_local(rook_scope* scope, int stack_idx);
int rook_scope_get_local(rook_scope* scope, rook_cstring name);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_SCOPE
