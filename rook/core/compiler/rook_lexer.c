#include "rook_lexer.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <utility/stb_sb.h>
#include <utility/rook_malloc.h>
#include <utility/rook_error.h>

#include "rook_parser.h"
#include "rook_token.h"
#include "rook_token_type.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

static const char escape_table[256] =
{
	['a'] = '\a', ['b']  = '\b', ['f']  = '\f',
	['n'] = '\n', ['r']  = '\r', ['t']  = '\t',
	['v'] = '\v', ['\\'] = '\\', ['\''] = '\'',
	['"'] =  '"', ['?']  = '\?',
};

typedef struct token_stream_builder
{
	rook_token* tokens;
	int size;
	int capacity;
} token_stream_builder;

static rook_token_type check_keyword(const char* const begin, const char* const end, int start, int length, const char* rest, rook_token_type type);
static rook_token_type get_keyword_or_identifier(const char* const begin, const char* const end);
static const char* build_symbol(token_stream_builder* builder, int* line, int* column, const char* it);

static void add_token(token_stream_builder* tokens, rook_token_type type, const char* lexeme, int lexeme_length, int line, int column)
{
	if (tokens->size == tokens->capacity)
	{
		tokens->capacity *= 2;
		rook_token* new_tokens = ROOK_REALLOC(tokens->tokens, sizeof(*tokens->tokens) * tokens->capacity);
		assert(new_tokens);
		tokens->tokens = new_tokens;
	}

	rook_token new_token;
	new_token.type = type;
	new_token.lexeme.chars = lexeme;
	new_token.lexeme.length = lexeme_length;
	new_token.line = line;
	new_token.column = column;

	tokens->tokens[tokens->size++] = new_token;
}

int rook_lex(const char* codepiece, rook_parser* parser)
{	
	token_stream_builder builder;
	builder.capacity = 512;
	builder.size = 0;
	builder.tokens = ROOK_ALLOC(builder.capacity * sizeof(*builder.tokens));

	int line = 1;
	int column = 0;

	const char* it = codepiece;
	const char* last = it;

	while (1)
	{
		column += (int)(it - last);
		last = it;

		switch (*it)
		{
		case '\0':
			parser->tokens = builder.tokens;
			parser->token_count = builder.size;
			return sb_count(*parser->errors) == 0;
		case '\n':
			++line;
			last = it + 1;
			column = 0;
		case '\r':
		case ' ':
			++it;
			break;
		case '\t':
			++it;
			column += 3;
			break;

		case ';':
			add_token(&builder, rook_token_type_semicolon, it, 1, line, column);
			++it;
			break;
		case ',':
			add_token(&builder, rook_token_type_comma, it, 1, line, column);
			++it;
			break;
		case '.':
			add_token(&builder, rook_token_type_dot, it, 1, line, column);
			++it;
			break;
		case '(':
			add_token(&builder, rook_token_type_left_par, it, 1, line, column);
			++it;
			break;
		case ')':
			add_token(&builder, rook_token_type_right_par, it, 1, line, column);
			++it;
			break;
		case '[':
			add_token(&builder, rook_token_type_left_square, it, 1, line, column);
			++it;
			break;
		case ']':
			add_token(&builder, rook_token_type_right_square, it, 1, line, column);
			++it;
			break;
		case '{':
			add_token(&builder, rook_token_type_left_curly, it, 1, line, column);
			++it;
			break;
		case '}':
			add_token(&builder, rook_token_type_right_curly, it, 1, line, column);
			++it;
			break;

		case ':':
			add_token(&builder, rook_token_type_colon, it, 1, line, column);
			++it;
			break;

		case '=':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_equal, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_assignment, it, 1, line, column);
				++it;
			}
			break;

		case '+':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_add_assign, it, 2, line, column);
				it += 2;
			}
			else if (it[1] == '+')
			{
				add_token(&builder, rook_token_type_increment, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_add, it, 1, line, column);
				++it;
			}
			break;
		case '-':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_sub_assign, it, 2, line, column);
				it += 2;
			}
			else if (it[1] == '>')
			{
				add_token(&builder, rook_token_type_arrow, it, 2, line, column);
				it += 2;
			}
			else if (it[1] == '-')
			{
				add_token(&builder, rook_token_type_decrement, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_sub, it, 1, line, column);
				++it;
			}
			break;
		case '*':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_star_assign, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_star, it, 1, line, column);
				++it;
			}
			break;
		case '/':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_div_assign, it, 2, line, column);
				it += 2;
			}
			else if (it[1] == '/')
			{
				for (it += 2; *it != '\n' && *it != '\r' && *it != '\0'; ++it);
			}
			else
			{
				add_token(&builder, rook_token_type_div, it, 1, line, column);
				++it;
			}
			break;
		case '%':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_modulo_assign, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_modulo, it, 1, line, column);
				++it;
			}
			break;

		case '&':
			if (it[1] == '&')
			{
				add_token(&builder, rook_token_type_and, it, 2, line, column);
				it += 2;
			}
			else
			{
				rook_errorlist_add_literal(parser->errors, "Binary and '&' not supported", parser->file, parser->code, line, column);
				++it;
			}
			break;
		case '|':
			if (it[1] == '|')
			{
				add_token(&builder, rook_token_type_or, it, 2, line, column);
				it += 2;
			}
			else
			{
				rook_errorlist_add_literal(parser->errors, "Binary or '|' not supported", parser->file, parser->code, line, column);
				++it;
			}
			break;
		case '<':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_less_eq, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_less, it, 1, line, column);
				++it;
			}
			break;
		case '>':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_greater_eq, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_greater, it, 1, line, column);
				++it;
			}
			break;
		case '!':
			if (it[1] == '=')
			{
				add_token(&builder, rook_token_type_not_eq, it, 2, line, column);
				it += 2;
			}
			else
			{
				add_token(&builder, rook_token_type_not, it, 1, line, column);
				++it;
			}
			break;

		case '\"':
		case '\'':
		{
			const char delimiter = *it;
			const char* first = ++it;
			char* allocated = 0;
			int escaped = 0;
			int length = 0;
			int i = 0;
			int allocated_length = 0;

			while (*it != delimiter)
			{
				unsigned char c = *(unsigned char*)it++;

				if (c == '\\')
				{
					c = *(unsigned char*)it++;
					
					if (escape_table[c])
						escaped = 1;
				}

				if (c == '\0')
				{
					rook_errorlist_add_literal(parser->errors, "Unexpected nul terminator '\\0' in middle of string", parser->file, parser->code, line, column);
					return 0;
				}
				
			}

			length = (int)(it - first);

			if (escaped)
			{
				allocated = (char*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(char) * length);

				for (i = 0; i < length; ++i)
				{
					if (first[i] != '\\')
					{
						allocated[allocated_length++] = first[i];
					}
					else
					{
						char next = first[++i];
						if (escape_table[next])
							allocated[allocated_length++] = escape_table[next];
						else
							allocated[allocated_length++] = next;
					}
				}

				first = allocated;
				length = allocated_length;
			}

			add_token(&builder, rook_token_type_string, first, length, line, column);
			++it;
		}
		break;

		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		{
			int has_dot = 0;

			for (const char* first = it++;;)
			{
				switch (*it)
				{
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					++it;
					break;
				case '.':
					if (has_dot)
					{
						rook_errorlist_add_literal(parser->errors, "More than one dot in number literal", parser->file, parser->code, line, column);
						++it;
						goto number_lexing_done;
					}
					has_dot = 1;
					++it;
					break;
				default:
					add_token(&builder, rook_token_type_number, first, (int)(it - first), line, column);
					goto number_lexing_done;
				}
			}
		}
	number_lexing_done:
		break;

		default:
			if (*it != '_' && ((*it < 'a' || *it > 'z') && (*it < 'A' || *it > 'Z')))
			{
				rook_errorlist_add_errorf(parser->errors, parser->file, parser->code, line, column, "Invalid token '%c'", *it);
				++it;
				break;
			}
			it = build_symbol(&builder, &line, &column, it);
			break;
		}
	}
}

static const char* build_symbol(token_stream_builder* builder, int* line, int* column, const char* it)
{
	for (const char* first = it;;)
	{
		const char c = *++it;
		if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_'))
		{
			rook_token_type type = get_keyword_or_identifier(first, it);
			add_token(builder, type, first, (int)(it - first), *line, *column);
			return it;
		}
	}
}

static rook_token_type check_keyword(const char* const begin, const char* const end, int start, int length, const char* rest, rook_token_type type)
{
	if ((size_t)(end - begin) == (size_t)start + (size_t)length && memcmp(begin + start, rest, length) == 0)
		return type;

	return rook_token_type_identifier;
}

static rook_token_type get_keyword_or_identifier(const char* const begin, const char* const end)
{
	switch (*begin)
	{
	case 'b':
		return check_keyword(begin, end, 1, 4, "reak", rook_token_type_break);
	case 'i':
		if (end - begin == 2 && begin[1] == 'f')
			return rook_token_type_if;
		break;
	case 'f':
		if (end - begin > 1)
		{
			switch (begin[1])
			{
			case 'a':
				return check_keyword(begin, end, 2, 3, "lse", rook_token_type_false);
			case 'o':
				return check_keyword(begin, end, 2, 1, "r", rook_token_type_for);
			}
		}
		break;
	case 'c':
		if (end - begin > 1)
		{
			switch (begin[1])
			{
			case 'o':
				if (end - begin == sizeof("continue") - 1 && memcmp(begin + 2, "ntinue", sizeof("ntinue") - 1) == 0)
					return rook_token_type_continue;
				if (end - begin == sizeof("const") - 1 && memcmp(begin + 2, "nst", sizeof("nst") - 1) == 0)
					return rook_token_type_const;
				break;
			case 'a':
				if (end - begin == 4 && begin[2] == 's' && begin[3] == 'e')
					return rook_token_type_case;
				break;
			}
		}
		break;
	case 'n':
		return check_keyword(begin, end, 1, 3, "ull", rook_token_type_null);
	case 't':
		return check_keyword(begin, end, 1, 3, "rue", rook_token_type_true);
	case 'e':
		return check_keyword(begin, end, 1, 3, "lse", rook_token_type_else);
	case 'l':
		return check_keyword(begin, end, 1, 4, "ocal", rook_token_type_local);
	case 'w':
		return check_keyword(begin, end, 1, 4, "hile", rook_token_type_while);
	case 'r':
		return check_keyword(begin, end, 1, 5, "eturn", rook_token_type_return);
	case 'd':
		return check_keyword(begin, end, 1, 4, "efer", rook_token_type_defer);
	case 'g':
		return check_keyword(begin, end, 1, 5, "lobal", rook_token_type_global);
	}

	return rook_token_type_identifier;
}

#ifdef __cplusplus
}
#endif // __cplusplus
