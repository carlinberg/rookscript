#ifndef ROOK_LEXER
#define ROOK_LEXER

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_parser rook_parser;

int rook_lex(const char* codepiece, rook_parser* parser);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_LEXER
