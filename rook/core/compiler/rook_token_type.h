#ifndef ROOK_TOKEN_TYPE
#define ROOK_TOKEN_TYPE

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef enum rook_token_type
{
	rook_token_type_identifier,
	rook_token_type_null,
	rook_token_type_true,
	rook_token_type_false,
	rook_token_type_number,
	rook_token_type_string,

	rook_token_type_assignment,
	rook_token_type_colon,
	rook_token_type_semicolon,
	rook_token_type_comma,
	rook_token_type_dot,
	rook_token_type_arrow,

	rook_token_type_left_par,
	rook_token_type_right_par,
	
	rook_token_type_left_square,
	rook_token_type_right_square,

	rook_token_type_left_curly,
	rook_token_type_right_curly,
	
	rook_token_type_add,
	rook_token_type_sub,
	rook_token_type_star,
	rook_token_type_div,
	rook_token_type_modulo,

	rook_token_type_add_assign,
	rook_token_type_sub_assign,
	rook_token_type_star_assign,
	rook_token_type_div_assign,
	rook_token_type_modulo_assign,
	rook_token_type_increment,
	rook_token_type_decrement,

	rook_token_type_equal,
	rook_token_type_not_eq,
	rook_token_type_less,
	rook_token_type_less_eq,
	rook_token_type_greater,
	rook_token_type_greater_eq,

	rook_token_type_not,
	rook_token_type_and,
	rook_token_type_or,

	rook_token_type_global,
	rook_token_type_local,
	rook_token_type_const,
	rook_token_type_if,
	rook_token_type_else,
	rook_token_type_while,
	rook_token_type_for,
	rook_token_type_case,
	rook_token_type_break,
	rook_token_type_continue,
	rook_token_type_defer,
	rook_token_type_return,
} rook_token_type;

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_TOKEN_TYPE
