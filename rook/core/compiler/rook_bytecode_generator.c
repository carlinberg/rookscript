#include "rook_bytecode_generator.h"

#include <utility/rook_error.h>
#include <utility/stb_sb.h>

#include "rook_stmt.h"
#include "rook_expr.h"
#include "rook_token.h"
#include "rook_token_type.h"

#include <core/runtime/rook_object.h>
#include <core/runtime/rook_func.h>


#include <assert.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

static void generate_stmt(rook_bytecode_generator* g, rook_stmt_base* stmt);
static void generate_expr(rook_bytecode_generator* g, rook_expr_base* expr);

static void generate_local(rook_bytecode_generator* g, rook_stmt_assignment* stmt);
static void generate_global(rook_bytecode_generator* g, rook_stmt_assignment* stmt);
static void generate_assignment(rook_bytecode_generator* g, rook_stmt_assignment* stmt);
static void generate_set(rook_bytecode_generator* g, rook_stmt_set* stmt);
static void generate_subscr_set(rook_bytecode_generator* g, rook_stmt_subscr_set* stmt);
static void generate_set_comp_assign(rook_bytecode_generator* g, rook_stmt_set_comp_assign* stmt);
static void generate_exprstmt(rook_bytecode_generator* g, rook_stmt_exprstmt* stmt);
static void generate_block(rook_bytecode_generator* g, rook_stmt_block* stmt);
static void generate_return(rook_bytecode_generator* g, rook_stmt_return* stmt);
static void generate_if(rook_bytecode_generator* g, rook_stmt_if* stmt);
static void generate_while(rook_bytecode_generator* g, rook_stmt_while* stmt);
static void generate_for(rook_bytecode_generator* g, rook_stmt_for* stmt);
static void generate_for_each(rook_bytecode_generator* g, rook_stmt_for_each* stmt);
static void generate_break(rook_bytecode_generator* g, rook_stmt_break* stmt);
static void generate_continue(rook_bytecode_generator* g, rook_stmt_continue* stmt);

static void generate_basic_literal(rook_bytecode_generator* g, rook_expr_basic_literal* expr);
static void generate_array_literal(rook_bytecode_generator* g, rook_expr_array_literal* expr);
static void generate_map_literal(rook_bytecode_generator* g, rook_expr_map_literal* expr);
static void generate_func_decl(rook_bytecode_generator* g, rook_expr_func_decl* expr);
static void generate_preunary(rook_bytecode_generator* g, rook_expr_unary* expr);
static void generate_postunary(rook_bytecode_generator* g, rook_expr_unary* expr);
static void generate_binary(rook_bytecode_generator* g, rook_expr_binary* expr);
static void generate_logical(rook_bytecode_generator* g, rook_expr_binary* expr);
static void generate_var_read_local(rook_bytecode_generator* g, rook_expr_var_read_indexed* expr);
static void generate_var_read_captured_value(rook_bytecode_generator* g, rook_expr_var_read_indexed* expr);
static void generate_var_read_global(rook_bytecode_generator* g, rook_expr_var_read* expr);
static void generate_func_call(rook_bytecode_generator* g, rook_expr_func_call* expr);
static void generate_subscr(rook_bytecode_generator* g, rook_expr_subscr* expr);
static void generate_field_access(rook_bytecode_generator* g, rook_expr_field_access* expr);
static void generate_compound(rook_bytecode_generator* g, rook_expr_compound* expr);

static int add_jump(rook_bytecode_generator* g, u8 jump_code, void* stmt);
static void patch_jump(rook_bytecode_generator* g, void* stmt, int to);
static void patch_jump_back(rook_bytecode_generator* g, void* stmt, int to);

#define add_generator_errorc(g, s, m) rook_errorlist_add_literal((g)->errors, m, (g)->file, (g)->code, (s)->base.begin->line, (s)->base.begin->column)

void rook_bytecode_generator_init(rook_bytecode_generator* g, rook_allocator* runtime_allocator, rook_cstring file, rook_cstring code, rook_error** errors, rook_callframe* parent_callframe)
{
	rook_scope_init(&g->scope, 0);
	rook_callframe_init(&g->callframe, parent_callframe, &g->scope);
	rook_callframe_add_local(&g->callframe, rook_cstring_from_literal("this"));

	g->chunk = 0;
	g->errors = errors;
	g->runtime_allocator = runtime_allocator;
	g->file = file;
	g->code = code;
}

void rook_bytecode_generator_free(rook_bytecode_generator* g)
{
	if(g->chunk)
		rook_bytecode_chunk_close_local_infos(g->chunk, 0, sb_count(g->chunk->code));

	rook_scope_free(&g->scope);
	rook_callframe_free(&g->callframe);

	rook_cstring_init(&g->file, 0, 0);
	rook_cstring_init(&g->code, 0, 0);

	g->chunk = 0;
	g->runtime_allocator = 0;
	g->errors = 0;
}

int rook_bytecode_generator_run(rook_bytecode_generator* g, rook_stmt_base** stmts, int stmt_count, rook_bytecode_chunk* chunk)
{
	g->chunk = chunk;

	if (stmts != 0 && stmt_count > 0)
	{
		for (int i = 0; i < stmt_count; ++i)
		{
			rook_stmt_base* stmt = stmts[i];

			generate_stmt(g, stmt);
		}

		rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmts[stmt_count - 1]->end->line, stmts[stmt_count - 1]->end->column);
		rook_bytecode_chunk_write(g->chunk, rook_opcode_return, stmts[stmt_count - 1]->end->line, stmts[stmt_count - 1]->end->column);
	}
	else
	{
		rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, 0, 0);
		rook_bytecode_chunk_write(g->chunk, rook_opcode_return, 0, 0);
	}

	return sb_count(*g->errors) == 0;
}

void generate_stmt(rook_bytecode_generator* g, rook_stmt_base* stmt)
{
	switch (stmt->type)
	{
	case rook_stmt_type_empty:
		break;
	case rook_stmt_type_local:
		generate_local(g, (rook_stmt_assignment*)stmt);
		break;
	case rook_stmt_type_global:
		generate_global(g, (rook_stmt_assignment*)stmt);
		break;
	case rook_stmt_type_assignment:
		generate_assignment(g, (rook_stmt_assignment*)stmt);
		break;
	case rook_stmt_type_set:
		generate_set(g, (rook_stmt_set*)stmt);
		break;
	case rook_stmt_type_subscr_set:
		generate_subscr_set(g, (rook_stmt_subscr_set*)stmt);
		break;
	case rook_stmt_type_set_comp_assign:
		generate_set_comp_assign(g, (rook_stmt_set_comp_assign*)stmt);
		break;
	case rook_stmt_type_exprstmt:
		generate_exprstmt(g, (rook_stmt_exprstmt*)stmt);
		break;
	case rook_stmt_type_block:
		generate_block(g, (rook_stmt_block*)stmt);
		break;
	case rook_stmt_type_return:
		generate_return(g, (rook_stmt_return*)stmt);
		break;
	case rook_stmt_type_if:
		generate_if(g, (rook_stmt_if*)stmt);
		break;
	case rook_stmt_type_while:
		generate_while(g, (rook_stmt_while*)stmt);
		break;
	case rook_stmt_type_for:
		generate_for(g, (rook_stmt_for*)stmt);
		break;
	case rook_stmt_type_for_each:
		generate_for_each(g, (rook_stmt_for_each*)stmt);
		break;
	case rook_stmt_type_break:
		generate_break(g, (rook_stmt_break*)stmt);
		break;
	case rook_stmt_type_continue:
		generate_continue(g, (rook_stmt_continue*)stmt);
		break;
	default:
		assert(0);
	}
}

void generate_expr(rook_bytecode_generator* g, rook_expr_base* expr)
{
	switch (expr->type)
	{
	case rook_expr_type_basic_literal:
		generate_basic_literal(g, (rook_expr_basic_literal*)expr);
		break;
	case rook_expr_type_array_literal:
		generate_array_literal(g, (rook_expr_array_literal*)expr);
		break;
	case rook_expr_type_map_literal:
		generate_map_literal(g, (rook_expr_map_literal*)expr);
		break;
	case rook_expr_type_func_decl:
		generate_func_decl(g, (rook_expr_func_decl*)expr);
		break;
	case rook_expr_type_unary:
		generate_preunary(g, (rook_expr_unary*)expr);
		break;
	case rook_expr_type_postunary:
		generate_postunary(g, (rook_expr_unary*)expr);
		break;
	case rook_expr_type_binary:
		generate_binary(g, (rook_expr_binary*)expr);
		break;
	case rook_expr_type_logical:
		generate_logical(g, (rook_expr_binary*)expr);
		break;
	case rook_expr_type_var_read_local:
		generate_var_read_local(g, (rook_expr_var_read_indexed*)expr);
		break;
	case rook_expr_type_var_read_captured_value:
		generate_var_read_captured_value(g, (rook_expr_var_read_indexed*)expr);
		break;
	case rook_expr_type_var_read_global:
		generate_var_read_global(g, (rook_expr_var_read*)expr);
		break;
	case rook_expr_type_func_call:
		generate_func_call(g, (rook_expr_func_call*)expr);
		break;
	case rook_expr_type_subscr:
		generate_subscr(g, (rook_expr_subscr*)expr);
		break;
	case rook_expr_type_field_access:
		generate_field_access(g, (rook_expr_field_access*)expr);
		break;
	case rook_expr_type_compound:
		generate_compound(g, (rook_expr_compound*)expr);
		break;
	default:
		assert(0);
	}
}

void generate_local(rook_bytecode_generator* g, rook_stmt_assignment* stmt)
{
	int i;
	int first_stack_idx = g->callframe.local_count;

	for (i = 0; i < stmt->name_count; ++i)
		rook_callframe_add_local(&g->callframe, stmt->names[i]->lexeme);

	if (stmt->vals)
	{
		rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
		for (i = 0; i < stmt->val_count; ++i)
		{
			if (stmt->vals[i]->type == rook_expr_type_func_decl && i < stmt->name_count)
				((rook_expr_func_decl*)stmt->vals[i])->name = stmt->names[i]->lexeme;
			generate_expr(g, stmt->vals[i]);
		}

		rook_bytecode_chunk_write(g->chunk, rook_opcode_declare_locals, stmt->base.begin->line, stmt->base.begin->column);
		rook_bytecode_chunk_write(g->chunk, (u8)stmt->name_count, stmt->base.begin->line, stmt->base.begin->column);
	}
	else
	{
		for (i = 0; i < stmt->name_count; ++i)
			rook_bytecode_chunk_write(g->chunk, rook_opcode_load_null, stmt->base.begin->line, stmt->base.begin->column);
	}

	for (i = 0; i < stmt->name_count; ++i)
		rook_bytecode_chunk_add_local_info(g->chunk, rook_string_from_cstring(stmt->names[i]->lexeme), first_stack_idx++, sb_count(g->chunk->code));
}

void generate_global(rook_bytecode_generator* g, rook_stmt_assignment* stmt)
{
	int i = 0;

	if (stmt->vals)
	{
		for (i = 0; i < stmt->val_count; ++i)
		{
			rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
			if (stmt->vals[i]->type == rook_expr_type_func_decl && i < stmt->name_count)
				((rook_expr_func_decl*)stmt->vals[i])->name = stmt->names[i]->lexeme;
			generate_expr(g, stmt->vals[i]);
			rook_bytecode_chunk_write(g->chunk, rook_opcode_leave_first_expr_result, stmt->base.begin->line, stmt->base.begin->column);
		}

		for (; i < stmt->name_count; ++i)
			rook_bytecode_chunk_write(g->chunk, rook_opcode_load_null, stmt->base.begin->line, stmt->base.begin->column);
	}
	else
	{
		rook_bytecode_chunk_write(g->chunk, rook_opcode_load_null, stmt->base.begin->line, stmt->base.begin->column);
	}

	for (i = 0; i < stmt->name_count; ++i)
	{
		rook_bytecode_chunk_write(g->chunk, rook_opcode_declare_global, stmt->base.begin->line, stmt->base.begin->column);

		rook_obj str = rook_obj_create_string(g->runtime_allocator, stmt->names[i]->lexeme);
		rook_bytecode_chunk_add_and_write_constant(g->chunk, str, g->runtime_allocator, stmt->base.begin->line, stmt->base.begin->column, g->errors, g->file, g->code);
	}
}

void generate_assignment(rook_bytecode_generator* g, rook_stmt_assignment* stmt)
{
	int i = 0;
	rook_obj str = { 0 };

	rook_bytecode_chunk_write(g->chunk, rook_opcode_load_null, stmt->base.begin->line, stmt->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);

	for (i = 0; i < stmt->val_count; ++i)
	{
		generate_expr(g, stmt->vals[i]);
	}

	rook_bytecode_chunk_write(g->chunk, rook_opcode_assign_start, stmt->base.begin->line, stmt->base.begin->column);

	for (i = 0; i < stmt->name_count; ++i)
	{
		switch (stmt->types[i])
		{
		case rook_stmt_type_assignment_local:
			rook_bytecode_chunk_write(g->chunk, rook_opcode_assign_local, stmt->base.begin->line, stmt->base.begin->column);
			rook_bytecode_chunk_write(g->chunk, (u8)stmt->idxs[i], stmt->base.begin->line, stmt->base.begin->column);
			break;
		case rook_stmt_type_assignment_captured_value:
			rook_bytecode_chunk_write(g->chunk, rook_opcode_assign_capture, stmt->base.begin->line, stmt->base.begin->column);
			rook_bytecode_chunk_write(g->chunk, (u8)stmt->idxs[i], stmt->base.begin->line, stmt->base.begin->column);
			break;
		case rook_stmt_type_assignment_global:
			rook_bytecode_chunk_write(g->chunk, rook_opcode_assign_global, stmt->base.begin->line, stmt->base.begin->column);
			str = rook_obj_create_string(g->runtime_allocator, stmt->names[i]->lexeme);
			rook_bytecode_chunk_add_and_write_constant(g->chunk, str, g->runtime_allocator, stmt->base.begin->line, stmt->base.begin->column, g->errors, g->file, g->code);
			break;
		default:
			assert(0 && "Shouldn't happen");
			break;
		}
	}

	rook_bytecode_chunk_write(g->chunk, rook_opcode_exprstmt_end, stmt->base.begin->line, stmt->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_pop, stmt->base.begin->line, stmt->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, 1, stmt->base.begin->line, stmt->base.begin->column);
}

void generate_set(rook_bytecode_generator* g, rook_stmt_set* stmt)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->map);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	if (stmt->val->type == rook_expr_type_func_decl)
		((rook_expr_func_decl*)stmt->val)->name = stmt->name->lexeme;
	generate_expr(g, stmt->val);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_property_set, stmt->base.begin->line, stmt->base.begin->column);

	rook_obj key = rook_obj_create_string(g->runtime_allocator, stmt->name->lexeme);
	rook_bytecode_chunk_add_and_write_constant(g->chunk, key, g->runtime_allocator, stmt->base.begin->line, stmt->base.begin->column, g->errors, g->file, g->code);
}

void generate_subscr_set(rook_bytecode_generator* g, rook_stmt_subscr_set* stmt)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->collection);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->key);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	if (stmt->val->type == rook_expr_type_func_decl && stmt->key->type == rook_expr_type_basic_literal && ((rook_expr_basic_literal*)stmt->key)->type == rook_token_type_string)
		((rook_expr_func_decl*)stmt->val)->name = ((rook_expr_basic_literal*)stmt->key)->value.string;
	generate_expr(g, stmt->val);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_subscr_set, stmt->base.begin->line, stmt->base.begin->column);
}

static void generate_set_comp_assign(rook_bytecode_generator* g, rook_stmt_set_comp_assign* stmt)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->collection);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->key);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->val);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_set_comp_assign, stmt->base.begin->line, stmt->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, (u8)stmt->op, stmt->base.begin->line, stmt->base.begin->column);
}

void generate_exprstmt(rook_bytecode_generator* g, rook_stmt_exprstmt* stmt)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->expr);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_exprstmt_end, stmt->base.begin->line, stmt->base.begin->column);
}

void generate_block(rook_bytecode_generator* g, rook_stmt_block* stmt)
{
	if (stmt->stmt_count)
	{
		rook_scope new_scope;
		rook_scope_init(&new_scope, g->callframe.current_scope);
		g->callframe.current_scope = &new_scope;

		for (int i = 0; i < stmt->stmt_count; ++i)
			generate_stmt(g, stmt->stmts[i]);

		rook_bytecode_chunk_write(g->chunk, rook_opcode_pop, stmt->base.end->line, stmt->base.end->column);
		rook_bytecode_chunk_write(g->chunk, (u8)sb_count(new_scope.locals), stmt->base.end->line, stmt->base.end->column);
		
		g->callframe.current_scope = g->callframe.current_scope->parent;
		g->callframe.local_count -= sb_count(new_scope.locals);
		rook_scope_free(&new_scope);

		rook_bytecode_chunk_close_local_infos(g->chunk, g->callframe.local_count, sb_count(g->chunk->code));
	}
}

void generate_return(rook_bytecode_generator* g, rook_stmt_return* stmt)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);

	for (int i = 0; i < stmt->expr_count; ++i)
		generate_expr(g, stmt->exprs[i]);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_return, stmt->base.begin->line, stmt->base.begin->column);
}

void generate_if(rook_bytecode_generator* g, rook_stmt_if* stmt)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->cond);
	int thenTo = add_jump(g, rook_opcode_pop_and_jump_if_false, stmt);
	generate_stmt(g, stmt->then);

	if (stmt->else_)
	{
		int elseTo = add_jump(g, rook_opcode_jump, stmt);
		patch_jump(g, stmt, thenTo);
		generate_stmt(g, stmt->else_);
		patch_jump(g, stmt, elseTo);
	}
	else
	{
		patch_jump(g, stmt, thenTo);
	}
}

void generate_while(rook_bytecode_generator* g, rook_stmt_while* stmt)
{
	int loopStart = sb_count(g->chunk->code);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->cond);

	int ifFalse = add_jump(g, rook_opcode_pop_and_jump_if_false, stmt);

	generate_stmt(g, stmt->body);
	patch_jump_back(g, stmt, loopStart);
	patch_jump(g, stmt, ifFalse);
}

void generate_for(rook_bytecode_generator* g, rook_stmt_for* stmt)
{
	rook_scope init_scope;
	rook_scope body_scope;
	rook_scope* previous_scope = g->callframe.current_scope;

	rook_scope_init(&init_scope, g->callframe.current_scope);
	g->callframe.current_scope = &init_scope;

	if (stmt->init)
	{
		generate_stmt(g, stmt->init);
	}

	int loopStart = sb_count(g->chunk->code);
	int ifFalse = 0;

	if (stmt->cond)
	{
		rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->cond->begin->line, stmt->cond->begin->column);
		generate_expr(g, stmt->cond);
		ifFalse = add_jump(g, rook_opcode_pop_and_jump_if_false, stmt);
	}

	rook_scope_init(&body_scope, &init_scope);
	g->callframe.current_scope = &body_scope;
	
	if (stmt->body)
	{
		generate_stmt(g, stmt->body);
	}

	if (stmt->incr)
	{
		rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->incr->begin->line, stmt->incr->begin->column);
		g->callframe.current_scope = &init_scope;
		generate_expr(g, stmt->incr);
		rook_bytecode_chunk_write(g->chunk, rook_opcode_exprstmt_end, stmt->incr->begin->line, stmt->incr->begin->column);
	}

	patch_jump_back(g, stmt, loopStart);

	if (stmt->cond)
	{
		patch_jump(g, stmt, ifFalse);
	}

	rook_bytecode_chunk_write(g->chunk, rook_opcode_pop, stmt->base.end->line, stmt->base.end->column);
	int pop_count = sb_count(init_scope.locals) + sb_count(body_scope.locals);
	if (pop_count > 0xFF)
		add_generator_errorc(g, stmt, "Too many objects to pop.");
	rook_bytecode_chunk_write(g->chunk, (u8)pop_count, stmt->base.end->line, stmt->base.end->column);

	g->callframe.current_scope = previous_scope;
	g->callframe.local_count -= pop_count;
	rook_scope_free(&init_scope);
	rook_scope_free(&body_scope);
}

void generate_for_each(rook_bytecode_generator* g, rook_stmt_for_each* stmt)
{
	rook_scope scope;
	rook_scope* previous_scope = g->callframe.current_scope;

	rook_scope_init(&scope, g->callframe.current_scope);
	g->callframe.current_scope = &scope;

	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, stmt->base.begin->line, stmt->base.begin->column);
	generate_expr(g, stmt->container);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_leave_first_expr_result, stmt->base.begin->line, stmt->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_foreach_setup, stmt->base.begin->line, stmt->base.begin->column);

	//container is unnamed local
	rook_bytecode_chunk_add_local_info(g->chunk, rook_string_from_literal("[container]"), g->callframe.local_count, sb_count(g->chunk->code));
	rook_callframe_add_unnamed_local(&g->callframe);
	//iterator is unnamed local
	rook_bytecode_chunk_add_local_info(g->chunk, rook_string_from_literal("[iterator]"), g->callframe.local_count, sb_count(g->chunk->code));
	rook_callframe_add_unnamed_local(&g->callframe);

	if (stmt->key_name)
	{
		rook_bytecode_chunk_add_local_info(g->chunk, rook_string_from_cstring(stmt->key_name->lexeme), g->callframe.local_count, sb_count(g->chunk->code));
		rook_callframe_add_local(&g->callframe, stmt->key_name->lexeme);
	}
	else
	{
		rook_callframe_add_unnamed_local(&g->callframe);
	}
	
	rook_bytecode_chunk_add_local_info(g->chunk, rook_string_from_cstring(stmt->value_name->lexeme), g->callframe.local_count, sb_count(g->chunk->code));
	rook_callframe_add_local(&g->callframe, stmt->value_name->lexeme);


	int loopStart = sb_count(g->chunk->code);
	int ifDone = add_jump(g, rook_opcode_foreach_do, stmt);
	generate_stmt(g, stmt->body);
	patch_jump_back(g, stmt, loopStart);
	patch_jump(g, stmt, ifDone);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_pop, stmt->base.end->line, stmt->base.end->column);
	rook_bytecode_chunk_write(g->chunk, (u8)sb_count(scope.locals), stmt->base.end->line, stmt->base.end->column);

	g->callframe.current_scope = previous_scope;
	g->callframe.local_count -= sb_count(scope.locals);
	rook_scope_free(&scope);

	rook_bytecode_chunk_close_local_infos(g->chunk, g->callframe.local_count, sb_count(g->chunk->code));
}

void generate_break(rook_bytecode_generator* g, rook_stmt_break* stmt)
{
	assert(0);
}

void generate_continue(rook_bytecode_generator* g, rook_stmt_continue* stmt)
{
	assert(0);
}

void generate_basic_literal(rook_bytecode_generator* g, rook_expr_basic_literal* expr)
{
	switch (expr->type)
	{
	case rook_token_type_null:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_load_null, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_true:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_load_constant, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_add_and_write_constant(g->chunk, rook_obj_create_boolean(1), g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);
		break;
	case rook_token_type_false:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_load_constant, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_add_and_write_constant(g->chunk, rook_obj_create_boolean(0), g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);
		break;
	case rook_token_type_number:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_load_constant, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_add_and_write_constant(g->chunk, rook_obj_create_number(expr->value.number), g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);
		break;
	case rook_token_type_string:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_load_constant, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_add_and_write_constant(g->chunk, rook_obj_create_string(g->runtime_allocator, expr->value.string), g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);
		break;
	default:
		assert(0);
		break;
	}
}

void generate_array_literal(rook_bytecode_generator* g, rook_expr_array_literal* expr)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);
	
	for (int i = 0; i < expr->initializer_count; ++i)
		generate_expr(g, expr->initializers[i]);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_declare_array, expr->base.begin->line, expr->base.begin->column);
}

void generate_map_literal(rook_bytecode_generator* g, rook_expr_map_literal* expr)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);

	for (int i = 0; i < expr->initializer_count; ++i)
	{
		rook_bytecode_chunk_write(g->chunk, rook_opcode_load_constant, expr->base.begin->line, expr->base.begin->column);

		rook_cstring key = rook_cstring_from_chars_ex(expr->keys[i], expr->key_lengths[i]);
		rook_obj key_obj = rook_obj_create_string(g->runtime_allocator, key);
		rook_bytecode_chunk_add_and_write_constant(g->chunk, key_obj, g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);

		rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);
		if (expr->values[i]->type == rook_expr_type_func_decl)
			((rook_expr_func_decl*)expr->values[i])->name = key;
		generate_expr(g, expr->values[i]);
		rook_bytecode_chunk_write(g->chunk, rook_opcode_declare_map_value_end, expr->base.begin->line, expr->base.begin->column);
	}

	rook_bytecode_chunk_write(g->chunk, rook_opcode_declare_map, expr->base.begin->line, expr->base.begin->column);
}

void generate_func_decl(rook_bytecode_generator* g, rook_expr_func_decl* expr)
{
	rook_bytecode_generator generator;
	rook_bytecode_chunk chunk;
	rook_string code;
	rook_obj new_func = rook_obj_create(g->runtime_allocator, rook_obj_type_func);
	int i;

	rook_bytecode_chunk_init(&chunk);
	rook_string_init_ex(&code, expr->base.begin->lexeme.chars, (int)((expr->base.end->lexeme.chars + expr->base.end->lexeme.length) - expr->base.begin->lexeme.chars));
	rook_bytecode_generator_init(&generator, g->runtime_allocator, g->file, rook_cstring_from_string(code), g->errors, &g->callframe);

	for (i = 0; i < expr->parameter_count; ++i)
	{
		rook_bytecode_chunk_add_local_info(&chunk, rook_string_from_cstring(expr->parameter_names[i]->lexeme), generator.callframe.local_count, sb_count(chunk.code));
		rook_callframe_add_local(&generator.callframe, expr->parameter_names[i]->lexeme);
	}

	rook_bytecode_generator_run(&generator, expr->stmts, expr->stmt_count, &chunk);

	rook_func_init(new_func.as.func, expr->parameter_names, expr->parameter_types, expr->parameter_count, chunk, expr->name, code);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_declare_func, expr->base.begin->line, expr->base.begin->column);
	rook_bytecode_chunk_add_and_write_constant(g->chunk, new_func, g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);

	if (expr->captured_value_count > 0xFF)
		add_generator_errorc(g, expr, "Too many captured values in function.");

	rook_bytecode_chunk_write(g->chunk, (u8)expr->captured_value_count, expr->base.begin->line, expr->base.begin->column);
	
	for (i = 0; i < expr->captured_value_count; ++i)
	{
		rook_bytecode_chunk_write(g->chunk, (u8)expr->captured_values[i].idx, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_write(g->chunk, (u8)expr->captured_values[i].is_local, expr->base.begin->line, expr->base.begin->column);
	}

	rook_bytecode_chunk_close_local_infos(&chunk, 0, sb_count(chunk.code));
	rook_bytecode_generator_free(&generator);
}

void generate_incrdecr(rook_bytecode_generator* g, rook_expr_unary* expr, u8 op)
{
	switch (expr->right->type)
	{
	case rook_expr_type_var_read_local:
		rook_bytecode_chunk_write(g->chunk, op, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_write(g->chunk, 1, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_write(g->chunk, ((rook_expr_var_read_indexed*)expr->right)->idx, expr->right->begin->line, expr->right->begin->column);
		break;
	case rook_expr_type_var_read_captured_value:
		rook_bytecode_chunk_write(g->chunk, op, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_write(g->chunk, 2, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_write(g->chunk, ((rook_expr_var_read_indexed*)expr->right)->idx, expr->right->begin->line, expr->right->begin->column);
		break;
	case rook_expr_type_var_read_global:
		rook_bytecode_chunk_write(g->chunk, op, expr->base.begin->line, expr->base.begin->column);
		rook_bytecode_chunk_write(g->chunk, 3, expr->base.begin->line, expr->base.begin->column);
		{
			rook_obj name_obj = rook_obj_create_string(g->runtime_allocator, ((rook_expr_var_read*)expr->right)->name->lexeme);
			rook_bytecode_chunk_add_and_write_constant(g->chunk, name_obj, g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);
		}
		break;
	case rook_expr_type_subscr:
		{
			rook_expr_subscr* e = (rook_expr_subscr*)expr->right;

			rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, e->base.begin->line, e->base.begin->column);
			generate_expr(g, e->collection);
			rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, e->base.begin->line, e->base.begin->column);
			generate_expr(g, e->key);

			rook_bytecode_chunk_write(g->chunk, op, expr->base.begin->line, expr->base.begin->column);
			rook_bytecode_chunk_write(g->chunk, 4, expr->right->begin->line, expr->right->begin->column);
		}
		break;
	case rook_expr_type_field_access:
		{
			rook_expr_field_access* e = (rook_expr_field_access*)expr->right;

			rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, e->base.begin->line, e->base.begin->column);
			generate_expr(g, e->obj);
			rook_obj key = rook_obj_create_string(g->runtime_allocator, e->field->lexeme);
			
			rook_bytecode_chunk_add_and_write_constant(g->chunk, key, g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);

			rook_bytecode_chunk_write(g->chunk, op, expr->base.begin->line, expr->base.begin->column);
			rook_bytecode_chunk_write(g->chunk, 5, expr->right->begin->line, expr->right->begin->column);
		}
		break;
	default:
		break;
	}
}

void generate_preunary(rook_bytecode_generator* g, rook_expr_unary* expr)
{
	if (expr->op == rook_token_type_increment)
	{
		generate_incrdecr(g, expr, rook_opcode_preincr);
	}
	else if (expr->op == rook_token_type_decrement)
	{
		generate_incrdecr(g, expr, rook_opcode_predecr);
	}
	else
	{
		generate_expr(g, expr->right);

		switch (expr->op)
		{
		case rook_token_type_not:
			rook_bytecode_chunk_write(g->chunk, rook_opcode_not, expr->base.begin->line, expr->base.begin->column);
			break;
		case rook_token_type_sub:
			rook_bytecode_chunk_write(g->chunk, rook_opcode_negate, expr->base.begin->line, expr->base.begin->column);
			break;
		default:
			assert(0);
			break;
		}
	}
}

void generate_postunary(rook_bytecode_generator* g, rook_expr_unary* expr)
{
	if (expr->op == rook_token_type_increment)
	{
		generate_incrdecr(g, expr, rook_opcode_postincr);
	}
	else if (expr->op == rook_token_type_decrement)
	{
		generate_incrdecr(g, expr, rook_opcode_postdecr);
	}
	else
	{
		assert(0);
	}
}

void generate_binary(rook_bytecode_generator* g, rook_expr_binary* expr)
{
	generate_expr(g, expr->left);
	generate_expr(g, expr->right);

	switch (expr->op)
	{
	case rook_token_type_add:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_add, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_sub:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_subtract, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_star:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_multiply, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_div:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_divide, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_modulo:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_modulo, expr->base.begin->line, expr->base.begin->column);
		break;

	case rook_token_type_equal:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_equal, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_not_eq:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_not_equal, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_less:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_less, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_less_eq:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_less_equal, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_greater:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_greater, expr->base.begin->line, expr->base.begin->column);
		break;
	case rook_token_type_greater_eq:
		rook_bytecode_chunk_write(g->chunk, rook_opcode_greater_equal, expr->base.begin->line, expr->base.begin->column);
		break;
	default:
		assert(0);
		break;
	}
}

void generate_logical(rook_bytecode_generator* g, rook_expr_binary* expr)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);
	generate_expr(g, expr->left);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_leave_first_expr_result, expr->base.begin->line, expr->base.begin->column);

	int earlyOut = add_jump(g, expr->op == rook_token_type_and ? rook_opcode_jump_if_false : rook_opcode_jump_if_true, expr);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_pop, expr->base.begin->line, expr->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, 1, expr->base.begin->line, expr->base.begin->column);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);
	generate_expr(g, expr->right);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_leave_first_expr_result, expr->base.begin->line, expr->base.begin->column);

	patch_jump(g, expr, earlyOut);
}

static void generate_var_read_local(rook_bytecode_generator* g, rook_expr_var_read_indexed* expr)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_load_local, expr->base.begin->line, expr->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, expr->idx, expr->base.begin->line, expr->base.begin->column);
}

static void generate_var_read_captured_value(rook_bytecode_generator* g, rook_expr_var_read_indexed* expr)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_load_captured_value, expr->base.begin->line, expr->base.begin->column);
	rook_bytecode_chunk_write(g->chunk, expr->idx, expr->base.begin->line, expr->base.begin->column);
}

void generate_var_read_global(rook_bytecode_generator* g, rook_expr_var_read* expr)
{
	rook_obj name = rook_obj_create_string(g->runtime_allocator, expr->name->lexeme);
	
	rook_bytecode_chunk_write(g->chunk, rook_opcode_load_global, expr->base.begin->line, expr->base.begin->column);
	rook_bytecode_chunk_add_and_write_constant(g->chunk, name, g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);
}

void generate_func_call(rook_bytecode_generator* g, rook_expr_func_call* expr)
{
	//if (expr->callee->type == rook_expr_type_field_access)
	//{
	//	rook_expr_field_access* fa = (rook_expr_field_access*)expr->callee;
	//	generate_expr(g, fa->obj);

	//	rook_bytecode_chunk_write(g->chunk, rook_opcode_invoke_method, expr->base.begin->line, expr->base.begin->column);
	//}
	//else
	{
		generate_expr(g, expr->callee);

		rook_bytecode_chunk_write(g->chunk, rook_opcode_call_prepare, expr->base.begin->line, expr->base.begin->column);

		for (int i = 0; i < expr->argument_count; ++i)
			generate_expr(g, expr->arguments[i]);

		rook_bytecode_chunk_write(g->chunk, rook_opcode_call, expr->base.begin->line, expr->base.begin->column);
	}
}

void generate_subscr(rook_bytecode_generator* g, rook_expr_subscr* expr)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);
	generate_expr(g, expr->collection);
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);
	generate_expr(g, expr->key);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_subscr_get, expr->base.begin->line, expr->base.begin->column);
}

void generate_field_access(rook_bytecode_generator* g, rook_expr_field_access* expr)
{
	rook_bytecode_chunk_write(g->chunk, rook_opcode_save_stack_size, expr->base.begin->line, expr->base.begin->column);
	generate_expr(g, expr->obj);

	rook_bytecode_chunk_write(g->chunk, rook_opcode_property_get, expr->base.begin->line, expr->base.begin->column);
	rook_obj key = rook_obj_create_string(g->runtime_allocator, expr->field->lexeme);
	rook_bytecode_chunk_add_and_write_constant(g->chunk, key, g->runtime_allocator, expr->base.begin->line, expr->base.begin->column, g->errors, g->file, g->code);
}

void generate_compound(rook_bytecode_generator* g, rook_expr_compound* expr)
{
	generate_expr(g, expr->inner);
}

static int add_jump(rook_bytecode_generator* g, u8 jump_code, void* stmt)
{
	rook_stmt_empty* s = (rook_stmt_empty*)stmt;
	rook_bytecode_chunk_write(g->chunk, jump_code, s->base.begin->line, s->base.begin->column);
	int to = sb_count(g->chunk->code);

	// Add placeholders for jump length
	for(int i =0; i < ROOK_BYTECODE_JUMP_WIDTH; ++i)
	{
		rook_bytecode_chunk_write(g->chunk, 0xFF, s->base.begin->line, s->base.begin->column);
	}

	return to;
}

#if ROOK_BYTECODE_JUMP_WIDTH == 1
#define MAX_JUMP 0xFF
#elif ROOK_BYTECODE_JUMP_WIDTH == 2
#define MAX_JUMP 0xFFFF
#else
#error MAX JUMP UNDEFINED
#endif

static void patch_jump(rook_bytecode_generator* g, void* stmt, int to)
{
	rook_stmt_empty* s = (rook_stmt_empty*)stmt;
	int jump_length = sb_count(g->chunk->code) - to - ROOK_BYTECODE_JUMP_WIDTH;
	if (jump_length > MAX_JUMP)
		add_generator_errorc(g, s, "Too much code to jump over.");
	
	for(int i =0; i < ROOK_BYTECODE_JUMP_WIDTH; ++i)
	{
		g->chunk->code[to+i]	= (u8)((jump_length >> (8 * i)) & 0xFF);
	}
}

static void patch_jump_back(rook_bytecode_generator* g, void* stmt, int to)
{
	rook_stmt_empty* s = (rook_stmt_empty*)stmt;
	rook_bytecode_chunk_write(g->chunk, rook_opcode_jump_back, s->base.begin->line, s->base.begin->column);
	int jump_length = sb_count(g->chunk->code) - to + ROOK_BYTECODE_JUMP_WIDTH;
	if (jump_length > MAX_JUMP)
		add_generator_errorc(g, s, "Too much code to jump over.");
	
	for(int i =0; i < ROOK_BYTECODE_JUMP_WIDTH; ++i)
	{
		rook_bytecode_chunk_write(g->chunk, (u8)((jump_length >> (8 * i)) & 0xFF), s->base.begin->line, s->base.begin->column);
	}
}

#ifdef __cplusplus
}
#endif // __cplusplus
