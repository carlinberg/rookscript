#include "rook_variable_resolver.h"


#include <string.h>
#include <assert.h>

#include <utility/stb_sb.h>
#include <utility/rook_error.h>
#include <utility/rook_allocators.h>

#include "rook_parser.h"
#include "rook_token.h"
#include "rook_stmt.h"
#include "rook_expr.h"
#include "rook_callframe.h"
#include "rook_scope.h"
#include "rook_token_type.h"

#include <core/runtime/rook_env.h>
#include <core/runtime/rook_object.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define add_resolve_error(p, s, fmt, ...) rook_errorlist_add_errorf((p)->errors, (p)->file, (p)->code, (s)->base.begin->line, (s)->base.begin->column, fmt, __VA_ARGS__)
#define add_resolve_error2(p, s, msg) rook_errorlist_add_literal((p)->errors, (msg), (p)->file, (p)->code, (s)->base.begin->line, (s)->base.begin->column)

static void resolve_stmt(rook_parser* parser, rook_callframe* callframe, rook_stmt_base* stmt);
static void resolve_expr(rook_parser* parser, rook_callframe* callframe, rook_expr_base** expr);

int rook_variable_resolver_run(rook_parser* parser)
{
	rook_stmt_base** it;
	rook_callframe callframe;
	rook_scope scope;

	rook_scope_init(&scope, 0);
	rook_callframe_init(&callframe, 0, &scope);

	rook_callframe_add_local(&callframe, rook_cstring_from_literal("this"));

	for (it = parser->stmts; it < parser->stmts + sb_count(parser->stmts); ++it)
	{
		resolve_stmt(parser, &callframe, *it);
	}

	rook_scope_free(&scope);
	rook_callframe_free(&callframe);

	return sb_count(*parser->errors) == 0;
}

static void resolve_local(rook_parser* parser, rook_callframe* callframe, rook_stmt_assignment* stmt);
static void resolve_global(rook_parser* parser, rook_callframe* callframe, rook_stmt_assignment* stmt);
static void resolve_assignment(rook_parser* parser, rook_callframe* callframe, rook_stmt_assignment* stmt);
static void resolve_set(rook_parser* parser, rook_callframe* callframe, rook_stmt_set* stmt);
static void resolve_subscr_set(rook_parser* parser, rook_callframe* callframe, rook_stmt_subscr_set* stmt);
static void resolve_set_comp_assign(rook_parser* parser, rook_callframe* callframe, rook_stmt_set_comp_assign* stmt);
static void resolve_exprstmt(rook_parser* parser, rook_callframe* callframe, rook_stmt_exprstmt* stmt);
static void resolve_block(rook_parser* parser, rook_callframe* callframe, rook_stmt_block* stmt);
static void resolve_return(rook_parser* parser, rook_callframe* callframe, rook_stmt_return* stmt);
static void resolve_if(rook_parser* parser, rook_callframe* callframe, rook_stmt_if* stmt);
static void resolve_while(rook_parser* parser, rook_callframe* callframe, rook_stmt_while* stmt);
static void resolve_for(rook_parser* parser, rook_callframe* callframe, rook_stmt_for* stmt);
static void resolve_for_each(rook_parser* parser, rook_callframe* callframe, rook_stmt_for_each* stmt);

static void resolve_array_literal(rook_parser* parser, rook_callframe* callframe, rook_expr_array_literal* expr);
static void resolve_map_literal(rook_parser* parser, rook_callframe* callframe, rook_expr_map_literal* expr);
static void resolve_func_decl(rook_parser* parser, rook_callframe* callframe, rook_expr_func_decl* expr);
static void resolve_preunary(rook_parser* parser, rook_callframe* callframe, rook_expr_unary* expr);
static void resolve_postunary(rook_parser* parser, rook_callframe* callframe, rook_expr_unary* expr);
static void resolve_binary(rook_parser* parser, rook_callframe* callframe, rook_expr_binary* expr);
static void resolve_logical(rook_parser* parser, rook_callframe* callframe, rook_expr_binary* expr);
static void resolve_var_read(rook_parser* parser, rook_callframe* callframe, rook_expr_var_read** expr);
static void resolve_func_call(rook_parser* parser, rook_callframe* callframe, rook_expr_func_call* expr);
static void resolve_subscr(rook_parser* parser, rook_callframe* callframe, rook_expr_subscr* expr);
static void resolve_field_access(rook_parser* parser, rook_callframe* callframe, rook_expr_field_access* expr);
static void resolve_compound(rook_parser* parser, rook_callframe* callframe, rook_expr_compound* expr);

static int add_captured_value(rook_callframe* callframe, int idx, rook_cstring name, int is_local)
{
	for (int i = 0; i < sb_count(callframe->captured_values); ++i)
	{
		if (callframe->captured_values[i].idx == idx &&
			callframe->captured_values[i].is_local == is_local &&
			rook_string_equals(&callframe->captured_values[i].name, &name))
		{
			return i;
		}
	}

	rook_captured_value captured_value;
	captured_value.idx = idx;
	captured_value.name = name;
	captured_value.is_local = is_local;
	sb_push(callframe->captured_values, captured_value);

	return sb_count(callframe->captured_values) - 1;
}

static int resolve_captured_value(rook_cstring name, rook_callframe* callframe)
{
	if (callframe->parent == 0)
		return -1;

	int idx = rook_scope_get_local(callframe->parent->current_scope, name);
	if (idx != -1)
		return add_captured_value(callframe, idx, name, 1);

	int captured_value = resolve_captured_value(name, callframe->parent);
	if (captured_value != -1)
		return add_captured_value(callframe, captured_value, name, 0);

	return -1;
}

static void resolve_stmt(rook_parser* parser, rook_callframe* callframe, rook_stmt_base* stmt)
{
	switch (stmt->type)
	{
	case rook_stmt_type_empty:
		break;
	case rook_stmt_type_local:
		resolve_local(parser, callframe, (rook_stmt_assignment*)stmt);
		break;
	case rook_stmt_type_global:
		resolve_global(parser, callframe, (rook_stmt_assignment*)stmt);
		break;
	case rook_stmt_type_assignment:
		resolve_assignment(parser, callframe, (rook_stmt_assignment*)stmt);
		break;
	case rook_stmt_type_set:
		resolve_set(parser, callframe, (rook_stmt_set*)stmt);
		break;
	case rook_stmt_type_subscr_set:
		resolve_subscr_set(parser, callframe, (rook_stmt_subscr_set*)stmt);
		break;
	case rook_stmt_type_set_comp_assign:
		resolve_set_comp_assign(parser, callframe, (rook_stmt_set_comp_assign*)stmt);
		break;
	case rook_stmt_type_exprstmt:
		resolve_exprstmt(parser, callframe, (rook_stmt_exprstmt*)stmt);
		break;
	case rook_stmt_type_block:
		resolve_block(parser, callframe, (rook_stmt_block*)stmt);
		break;
	case rook_stmt_type_return:
		resolve_return(parser, callframe, (rook_stmt_return*)stmt);
		break;
	case rook_stmt_type_if:
		resolve_if(parser, callframe, (rook_stmt_if*)stmt);
		break;
	case rook_stmt_type_while:
		resolve_while(parser, callframe, (rook_stmt_while*)stmt);
		break;
	case rook_stmt_type_for:
		resolve_for(parser, callframe, (rook_stmt_for*)stmt);
		break;
	case rook_stmt_type_for_each:
		resolve_for_each(parser, callframe, (rook_stmt_for_each*)stmt);
		break;
	case rook_stmt_type_break:
		// no variables in here to resolve
		break;
	case rook_stmt_type_continue:
		// no variables in here to resolve
		break;
	default:
		assert(0);
	}
}

static void resolve_expr(rook_parser* parser, rook_callframe* callframe, rook_expr_base** expr)
{
	switch ((*expr)->type)
	{
	case rook_expr_type_basic_literal:
		// no variables in here to resolve
		break;
	case rook_expr_type_array_literal:
		resolve_array_literal(parser, callframe, *(rook_expr_array_literal**)expr);
		break;
	case rook_expr_type_map_literal:
		resolve_map_literal(parser, callframe, *(rook_expr_map_literal**)expr);
		break;
	case rook_expr_type_func_decl:
		resolve_func_decl(parser, callframe, *(rook_expr_func_decl**)expr);
		break;
	case rook_expr_type_unary:
		resolve_preunary(parser, callframe, *(rook_expr_unary**)expr);
		break;
	case rook_expr_type_postunary:
		resolve_postunary(parser, callframe, *(rook_expr_unary**)expr);
		break;
	case rook_expr_type_binary:
		resolve_binary(parser, callframe, *(rook_expr_binary**)expr);
		break;
	case rook_expr_type_logical:
		resolve_logical(parser, callframe, *(rook_expr_binary**)expr);
		break;
	case rook_expr_type_var_read:
		resolve_var_read(parser, callframe, (rook_expr_var_read**)expr);
		break;
	case rook_expr_type_func_call:
		resolve_func_call(parser, callframe, *(rook_expr_func_call**)expr);
		break;
	case rook_expr_type_subscr:
		resolve_subscr(parser, callframe, *(rook_expr_subscr**)expr);
		break;
	case rook_expr_type_field_access:
		resolve_field_access(parser, callframe, *(rook_expr_field_access**)expr);
		break;
	case rook_expr_type_compound:
		resolve_compound(parser, callframe, *(rook_expr_compound**)expr);
		break;
	default:
		assert(0);
	}
}

static void resolve_local(rook_parser* parser, rook_callframe* callframe, rook_stmt_assignment* stmt)
{
	int i;

	for (i = 0; i < stmt->name_count; ++i)
	{
		if (!rook_callframe_add_local(callframe, stmt->names[i]->lexeme))
			add_resolve_error(parser, stmt, "Local with name '%S' already declared", stmt->names[i]->lexeme);
	}

	for (i = 0; i < stmt->val_count; ++i)
	{
		resolve_expr(parser, callframe, &stmt->vals[i]);
	}
}

static void resolve_global(rook_parser* parser, rook_callframe* callframe, rook_stmt_assignment* stmt)
{
	int i;

	for (i = 0; i < stmt->val_count; ++i)
	{
		resolve_expr(parser, callframe, &stmt->vals[i]);
	}
}

static void resolve_assignment(rook_parser* parser, rook_callframe* callframe, rook_stmt_assignment* stmt)
{
	int i, type, idx;

	for (i = 0; i < stmt->name_count; ++i)
	{
		type = rook_stmt_type_assignment_local;
		idx = rook_scope_get_local(callframe->current_scope, stmt->names[i]->lexeme);

		if (idx == -1)
		{
			idx = resolve_captured_value(stmt->names[i]->lexeme, callframe);
			if (idx != -1)
				type = rook_stmt_type_assignment_captured_value;
			else
				type = rook_stmt_type_assignment_global;
		}

		stmt->idxs[i] = idx;
		stmt->types[i] = type;
	}

	for (i = 0; i < stmt->val_count; ++i)
	{
		resolve_expr(parser, callframe, &stmt->vals[i]);
	}
}

static void resolve_set(rook_parser* parser, rook_callframe* callframe, rook_stmt_set* stmt)
{
	resolve_expr(parser, callframe, &stmt->map);
	resolve_expr(parser, callframe, &stmt->val);
}

static void resolve_subscr_set(rook_parser* parser, rook_callframe* callframe, rook_stmt_subscr_set* stmt)
{
	resolve_expr(parser, callframe, &stmt->collection);
	resolve_expr(parser, callframe, &stmt->key);
	resolve_expr(parser, callframe, &stmt->val);
}

static void resolve_set_comp_assign(rook_parser* parser, rook_callframe* callframe, rook_stmt_set_comp_assign* stmt)
{
	resolve_expr(parser, callframe, &stmt->collection);
	resolve_expr(parser, callframe, &stmt->key);
	resolve_expr(parser, callframe, &stmt->val);
}

static void resolve_exprstmt(rook_parser* parser, rook_callframe* callframe, rook_stmt_exprstmt* stmt)
{
	resolve_expr(parser, callframe, &stmt->expr);
}

static void resolve_block(rook_parser* parser, rook_callframe* callframe, rook_stmt_block* stmt)
{
	int i;
	rook_scope scope;
	
	rook_scope_init(&scope, callframe->current_scope);
	callframe->current_scope = &scope;

	for (i = 0; i < stmt->stmt_count; ++i)
		resolve_stmt(parser, callframe, stmt->stmts[i]);

	callframe->current_scope = scope.parent;
	callframe->local_count -= sb_count(scope.locals);
	rook_scope_free(&scope);
}

static void resolve_return(rook_parser* parser, rook_callframe* callframe, rook_stmt_return* stmt)
{
	int i;
	for (i = 0; i < stmt->expr_count; ++i)
		resolve_expr(parser, callframe, &stmt->exprs[i]);
}

static void resolve_if(rook_parser* parser, rook_callframe* callframe, rook_stmt_if* stmt)
{
	resolve_expr(parser, callframe, &stmt->cond);
	resolve_stmt(parser, callframe, stmt->then);

	if (stmt->else_)
		resolve_stmt(parser, callframe, stmt->else_);
}

static void resolve_while(rook_parser* parser, rook_callframe* callframe, rook_stmt_while* stmt)
{
	resolve_expr(parser, callframe, &stmt->cond);
	resolve_stmt(parser, callframe, stmt->body);
}

static void resolve_for(rook_parser* parser, rook_callframe* callframe, rook_stmt_for* stmt)
{
	rook_scope init_scope;
	rook_scope body_scope;
	rook_scope_init(&init_scope, callframe->current_scope);
	callframe->current_scope = &init_scope;

	if (stmt->init)
		resolve_stmt(parser, callframe, stmt->init);
	if (stmt->cond)
		resolve_expr(parser, callframe, &stmt->cond);
	if (stmt->incr)
		resolve_expr(parser, callframe, &stmt->incr);

	rook_scope_init(&body_scope, &init_scope);
	callframe->current_scope = &body_scope;

	resolve_stmt(parser, callframe, stmt->body);

	callframe->current_scope = init_scope.parent;
	callframe->local_count -= sb_count(init_scope.locals) + sb_count(body_scope.locals);
	rook_scope_free(&init_scope);
	rook_scope_free(&body_scope);
}

static void resolve_for_each(rook_parser* parser, rook_callframe* callframe, rook_stmt_for_each* stmt)
{
	rook_scope scope;

	resolve_expr(parser, callframe, &stmt->container);

	rook_scope_init(&scope, callframe->current_scope);
	callframe->current_scope = &scope;

	rook_scope_add_unnamed_local(callframe->current_scope, callframe->local_count++);
	rook_scope_add_unnamed_local(callframe->current_scope, callframe->local_count++);

	if (stmt->key_name)
	{
		if (!rook_callframe_add_local(callframe, stmt->key_name->lexeme))
			add_resolve_error(parser, stmt, "Local with name '%S' already declared", stmt->key_name->lexeme);
	}
	else
	{
		rook_scope_add_unnamed_local(callframe->current_scope, callframe->local_count++);
	}

	if (!rook_callframe_add_local(callframe, stmt->value_name->lexeme))
	{
		add_resolve_error(parser, stmt, "Local with name '%S' already declared", stmt->value_name->lexeme);
	}

	resolve_stmt(parser, callframe, stmt->body);

	callframe->current_scope = scope.parent;
	callframe->local_count -= sb_count(scope.locals);
	rook_scope_free(&scope);
}

static void resolve_array_literal(rook_parser* parser, rook_callframe* callframe, rook_expr_array_literal* expr)
{
	int i;
	for (i = 0; i < expr->initializer_count; ++i)
		resolve_expr(parser, callframe, &expr->initializers[i]);
}

static void resolve_map_literal(rook_parser* parser, rook_callframe* callframe, rook_expr_map_literal* expr)
{
	int i;
	for (i = 0; i < expr->initializer_count; ++i)
		resolve_expr(parser, callframe, &expr->values[i]);
}

static void resolve_func_decl(rook_parser* parser, rook_callframe* callframe, rook_expr_func_decl* expr)
{
	int i;
	rook_callframe new_callframe;
	rook_scope parameter_scope;
	rook_scope body_scope;

	rook_scope_init(&parameter_scope, 0);
	rook_callframe_init(&new_callframe, callframe, &parameter_scope);

	rook_callframe_add_local(&new_callframe, rook_cstring_from_literal("this"));
	for (i = 0; i < expr->parameter_count; ++i)
	{
		if (!rook_callframe_add_local(&new_callframe, expr->parameter_names[i]->lexeme))
			add_resolve_error(parser, expr, "Local with name '%S' already declared", expr->parameter_names[i]->lexeme);
	}

	rook_scope_init(&body_scope, &parameter_scope);
	new_callframe.current_scope = &body_scope;

	for (i = 0; i < expr->stmt_count; ++i)
	{
		resolve_stmt(parser, &new_callframe, expr->stmts[i]);
	}

	if (sb_count(new_callframe.captured_values) > 0)
	{
		expr->captured_values = rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*expr->captured_values) * sb_count(new_callframe.captured_values));
		memcpy(expr->captured_values, new_callframe.captured_values, sizeof(*expr->captured_values) * sb_count(new_callframe.captured_values));
		expr->captured_value_count = sb_count(new_callframe.captured_values);
	}
	else
	{
		expr->captured_value_count = 0;
		expr->captured_values = 0;
	}

	rook_callframe_free(&new_callframe);

	rook_scope_free(&body_scope);
	rook_scope_free(&parameter_scope);
}

static void resolve_preunary(rook_parser* parser, rook_callframe* callframe, rook_expr_unary* expr)
{
	resolve_expr(parser, callframe, &expr->right);
}

static void resolve_postunary(rook_parser* parser, rook_callframe* callframe, rook_expr_unary* expr)
{
	resolve_expr(parser, callframe, &expr->right);
}

static void resolve_binary(rook_parser* parser, rook_callframe* callframe, rook_expr_binary* expr)
{
	resolve_expr(parser, callframe, &expr->left);
	resolve_expr(parser, callframe, &expr->right);
}

static void resolve_logical(rook_parser* parser, rook_callframe* callframe, rook_expr_binary* expr)
{
	resolve_expr(parser, callframe, &expr->left);
	resolve_expr(parser, callframe, &expr->right);
}

static void resolve_var_read(rook_parser* parser, rook_callframe* callframe, rook_expr_var_read** expr)
{
	int type = rook_expr_type_var_read_local;
	int idx = rook_scope_get_local(callframe->current_scope, (*expr)->name->lexeme);

	if (idx == -1)
	{
		idx = resolve_captured_value((*expr)->name->lexeme, callframe);
		if (idx != -1)
			type = rook_expr_type_var_read_captured_value;
	}

	if (idx != -1)
	{
		if (idx > 0xFF)
			add_resolve_error2(parser, *expr, "Too many locals or captured values added to function");

		rook_expr_var_read_indexed* indexed = rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*indexed));
		indexed->base = (*expr)->base;
		indexed->base.type = type;
		indexed->idx = (u8)idx;
		*expr = (rook_expr_var_read*)indexed;
	}
	else
	{
		(*expr)->base.type = rook_expr_type_var_read_global;
	}
}

static void resolve_func_call(rook_parser* parser, rook_callframe* callframe, rook_expr_func_call* expr)
{
	int i;
	resolve_expr(parser, callframe, &expr->callee);
	for (i = 0; i < expr->argument_count; ++i)
		resolve_expr(parser, callframe, &expr->arguments[i]);
}

static void resolve_subscr(rook_parser* parser, rook_callframe* callframe, rook_expr_subscr* expr)
{
	resolve_expr(parser, callframe, &expr->collection);
	resolve_expr(parser, callframe, &expr->key);
}

static void resolve_field_access(rook_parser* parser, rook_callframe* callframe, rook_expr_field_access* expr)
{
	resolve_expr(parser, callframe, &expr->obj);
}

static void resolve_compound(rook_parser* parser, rook_callframe* callframe, rook_expr_compound* expr)
{
	resolve_expr(parser, callframe, &expr->inner);
}

#ifdef __cplusplus
}
#endif // __cplusplus
