#include "rook_parser.h"
#include "rook_parser.h"

#include <string.h>
#include <assert.h>

#include <utility/stb_sb.h>
#include <utility/rook_settings.h>
#include <utility/rook_allocators.h>
#include <utility/rook_string.h>
#include <utility/rook_error.h>
#include <utility/rook_malloc.h>

#include "rook_token.h"
#include "rook_token_type.h"
#include "rook_parse_rule.h"
#include "rook_stmt.h"
#include "rook_expr.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

extern const rook_parse_rule rules[];

void rook_parser_init(rook_parser* parser, rook_cstring file, rook_cstring code, rook_error** errors)
{
	parser->tokens = 0;
	parser->token_count = 0;
	parser->current = 0;
	
	parser->stmts = 0;

	rook_allocator_init(&parser->compiletime_allocator);

	parser->file = file;
	parser->code = code;
	parser->errors = errors;
}

void rook_parser_free(rook_parser* parser)
{
	rook_cstring_init(&parser->file, 0, 0);
	rook_cstring_init(&parser->code, 0, 0);

	sb_free(parser->stmts);
	parser->stmts = 0;

	ROOK_FREE(parser->tokens);
	parser->tokens = 0;

	rook_allocator_free(&parser->compiletime_allocator);
}

static void panic(rook_parser* parser);
static int is_at_end(rook_parser* parser);
static rook_token* next_token(rook_parser* parser);

#define CREATE_STMT(t, n) rook_stmt_##t* n = (rook_stmt_##t*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*n)); n->base.type =  rook_stmt_type_##t;
#define CREATE_EXPR(t, n) rook_expr_##t* n = (rook_expr_##t*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*n)); n->base.type =  rook_expr_type_##t;

#define SET_TOKEN_SPAN(x, tb, te) ((x)->base.begin = tb, (x)->base.end = te)

#define RETURN_STMT(s) return ((rook_stmt_base*)(s))
#define RETURN_EXPR(e) return ((rook_expr_base*)(e))

static rook_stmt_base* make_stmt(rook_parser* parser);

static rook_expr_base* make_expr(rook_parser* parser, int precedence);

int rook_parse(rook_parser* parser)
{
	parser->current = parser->tokens;

	int success = 1;
	while (!is_at_end(parser))
	{
		rook_stmt_base* stmt = make_stmt(parser);
		if (stmt)
		{
			sb_push(parser->stmts, stmt);
		}
		else
		{
			panic(parser);
			success = 0;
		}
	}

	return success;
}

static int match(rook_parser* parser, int type)
{
	if (parser->current->type == type)
	{
		next_token(parser);
		return 1;
	}

	return 0;
}

static int match_ex(rook_parser* parser, rook_token** current, int type)
{
	if ((*current)->type == type)
	{
		if (*current + 1 < parser->tokens + parser->token_count)
			++(*current);
		return 1;
	}

	return 0;
}

static void panic(rook_parser* parser)
{
	while (!is_at_end(parser))
	{
		switch (parser->current->type)
		{
		case rook_token_type_right_curly:
		case rook_token_type_semicolon:
			++parser->current;
		case rook_token_type_global:
		case rook_token_type_local:
		case rook_token_type_const:
		case rook_token_type_if:
		case rook_token_type_while:
		case rook_token_type_for:
		case rook_token_type_case:
		case rook_token_type_break:
		case rook_token_type_continue:
		case rook_token_type_defer:
		case rook_token_type_return:
			return;
		default:
			++parser->current;
			break;
		}
	}
}

static int is_at_end(rook_parser* parser)
{
	return parser->current >= parser->tokens + parser->token_count;
}

static rook_token* next_token(rook_parser* parser)
{
	if (parser->current >= parser->tokens + parser->token_count)
		return 0;
	return parser->current++;
}

#define add_parse_error(p, msg) rook_errorlist_add_error((p)->errors, (msg), sizeof(msg) - 1, (p)->file, (p)->code, (p)->current->line, (p)->current->column)
#define add_parse_errorf(p, fmt, ...) rook_errorlist_add_errorf((p)->errors, (p)->file, (p)->code, (p)->current->line, (p)->current->column, (fmt), __VA_ARGS__)

static rook_stmt_base* make_expr_stmt(rook_parser* parser)
{
	rook_expr_base* expr = make_expr(parser, rook_precedence_none);
	if (!expr)
		return 0;

	if (!match(parser, rook_token_type_semicolon))
	{
		add_parse_error(parser, "Expected ';' after expr stmt");
		return 0;
	}

	CREATE_STMT(exprstmt, stmt);
	stmt->expr = expr;
	SET_TOKEN_SPAN(stmt, expr->begin, expr->end);
	RETURN_STMT(stmt);
}

static rook_stmt_base* make_stmt(rook_parser* parser)
{
	const rook_token* stmt_begin = parser->current;

	const rook_parse_rule* rule = &rules[parser->current->type];
	if (rule->stmt)
	{
		rook_token* next = next_token(parser);
		if (!next)
			return 0;
		return rule->stmt(parser, next);
	}
	else
	{
		rook_stmt_base* stmt = make_expr_stmt(parser);
		if (!stmt)
		{
			rook_errorlist_add_error(parser->errors, ("Expected statement"), sizeof("Expected statement") - 1, parser->file, parser->code, stmt_begin->line, stmt_begin->column);
			return 0;
		}

		return stmt;
	}
}

static rook_expr_base* make_expr(rook_parser* parser, int precedence)
{
	const rook_parse_rule* rule = &rules[parser->current->type];
	if (!rule->prefix)
	{
		add_parse_error(parser, "Expected expression");
		return 0;
	}

	rook_token* next = next_token(parser);
	if (!next)
		return 0;

	rook_expr_base* left = rule->prefix(parser, next);
	if (!left)
		return 0;

	rule = &rules[parser->current->type];
	while (rule->infix && precedence <= rule->precedence)
	{
		next = next_token(parser);
		if (!next)
			return 0;

		left = rule->infix(parser, left, next);
		if (!left)
			return 0;
		rule = &rules[parser->current->type];
	}

	return left;
}

#define STMT_FUNC(name) static rook_stmt_base* name(rook_parser* parser, rook_token* token)
#define PREFIX_FUNC(name) static rook_expr_base* name(rook_parser* parser, rook_token* token)
#define INFIX_FUNC(name) static rook_expr_base* name(rook_parser* parser, rook_expr_base* left, rook_token* token)

static rook_expr_base* make_assignment_value(rook_parser* parser)
{
	rook_expr_base* val = make_expr(parser, rook_precedence_none);
	if (!val)
		return 0;

	if (!match(parser, rook_token_type_semicolon))
	{
		add_parse_error(parser, "Expected ';' after assignment");
		return 0;
	}

	return val;
}

static rook_stmt_base* make_simple_assignment(rook_parser* parser, rook_token* token)
{
	int i;
	rook_token** names = 0;
	rook_expr_base** vals = 0;
	rook_expr_base* val;

	sb_push(names, token);

	if (match(parser, rook_token_type_comma))
	{
		do
		{
			sb_push(names, parser->current);

			if (!match(parser, rook_token_type_identifier))
			{
				add_parse_error(parser, "Expected identifier (or comma-separated list of identifiers)");
				sb_free(names);
				return 0;
			}
		}
		while (match(parser, rook_token_type_comma));
	}

	if (!match(parser, rook_token_type_assignment))
	{
		add_parse_error(parser, "Expected '= <expr>' or ';' after identifier in assignment");
		sb_free(names);
		return 0;
	}

	do
	{
		val = make_expr(parser, rook_precedence_none);
		if (!val)
		{
			sb_free(names);
			sb_free(vals);
			return 0;
		}

		sb_push(vals, val);
	} while (match(parser, rook_token_type_comma));

	if (!match(parser, rook_token_type_semicolon))
	{
		add_parse_error(parser, "Expected ';' after assignment");
		sb_free(names);
		sb_free(vals);
		return 0;
	}

	CREATE_STMT(assignment, stmt);

	stmt->name_count = sb_count(names);
	stmt->names = (rook_token**)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->names) * sb_count(names));
	for (i = 0; i < sb_count(names); ++i)
		stmt->names[i] = names[i];

	stmt->val_count = sb_count(vals);
	stmt->vals = !stmt->val_count ? 0 : rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->vals) * sb_count(vals));
	for (i = 0; i < sb_count(vals); ++i)
		stmt->vals[i] = vals[i];

	stmt->idxs = !stmt->name_count ? 0 : (int*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->idxs) * stmt->name_count);
	stmt->types = !stmt->name_count ? 0 : (rook_stmt_type*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->types) * stmt->name_count);

	SET_TOKEN_SPAN(stmt, token, parser->current);

	sb_free(names);
	sb_free(vals);

	RETURN_STMT(stmt);
}

static rook_stmt_base* make_field_access_assignment(rook_parser* parser, rook_token* token, rook_expr_field_access* fa)
{
	rook_expr_base* val = make_assignment_value(parser);
	if (!val)
		return 0;

	CREATE_STMT(set, stmt);
	stmt->map = fa->obj;
	stmt->name = fa->field;
	stmt->val = val;
	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

static rook_stmt_base* make_subscript_assignment(rook_parser* parser, rook_token* token, rook_expr_subscr* ssc)
{
	rook_expr_base* val = make_assignment_value(parser);
	if (!val)
		return 0;

	CREATE_STMT(subscr_set, stmt);
	stmt->collection = ssc->collection;
	stmt->key = ssc->key;
	stmt->val = val;
	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

static rook_stmt_base* make_simple_compound_assignment(rook_parser* parser, rook_token* token, rook_token_type op)
{
	rook_expr_base* right = make_assignment_value(parser);
	if (!right)
		return 0;

	CREATE_EXPR(var_read, left);
	left->base.type = rook_expr_type_var_read;
	left->name = token;
	SET_TOKEN_SPAN(left, token, right->begin);

	CREATE_EXPR(binary, binary);
	binary->left = (rook_expr_base*)left;
	binary->op = op;
	binary->right = right;
	SET_TOKEN_SPAN(binary, token, right->end);

	CREATE_STMT(assignment, stmt);

	stmt->name_count = 1;
	stmt->names = (rook_token**)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->names));
	*stmt->names = token;

	stmt->val_count = 1;
	stmt->vals = (rook_expr_base**)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->vals));
	*stmt->vals = (rook_expr_base*)binary;

	stmt->idxs = (int*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->idxs));
	stmt->types = (rook_stmt_type*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->types));

	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

static rook_stmt_base* make_set_compound_assignment(rook_parser* parser, rook_token* token, rook_expr_base* expr, rook_token_type op)
{
	rook_expr_base* val = make_assignment_value(parser);
	if (!val)
		return 0;

	CREATE_STMT(set_comp_assign, stmt);
	stmt->collection = expr->type == rook_expr_type_subscr ? ((rook_expr_subscr*)expr)->collection : ((rook_expr_field_access*)expr)->obj;
	stmt->key = expr->type == rook_expr_type_subscr ? ((rook_expr_subscr*)expr)->key : ((rook_expr_field_access*)expr)->key;
	stmt->val = val;
	stmt->op = op;
	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

STMT_FUNC(make_assignment)
{
	if (parser->current->type == rook_token_type_assignment || parser->current->type == rook_token_type_comma)
		return make_simple_assignment(parser, token);

	if (match(parser, rook_token_type_add_assign))
		return make_simple_compound_assignment(parser, token, rook_token_type_add);
	if (match(parser, rook_token_type_sub_assign))
		return make_simple_compound_assignment(parser, token, rook_token_type_sub);
	if (match(parser, rook_token_type_star_assign))
		return make_simple_compound_assignment(parser, token, rook_token_type_star);
	if (match(parser, rook_token_type_div_assign))
		return make_simple_compound_assignment(parser, token, rook_token_type_div);
	if (match(parser, rook_token_type_modulo_assign))
		return make_simple_compound_assignment(parser, token, rook_token_type_modulo);

	parser->current = token;
	rook_expr_base* expr = make_expr(parser, rook_precedence_none);
	if (!expr)
		return 0;

	if (expr->type == rook_expr_type_field_access || expr->type == rook_expr_type_subscr)
	{
		if (expr->type == rook_expr_type_field_access)
		{
			if (match(parser, rook_token_type_assignment))
				return make_field_access_assignment(parser, token, (rook_expr_field_access*)expr);
		}

		if (expr->type == rook_expr_type_subscr)
		{
			if (match(parser, rook_token_type_assignment))
				return make_subscript_assignment(parser, token, (rook_expr_subscr*)expr);
		}

		if (match(parser, rook_token_type_add_assign))
			return make_set_compound_assignment(parser, token, expr, rook_token_type_add);
		if (match(parser, rook_token_type_sub_assign))
			return make_set_compound_assignment(parser, token, expr, rook_token_type_sub);
		if (match(parser, rook_token_type_star_assign))
			return make_set_compound_assignment(parser, token, expr, rook_token_type_star);
		if (match(parser, rook_token_type_div_assign))
			return make_set_compound_assignment(parser, token, expr, rook_token_type_div);
		if (match(parser, rook_token_type_modulo_assign))
			return make_set_compound_assignment(parser, token, expr, rook_token_type_modulo);
	}

	if (!match(parser, rook_token_type_semicolon))
	{
		add_parse_error(parser, "Expected ';' after expr stmt");
		return 0;
	}

	CREATE_STMT(exprstmt, stmt);
	stmt->expr = expr;
	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

static rook_stmt_base* make_declaration(rook_parser* parser, rook_token* token, rook_stmt_type type, const char* typestr)
{
	int i;
	rook_token** names = 0;
	rook_expr_base** vals = 0;
	rook_expr_base* val;

	do
	{
		sb_push(names, parser->current);

		if (!match(parser, rook_token_type_identifier))
		{
			add_parse_errorf(parser, "Expected identifier (or comma-separated list of identifiers) after '%s'", typestr);
			sb_free(names);
			return 0;
		}
	} while (match(parser, rook_token_type_comma));

	if (!match(parser, rook_token_type_semicolon))
	{
		if (!match(parser, rook_token_type_assignment))
		{
			add_parse_errorf(parser, "Expected '= <expr>' or ';' after identifier in %s decl", typestr);
			sb_free(names);
			return 0;
		}

		i = 0;

		do
		{
			val = make_expr(parser, rook_precedence_none);
			if (!val)
			{
				if (i)
					add_parse_errorf(parser, "Expected ';' after %s declaration", typestr);
				sb_free(names);
				sb_free(vals);
				return 0;
			}
			
			i = 1;

			sb_push(vals, val);
		} while (match(parser, rook_token_type_comma));

		int is_obj_decl = sb_count(names) == 1 && sb_count(vals) == 1 &&
						 (vals[0]->type == rook_expr_type_array_literal ||
						  vals[0]->type == rook_expr_type_map_literal);

		if (!is_obj_decl && !match(parser, rook_token_type_semicolon))
		{
			add_parse_errorf(parser, "Expected ';' after %s decl", typestr);
			sb_free(names);
			sb_free(vals);
			return 0;
		}
	}

	CREATE_STMT(assignment, stmt);
	stmt->base.type = type;

	stmt->name_count = sb_count(names);
	stmt->names = (rook_token**)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->names) * stmt->name_count);
	for (i = 0; i < sb_count(names); ++i)
		stmt->names[i] = names[i];

	stmt->val_count = sb_count(vals);
	stmt->vals = !stmt->val_count ? 0 : (rook_expr_base**)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->vals) * stmt->val_count);
	for (i = 0; i < sb_count(vals); ++i)
		stmt->vals[i] = vals[i];

	stmt->idxs = !stmt->name_count ? 0 : (int*)rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmt->idxs) * stmt->name_count);
	stmt->types = 0;

	SET_TOKEN_SPAN(stmt, token, parser->current);

	sb_free(names);
	sb_free(vals);

	RETURN_STMT(stmt);
}

STMT_FUNC(make_global)
{
	return make_declaration(parser, token, rook_stmt_type_global, "global");
}

STMT_FUNC(make_local)
{
	return make_declaration(parser, token, rook_stmt_type_local, "local");
}

//STMT_FUNC(make_const)
//{
//	return make_declaration(parser, token, rook_stmt_type_const, "const");
//}

STMT_FUNC(make_if)
{
	if (!match(parser, rook_token_type_left_par))
	{
		add_parse_error(parser, "Expected '(' after 'if'");
		return 0;
	}

	rook_expr_base* condition = make_expr(parser, rook_precedence_none);
	if (!condition)
		return 0;

	if (!match(parser, rook_token_type_right_par))
	{
		add_parse_error(parser, "Expected ')' after condition in if-stmt");
		return 0;
	}

	rook_stmt_base* then = make_stmt(parser);
	if (!then)
		return 0;

	rook_stmt_base* else_ = 0;
	if (match(parser, rook_token_type_else))
	{
		else_ = make_stmt(parser);
		if (!else_)
			return 0;
	}

	CREATE_STMT(if, if_stmt);
	if_stmt->cond = condition;
	if_stmt->then = then;
	if_stmt->else_ = else_;
	SET_TOKEN_SPAN(if_stmt, token, parser->current);
	RETURN_STMT(if_stmt);
}

STMT_FUNC(make_while)
{
	if (!match(parser, rook_token_type_left_par))
	{
		add_parse_error(parser, "Expected '(' after 'while'");
		return 0;
	}

	rook_expr_base* condition = make_expr(parser, rook_precedence_none);
	if (!condition)
		return 0;

	if (!match(parser, rook_token_type_right_par))
	{
		add_parse_error(parser, "Expected ')' after condition in while-stmt");
		return 0;
	}

	rook_stmt_base* body = make_stmt(parser);
	if (!body)
		return 0;

	CREATE_STMT(while, while_stmt);
	while_stmt->cond = condition;
	while_stmt->body = body;
	SET_TOKEN_SPAN(while_stmt, token, parser->current);
	RETURN_STMT(while_stmt);
}

STMT_FUNC(make_for)
{
	if (!match(parser, rook_token_type_left_par))
	{
		add_parse_error(parser, "Expected '(' after 'for'");
		return 0;
	}

	if (parser->current[0].type == rook_token_type_identifier &&
		(parser->current[1].type == rook_token_type_colon ||
			(parser->current[1].type == rook_token_type_comma &&
			 parser->current[2].type == rook_token_type_identifier &&
			 parser->current[3].type == rook_token_type_colon)))
	{
		rook_token* key_name = 0;

		if (parser->current[1].type == rook_token_type_comma)
		{
			key_name = parser->current;
			parser->current += 2;
		}

		rook_token* value_name = parser->current;
		parser->current += 2;

		rook_expr_base* container = make_expr(parser, rook_precedence_none);
		if (!container)
			return 0;

		if (!match(parser, rook_token_type_right_par))
		{
			add_parse_error(parser, "Expected ')' in en of for header");
			return 0;
		}

		rook_stmt_base* body = make_stmt(parser);
		if (!body)
			return 0;
		
		CREATE_STMT(for_each, stmt);
		stmt->key_name = key_name;
		stmt->value_name = value_name;
		stmt->container = container;
		stmt->body = body;
		SET_TOKEN_SPAN(stmt, token, parser->current);
		RETURN_STMT(stmt);
	}
	else
	{
		rook_stmt_base* init = 0;
		if (!match(parser, rook_token_type_semicolon))
		{
			init = make_stmt(parser);
			if (!init)
				return 0;
		}

		rook_expr_base* condition = 0;
		if (!match(parser, rook_token_type_semicolon))
		{
			condition = make_expr(parser, rook_precedence_none);
			if (!condition)
				return 0;

			if (!match(parser, rook_token_type_semicolon))
			{
				add_parse_error(parser, "Expected ';' after condition in for stmt");
				return 0;
			}
		}

		rook_expr_base* incr = 0;
		if (!match(parser, rook_token_type_right_par))
		{
			incr = make_expr(parser, rook_precedence_none);
			if (!incr)
				return 0;

			if (!match(parser, rook_token_type_right_par))
			{
				add_parse_error(parser, "Expected ')' in en of for header");
				return 0;
			}
		}

		rook_stmt_base* body = make_stmt(parser);
		if (!body)
			return 0;

		CREATE_STMT(for, for_stmt);
		for_stmt->init = init;
		for_stmt->cond = condition;
		for_stmt->incr = incr;
		for_stmt->body = body;
		SET_TOKEN_SPAN(for_stmt, token, parser->current);
		RETURN_STMT(for_stmt);
	}
}

STMT_FUNC(make_break)
{
	CREATE_STMT(break, stmt);
	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

STMT_FUNC(make_continue)
{
	CREATE_STMT(continue, stmt);
	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

STMT_FUNC(make_return_stmt)
{
	if (!match(parser, rook_token_type_semicolon))
	{
		rook_expr_base** exprs = 0;
		do
		{
			rook_expr_base* expr = make_expr(parser, rook_precedence_none);
			if (!expr)
			{
				sb_free(exprs);
				return 0;
			}

			sb_push(exprs, expr);
		}
		while (match(parser, rook_token_type_comma));

		if (token && !match(parser, rook_token_type_semicolon))
		{
			sb_free(exprs);
			return 0;
		}

		rook_stmt_return* stmt = (rook_stmt_return*)rook_allocator_alloc(&parser->compiletime_allocator, offsetof(rook_stmt_return, exprs) + sizeof(*stmt->exprs) * sb_count(exprs));
		stmt->base.type = rook_stmt_type_return;
		stmt->expr_count = sb_count(exprs);
		if (sb_count(exprs))
			memcpy(stmt->exprs, exprs, sizeof(*stmt->exprs) * sb_count(exprs));

		sb_free(exprs);

		SET_TOKEN_SPAN(stmt, token, parser->current);
		RETURN_STMT(stmt);
	}
	else
	{
		rook_stmt_return* stmt = (rook_stmt_return*)rook_allocator_alloc(&parser->compiletime_allocator, offsetof(rook_stmt_return, exprs));

		stmt->base.type = rook_stmt_type_return;
		stmt->expr_count = 0;

		SET_TOKEN_SPAN(stmt, token, parser->current);
		RETURN_STMT(stmt);
	}
}

STMT_FUNC(make_empty_stmt)
{
	CREATE_STMT(empty, stmt);
	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

STMT_FUNC(make_attribute)
{
	if (parser->current->type == rook_token_type_right_square)
	{
		add_parse_error(parser, "Empty attribute list not allowed");
		return 0;
	}

	rook_cstring* attrs = 0;
	do
	{
		rook_expr_base* expr = make_expr(parser, rook_precedence_none);
		if (!expr)
		{
			sb_free(attrs);
			return 0;
		}

#define ONLY_STRINGS_ALLOWED_AS_ATTRS "Only strings allowed as attributes at the moment"

		if (expr->type != rook_expr_type_basic_literal)
		{
			add_parse_error(parser, ONLY_STRINGS_ALLOWED_AS_ATTRS);
			return 0;
		}

		rook_expr_basic_literal* attr = (rook_expr_basic_literal*)expr;
		if (attr->type != rook_token_type_string)
		{
			add_parse_error(parser, ONLY_STRINGS_ALLOWED_AS_ATTRS);
			return 0;
		}

#undef ONLY_STRINGS_ALLOWED_AS_ATTRS

		sb_push(attrs, attr->value.string);
	} while (match(parser, rook_token_type_comma));

	if (!match(parser, rook_token_type_right_square))
	{
		sb_free(attrs);
		add_parse_error(parser, "Expected ']' after end of attribute list");
		return 0;
	}

	rook_stmt_base* stmt = make_stmt(parser);
	if (!stmt)
	{
		sb_free(attrs);
		return 0;
	}

	if (stmt->type != rook_stmt_type_local &&
		stmt->type != rook_stmt_type_global)
	{
		sb_free(attrs);
		add_parse_error(parser, "Can only add attributes to declarations or map members");
		return 0;
	}

	rook_stmt_assignment* decl = (rook_stmt_assignment*)stmt;
	
	decl->attrs = rook_allocator_alloc(&parser->compiletime_allocator, sb_count(attrs) * sizeof(*decl->attrs));
	decl->attr_count = sb_count(attrs);

	for (int i = 0; i < sb_count(attrs); ++i)
	{
		decl->attrs[i] = attrs[i];
	}

	sb_free(attrs);

	SET_TOKEN_SPAN(decl, token, parser->current);
	return stmt;
}

STMT_FUNC(make_scope)
{
	rook_stmt_base** stmt_array = 0;

	if (!match(parser, rook_token_type_right_curly))
	{
		do
		{
			rook_stmt_base* stmt = make_stmt(parser);
			if (!stmt)
				return 0;
			sb_push(stmt_array, stmt);
		}
		while (!match(parser, rook_token_type_right_curly));
	}

	rook_stmt_block* stmt = (rook_stmt_block*)rook_allocator_alloc(&parser->compiletime_allocator, offsetof(rook_stmt_block, stmts) + sb_count(stmt_array) * sizeof(*stmt->stmts));
	stmt->base.type = rook_stmt_type_block;

	if (sb_count(stmt_array))
	{
		stmt->stmt_count = sb_count(stmt_array);
		memcpy(stmt->stmts, stmt_array, stmt->stmt_count * sizeof(rook_stmt_base*));
	}
	
	sb_free(stmt_array);

	SET_TOKEN_SPAN(stmt, token, parser->current);
	RETURN_STMT(stmt);
}

PREFIX_FUNC(make_identifier)
{
	CREATE_EXPR(var_read, expr);
	expr->name = token;
	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}


static rook_float string_to_rook_float(const char* str, int length)
{
	int start = 0;
	rook_float number = 0;

	for (; *str >= '0' && *str <= '9' && start < length; ++str, ++start)
	{
		number = number * 10 + *str - '0';
	}

	if (*str++ == '.' && start++ < length)
	{
		rook_float divider = 1.0;

		for (; *str >= '0' && *str <= '9' && start < length; ++str, ++start)
		{
			number += ((long long)*str - '0') * (divider /= (rook_float)10);
		}
	}

	return number;
}

PREFIX_FUNC(make_basic_literal)
{
	CREATE_EXPR(basic_literal, expr);
	expr->type = token->type;

	switch (token->type)
	{
	case rook_token_type_number:
		expr->value.number = string_to_rook_float(token->lexeme.chars, token->lexeme.length);
		break;
	case rook_token_type_string:
	case rook_token_type_identifier:
		expr->value.string = token->lexeme;
		break;
	case rook_token_type_null:
	case rook_token_type_true:
	case rook_token_type_false:
		break;
	default:
		assert(0 && "Bad token type in basic literal");
		break;
	}

	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}

PREFIX_FUNC(make_unary)
{
	rook_expr_base* right = make_expr(parser, rook_precedence_call);
	if (!right)
		return 0;

	if (token->type == rook_token_type_increment || token->type == rook_token_type_decrement)
	{
		if (right->type != rook_expr_type_var_read &&
			right->type != rook_expr_type_subscr &&
			right->type != rook_expr_type_field_access)
		{
			add_parse_error(parser, "'++' and '--' are assignment operators, and can only be applied to variables, map members or array elements");
			return 0;
		}
	}

	CREATE_EXPR(unary, expr);
	expr->op = token->type;
	expr->right = right;
	expr->idx = -1;
	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}

static rook_expr_base* try_make_funcdecl(rook_parser* parser, int* is_func)
{
	rook_token* current = parser->current;

	CREATE_EXPR(func_decl, func);
	func->parameter_count = 0;
	func->name = rook_cstring_from_literal("");

	if (current->type != rook_token_type_right_par)
	{
		do
		{
			if (func->parameter_count == 8)
			{
				add_parse_error(parser, "Only 8 parameters allowed in functions");
				parser->current = current;
				*is_func = 1;
				return 0;
			}

			if (current->type != rook_token_type_identifier)
				return 0;

			func->parameter_names[func->parameter_count++] = current++;

			if (match_ex(parser, &current, rook_token_type_colon))
			{
				func->parameter_types[func->parameter_count - 1] = current;
				if (!match_ex(parser, &current, rook_token_type_identifier))
				{
					add_parse_error(parser, "Expected parameter type after ':' in function parameter list.");
					parser->current = current;
					*is_func = 1;
					return 0;
				}
			}
			else
			{
				func->parameter_types[func->parameter_count - 1] = 0;
			}
		}
		while (match_ex(parser, &current, rook_token_type_comma));
	}

	if (!match_ex(parser, &current, rook_token_type_right_par))
		return 0;

	if (!match_ex(parser, &current, rook_token_type_arrow))
		return 0;

	*is_func = 1;
	parser->current = current;

	if (match(parser, rook_token_type_left_curly))
	{
		func->stmt_count = 0;

		if (!match(parser, rook_token_type_right_curly))
		{
			rook_stmt_base** stmts = 0;

			do
			{
				rook_stmt_base* stmt = make_stmt(parser);
				if (!stmt)
				{
					sb_free(stmts);
					return 0;
				}

				sb_push(stmts, stmt);
				++func->stmt_count;
			}
			while (!match(parser, rook_token_type_right_curly));

			func->stmts = rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*stmts) * func->stmt_count);
			memcpy(func->stmts, stmts, sizeof(*stmts) * func->stmt_count);

			sb_free(stmts);
		}
		else
		{
			func->stmts = 0;
		}
	}
	else
	{
		int multi_ret = match(parser, rook_token_type_left_par);

		rook_stmt_return* body = (rook_stmt_return*)make_return_stmt(parser, 0);
		if (!body)
			return 0;

		if (body->expr_count == 0)
		{
			add_parse_error(parser, "Expected expression in return statement in braceless function");
			return 0;
		}

		body->base.begin = body->exprs[0]->begin;

		if (multi_ret)
		{
			if (!match(parser, rook_token_type_right_par))
			{
				add_parse_error(parser, "Expected ')' after return statement in braceless function");
				return 0;
			}
		}
		else if (body->expr_count > 1)
		{
			add_parse_error(parser, "Need () around expression list when returning multiple values in braceless function");
			return 0;
		}

		func->stmt_count = 1;
		func->stmts = rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*func->stmts));
		*func->stmts = (rook_stmt_base*)body;
	}

	RETURN_EXPR(func);
}

PREFIX_FUNC(make_compound_expr)
{
	int is_func = 0;
	rook_expr_base* funcdecl = try_make_funcdecl(parser, &is_func);
	if (funcdecl)
	{
		funcdecl->begin = token;
		funcdecl->end = parser->current;
		return funcdecl;
	}
	else if (is_func)
	{
		return 0;
	}

	if (parser->current->type == rook_token_type_right_par)
	{
		add_parse_error(parser, "Empty expr grouping not allowed");
		return 0;
	}

	rook_expr_base* expr = make_expr(parser, rook_precedence_none);
	if (!expr)
		return 0;

	if (!match(parser, rook_token_type_right_par))
	{
		add_parse_error(parser, "Expected ')' after compound expr");
		return 0;
	}

	CREATE_EXPR(compound, compound);
	compound->inner = expr;
	SET_TOKEN_SPAN(compound, token, parser->current);
	RETURN_EXPR(compound);
}

PREFIX_FUNC(make_array_literal)
{
	CREATE_EXPR(array_literal, array);
	array->initializer_count = 0;

	if (!match(parser, rook_token_type_right_square))
	{
		rook_expr_base** initializers = 0;

		do
		{
			rook_expr_base* initializer = make_expr(parser, rook_precedence_none);
			if (!initializer)
			{
				sb_free(initializers);
				return 0;
			}

			sb_push(initializers, initializer);
			++array->initializer_count;
		}
		while (match(parser, rook_token_type_comma));

		if (!match(parser, rook_token_type_right_square))
		{
			add_parse_error(parser, "Expected ']' at the end of array");
			sb_free(initializers);
			return 0;
		}

		array->initializers = rook_allocator_alloc(&parser->compiletime_allocator, sizeof(*array->initializers) * array->initializer_count);
		memcpy(array->initializers, initializers, sizeof(*array->initializers) * array->initializer_count);
		sb_free(initializers);
	}
	else
	{
		array->initializers = 0;
	}

	SET_TOKEN_SPAN(array, token, parser->current);
	RETURN_EXPR(array);
}

PREFIX_FUNC(make_map_literal)
{
	CREATE_EXPR(map_literal, map);
	map->initializer_count = 0;

	if (!match(parser, rook_token_type_right_curly))
	{
		typedef struct entry
		{
			rook_expr_base* value;
			const char* key;
			int length;
		} entry;

		entry* entries = 0;

		do
		{
			const char* key = parser->current->lexeme.chars;
			int key_length = parser->current->lexeme.length;

			if (match(parser, rook_token_type_string))
			{
				key += 1;
				key_length -= 2;
			}
			else if (!match(parser, rook_token_type_identifier))
			{
				add_parse_error(parser, "Expected new key in map constructor");
				sb_free(entries);
				return 0;
			}

			if (!match(parser, rook_token_type_assignment))
			{
				add_parse_error(parser, "Expected '=' after key in map constructor");
				sb_free(entries);
				return 0;
			}

			rook_expr_base* value = make_expr(parser, rook_precedence_none);
			if (!value)
			{
				sb_free(entries);
				return 0;
			}

			entry e;
			e.value = value;
			e.key = key;
			e.length = key_length;

			sb_push(entries, e);
			++map->initializer_count;
		}
		while (match(parser, rook_token_type_comma));

		if (!match(parser, rook_token_type_right_curly))
		{
			add_parse_error(parser, "Expected '}' at the end of map");
			sb_free(entries);
			return 0;
		}

		void* memory = rook_allocator_alloc(&parser->compiletime_allocator, (sizeof(*map->values) + sizeof(*map->keys) + sizeof(*map->key_lengths)) * map->initializer_count);
		map->values = memory;
		map->keys = (const char**)(map->values + map->initializer_count);
		map->key_lengths = (int*)(map->keys + map->initializer_count);

		for (int i = 0; i < map->initializer_count; ++i)
		{
			map->values[i] = entries[i].value;
			map->keys[i] = entries[i].key;
			map->key_lengths[i] = entries[i].length;
		}

		sb_free(entries);
	}
	else
	{
		map->keys = 0;
		map->values = 0;
	}

	SET_TOKEN_SPAN(map, token, parser->current);
	RETURN_EXPR(map);
}

INFIX_FUNC(make_field_access)
{
	rook_token* name = parser->current;
	if (parser->current->type != rook_token_type_identifier)
	{
		add_parse_error(parser, "Expected field name after '.'");
		return 0;
	}

	CREATE_EXPR(basic_literal, name_expr);
	name_expr->base.type = rook_expr_type_basic_literal;
	name_expr->type = rook_token_type_string;
	name_expr->value.string = parser->current->lexeme;
	SET_TOKEN_SPAN(name_expr, token, parser->current);
	
	++parser->current;

	CREATE_EXPR(field_access, expr);
	expr->obj = left;
	expr->field = name;
	expr->key = (rook_expr_base*)name_expr;
	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}

INFIX_FUNC(make_call)
{
	CREATE_EXPR(func_call, call);
	call->callee = left;
	call->argument_count = 0;

	if (parser->current->type != rook_token_type_right_par)
	{
		do
		{
			if (call->argument_count == 8)
			{
				add_parse_error(parser, "Only 8 argumens allowed in function calls");
				return 0;
			}

			rook_expr_base* arg = make_expr(parser, rook_precedence_none);
			if (!arg)
				return 0;

			call->arguments[call->argument_count++] = arg;
		}
		while (match(parser, rook_token_type_comma));
	}

	if (!match(parser, rook_token_type_right_par))
	{
		add_parse_error(parser, "Expected ')' after arguments in function call");
		return 0;
	}

	SET_TOKEN_SPAN(call, token, parser->current);
	RETURN_EXPR(call);
}

INFIX_FUNC(make_subscripting)
{
	rook_expr_base* key = make_expr(parser, rook_precedence_none);
	if (!key)
		return 0;

	if (!match(parser, rook_token_type_right_square))
		return 0;

	CREATE_EXPR(subscr, expr);
	expr->collection = left;
	expr->key = key;
	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}

INFIX_FUNC(make_post_unary)
{
	CREATE_EXPR(unary, expr);
	expr->base.type = rook_expr_type_postunary;

	if (left->type != rook_expr_type_var_read &&
		left->type != rook_expr_type_subscr &&
		left->type != rook_expr_type_field_access)
	{
		add_parse_error(parser, "'++' and '--' are assignment operators, and can only be applied to variables, map members or array elements");
		return 0;
	}

	expr->right = left;
	expr->op = token->type;
	expr->idx = -1;

	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}

INFIX_FUNC(make_binary)
{
	rook_expr_base* right = make_expr(parser, rules[token->type].precedence + 1);
	if (!right)
		return 0;

	CREATE_EXPR(binary, expr);
	expr->op = token->type;
	expr->left = left;
	expr->right = right;
	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}

INFIX_FUNC(make_logical)
{
	rook_expr_base* right = make_expr(parser, rules[token->type].precedence + 1);
	if (!right)
		return 0;

	CREATE_EXPR(binary, expr);
	expr->base.type = rook_expr_type_logical;
	expr->op = token->type;
	expr->left = left;
	expr->right = right;
	SET_TOKEN_SPAN(expr, token, parser->current);
	RETURN_EXPR(expr);
}

const rook_parse_rule rules[] =
{
	{ make_assignment, make_identifier, 0, rook_precedence_none }, //rook_token_type_identifier,
	{ 0, make_basic_literal, 0, rook_precedence_none }, //rook_token_type_null,
	{ 0, make_basic_literal, 0, rook_precedence_none }, //rook_token_type_true,
	{ 0, make_basic_literal, 0, rook_precedence_none }, //rook_token_type_false,
	{ 0, make_basic_literal, 0, rook_precedence_none }, //rook_token_type_number,
	{ 0, make_basic_literal, 0, rook_precedence_none }, //rook_token_type_string,

	{ 0, 0, 0, 0 }, //rook_token_type_assignment,
	{ 0, 0, 0, 0 }, //rook_token_type_colon,
	{ make_empty_stmt, 0, 0, 0 }, //rook_token_type_semicolon,
	{ 0, 0, 0, 0 }, //rook_token_type_comma,
	{ 0, 0, make_field_access, rook_precedence_call }, //rook_token_type_dot,
	{ 0, 0, 0, 0 }, //rook_token_type_arrow,

	{ 0, make_compound_expr, make_call, rook_precedence_call }, //rook_token_type_left_par,
	{ 0, 0, 0, 0 }, //rook_token_type_right_par,

	{ make_attribute, make_array_literal, make_subscripting, rook_precedence_call }, //rook_token_type_left_square,
	{ 0, 0, 0, 0 }, //rook_token_type_right_square,

	{ make_scope, make_map_literal, 0, rook_precedence_none }, //rook_token_type_left_curly,
	{ 0, 0, 0, 0 }, //rook_token_type_right_curly,

	{ 0, 0, make_binary, rook_precedence_sum }, //rook_token_type_add,
	{ 0, make_unary, make_binary, rook_precedence_sum }, //rook_token_type_sub,
	{ 0, 0, make_binary, rook_precedence_product }, //rook_token_type_star,
	{ 0, 0, make_binary, rook_precedence_product }, //rook_token_type_div,
	{ 0, 0, make_binary, rook_precedence_product }, //rook_token_type_modulo,

	{ 0, 0, 0, 0 }, //rook_token_type_add_assign,
	{ 0, 0, 0, 0 }, //rook_token_type_sub_assign,
	{ 0, 0, 0, 0 }, //rook_token_type_star_assign,
	{ 0, 0, 0, 0 }, //rook_token_type_div_assign,
	{ 0, 0, 0, 0 }, //rook_token_type_modulo_assign,
	{ 0, make_unary, make_post_unary, rook_precedence_none }, //rook_token_type_increment,
	{ 0, make_unary, make_post_unary, rook_precedence_none }, //rook_token_type_decrement,

	{ 0, 0, make_binary, rook_precedence_comparison }, //rook_token_type_equal,
	{ 0, 0, make_binary, rook_precedence_comparison }, //rook_token_type_not_eq,
	{ 0, 0, make_binary, rook_precedence_comparison }, //rook_token_type_less,
	{ 0, 0, make_binary, rook_precedence_comparison }, //rook_token_type_less_eq,
	{ 0, 0, make_binary, rook_precedence_comparison }, //rook_token_type_greater,
	{ 0, 0, make_binary, rook_precedence_comparison }, //rook_token_type_greater_eq,

	{ 0, make_unary, 0, 0 }, //rook_token_type_not,
	{ 0, 0, make_logical, 0 }, //rook_token_type_and,
	{ 0, 0, make_logical, 0 }, //rook_token_type_or,

	{ make_global, 0, 0, 0 }, //rook_token_type_global,
	{ make_local, 0, 0, 0 }, //rook_token_type_local,
	{ /*make_const*/0, 0, 0, 0 }, //rook_token_type_const,
	{ make_if, 0, 0, 0 }, //rook_token_type_if,
	{ 0, 0, 0, 0 }, //rook_token_type_else,
	{ make_while, 0, 0, 0 }, //rook_token_type_while,
	{ make_for, 0, 0, 0 }, //rook_token_type_for,
	{ 0, 0, 0, 0 }, //rook_token_type_case,
	{ make_break, 0, 0, 0 }, //rook_token_type_break,
	{ make_continue, 0, 0, 0 }, //rook_token_type_continue,
	{ 0, 0, 0, 0 }, //rook_token_type_defer,
	{ make_return_stmt, 0, 0, 0 }, //rook_token_type_return
};

#ifdef __cplusplus
}
#endif // __cplusplus
