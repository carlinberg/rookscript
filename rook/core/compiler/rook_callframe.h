#ifndef ROOK_CALLFRAME
#define ROOK_CALLFRAME

#include <utility/rook_string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_scope rook_scope;

typedef struct rook_captured_value
{
	rook_cstring name;
	int idx;
	int is_local;
} rook_captured_value;

typedef struct rook_callframe
{
	struct rook_callframe* parent;
	rook_scope* current_scope;
	rook_captured_value* captured_values;
	int local_count;
} rook_callframe;

#define rook_callframe_init(c, p, s) ((void)((c)->parent = (p), (c)->current_scope = (s), (c)->captured_values = 0, (c)->local_count = 0))
#define rook_callframe_free(c) (sb_free((c)->captured_values), (c)->captured_values = 0, (c)->local_count = 0)
#define rook_callframe_add_local(c, n) rook_scope_add_local((c)->current_scope, (n), (c)->local_count++)
#define rook_callframe_add_unnamed_local(c) rook_scope_add_unnamed_local((c)->current_scope, (c)->local_count++)

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_CALLFRAME
