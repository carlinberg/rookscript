#ifndef ROOK_PARSE_RULE
#define ROOK_PARSE_RULE

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_token rook_token;
typedef struct rook_parser rook_parser;
typedef struct rook_stmt_base rook_stmt_base;
typedef struct rook_expr_base rook_expr_base;

typedef enum rook_precedence
{
	rook_precedence_none,
	rook_precedence_conditional,		// ? :
	rook_precedence_or,				// or
	rook_precedence_and,				// and
	rook_precedence_comparison,		// == != < > <= >=
	rook_precedence_sum,				// + -
	rook_precedence_product,			// * /
	rook_precedence_unary,			// ! - + * &
	rook_precedence_call,			// . () []
	rook_precedence_primary
} rook_precedence;

typedef struct rook_parse_rule
{
	rook_stmt_base*(*stmt)(rook_parser*, rook_token*);
	rook_expr_base*(*prefix)(rook_parser*, rook_token*);
	rook_expr_base*(*infix)(rook_parser*, rook_expr_base*, rook_token*);
	rook_precedence precedence;
} rook_parse_rule;

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_PARSE_RULE
