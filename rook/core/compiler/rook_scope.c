#include "rook_scope.h"

#include <string.h>

#include <utility/stb_sb.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_scope_init(rook_scope* scope, rook_scope* parent)
{
	scope->parent = parent;
	scope->locals = 0;
}

void rook_scope_free(rook_scope* scope)
{
	sb_free(scope->locals);
	scope->locals = 0;
}

int rook_scope_add_local(rook_scope* scope, rook_cstring name, int stack_idx)
{
	for (rook_local_var* it = scope->locals; it < scope->locals + sb_count(scope->locals); ++it)
	{
		if (it->name.length == name.length && strncmp(it->name.chars, name.chars, name.length) == 0)
		{
			return 0;
		}
	}

	rook_local_var new_local;
	new_local.name = name;
	new_local.stack_idx = stack_idx;
	sb_push(scope->locals, new_local);

	return 1;
}

void rook_scope_add_unnamed_local(rook_scope* scope, int stack_idx)
{
	rook_local_var new_local;
	new_local.name = rook_cstring_from_literal("");
	new_local.stack_idx = stack_idx;
	sb_push(scope->locals, new_local);
}

int rook_scope_get_local(rook_scope* scope, rook_cstring name)
{
	while (scope)
	{
		for (rook_local_var* it = scope->locals; it < scope->locals + sb_count(scope->locals); ++it)
		{
			if (it->name.length == name.length && strncmp(it->name.chars, name.chars, name.length) == 0)
			{
				return it->stack_idx;
			}
		}

		scope = scope->parent;
	}

	return -1;
}

#ifdef __cplusplus
}
#endif // __cplusplus
