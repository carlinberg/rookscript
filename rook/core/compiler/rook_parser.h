#ifndef ROOK_PARSER
#define ROOK_PARSER

#include <utility/rook_allocators.h>
#include <utility/rook_string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_stmt_base rook_stmt_base;
typedef struct rook_token rook_token;
typedef struct rook_error rook_error;

typedef struct rook_parser
{
	rook_token* tokens;
	int token_count;
	rook_token* current;

	rook_stmt_base** stmts;

	rook_allocator compiletime_allocator;

	rook_cstring file;
	rook_cstring code;

	rook_error** errors;
} rook_parser;

void rook_parser_init(rook_parser* parser, rook_cstring file, rook_cstring code, rook_error** errors);
void rook_parser_free(rook_parser* parser);

int rook_parse(rook_parser* parser);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_PARSER
