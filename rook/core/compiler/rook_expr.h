#ifndef ROOK_EXPR
#define ROOK_EXPR

#include <utility/rook_settings.h>
#include <utility/rook_string.h>
#include <utility/rook_number_types.h>

#include "rook_token_type.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_stmt_base rook_stmt_base;
typedef struct rook_captured_value rook_captured_value;

typedef enum rook_expr_type
{
	rook_expr_type_basic_literal,
	rook_expr_type_array_literal,
	rook_expr_type_map_literal,
	rook_expr_type_func_decl,

	rook_expr_type_unary,
	rook_expr_type_postunary,
	rook_expr_type_binary,
	rook_expr_type_logical,

	rook_expr_type_var_read,
	rook_expr_type_var_read_local,
	rook_expr_type_var_read_captured_value,
	rook_expr_type_var_read_global,
	rook_expr_type_func_call,
	rook_expr_type_subscr,
	rook_expr_type_field_access,
	rook_expr_type_compound
} rook_expr_type;

typedef struct rook_expr_base
{
	rook_expr_type type;
	rook_token* begin;
	rook_token* end;
} rook_expr_base;

typedef struct rook_expr_basic_literal
{
	rook_expr_base base;

	rook_token_type type;

	union
	{
		rook_float number;
		rook_cstring string;
	} value;
} rook_expr_basic_literal;

typedef struct rook_expr_array_literal
{
	rook_expr_base base;

	int initializer_count;
	rook_expr_base** initializers;
} rook_expr_array_literal;

typedef struct rook_expr_map_literal
{
	rook_expr_base base;

	int initializer_count;
	const char** keys;
	int* key_lengths;
	rook_expr_base** values;
	rook_string* attrs;
} rook_expr_map_literal;

typedef struct rook_expr_func_decl
{
	rook_expr_base base;

	int parameter_count;
	rook_token* parameter_names[8];
	rook_token* parameter_types[8];

	int captured_value_count;
	rook_captured_value* captured_values;

	int stmt_count;
	rook_stmt_base** stmts;

	rook_cstring name;
} rook_expr_func_decl;

typedef struct rook_expr_unary
{
	rook_expr_base base;

	rook_token_type op;
	int idx;

	rook_expr_base* right;
} rook_expr_unary;

typedef struct rook_expr_binary
{
	rook_expr_base base;

	rook_token_type op;

	rook_expr_base* left;
	rook_expr_base* right;
} rook_expr_binary;

typedef struct rook_expr_var_read
{
	rook_expr_base base;

	rook_token* name;
} rook_expr_var_read;

typedef struct rook_expr_var_read_indexed
{
	rook_expr_base base;

	u8 idx;
} rook_expr_var_read_indexed;

typedef struct rook_expr_func_call
{
	rook_expr_base base;

	rook_expr_base* callee;

	int argument_count;
	rook_expr_base* arguments[8];
} rook_expr_func_call;

typedef struct rook_expr_subscr
{
	rook_expr_base base;

	rook_expr_base* collection;

	rook_expr_base* key;
} rook_expr_subscr;

typedef struct rook_expr_field_access
{
	rook_expr_base base;

	rook_expr_base* obj;
	rook_token* field;
	rook_expr_base* key;
} rook_expr_field_access;

typedef struct rook_expr_compound
{
	rook_expr_base base;

	rook_expr_base* inner;
} rook_expr_compound;

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_EXPR
