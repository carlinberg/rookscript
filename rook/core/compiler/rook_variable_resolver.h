#ifndef ROOK_VARIABLE_RESOLVER
#define ROOK_VARIABLE_RESOLVER

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_parser rook_parser;

//typedef struct rook_variable_resolver
//{
//	callframe;
//	stmts;
//	compiletime_alloc;
//	rook_error** errors;
//} rook_variable_resolver;

int rook_variable_resolver_run(rook_parser* parser);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_VARIABLE_RESOLVER
