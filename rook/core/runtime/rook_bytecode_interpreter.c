#include "rook_bytecode_interpreter.h"

#include <utility/stb_sb.h>
#include <utility/rook_error.h>

#include <core/compiler/rook_token_type.h>

#include "rook_vm.h"
#include "rook_bytecode_chunk.h"
#include "rook_object.h"
#include "rook_obj_string.h"
#include "rook_array.h"
#include "rook_map.h"
#include "rook_cfunc.h"
#include "rook_func.h"
#include "rook_bound_func.h"
#include "rook_closure.h"



#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define BINARY_OP_ARITHMETIC(op)																\
	do {																						\
		if (rook_env_stack_size(&i->env) >= 2)													\
		{																						\
			rook_obj* left = rook_env_stack_get(&i->env, -2);									\
			rook_obj* right = rook_env_stack_get(&i->env, -1);									\
			if (!rook_obj_isnumber(*left) || !rook_obj_isnumber(*right))						\
			{																					\
				add_errorf(vm, "Tried to do arithmetic on %s and %s. Can only do arithmetic on numbers (and + on strings)", rook_obj_type_names[rook_obj_get_type(*left)], rook_obj_type_names[rook_obj_get_type(*right)]);	\
			}																					\
			rook_obj res = rook_obj_create(0, rook_obj_type_number);							\
			res.as.number = left->as.number op right->as.number;								\
			rook_env_pop_many(&i->env, 2);														\
			rook_env_push_obj(&i->env, res);													\
		}																						\
		else																					\
		{																						\
			add_error(vm, "Stack underflow");												\
		}																						\
	} while (0)

#define BINARY_OP_RELATIONAL(op)																\
	do {																						\
		if (rook_env_stack_size(&i->env) >= 2)													\
		{																						\
			rook_obj* left = rook_env_stack_get(&i->env, -2);									\
			rook_obj* right = rook_env_stack_get(&i->env, -1);									\
			if (rook_obj_get_type(*left) != rook_obj_type_number || rook_obj_get_type(*right) != rook_obj_type_number)		\
			{																					\
				add_errorf(vm, "Tried to compare %s and %s. Can only compare numbers", rook_obj_type_names[rook_obj_get_type(*left)], rook_obj_type_names[rook_obj_get_type(*right)]);	\
			}																					\
			rook_obj res = rook_obj_create(0, rook_obj_type_boolean);						\
			res.as.boolean = left->as.number op right->as.number;								\
			rook_env_pop_many(&i->env, 2);														\
			rook_env_push_obj(&i->env, res);													\
		}																						\
		else																					\
		{																						\
			add_error(vm, "Stack underflow");													\
		}																						\
	} while (0)

#define add_error(vm, msg) do { rook_vm_runtime_error((vm), rook_cstring_from_literal(msg)); goto error; } while(0)
#define add_errorf(vm, f, ...) do { rook_vm_runtime_errorf((vm), (f), __VA_ARGS__); goto error; } while(0)

void rook_bytecode_interpreter_init(rook_bytecode_interpreter* i, rook_allocator* runtime_allocator, rook_error** errors, rook_vm* vm)
{
	i->callframe_stack_base_stack = 0;
	i->runtime_allocator = runtime_allocator;
	rook_env_init(&i->env, runtime_allocator, vm);
	i->errors = errors;
	
	i->current_line = 0;
	i->current_column = 0;

	for (int ii = 0; ii < rook_obj_type_length; ++ii)
		i->extension_funcs[ii] = rook_obj_create_map(i->runtime_allocator, 0);

	i->current_call_info = 0;
	i->ip = 0;
	i->chunk_stack = 0;
	i->tag_names = 0;
	i->tag_extension_funcs = 0;

	sb_push(i->tag_names, rook_string_from_literal("rook_base_obj"));
	sb_push(i->tag_extension_funcs, rook_obj_create_map(runtime_allocator, 0));
}

void rook_bytecode_interpreter_free(rook_bytecode_interpreter* i)
{
	int ii = 0;

	for (ii = 0; ii < rook_obj_type_length; ++ii)
		rook_obj_free(i->runtime_allocator, &i->extension_funcs[ii]);

	for (ii = 0; ii < sb_count(i->tag_names); ++ii)
		rook_string_free(&i->tag_names[ii]);

	sb_free(i->tag_names);
	i->tag_names = 0;
	
	for (ii = 0; ii < sb_count(i->tag_extension_funcs); ++ii)
		rook_obj_free(i->runtime_allocator, &i->tag_extension_funcs[ii]);

	sb_free(i->tag_extension_funcs);
	i->tag_extension_funcs = 0;

	rook_env_free(&i->env);
	sb_free(i->callframe_stack_base_stack);
	sb_free(i->chunk_stack);

	i->runtime_allocator = 0;
	i->errors = 0;
	i->callframe_stack_base_stack = 0;
	i->current_line = 0;
	i->current_column = 0;

	i->current_call_info = 0;
	i->ip = 0;
	i->chunk_stack = 0;
}

static int rook_bytecode_interpreter_run_impl(rook_vm* vm, rook_bytecode_interpreter* i, rook_bytecode_chunk* c, int stack_base);

static int call_func(rook_vm* vm, rook_func* callee, int arg_count, rook_env* env, rook_bytecode_interpreter* i)
{
	if (callee->parameter_count != -1 && callee->parameter_count != arg_count)
	{
		add_errorf(vm, "Function %S expected %d arguments, got %d.", callee->name, callee->parameter_count, arg_count);
		error:
		return 0;
	}
	int result_count = rook_bytecode_interpreter_run_impl(vm, i, &callee->chunk, rook_env_stack_size(env) - arg_count - 1);
	if (result_count == -1)
		return 0;

	int result_begin = rook_env_stack_size(env) - result_count;

	rook_obj* results = 0;

	for (int j = result_begin; j < rook_env_stack_size(env); ++j)
		sb_push(results, env->stack[j]);

	rook_env_shrink_stack(env, result_count);
	rook_env_pop_many(env, 1);

	for (int j = 0; j < result_count; ++j)
		rook_env_push_obj(env, results[j]);

	sb_free(results);

	return 1;
}


#define JUMP_TYPE unsigned int

static JUMP_TYPE read_jump_impl(rook_bytecode_interpreter* interpreter)
{
	JUMP_TYPE jump = 0;
	for(int i = 0; i < ROOK_BYTECODE_JUMP_WIDTH; ++i)
	{
		jump |= (((JUMP_TYPE)*(interpreter->ip++)) << 8 * i);
	}
	return jump;
}

static int rook_bytecode_interpreter_run_impl(rook_vm* vm, rook_bytecode_interpreter* i, rook_bytecode_chunk* c, int stack_base)
{
#define READ_BYTE() (*(i->ip++))
#define READ_CONSTANT() c->constants[READ_BYTE()]
#define READ_JUMP() read_jump_impl(i)
	if (!c->code)
	{
		rook_errorlist_add_literal(i->errors, "rook_bytecode_interpreter_run_impl: No code to run", vm->current_file, vm->current_code, 0, 0);
		return -1;
	}

	i->ip = c->code;
	rook_env* env = &i->env;
	int* expr_result_stack = 0;
	int* call_arg_count_stack = 0;
	int result_count = 0;

	sb_push(i->callframe_stack_base_stack, stack_base);
	sb_push(i->chunk_stack, c);

	while (1)
	{
		rook_opcode op = READ_BYTE();

		int ip_number = (int)(i->ip - c->code);
		i->current_line = c->lines_and_cols[ip_number].line;
		i->current_column = c->lines_and_cols[ip_number].col;
		
		switch (op)
		{
		case rook_opcode_load_null:
			rook_env_push_null(env);
			break;
		case rook_opcode_load_constant:
			if (!rook_env_push_obj(env, READ_CONSTANT()))
				goto error;
			rook_obj_incref(rook_env_stack_top(env));
			break;
		case rook_opcode_load_global:
			{
				rook_obj name = READ_CONSTANT();
				if (!rook_obj_isstring(name))
					add_error(vm, "Global name must be string");

				rook_obj g;
				if (!rook_env_get_global(env, rook_cstring_from_chars_ex(name.as.string->chars, name.as.string->length), &g))
					add_errorf(vm, "Global '%s' not found", name.as.string->chars);

				rook_obj_incref(&g);
				rook_env_push_obj(env, g);
			}
			break;
		case rook_opcode_load_local:
			{
				int idx = READ_BYTE();
				idx += sb_last(i->callframe_stack_base_stack);
				rook_obj* obj = rook_env_stack_get(env, idx);
				if (!obj)
					add_error(vm, "Tried reading local outside of stack");
				rook_obj_incref(obj);
				rook_env_push_obj(env, *obj);
			}
			break;
		case rook_opcode_load_captured_value:
			{
				int idx = READ_BYTE();

				int callee_idx = sb_last(i->callframe_stack_base_stack) - 1;
				rook_obj* callee = rook_env_stack_get(env, callee_idx);
				if (rook_obj_isbound_func(*callee))
					callee = &callee->as.bound_func->func;

				if (idx >= sb_count(callee->as.closure->captured_values))
					add_error(vm, "Captured value index out of range");
				rook_obj_incref(&callee->as.closure->captured_values[idx]);
				rook_env_push_obj(env, callee->as.closure->captured_values[idx]);
			}
			break;
		case rook_opcode_pop:
			rook_env_pop_many(env, READ_BYTE());
			break;
		case rook_opcode_save_stack_size:
			sb_push(expr_result_stack, rook_env_stack_size(env));
			break;

		case rook_opcode_not:
			{
				rook_obj* obj = rook_env_stack_top(env);
				if (!obj)
					goto error;
				int truthy = rook_obj_istruthy(obj);
				rook_obj_free(i->runtime_allocator, obj);
				*obj = rook_obj_create_boolean(!truthy);
			}
			break;
		case rook_opcode_negate:
			{
				rook_obj* obj = rook_env_stack_top(env);
				if (!obj)
					goto error;
				if (!rook_obj_isnumber(*obj))
					add_errorf(vm, "Can only negate numbers, tried to negate %s", rook_obj_type_names[rook_obj_get_type(*obj)]);

				obj->as.number = -obj->as.number;
			}
			break;

		case rook_opcode_preincr:
		case rook_opcode_predecr:
			{
				int one = op == rook_opcode_preincr ? 1 : -1;

				switch (READ_BYTE())
				{
				case 1:
					{
						//local

						int idx = READ_BYTE();
						idx += sb_last(i->callframe_stack_base_stack);
						rook_obj* obj = rook_env_stack_get(env, idx);
						if (!obj)
							add_error(vm, "Tried reading local outside of stack");
						if (!rook_obj_isnumber(*obj))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(*obj)]);
						obj->as.number = obj->as.number + one;
						rook_env_push_obj(env, *obj);
					}
					break;
				case 2:
					{
						//capture

						int callee_idx = rook_env_stack_size(env) - sb_last(i->callframe_stack_base_stack);
						rook_obj* callee = rook_env_stack_get(env, -callee_idx - 2);
						if (rook_obj_isbound_func(*callee))
							callee = &callee->as.bound_func->func;

						int idx = READ_BYTE();
						if (idx >= sb_count(callee->as.closure->captured_values))
							add_error(vm, "Captured value index out of range");

						rook_obj* obj = &callee->as.closure->captured_values[idx];

						if (!rook_obj_isnumber(*obj))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(*obj)]);

						obj->as.number = obj->as.number + one;
						if (!rook_env_push_obj(env, *obj))
							goto error;
					}
					break;
				case 3:
					{
						//global

						rook_obj name_obj = READ_CONSTANT();
						rook_obj obj;
						rook_cstring name = rook_cstring_from_chars_ex(name_obj.as.string->chars, name_obj.as.string->length);

						if (!rook_env_get_global(env, name, &obj))
							add_error(vm, "Cannot find global to increment");
						if (!rook_obj_isnumber(obj))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(obj)]);

						obj.as.number = obj.as.number + one;
						rook_env_set_global(env, name, obj);

						if (!rook_env_push_obj(env, obj))
							goto error;
					}
					break;
				case 4:
					{
						//sub script

						int key_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
						int collection_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

						rook_obj* key = rook_env_stack_get(env, -key_idx);
						rook_obj* collection = rook_env_stack_get(env, -collection_idx);

						rook_obj value;

						if (!key)
							add_error(vm, "Tried to index collection entry with value outside of stack");
						if (!collection)
							add_error(vm, "Tried to index entry on collection outside of stack");

						if (rook_obj_ismap(*collection))
						{
							if (!rook_obj_isstring(*key))
								add_error(vm, "Tried to index map with non-string key");

							if (!rook_map_get(collection->as.map, key->as.string->chars, key->as.string->length, &value))
								add_errorf(vm, "Key not found: %s", key->as.string->chars);

							if (!rook_obj_isnumber(value))
								add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(value)]);

							value.as.number = value.as.number + one;

							rook_map_set(collection->as.map, key->as.string->chars, key->as.string->length, value, i->runtime_allocator);
						}
						else if (rook_obj_isarray(*collection))
						{
							if (!rook_obj_isnumber(*key))
								add_error(vm, "Tried to index array with non-number key");
							int idx = (int)key->as.number;
							if (idx < 0 || idx >= collection->as.array->size)
								add_error(vm, "Array index out of range");

							value = collection->as.array->array[idx];
							if (!rook_obj_isnumber(value))
								add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(value)]);

							value.as.number = value.as.number + one;
							collection->as.array->array[idx] = value;
						}
						else
						{
							add_error(vm, "Tried to index entry on something other than map or array");
						}

						if (!rook_env_pop_many(env, collection_idx))
							goto error;
						if (rook_env_push_obj(env, value))
							goto error;
					}
					break;
				case 5:
					{
						//field access

						int map_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

						rook_obj* map = rook_env_stack_get(env, -map_idx);
						rook_obj key = READ_CONSTANT();

						rook_obj value;

						if (!map)
							add_error(vm, "Tried to index entry on map outside of stack");
						if (!rook_obj_ismap(*map))
							add_error(vm, "Tried to index object other than map");
						if (!rook_obj_isstring(key))
							add_error(vm, "Tried to index map with non-string key");

						if (!rook_map_get(map->as.map, key.as.string->chars, key.as.string->length, &value))
							add_errorf(vm, "Key not found: %s", key.as.string->chars);

						if (!rook_obj_isnumber(value))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(value)]);

						value.as.number = value.as.number + one;

						rook_map_set(map->as.map, key.as.string->chars, key.as.string->length, value, i->runtime_allocator);

						if (!rook_env_pop_many(env, map_idx))
							goto error;
						if (rook_env_push_obj(env, value))
							goto error;
					}
					break;
				default:
					add_error(vm, "Bad argument to opcode pre increment");
				}
			}
			break;

		case rook_opcode_postincr:
		case rook_opcode_postdecr:
			{
				int one = op == rook_opcode_postincr ? 1 : -1;

				switch (READ_BYTE())
				{
				case 1:
					{
						//local

						int idx = READ_BYTE();
						idx += sb_last(i->callframe_stack_base_stack);
						rook_obj* obj = rook_env_stack_get(env, idx);
						if (!obj)
							add_error(vm, "Tried reading local outside of stack");
						if (!rook_obj_isnumber(*obj))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(*obj)]);
						rook_env_push_obj(env, *obj);
						obj->as.number = obj->as.number + one;
					}
					break;
				case 2:
					{
						//capture

						int callee_idx = rook_env_stack_size(env) - sb_last(i->callframe_stack_base_stack);
						rook_obj* callee = rook_env_stack_get(env, -callee_idx - 2);
						if (rook_obj_isbound_func(*callee))
							callee = &callee->as.bound_func->func;

						int idx = READ_BYTE();
						if (idx >= sb_count(callee->as.closure->captured_values))
							add_error(vm, "Captured value index out of range");

						rook_obj* obj = &callee->as.closure->captured_values[idx];

						if (!obj)
							add_error(vm, "Tried reading local outside of stack");
						if (!rook_obj_isnumber(*obj))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(*obj)]);

						if (!rook_env_push_obj(env, *obj))
							goto error;

						obj->as.number = obj->as.number + one;
					}
					break;
				case 3:
					{
						//global

						rook_obj name_obj = READ_CONSTANT();
						rook_obj obj;
						rook_cstring name = rook_cstring_from_chars_ex(name_obj.as.string->chars, name_obj.as.string->length);

						if (!rook_env_get_global(env, name, &obj))
							add_error(vm, "Cannot find global to increment");
						if (!rook_obj_isnumber(obj))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(obj)]);

						if (!rook_env_push_obj(env, obj))
							goto error;

						obj.as.number = obj.as.number + one;
						rook_env_set_global(env, name, obj);
					}
					break;
				case 4:
					{
						//sub script

						int key_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
						int collection_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

						rook_obj* key = rook_env_stack_get(env, -key_idx);
						rook_obj* collection = rook_env_stack_get(env, -collection_idx);

						rook_obj value;

						if (!key)
							add_error(vm, "Tried to index collection entry with value outside of stack");
						if (!collection)
							add_error(vm, "Tried to index entry on collection outside of stack");

						if (rook_obj_ismap(*collection))
						{
							if (!rook_obj_isstring(*key))
								add_error(vm, "Tried to index map with non-string key");

							if (!rook_map_get(collection->as.map, key->as.string->chars, key->as.string->length, &value))
								add_errorf(vm, "Key not found: %s", key->as.string->chars);

							if (!rook_obj_isnumber(value))
								add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(value)]);

							rook_obj new_value = rook_obj_create_number(value.as.number + one);

							rook_map_set(collection->as.map, key->as.string->chars, key->as.string->length, new_value, i->runtime_allocator);
						}
						else if (rook_obj_isarray(*collection))
						{
							if (!rook_obj_isnumber(*key))
								add_error(vm, "Tried to index array with non-number key");
							int idx = (int)key->as.number;
							if (idx < 0 || idx >= collection->as.array->size)
								add_error(vm, "Array index out of range");

							value = collection->as.array->array[idx];
							if (!rook_obj_isnumber(value))
								add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(value)]);

							collection->as.array->array[idx].as.number = value.as.number + one;
						}
						else
						{
							add_error(vm, "Tried to index entry on something other than map or array");
						}

						if (!rook_env_pop_many(env, collection_idx))
							goto error;
						if (rook_env_push_obj(env, value))
							goto error;
					}
					break;
				case 5:
					{
						//field access

						int map_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

						rook_obj* map = rook_env_stack_get(env, -map_idx);
						rook_obj key = READ_CONSTANT();

						rook_obj value;

						if (!map)
							add_error(vm, "Tried to index entry on map outside of stack");
						if (!rook_obj_ismap(*map))
							add_error(vm, "Tried to index object other than map");
						if (!rook_obj_isstring(key))
							add_error(vm, "Tried to index map with non-string key");

						if (!rook_map_get(map->as.map, key.as.string->chars, key.as.string->length, &value))
							add_errorf(vm, "Key not found: %s", key.as.string->chars);

						if (!rook_obj_isnumber(value))
							add_errorf(vm, "Can only do ++ and -- on numbers, tried on %s", rook_obj_type_names[rook_obj_get_type(value)]);

						rook_obj new_value = rook_obj_create_number(value.as.number + one);

						rook_map_set(map->as.map, key.as.string->chars, key.as.string->length, new_value, i->runtime_allocator);

						if (!rook_env_pop_many(env, map_idx))
							goto error;
						if (rook_env_push_obj(env, value))
							goto error;
					}
					break;
				default:
					add_error(vm, "Bad argument to opcode post increment");
				}
			}
			break;

		case rook_opcode_add:
			if (rook_env_stack_size(&i->env) >= 1 && rook_obj_isnumber(*rook_env_stack_top(env)))
			{
				BINARY_OP_ARITHMETIC(+);
			}
			else if (rook_env_stack_size(&i->env) >= 2)
			{
				rook_obj* left = rook_env_stack_get(&i->env, -2);
				rook_obj* right = rook_env_stack_get(&i->env, -1);
				if (!rook_obj_isstring(*left) || !rook_obj_isstring(*right))
					add_error(vm, "Can only add numbers or strings");

				rook_obj res = rook_obj_create(i->runtime_allocator, rook_obj_type_string);
				rook_obj_string_init_ex(res.as.string, left->as.string->chars, left->as.string->length);
				rook_obj_string_append(res.as.string, right->as.string);
				
				if (!rook_env_pop_many(&i->env, 2))
					goto error;
				if (!rook_env_push_obj(&i->env, res))
					goto error;
			}
			break;
		case rook_opcode_subtract:
			BINARY_OP_ARITHMETIC(-);
			break;
		case rook_opcode_multiply:
			BINARY_OP_ARITHMETIC(*);
			break;
		case rook_opcode_divide:
			BINARY_OP_ARITHMETIC(/);
			break;
		case rook_opcode_modulo:
			if (rook_env_stack_size(&i->env) >= 2)												
			{																					
				rook_obj* left = rook_env_stack_get(&i->env, -2);
				rook_obj* right = rook_env_stack_get(&i->env, -1);
				if (rook_obj_get_type(*left) != rook_obj_type_number || rook_obj_get_type(*right) != rook_obj_type_number)
					add_error(vm, "Can only do modulo % on numbers");
				rook_obj res = rook_obj_create(0, rook_obj_type_number);
				res.as.number = (rook_float)((int)left->as.number % (int)right->as.number);
				rook_env_pop_many(&i->env, 2);
				rook_env_push_obj(&i->env, res);
			}
			else
			{
				add_error(vm, "Stack underflow");
			}
			break;

		case rook_opcode_equal:
			if (rook_env_stack_size(env) >= 2)
			{
				rook_obj* left = rook_env_stack_get(env, -2);
				rook_obj* right = rook_env_stack_get(env, -1);
				int are_equal = rook_obj_equal(*left, *right);
				rook_env_pop_many(env, 2);
				rook_env_push_obj(env, rook_obj_create_boolean(are_equal));
			}
			else
			{
				add_error(vm, "Stack underflow");
			}
			break;
		case rook_opcode_not_equal:
			if (rook_env_stack_size(env) >= 2)
			{
				rook_obj* left = rook_env_stack_get(env, -2);
				rook_obj* right = rook_env_stack_get(env, -1);
				int are_equal = rook_obj_equal(*left, *right);
				rook_env_pop_many(env, 2);
				rook_env_push_obj(env, rook_obj_create_boolean(!are_equal));
			}
			else
			{
				add_error(vm, "Stack underflow");
			}
			break;
		case rook_opcode_less:
			BINARY_OP_RELATIONAL(<);
			break;
		case rook_opcode_less_equal:
			BINARY_OP_RELATIONAL(<=);
			break;
		case rook_opcode_greater:
			BINARY_OP_RELATIONAL(>);
			break;
		case rook_opcode_greater_equal:
			BINARY_OP_RELATIONAL(>=);
			break;

		case rook_opcode_jump_if_false:
		{
			if (rook_env_stack_size(env) < 1)
				add_error(vm, "Stack underflow");

			JUMP_TYPE jump_length = READ_JUMP();
			if (rook_obj_isfalsey(rook_env_stack_top(env)))
			{
				i->ip += jump_length;
				if (i->ip >= c->code + sb_count(c->code))
					add_error(vm, "Jumped outside of code");
			}
			break;
		}
		case rook_opcode_jump_if_true:
		{
			if (rook_env_stack_size(env) < 1)
				add_error(vm, "Stack underflow");

			JUMP_TYPE jump_length = READ_JUMP();
			if (rook_obj_istruthy(rook_env_stack_top(env)))
			{
				i->ip += jump_length;
				if (i->ip >= c->code + sb_count(c->code))
					add_error(vm, "Jumped outside of code");
			}
			break;
		}
		case rook_opcode_jump:
			i->ip += READ_JUMP();
			if (i->ip < c->code || i->ip >= c->code + sb_count(c->code))
				add_error(vm, "Jumped outside of code");
			break;
		case rook_opcode_jump_back:
			i->ip -= READ_JUMP();
			if (i->ip < c->code || i->ip >= c->code + sb_count(c->code))
				add_error(vm, "Jumped outside of code");
			break;
		case rook_opcode_pop_and_jump_if_false:
			if (rook_env_stack_size(env) < 1)
			{
				add_error(vm, "Stack underflow");
			}
			else
			{
				rook_env_pop_many(env, rook_env_stack_size(env) - sb_pop(expr_result_stack) - 1);

				JUMP_TYPE jump_length = READ_JUMP();
				if (rook_obj_isfalsey(rook_env_stack_top(env)))
					i->ip += jump_length;
			}

			rook_env_pop(env);
			break;

		case rook_opcode_exprstmt_end:
			rook_env_pop_many(env, rook_env_stack_size(env) - sb_pop(expr_result_stack));
			break;
		case rook_opcode_return:
			{
				int result_begin = sb_pop(expr_result_stack);
				result_count = rook_env_stack_size(env) - result_begin;

				rook_obj* results = 0;
				for (int ii = result_begin; ii < rook_env_stack_size(env); ++ii)
				{
					sb_push(results, env->stack[ii]);
				}

				rook_env_shrink_stack(env, result_count);

				int pop_count = rook_env_stack_size(env) - sb_pop(i->callframe_stack_base_stack);
				rook_env_pop_many(env, pop_count);

				for (int ii = 0; ii < result_count; ++ii)
				{
					rook_env_push_obj(env, results[ii]);
				}

				sb_free(results);

				goto success;
			}

		case rook_opcode_call_prepare:
			if (!rook_env_push_null(env))
				goto error;
			sb_push(call_arg_count_stack, rook_env_stack_size(env));
			break;
		case rook_opcode_call:
			{
				int arg_count = rook_env_stack_size(env) - sb_pop(call_arg_count_stack);
				rook_obj* callee = rook_env_stack_get(env, -arg_count - 2);
				rook_obj* this_ = rook_env_stack_get(env, -arg_count - 1);
				rook_obj* args = rook_env_stack_get(env, -arg_count);
				rook_obj* results = 0;

				if (rook_obj_isbound_func(*callee))
				{
					*this_ = callee->as.bound_func->this_;
					rook_obj_incref(this_);
					
					callee = &callee->as.bound_func->func;
				}

				add_call_info(call_info, i->current_call_info->file, rook_obj_get_func_name(*callee), i->current_line, i->current_call_info, i);
				u8* ip = i->ip;

				switch (rook_obj_get_type(*callee))
				{
				case rook_obj_type_func:
					if (!call_func(vm, callee->as.func, arg_count, env, i))
						goto error;
					break;
				case rook_obj_type_closure:
					if (!call_func(vm, callee->as.closure->func.as.func, arg_count, env, i))
						goto error;
					break;
				case rook_obj_type_cfunc:
					{
						if (callee->as.cfunc->parameter_count != -1 && callee->as.cfunc->parameter_count != arg_count)
							add_errorf(vm, "Function %S expected %d arguments, got %d", callee->as.cfunc->name, callee->as.cfunc->parameter_count, arg_count);
						

						int res_count = callee->as.cfunc->delegate(vm, callee->as.cfunc->userdata, this_, args, arg_count, &results);
						if (res_count == -1)
							goto error;

						rook_env_pop_many(env, 2 + arg_count);

						for (int ii = 0; ii < res_count; ++ii)
							rook_env_push_obj(env, results[ii]);

						rook_env_free_result_array(results);
					}
					break;


				default:
					add_error(vm, "Can only call functions");
				}

				pop_call_info(i);
				i->ip = ip;
			}
			break;

		case rook_opcode_declare_array:
			{
				int result_begin = sb_pop(expr_result_stack);
				int res_count = rook_env_stack_size(env) - result_begin;
				rook_obj array = rook_obj_create_array(i->runtime_allocator, res_count);

				if (res_count)
				{
					rook_obj* values = rook_env_stack_get(env, result_begin);
					if (!values)
						add_error(vm, "Error getting initializer values from stack to array");

					rook_array_push_many(array.as.array, values, res_count);
					rook_env_shrink_stack(env, res_count);
				}

				rook_env_push_obj(env, array);
			}
			break;
		case rook_opcode_declare_map_value_end:
			{
				int extra_values = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				if (extra_values > 0)
					rook_env_pop_many(env, extra_values - 1);
				else if (extra_values == 0)
					rook_env_push_null(env);
				else
					add_error(vm, "Messed up map value initializer");
			}
			break;
		case rook_opcode_declare_map:
			{
				int map_entry_begin = sb_pop(expr_result_stack);
				int expr_count = rook_env_stack_size(env) - map_entry_begin;
				if (expr_count % 2 != 0)
					add_error(vm, "Miss matching map key value pairs");

				rook_obj map = rook_obj_create_map(i->runtime_allocator, expr_count);

				for (int ii = map_entry_begin; ii < rook_env_stack_size(env); ii += 2)
				{
					rook_obj key_obj = env->stack[ii];
					rook_obj value_obj = env->stack[ii + 1];

					rook_map_set(map.as.map, key_obj.as.string->chars, key_obj.as.string->length, value_obj, i->runtime_allocator);

					rook_obj_free(i->runtime_allocator, &key_obj);
				}

				rook_env_shrink_stack(env, expr_count);
				rook_env_push_obj(env, map);
			}
			break;
		case rook_opcode_declare_func:
			{
				rook_obj func = READ_CONSTANT();
				u8 captured_value_count = READ_BYTE();

				int callframe_stack_base = sb_last(i->callframe_stack_base_stack);

				int callee_idx = 0;
				rook_obj* callee = 0;

				rook_obj_incref(&func);

				if (captured_value_count > 0)
				{
					rook_obj* captured_values = 0;

					for (int ii = 0; ii < captured_value_count; ++ii)
					{
						int idx = READ_BYTE();
						u8 is_local = READ_BYTE();

						if (is_local)
						{
							rook_obj* uv = rook_env_stack_get(env, idx + callframe_stack_base);
							if (!uv)
								add_error(vm, "Bad Captured value index and scope index");

							rook_obj_incref(uv);
							sb_push(captured_values, *uv);
						}
						else
						{
							if (!callee)
							{
								callee_idx = rook_env_stack_size(env) - callframe_stack_base;
								callee = rook_env_stack_get(env, -callee_idx - 2);
							}

							if (idx >= sb_count(callee->as.closure->captured_values))
								add_error(vm, "Captured value index out of range");

							rook_obj_incref(&callee->as.closure->captured_values[idx]);
							sb_push(captured_values, callee->as.closure->captured_values[idx]);
						}
					}

					rook_obj closure = rook_obj_create_closure(i->runtime_allocator, func, captured_values);
					func = closure;
				}

				rook_env_push_obj(env, func);
			}
			break;
		case rook_opcode_declare_global:
			{
				rook_obj name = READ_CONSTANT();
				if (!rook_obj_isstring(name))
					add_error(vm, "Global name must be string");

				rook_obj* g = rook_env_stack_top(env);
				if (!g)
					goto error;

				rook_env_register_global(env, rook_cstring_from_chars_ex(name.as.string->chars, name.as.string->length), *g);
				rook_env_shrink_stack(env, 1);
			}
			break;
		case rook_opcode_declare_locals:
			{
				int desired = READ_BYTE();
				int actual = rook_env_stack_size(env) - sb_pop(expr_result_stack);

				if (actual > desired)
					rook_env_pop_many(env, actual - desired);
				else if (desired > actual)
					rook_env_push_many_null(env, desired - actual);
			}
			break;
		case rook_opcode_leave_first_expr_result:
			if (!rook_env_pop_many(env, rook_env_stack_size(env) - sb_pop(expr_result_stack) - 1))
				goto error;
			break;

		case rook_opcode_assign_start:
			{
				int assign_base = sb_last(expr_result_stack);
				env->stack[assign_base - 1] = rook_obj_create_number(0);
			}
			break;
		case rook_opcode_assign_local:
			{
				int assign_base = sb_last(expr_result_stack);
				int val_idx = sb_last(expr_result_stack) + (int)env->stack[assign_base - 1].as.number;
				if (val_idx < rook_env_stack_size(env))
				{
					rook_obj* val = rook_env_stack_get(env, val_idx);

					int idx = READ_BYTE();
					idx += sb_last(i->callframe_stack_base_stack);
					rook_obj* obj = rook_env_stack_get(env, idx);
					if (!obj)
						add_error(vm, "Tried to assign local outside of stack");

					rook_obj_free(i->runtime_allocator, obj);
					rook_obj_incref(val);
					*obj = *val;

					env->stack[assign_base - 1].as.number += 1;
				}
			}
			break;
		case rook_opcode_assign_capture:
			{
				int assign_base = sb_last(expr_result_stack);
				int val_idx = sb_last(expr_result_stack) + (int)env->stack[assign_base - 1].as.number;
				if (val_idx < rook_env_stack_size(env))
				{
					rook_obj* val = rook_env_stack_get(env, val_idx);

					int callee_idx = sb_last(i->callframe_stack_base_stack) - 1;
					rook_obj* callee = rook_env_stack_get(env, callee_idx);
					if (rook_obj_isbound_func(*callee))
						callee = &callee->as.bound_func->func;

					int idx = READ_BYTE();
					if (idx >= sb_count(callee->as.closure->captured_values))
						add_error(vm, "Captured value index out of range");

					rook_obj_free(i->runtime_allocator, &callee->as.closure->captured_values[idx]);
					rook_obj_incref(val);
					callee->as.closure->captured_values[idx] = *val;
				}
			}
			break;
		case rook_opcode_assign_global:
			{
				int assign_base = sb_last(expr_result_stack);
				int val_idx = sb_last(expr_result_stack) + (int)env->stack[assign_base - 1].as.number;
				if (val_idx < rook_env_stack_size(env))
				{
					rook_obj* val = rook_env_stack_get(env, val_idx);

					rook_obj name = READ_CONSTANT();
					if (!rook_obj_isstring(name))
						add_error(vm, "Global name not a string");

					rook_obj_incref(val);
					rook_env_set_global(env, rook_cstring_from_chars_ex(name.as.string->chars, name.as.string->length), *val);
				}
			}
			break;
		case rook_opcode_property_get:
			{
				int collection_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				rook_obj* map = rook_env_stack_get(env, -collection_idx);
				rook_obj key = READ_CONSTANT();
				rook_obj value;

				if (!map)
					add_error(vm, "Tried to index entry on map outside of stack");
				if (!rook_obj_isstring(key))
					add_errorf(vm, "Tried to index map with non-string key, type was %s", rook_obj_type_names[rook_obj_get_type(key)]);

				//First, try finding it straight in the map
				int found = rook_obj_ismap(*map) && rook_map_get(map->as.map, key.as.string->chars, key.as.string->length, &value);

				//if not found, try finding it in the extensions for this tag/class
				if (!found)
					found = rook_obj_get_tag(*map) && rook_map_get(i->tag_extension_funcs[rook_obj_get_tag(*map)].as.map, key.as.string->chars, key.as.string->length, &value);

				//if not found there either, try finding it in the extensions for this type
				if (!found)
					found = rook_map_get(i->extension_funcs[rook_obj_get_type(*map)].as.map, key.as.string->chars, key.as.string->length, &value);

				if (!found)
					add_errorf(vm, "Key not found: %s", key.as.string->chars);

				rook_obj_incref(&value);

				if (rook_obj_isfunc(value) || rook_obj_isclosure(value) || rook_obj_iscfunc(value))
				{
					rook_obj_incref(map);
					rook_obj bound = rook_obj_create_bound_func(i->runtime_allocator, value, *map);
					value = bound;
				}

				if (!rook_env_pop_many(env, collection_idx))
					goto error;
				if (!rook_env_push_obj(env, value))
					goto error;
			}
			break;
		case rook_opcode_subscr_get:
			{
				int key_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				int collection_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

				rook_obj* key = rook_env_stack_get(env, -key_idx);
				rook_obj* collection = rook_env_stack_get(env, -collection_idx);

				rook_obj value;

				if (!key)
					add_error(vm, "Tried to index collection entry with value outside of stack");
				if (!collection)
					add_error(vm, "Tried to index entry on collection outside of stack");

				if (rook_obj_ismap(*collection))
				{
					if (!rook_obj_isstring(*key))
						add_error(vm, "Tried to index map with non-string key");

					if (!rook_map_get(collection->as.map, key->as.string->chars, key->as.string->length, &value))
						add_errorf(vm, "Key not found: %s", key->as.string->chars);
				}
				else if (rook_obj_isarray(*collection))
				{
					if (!rook_obj_isnumber(*key))
						add_error(vm, "Tried to index array with non-number key");
					int idx = (int)key->as.number;
					if (idx < 0 || idx >= collection->as.array->size)
						add_error(vm, "Array index out of range");

					value = collection->as.array->array[idx];
				}
				else
				{
					add_error(vm, "Tried to index entry on something other than map or array");
				}

				rook_obj_incref(&value);

				if (rook_obj_ismap(*collection) && (rook_obj_isfunc(value) || rook_obj_isclosure(value) || rook_obj_iscfunc(value)))
				{
					rook_obj_incref(collection);
					rook_obj bound = rook_obj_create_bound_func(i->runtime_allocator, value, *collection);
					value = bound;
				}

				if (!rook_env_pop_many(env, collection_idx))
					goto error;
				if (!rook_env_push_obj(env, value))
					goto error;
			}
			break;
		case rook_opcode_property_set:
			{
				int val_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				int map_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

				rook_obj* val = rook_env_stack_get(env, -val_idx);
				rook_obj* map = rook_env_stack_get(env, -map_idx);

				if (!val)
					add_error(vm, "Tried to set map entry with value outside of stack");
				if (!map)
					add_error(vm, "Tried to set entry on map outside of stack");
				if (!rook_obj_ismap(*map))
					add_error(vm, "Tried to set entry on something other than map");

				rook_obj key = READ_CONSTANT();

				rook_obj_incref(val);
				rook_map_set(map->as.map, key.as.string->chars, key.as.string->length, *val, i->runtime_allocator);

				if (!rook_env_pop_many(env, map_idx))
					goto error;
			}
			break;
		case rook_opcode_subscr_set:
			{
				int val_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				int key_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				int collection_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

				rook_obj* val = rook_env_stack_get(env, -val_idx);
				rook_obj* key = rook_env_stack_get(env, -key_idx);
				rook_obj* collection = rook_env_stack_get(env, -collection_idx);

				if (!val)
					add_error(vm, "Tried to index collection entry with value outside of stack");
				if (!key)
					add_error(vm, "Tried to index collection entry with key outside of stack");
				if (!collection)
					add_error(vm, "Tried to index entry on collection outside of stack");

				if (rook_obj_ismap(*collection))
				{
					if (!rook_obj_isstring(*key))
						add_error(vm, "Tried to index map with non-string key");

					rook_obj_incref(val);
					rook_map_set(collection->as.map, key->as.string->chars, key->as.string->length, *val, i->runtime_allocator);
				}
				else if (rook_obj_isarray(*collection))
				{
					if (!rook_obj_isnumber(*key))
						add_error(vm, "Tried to index array with non-number key");
					int idx = (int)key->as.number;
					if (idx < 0 || idx >= collection->as.array->size)
						add_error(vm, "Array index out of range");

					rook_obj_incref(val);
					rook_obj_free(i->runtime_allocator, &collection->as.array->array[idx]);
					collection->as.array->array[idx] = *val;
				}
				else
				{
					add_error(vm, "Tried to index entry on something other than map or array");
				}

				if (!rook_env_pop_many(env, collection_idx))
					goto error;
			}
			break;

		case rook_opcode_set_comp_assign:
			{
				rook_token_type math_op = READ_BYTE();
				int val_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				int key_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);
				int collection_idx = rook_env_stack_size(env) - sb_pop(expr_result_stack);

				rook_obj* val = rook_env_stack_get(env, -val_idx);
				rook_obj* key = rook_env_stack_get(env, -key_idx);
				rook_obj* collection = rook_env_stack_get(env, -collection_idx);
				rook_obj old_val;

				if (!val)
					add_error(vm, "Tried to index collection entry with value outside of stack");
				if (!key)
					add_error(vm, "Tried to index collection entry with key outside of stack");
				if (!collection)
					add_error(vm, "Tried to index entry on collection outside of stack");

				if (rook_obj_ismap(*collection))
				{
					if (!rook_obj_isstring(*key))
						add_error(vm, "Tried to index map with non-string key");

					if (!rook_map_get(collection->as.map, key->as.string->chars, key->as.string->length, &old_val))
						add_errorf(vm, "Key '%s' not found.", key->as.string->chars);

					if (rook_obj_get_type(old_val) != rook_obj_get_type(*val))
						add_errorf(vm, "Tried to add values of different type: %s and %s", rook_obj_type_names[rook_obj_get_type(*val)], rook_obj_type_names[rook_obj_get_type(old_val)]);
				}
				else if (rook_obj_isarray(*collection))
				{
					if (!rook_obj_isnumber(*key))
						add_error(vm, "Tried to index array with non-number key");
					int idx = (int)key->as.number;
					if (idx < 0 || idx >= collection->as.array->size)
						add_error(vm, "Array index out of range");

					old_val = collection->as.array->array[idx];
					if (rook_obj_get_type(old_val) != rook_obj_get_type(*val))
						add_errorf(vm, "Tried to add values of different type: %s and %s", rook_obj_type_names[rook_obj_get_type(*val)], rook_obj_type_names[rook_obj_get_type(old_val)]);
				}
				else
				{
					add_error(vm, "Tried to index entry on something other than map or array");
				}

				//do the op
				if (rook_obj_get_type(*val) == rook_obj_type_string)
				{
					if (math_op != rook_token_type_add)
						add_error(vm, "Can't use other arithmetics than add on strings.");

					rook_obj_string_append(old_val.as.string, val->as.string);
				}
				else if (rook_obj_get_type(*val) == rook_obj_type_number)
				{
					switch (math_op)
					{
					case rook_token_type_add:
						old_val.as.number += val->as.number;
						break;
					case rook_token_type_sub:
						old_val.as.number -= val->as.number;
						break;
					case rook_token_type_star:
						old_val.as.number *= val->as.number;
						break;
					case rook_token_type_div:
						old_val.as.number /= val->as.number;
						break;
					case rook_token_type_modulo:
						old_val.as.number = (rook_float)((long long)old_val.as.number % (long long)val->as.number);
						break;
					default:
						add_error(vm, "Bad math op token");
					}
				}

				if (rook_obj_ismap(*collection))
				{
					rook_map_set(collection->as.map, key->as.string->chars, key->as.string->length, old_val, i->runtime_allocator);
				}
				else if (rook_obj_isarray(*collection))
				{
					int idx = (int)key->as.number;
					collection->as.array->array[idx] = old_val;
				}

				if (!rook_env_pop_many(env, collection_idx))
					goto error;
			}
			break;

		case rook_opcode_foreach_setup:
			{
				rook_obj* container = rook_env_stack_top(env);
				if (!rook_obj_isarray(*container) && !rook_obj_ismap(*container))
					add_error(vm, "Can only iterate over arrays or maps");
			}

			if (!rook_env_push_obj(env, rook_obj_create_number(0)))
				goto error;
			if (!rook_env_push_null(env))
				goto error;
			if (!rook_env_push_null(env))
				goto error;
			break;
		case rook_opcode_foreach_do:
			{
				int jump_length = READ_JUMP();

				rook_obj* container = rook_env_stack_get(env, -4);
				rook_obj* iterator = rook_env_stack_get(env, -3);
				rook_obj* key = rook_env_stack_get(env, -2);
				rook_obj* value = rook_env_stack_get(env, -1);
				int idx = (int)iterator->as.number;

				if (rook_obj_isarray(*container))
				{
					if (idx >= container->as.array->size)
					{
						i->ip += jump_length;
						if (i->ip < c->code || i->ip >= c->code + sb_count(c->code))
							add_error(vm, "Jumped outside of code");
						break;
					}

					*key = *iterator;
					iterator->as.number = iterator->as.number + 1;

					rook_obj_free(i->runtime_allocator, value);
					rook_obj_incref(&container->as.array->array[idx]);
					*value = container->as.array->array[idx];
				}
				else
				{
					while (1)
					{
						if (idx >= container->as.map->capacity)
						{
							i->ip += jump_length;
							if (i->ip < c->code || i->ip >= c->code + sb_count(c->code))
								add_error(vm, "Jumped outside of code");
							break;
						}

						if (rook_map_entry_exists(container->as.map, idx))
						{
							rook_obj_free(i->runtime_allocator, key);
							*key = rook_obj_create_string(i->runtime_allocator, rook_map_get_key(container->as.map, idx));

							rook_obj_free(i->runtime_allocator, value);
							*value = rook_map_get_value(container->as.map, idx);
							rook_obj_incref(value);

							iterator->as.number = (rook_float)(idx + 1);

							break;
						}

						++idx;
					}
				}
			}
			break;

		default:
			add_error(vm, "Unknown opcode");
		}
	}

	success:
	sb_free(expr_result_stack);
	sb_free(call_arg_count_stack);
	sb_pop(i->chunk_stack);
	return result_count;

	error:
	sb_free(expr_result_stack);
	sb_free(call_arg_count_stack);
	sb_pop(i->chunk_stack);
	return -1;
}

int rook_bytecode_interpreter_run(rook_vm* vm, rook_bytecode_interpreter* i, rook_bytecode_chunk* c, int arg_count)
{
	add_call_info(call_info, rook_cstring_from_literal("script"), rook_cstring_from_literal("script"), 0, i->current_call_info, i);

	int result = rook_bytecode_interpreter_run_impl(vm, i, c, rook_env_stack_size(&i->env) - arg_count);
	
	pop_call_info(i);

	return result;
}

#ifdef __cplusplus
}
#endif // __cplusplus
