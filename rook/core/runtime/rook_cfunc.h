#ifndef ROOK_CFUNCTION
#define ROOK_CFUNCTION

#include <utility/rook_string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_obj rook_obj;
typedef struct rook_vm rook_vm;
typedef struct rook_stmt_base rook_stmt_base;
typedef struct rook_token rook_token;
typedef struct rook_allocator rook_allocator;

typedef int(*rook_delegate_type)(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);

typedef struct rook_cfunc_parameter
{
	rook_cstring name;
	rook_cstring type;
} rook_cfunc_parameter;

typedef struct rook_cfunc
{
	int refcount;

	int(*delegate)(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
	int parameter_count;
	void* userdata;
	rook_string* parameter_names;
	rook_string* parameter_types;

	rook_string name;
} rook_cfunc;

void rook_cfunc_init(rook_cfunc* func, rook_delegate_type d, rook_cstring name);
void rook_cfunk_free(rook_cfunc* func);
void rook_cfunc_set_parameters(rook_cfunc* func, rook_cfunc_parameter* parameters, int parameter_count);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_CFUNCTION
