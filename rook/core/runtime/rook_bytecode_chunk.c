#include "rook_bytecode_chunk.h"
#include <utility/rook_string.h>
#include <utility/stb_sb.h>
#include <utility/rook_error.h>
#include "rook_object.h"
#include "rook_obj_string.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_bytecode_chunk_init(rook_bytecode_chunk* c)
{
	c->code = 0;
	c->constants = 0;
	c->lines_and_cols = 0;
	c->local_infos = 0;
}

void rook_bytecode_chunk_free(rook_bytecode_chunk* c, rook_allocator* a)
{
	int i;

	for (i = 0; i < sb_count(c->constants); ++i)
		rook_obj_free(a, &c->constants[i]);

	for (i = 0; i < sb_count(c->local_infos); ++i)
		rook_string_free(&c->local_infos[i].name);

	sb_free(c->code);
	sb_free(c->constants);
	sb_free(c->lines_and_cols);
	sb_free(c->local_infos);
	c->code = 0;
	c->constants = 0;
	c->lines_and_cols = 0;
	c->local_infos = 0;
}

void rook_bytecode_chunk_write(rook_bytecode_chunk* c, u8 byte, int line, int column)
{
	sb_push(c->code, byte);

	rook_line_and_col lc;
	lc.line = line;
	lc.col = column;
	sb_push(c->lines_and_cols, lc);
}

int rook_bytecode_chunk_add_constant(rook_bytecode_chunk* c, rook_obj constant, rook_allocator* a)
{
	int count = sb_count(c->constants);
	for (int i = 0; i < count; ++i)
	{
		if (rook_obj_equal(c->constants[i], constant))
		{
			rook_obj_free(a, &constant);
			return i;
		}
	}

	sb_push(c->constants, constant);
	return count;
}

int rook_bytecode_chunk_add_and_write_constant(rook_bytecode_chunk* c, rook_obj constant, rook_allocator* a, int line, int column, rook_error** errors, rook_cstring file, rook_cstring code)
{
	int index = rook_bytecode_chunk_add_constant(c, constant, a);
	if (index < 0xFF)
		rook_bytecode_chunk_write(c, (u8)index, line, column);
	else
		rook_errorlist_add_literal(errors, "", file, code, line, column);
	return index < 0xFF;
}

void rook_bytecode_chunk_add_local_info(rook_bytecode_chunk* c, rook_string name, int s_idx, int pcs)
{
	rook_local_info info;
	info.name = name;
	info.stack_idx = s_idx;
	info.pc_start = pcs;
	info.pc_end = -1;

	sb_push(c->local_infos, info);
}

void rook_bytecode_chunk_close_local_infos(rook_bytecode_chunk* c, int s_idx, int pce)
{
	int i;

	for (i = 0; i < sb_count(c->local_infos); ++i)
	{
		if (c->local_infos[i].pc_end == -1 && c->local_infos[i].stack_idx >= s_idx)
			c->local_infos[i].pc_end = pce;
	}
}

void rook_bytecode_chunk_dissasemble(rook_bytecode_chunk* c, rook_string* output)
{
	int i = 0;
	int length = sb_count(c->code);

	while (i < length)
	{
		rook_string_append_literal(output, "[LINE: ");
		rook_string_append_number(output, (rook_float)c->lines_and_cols[i].line);
		rook_string_append_literal(output, " COLUMN: ");
		rook_string_append_number(output, (rook_float)c->lines_and_cols[i].col);
		rook_string_append_literal(output, "] OPCODE: ");

		switch (c->code[i])
		{
		case rook_opcode_return:
			rook_string_append_literal(output, "RETURN\n");
			++i;
			break;
		case rook_opcode_load_null:
			rook_string_append_literal(output, "LOAD 0\n");
			++i;
			break;
		case rook_opcode_load_constant:
			rook_string_append_literal(output, "LOAD CONSTANT - VALUE: ");
			{
				rook_obj* constant = &c->constants[c->code[i + 1]];
				switch (rook_obj_get_type(*constant))
				{
				case rook_obj_type_number:
					rook_string_append_number(output, constant->as.number);
					break;
				default:
					rook_string_append_literal(output, "Some value");
					break;
				}
			}
			rook_string_append_literal(output, "\n");
			i += 2;
			break;
		case rook_opcode_load_global:
			rook_string_append_literal(output, "LOAD GLOBAL - NAME: ");
			{
				rook_obj* name = &c->constants[c->code[i + 1]];
				rook_string_append_chars(output, name->as.string->chars, name->as.string->length);
			}
			rook_string_append_literal(output, "\n");
			i += 2;
			break;
		case rook_opcode_load_local:
			rook_string_append_literal(output, "LOAD LOCAL - INDEX: ");
			rook_string_append_number(output, (rook_float)c->code[i + 1]);
			rook_string_append_literal(output, "\n");
			i += 2;
			break;
		case rook_opcode_pop:
			rook_string_append_literal(output, "POP: ");
			rook_string_append_number(output, (rook_float)c->code[i + 1]);
			rook_string_append_literal(output, "\n");
			i += 2;
			break;

		case rook_opcode_add:
			rook_string_append_literal(output, "ADD\n");
			++i;
			break;
		case rook_opcode_subtract:
			rook_string_append_literal(output, "SUBTRACT\n");
			++i;
			break;
		case rook_opcode_multiply:
			rook_string_append_literal(output, "MULTIPLY\n");
			++i;
			break;
		case rook_opcode_divide:
			rook_string_append_literal(output, "DIVIDE\n");
			++i;
			break;


		case rook_opcode_pop_and_jump_if_false:
			rook_string_append_literal(output, "POP AND JUMP IF FALSE - JUMP LENGTH: ");
			rook_string_append_number(output, (rook_float)c->code[i + 1]);
			rook_string_append_literal(output, "\n");
			i += 2;
			break;
		case rook_opcode_jump:
			rook_string_append_literal(output, "POP AND JUMP - JUMP LENGTH: ");
			rook_string_append_number(output, (rook_float)c->code[i + 1]);
			rook_string_append_literal(output, "\n");
			i += 2;
			break;
		case rook_opcode_exprstmt_end:
			rook_string_append_literal(output, "EXPR STMT END\n");
			++i;
			break;

		case rook_opcode_call_prepare:
			rook_string_append_literal(output, "CALL PREPARE\n");
			++i;
			break;
		case rook_opcode_call:
			rook_string_append_literal(output, "CALL\n");
			++i;
			break;

		case rook_opcode_declare_func:
			rook_string_append_literal(output, "FUNC DECL\n");
			i += 2;
			break;
		case rook_opcode_declare_global:
			rook_string_append_literal(output, "GLOBAL DECL - NAME: \n");
			rook_obj* name = &c->constants[c->code[i + 1]];
			rook_string_append_chars(output, name->as.string->chars, name->as.string->length);
			i += 2;
			break;
			
		default:
			return;
		}
	}
}

#ifdef __cplusplus
}
#endif // __cplusplus
