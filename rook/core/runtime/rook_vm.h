#ifndef ROOK_VM
#define ROOK_VM

#include <utility/rook_settings.h>
#include <utility/rook_error.h>

#include <core/compiler/rook_parser.h>
#include <core/compiler/rook_bytecode_generator.h>

#include "rook_bytecode_interpreter.h"
#include "rook_object.h"
#include "rook_cfunc.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_local_watch
{
	rook_cstring name;
	int stack_idx;
} rook_local_watch;

typedef struct rook_vm
{
	rook_allocator runtime_allocator;
	rook_bytecode_interpreter interpreter;
	rook_cstring current_file;
	rook_cstring current_code;
	rook_string* directories;
	rook_error* errors;
} rook_vm;

void rook_vm_init(rook_vm* vm);
void rook_vm_free(rook_vm* vm);

void rook_vm_add_directory(rook_vm* vm, rook_cstring directory);

void rook_vm_runtime_error(rook_vm* vm, rook_cstring msg);
void rook_vm_runtime_error_ex(rook_vm* vm, rook_string msg);
#define rook_vm_runtime_errorf(vm, fmt, ...) rook_vm_runtime_error_ex((vm), rook_string_from_format((fmt), __VA_ARGS__))
#define rook_vm_runtime_errorl(vm, lit) rook_vm_runtime_error_ex((vm), rook_string_from_literal(lit))
void rook_vm_clear_errors(rook_vm* vm);

//void rook_vm_set_line_hook(rook_vm* vm, int(*callback)(void* data, int line), void* data);
rook_local_watch* rook_vm_getlocals(rook_vm* vm);

rook_obj rook_vm_compile_string(rook_vm* vm, rook_string code, rook_cstring name);
rook_obj rook_vm_compile_file(rook_vm* vm, rook_cstring path);

int rook_vm_run_string_args(rook_vm* vm, rook_cstring code, rook_cstring name, int argc, rook_obj* args);
int rook_vm_run_obj_args(rook_vm* vm, rook_obj obj, int argc, rook_obj* args);
int rook_vm_run_file_args(rook_vm* vm, rook_cstring path, int argc, rook_obj* args);

#define rook_vm_run_string(vm, code, name) rook_vm_run_string_args((vm), (code), (name), 0, 0)
#define rook_vm_run_obj(vm, obj) rook_vm_run_obj_args((vm), (obj), 0, 0)
#define rook_vm_run_file(vm, path) rook_vm_run_file_args((vm), (path), 0, 0)

void rook_vm_stack_push(rook_vm* vm, rook_obj obj);
rook_obj rook_vm_stack_get(rook_vm* vm, int idx);
int rook_vm_stack_size(rook_vm* vm);
void rook_vm_stack_pop(rook_vm* vm, int how_many);

void rook_vm_register_global(rook_vm* vm, rook_cstring name, rook_obj obj);
int rook_vm_get_global(rook_vm* vm, rook_cstring name, rook_obj* obj);
int rook_vm_set_global(rook_vm* vm, rook_cstring name, rook_obj obj);

void rook_vm_register_extension(rook_vm* vm, rook_obj_type type, rook_cstring name, rook_delegate_type func);

//returns tag id
int rook_vm_register_tag(rook_vm* vm, rook_cstring name);
//returns 1 if tag exists, 0 if not
int rook_vm_register_tag_extension(rook_vm* vm, int tag, rook_cstring func_name, rook_obj func);

rook_obj rook_vm_create_null(void);
rook_obj rook_vm_create_boolean(int b);
rook_obj rook_vm_create_number(rook_float n);
rook_obj rook_vm_create_userdata(void* u);
rook_obj rook_vm_create_string(rook_vm* vm, rook_cstring s);
rook_obj rook_vm_create_empty_string(rook_vm* vm);
rook_obj rook_vm_create_array(rook_vm* vm, int size);
rook_obj rook_vm_create_map(rook_vm* vm, int size);
rook_obj rook_vm_create_cfunc(rook_vm* vm, rook_delegate_type func, rook_cstring name);

void rook_vm_free_obj(rook_vm* vm, rook_obj* obj);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_VM
