#ifndef ROOK_SCRIPT_OBJECT
#define ROOK_SCRIPT_OBJECT

#include <utility/rook_settings.h>
#include "rook_cfunc.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_allocator rook_allocator;
typedef struct rook_cstring rook_cstring;
typedef struct rook_string rook_string;

typedef enum rook_obj_type
{
	rook_obj_type_null,
	rook_obj_type_boolean,
	rook_obj_type_number,
	rook_obj_type_string,
	rook_obj_type_userdata,
	rook_obj_type_array,
	rook_obj_type_map,
	rook_obj_type_func,
	rook_obj_type_closure,
	rook_obj_type_cfunc,
	rook_obj_type_bound_func,
	rook_obj_type_length
} rook_obj_type;

typedef struct rook_env rook_env;
typedef struct rook_obj_string rook_obj_string;
typedef struct rook_array rook_array;
typedef struct rook_map rook_map;
typedef struct rook_func rook_func;
typedef struct rook_closure rook_closure;
typedef struct rook_cfunc rook_cfunc;
typedef struct rook_bound_func rook_bound_func;

typedef struct rook_obj
{
	int type_and_tag;

	union
	{
		int boolean;
		rook_float number;
		void* userdata;
		int* refcount;
		rook_obj_string* string;
		rook_array* array;
		rook_map* map;
		rook_func* func;
		rook_closure* closure;
		rook_cfunc* cfunc;
		rook_bound_func* bound_func;
	} as;
} rook_obj;

rook_obj rook_obj_create(rook_allocator* a, rook_obj_type t);
void rook_obj_incref(rook_obj* obj);
void rook_obj_free(rook_allocator* a, rook_obj* obj);

rook_obj rook_obj_create_null(void);
rook_obj rook_obj_create_boolean(int b);
rook_obj rook_obj_create_number(rook_float n);
rook_obj rook_obj_create_userdata(void* ud);
rook_obj rook_obj_create_string(rook_allocator* a, rook_cstring s);
rook_obj rook_obj_create_empty_string(rook_allocator* a);
rook_obj rook_obj_create_array(rook_allocator* a, int c);
rook_obj rook_obj_create_map(rook_allocator* a, int c);
rook_obj rook_obj_create_cfunc(rook_allocator* a, rook_delegate_type d, rook_cstring n);
rook_obj rook_obj_create_closure(rook_allocator* a, rook_obj f, rook_obj* cv);
rook_obj rook_obj_create_bound_func(rook_allocator* a, rook_obj f, rook_obj t);

rook_cstring rook_obj_get_func_name(rook_obj f);
int rook_obj_get_parameter_count(rook_obj f);
rook_string* rook_obj_get_parameter_names(rook_obj f);
rook_string* rook_obj_get_parameter_types(rook_obj f);
rook_func* rook_obj_get_prototype(rook_obj f);
int rook_obj_get_member(rook_obj obj, rook_cstring key, rook_obj* value, rook_allocator* a);

int rook_obj_to_string(rook_obj obj, char* buffer, int buffer_length, int* obj_str_length);
rook_obj rook_obj_clone(rook_allocator* a, rook_obj obj);

int rook_obj_equal(rook_obj left, rook_obj right);
int rook_obj_istruthy(rook_obj* obj);

#define rook_obj_isfalsey(o) (!rook_obj_istruthy(o))

#define rook_obj_get_type(o) ((o).type_and_tag & 0xF)
#define rook_obj_get_tag(o) ((o).type_and_tag >> 4)

#define rook_obj_set_type_and_tag(o, type, tag) ((o).type_and_tag = (type | ((tag) << 4)))
#define rook_obj_set_type(o, t) rook_obj_set_type_and_tag(o, t, rook_obj_get_tag(o))
#define rook_obj_set_tag(o, t) rook_obj_set_type_and_tag(o, rook_obj_get_type(o), t)

#define rook_obj_isnull(o) (rook_obj_get_type(o) == rook_obj_type_null)
#define rook_obj_isboolean(o) (rook_obj_get_type(o) == rook_obj_type_boolean)
#define rook_obj_isnumber(o) (rook_obj_get_type(o) == rook_obj_type_number)
#define rook_obj_isstring(o) (rook_obj_get_type(o) == rook_obj_type_string)
#define rook_obj_isuserdata(o) (rook_obj_get_type(o) == rook_obj_type_userdata)
#define rook_obj_isarray(o) (rook_obj_get_type(o) == rook_obj_type_array)
#define rook_obj_ismap(o) (rook_obj_get_type(o) == rook_obj_type_map)
#define rook_obj_isfunc(o) (rook_obj_get_type(o) == rook_obj_type_func)
#define rook_obj_isclosure(o) (rook_obj_get_type(o) == rook_obj_type_closure)
#define rook_obj_iscfunc(o) (rook_obj_get_type(o) == rook_obj_type_cfunc)
#define rook_obj_isbound_func(o) (rook_obj_get_type(o) == rook_obj_type_bound_func)
#define rook_obj_iscallable(o) (rook_obj_isfunc(o) || rook_obj_isclosure(o) || rook_obj_iscfunc(o) || rook_obj_isbound_func(o))

extern const char* rook_obj_type_names[];

#define rook_obj_get_type_name(o) rook_obj_type_names[rook_obj_get_type(o)]

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_SCRIPT_OBJECT
