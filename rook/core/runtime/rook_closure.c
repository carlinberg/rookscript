#include "rook_closure.h"

#include <utility/stb_sb.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_closure_init(rook_closure* c, rook_obj func, rook_obj* captured_values)
{
	c->refcount = 1;
	c->func = func;
	c->captured_values = captured_values;
}

void rook_closure_free(rook_closure* c, rook_allocator* a)
{
	rook_obj_free(a, &c->func);

	for (int i = 0; i < sb_count(c->captured_values); ++i)
		rook_obj_free(a, &c->captured_values[i]);

	sb_free(c->captured_values);
	c->func = rook_obj_create_null();
	c->captured_values = 0;
}

#ifdef __cplusplus
}
#endif // __cplusplus
