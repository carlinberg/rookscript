#include "rook_cfunc.h"

#include <utility/rook_allocators.h>
#include <utility/rook_string.h>
#include <utility/stb_sb.h>

#include <core/compiler/rook_token.h>
#include <core/compiler/rook_stmt.h>

#include "rook_object.h"



#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_cfunc_init(rook_cfunc* func, rook_delegate_type d, rook_cstring name)
{
	func->refcount = 1;

	func->parameter_count = -1;
	func->delegate = d;
	func->userdata = 0;
	func->parameter_names = 0;
	func->parameter_types = 0;

	rook_string_initc(&func->name, name);
}

void rook_cfunk_free(rook_cfunc* func)
{
	int i = 0;

	for (i = 0; i < sb_count(func->parameter_names); ++i)
	{
		rook_string_free(&func->parameter_names[i]);
		rook_string_free(&func->parameter_types[i]);
	}

	sb_free(func->parameter_names);
	sb_free(func->parameter_types);

	rook_string_free(&func->name);


	func->parameter_count = -1;
	func->delegate = 0;
	func->userdata = 0;
	func->parameter_names = 0;
	func->parameter_types = 0;
}

void rook_cfunc_set_parameters(rook_cfunc* func, rook_cfunc_parameter* parameters, int parameter_count)
{
	int i = 0;

	sb_add(func->parameter_names, parameter_count);
	sb_add(func->parameter_types, parameter_count);

	for (i = 0; i < parameter_count; ++i)
	{
		rook_string_initc(&func->parameter_names[i], parameters[i].name);
		rook_string_initc(&func->parameter_types[i], parameters[i].type);
	}

	func->parameter_count = parameter_count;
}

#ifdef __cplusplus
}
#endif // __cplusplus
