#include "rook_func.h"

#include <utility/rook_malloc.h>
#include <utility/rook_string.h>
#include <utility/stb_sb.h>
#include <utility/rook_serializer.h>

#include <core/compiler/rook_token.h>
#include <core/compiler/rook_callframe.h>

#include "rook_object.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

static void serialize(rook_func* func, rook_serializer* s, rook_error** errors, rook_allocator* a);

void rook_func_init_empty(rook_func* func)
{
	func->refcount = 1;
	
	func->chunk.code = 0;
	func->chunk.constants = 0;
	func->chunk.lines_and_cols = 0;
	func->chunk.local_infos = 0;
	func->parameter_count = 0;
	func->parameter_names = 0;
	func->parameter_types = 0;
	func->name.chars = 0;
	func->name.length = 0;
	func->code.chars = 0;
	func->code.length = 0;
}

void rook_func_init(rook_func* func, rook_token** pn, rook_token** pt, int pc, rook_bytecode_chunk c, rook_cstring name, rook_string code)
{
	func->refcount = 1;

	func->parameter_count = pc;
	if (pc > 0)
	{
		func->parameter_names = ROOK_ALLOC(sizeof(*func->parameter_names) * pc);
		assert(func->parameter_names);
		func->parameter_types = ROOK_ALLOC(sizeof(*func->parameter_types) * pc);
		assert(func->parameter_types);

		for (int i = 0; i < pc; ++i)
		{
			rook_string_initc(&func->parameter_names[i], pn[i]->lexeme);
			if (pt[i])
				rook_string_initc(&func->parameter_types[i], pt[i]->lexeme);
			else
				rook_string_init(&func->parameter_types[i], 0);
		}
	}
	else
	{
		func->parameter_names = 0;
		func->parameter_types = 0;
	}

	func->chunk = c;
	rook_string_initc(&func->name, name);

	func->code = code;
}

void rook_func_free(rook_allocator* a, rook_func* func)
{
	if (func->parameter_count > 0)
	{
		for (rook_string* p = func->parameter_names; p < func->parameter_names + func->parameter_count; ++p)
			rook_string_free(p);
		for (rook_string* p = func->parameter_types; p < func->parameter_types + func->parameter_count; ++p)
			rook_string_free(p);
		
		ROOK_FREE(func->parameter_names);
		ROOK_FREE(func->parameter_types);
	}

	rook_bytecode_chunk_free(&func->chunk, a);

	func->parameter_count = 0;
	func->parameter_names = 0;
	func->parameter_types = 0;

	rook_string_free(&func->name);
	rook_string_free(&func->code);
}

int rook_func_serialize(rook_func* func, rook_string* result, rook_error** errors)
{
	rook_serializer writer = { 0 };
	rook_serializer_init_write(&writer);

	serialize(func, &writer, errors, 0);
	if (sb_count(errors))
	{
		rook_serializer_free_write(&writer);
		return 0;
	}

	*result = rook_serializer_to_str(&writer);
	rook_serializer_free_write(&writer);

	return  1;
}

int rook_func_deserialize(rook_string* func_data, rook_func* result, rook_error** errors, rook_allocator* a)
{
	rook_serializer reader = { 0 };
	rook_serializer_init_read(&reader, func_data->chars, func_data->length);

	serialize(result, &reader, errors, a);

	return sb_count(errors) == 0;
}

static void serialize(rook_func* func, rook_serializer* s, rook_error** errors, rook_allocator* a)
{
	char header[] = "Rook Function: ";
	int header_length = sizeof(header) - 1;

	rook_serialize_ex(s, header, header_length);
	
	rook_serialize(s, &func->name.length);
	if (func->name.length)
	{
		if (rook_serializer_is_reader(s))
			rook_string_allocate(&func->name, func->name.length);
		rook_serialize_ex(s, func->name.chars, func->name.length);
	}

	char line_break = '\n';
	rook_serialize(s, &line_break);

	rook_serialize(s, &func->parameter_count);
	if (rook_serializer_is_reader(s))
	{
		func->parameter_names = 0;
		func->parameter_types = 0;
		if (func->parameter_count > 0)
		{
			func->parameter_names = ROOK_ALLOC(sizeof(*func->parameter_names) * func->parameter_count);
			assert(func->parameter_names);
			func->parameter_types = ROOK_ALLOC(sizeof(*func->parameter_types) * func->parameter_count);
			assert(func->parameter_types);
		}
	}
	
	for (int i = 0; i < func->parameter_count; ++i)
	{
		rook_serialize(s, &func->parameter_names[i].length);
		if (rook_serializer_is_reader(s))
			rook_string_allocate(&func->parameter_names[i], func->parameter_names[i].length);
		rook_serialize_ex(s, func->parameter_names[i].chars, func->parameter_names[i].length);
	}

	for (int i = 0; i < func->parameter_count; ++i)
	{
		rook_serialize(s, &func->parameter_types[i].length);
		if (func->parameter_types[i].length)
		{
			if (rook_serializer_is_reader(s))
				rook_string_allocate(&func->parameter_types[i], func->parameter_types[i].length);
			rook_serialize_ex(s, func->parameter_types[i].chars, func->parameter_types[i].length);
		}
		else
		{
			if (rook_serializer_is_reader(s))
				rook_string_init(&func->parameter_types[i], 0);
		}
	}
	
	char constant_count = (char)sb_count(func->chunk.constants);
	rook_serialize(s, &constant_count);

	if (constant_count)
	{
		if (rook_serializer_is_reader(s))
			sb_add(func->chunk.constants, constant_count);
		
		for (int i = 0; i < constant_count; ++i)
			rook_serialize_obj(s, &func->chunk.constants[i], a);
	}

	char local_info_count = (char)sb_count(func->chunk.local_infos);
	rook_serialize(s, &local_info_count);

	if (local_info_count)
	{
		if (rook_serializer_is_reader(s))
			sb_add(func->chunk.local_infos, local_info_count);
		
		for (int i = 0; i < local_info_count; ++i)
		{
			rook_local_info* local_info = &func->chunk.local_infos[i];
			
			rook_serialize(s, &local_info->name.length);
			if (rook_serializer_is_reader(s))
				rook_string_allocate(&local_info->name, local_info->name.length);
			rook_serialize_ex(s, local_info->name.chars, local_info->name.length);

			rook_serialize(s, &local_info->stack_idx);
			rook_serialize(s, &local_info->pc_start);
			rook_serialize(s, &local_info->pc_end);
		}
	}
	
	int code_length = sb_count(func->chunk.code);
	rook_serialize(s, &code_length);

	if (code_length)
	{
		if (rook_serializer_is_reader(s))
			sb_add(func->chunk.code, code_length);
		
		rook_serialize_ex(s, func->chunk.code, code_length);
	}
	
	int line_and_col_length = sb_count(func->chunk.lines_and_cols);
	rook_serialize(s, &line_and_col_length);

	if (line_and_col_length)
	{
		if (rook_serializer_is_reader(s))
			sb_add(func->chunk.lines_and_cols, line_and_col_length);
		
		rook_serialize_ex(s, func->chunk.lines_and_cols, line_and_col_length);
	}
}

#ifdef __cplusplus
}
#endif // __cplusplus
