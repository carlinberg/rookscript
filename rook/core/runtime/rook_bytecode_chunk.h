#ifndef ROOK_BYTECODE_CHUNK
#define ROOK_BYTECODE_CHUNK

#include <utility/rook_number_types.h>
#include <utility/rook_string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define ROOK_BYTECODE_JUMP_WIDTH 2

typedef struct rook_obj rook_obj;
typedef struct rook_allocator rook_allocator;
typedef struct rook_error rook_error;

typedef enum rook_opcode
{
	rook_opcode_return,
	rook_opcode_load_null,
	rook_opcode_load_constant,
	rook_opcode_load_local,
	rook_opcode_load_captured_value,
	rook_opcode_load_global,
	rook_opcode_pop,
	rook_opcode_save_stack_size,

	rook_opcode_not,
	rook_opcode_negate,
	rook_opcode_preincr,
	rook_opcode_predecr,
	rook_opcode_postincr,
	rook_opcode_postdecr,

	rook_opcode_add,
	rook_opcode_subtract,
	rook_opcode_multiply,
	rook_opcode_divide,
	rook_opcode_modulo,

	rook_opcode_equal,
	rook_opcode_not_equal,
	rook_opcode_less,
	rook_opcode_less_equal,
	rook_opcode_greater,
	rook_opcode_greater_equal,
	
	rook_opcode_jump_if_false,
	rook_opcode_jump_if_true,
	rook_opcode_pop_and_jump_if_false,
	rook_opcode_jump,
	rook_opcode_jump_back,

	rook_opcode_exprstmt_end,
	rook_opcode_leave_first_expr_result,

	rook_opcode_call_prepare,
	rook_opcode_call,

	rook_opcode_declare_array,
	rook_opcode_declare_map,
	rook_opcode_declare_map_value_end,
	rook_opcode_declare_func,
	rook_opcode_declare_global,
	rook_opcode_declare_locals,

	rook_opcode_assign_start,
	rook_opcode_assign_local,
	rook_opcode_assign_capture,
	rook_opcode_assign_global,

	rook_opcode_subscr_get,
	rook_opcode_subscr_set,
	rook_opcode_property_get,
	rook_opcode_property_set,
	rook_opcode_set_comp_assign,

	rook_opcode_foreach_setup,
	rook_opcode_foreach_do,
} rook_opcode;

typedef struct rook_line_and_col
{
	int line, col;
} rook_line_and_col;

typedef struct rook_local_info
{
	rook_string name;
	int stack_idx;
	int pc_start;
	int pc_end;
} rook_local_info;

typedef	struct rook_bytecode_chunk
{
	u8* code;
	rook_obj* constants;
	rook_line_and_col* lines_and_cols;
	rook_local_info* local_infos;
} rook_bytecode_chunk;

void rook_bytecode_chunk_init(rook_bytecode_chunk* c);
void rook_bytecode_chunk_free(rook_bytecode_chunk* c, rook_allocator* a);
void rook_bytecode_chunk_write(rook_bytecode_chunk* c, u8 byte, int line, int column);
int rook_bytecode_chunk_add_constant(rook_bytecode_chunk* c, rook_obj constant, rook_allocator* a);
int rook_bytecode_chunk_add_and_write_constant(rook_bytecode_chunk* c, rook_obj constant, rook_allocator* a, int line, int column, rook_error** errors, rook_cstring file, rook_cstring code);
void rook_bytecode_chunk_dissasemble(rook_bytecode_chunk* c, rook_string* output);

void rook_bytecode_chunk_add_local_info(rook_bytecode_chunk* c, rook_string name, int s_idx, int pcs);
void rook_bytecode_chunk_close_local_infos(rook_bytecode_chunk* c, int s_idx, int pce);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_BYTECODE_CHUNK
