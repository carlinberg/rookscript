#ifndef ROOK_OBJ_STRING
#define ROOK_OBJ_STRING

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_obj_string
{
	int refcount;

	char* chars;
	int length;
	int capacity;
} rook_obj_string;

void rook_obj_string_init(rook_obj_string* str);
void rook_obj_string_init_ex(rook_obj_string* str, const char* chars, int length);
void rook_obj_string_allocate(rook_obj_string* str, int length);
void rook_obj_string_free(rook_obj_string* str);

void rook_obj_string_append(rook_obj_string* left, rook_obj_string* right);
int rook_obj_string_insert(rook_obj_string* str, int where, rook_obj_string* value);
void rook_obj_string_clone(rook_obj_string* in, rook_obj_string* out);
int rook_obj_string_equals(rook_obj_string* left, rook_obj_string* right);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_OBJ_STRING
