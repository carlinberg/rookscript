#ifndef ROOK_CALL_INFO
#define ROOK_CALL_INFO

#include <utility/rook_string.h>

typedef struct rook_call_info
{
	rook_cstring file;
	rook_cstring function;
	int call_site_line;
	struct rook_call_info* previous;
} rook_call_info;

#ifdef ROOK_DEBUG
#define add_call_info(name, fil, func, line, prev, i)		\
rook_call_info name;										\
name.file = fil;											\
name.function = func;										\
name.call_site_line = line;									\
name.previous = prev;										\
i->current_call_info = &name
#define pop_call_info(i) (i)->current_call_info = (i)->current_call_info->previous
#else
#define add_call_info(name, file, func, prev, i)
#define pop_call_info(i)
#endif

#endif // !ROOK_CALL_INFO
