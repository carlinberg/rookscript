#include "rook_env.h"
#include <utility/rook_string.h>
#include <utility/rook_error.h>
#include <utility/stb_sb.h>

#include "rook_vm.h"

#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define add_error(e, m) rook_vm_runtime_error((e)->vm, rook_cstring_from_literal(m))

void rook_env_init(rook_env* env, rook_allocator* runtime_allocator, rook_vm* vm)
{
	memset(env->stack, 0, sizeof(*env->stack) * ROOK_MAX_STACK);
	env->stack_end = env->stack;

	rook_map_init(&env->globals);

	env->runtime_allocator = runtime_allocator;
	env->vm = vm;
}

void rook_env_free(rook_env* env)
{
	rook_obj* it;
	for (it = env->stack; it < env->stack_end; ++it)
		rook_obj_free(env->runtime_allocator, it);

	memset(env->stack, 0, sizeof(*env->stack) * ROOK_MAX_STACK);
	env->stack_end = env->stack;

	rook_map_free(&env->globals, env->runtime_allocator);

	env->runtime_allocator = 0;
	env->vm = 0;
}

rook_obj* rook_env_stack_get(rook_env* env, int idx)
{
	if (idx < 0)
	{
		if (env->stack_end + idx < env->stack)
			return 0;

		return env->stack_end + idx;
	}
	else
	{
		if (env->stack + idx >= env->stack_end)
			return 0;

		return env->stack + idx;
	}
}

rook_obj* rook_env_stack_top(rook_env* env)
{
	if (env->stack_end > env->stack)
		return env->stack_end - 1;
	return 0;
}

int rook_env_stack_size(rook_env* env)
{
	return (int)(env->stack_end - env->stack);
}

void rook_env_register_global(rook_env* env, rook_cstring name, rook_obj obj)
{
	rook_map_set(&env->globals, name.chars, name.length, obj, env->runtime_allocator);
}

int rook_env_get_global(rook_env* env, rook_cstring name, rook_obj* obj)
{
	return rook_map_get(&env->globals, name.chars, name.length, obj);
}

int rook_env_set_global(rook_env* env, rook_cstring name, rook_obj obj)
{
	return rook_map_tryset(&env->globals, name.chars, name.length, obj, env->runtime_allocator);
}

int rook_env_push_obj(rook_env* env, rook_obj obj)
{
	if (env->stack_end - env->stack < ROOK_MAX_STACK)
	{
		*env->stack_end++ = obj;
		return 1;
	}

	add_error(env, "Stack overflow");
	return 0;
}

rook_obj* rook_env_push_empty(rook_env* env)
{
	if (env->stack_end - env->stack < ROOK_MAX_STACK)
	{
		return env->stack_end++;
	}

	add_error(env, "Stack overflow");
	return 0;
}

int rook_env_push_null(rook_env* env)
{
	if (env->stack_end - env->stack < ROOK_MAX_STACK)
	{
		env->stack_end->type_and_tag = 0;
		++env->stack_end;
		return 1;
	}

	add_error(env, "Stack overflow");
	return 0;
}

int rook_env_push_many_null(rook_env* env, int how_many)
{
	int i;

	if (env->stack_end - env->stack + how_many < ROOK_MAX_STACK)
	{
		for (i = 0; i < how_many; ++i)
		{
			env->stack_end->type_and_tag = 0;
			++env->stack_end;
		}
		return 1;
	}

	add_error(env, "Stack overflow");
	return 0;
}

int rook_env_pop(rook_env* env)
{
	if (env->stack_end > env->stack)
	{
		rook_obj_free(env->runtime_allocator, --env->stack_end);
		return 1;
	}

	add_error(env, "Stack underflow");
	return 0;
}

int rook_env_pop_many(rook_env* env, int how_many)
{
	if (env->stack_end - how_many >= env->stack)
	{
		for (int i = 0; i < how_many; ++i)
		{
			rook_obj_free(env->runtime_allocator, --env->stack_end);
		}

		return 1;
	}

	add_error(env, "Stack underflow");
	return 0;
}

int rook_env_shrink_stack(rook_env* env, int how_many)
{
	if (env->stack_end - how_many >= env->stack)
	{
#ifndef NDEBUG

		for (int i = 0; i < how_many; ++i)
		{
			*rook_env_stack_get(env, -1 - i) = rook_obj_create_null();
		}

#endif // !NDEBUG

		env->stack_end -= how_many;
		return 1;
	}

	add_error(env, "Stack underflow");
	return 0;
}

void rook_env_push_result_impl(rook_obj** results, rook_obj object)
{
	sb_push(*results, object);
}

void rook_env_free_result_array(rook_obj* results)
{
	sb_free(results);
}

#ifdef __cplusplus
}
#endif // __cplusplus
