#ifndef ROOK_CLOSURE
#define ROOK_CLOSURE

#include "rook_object.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_func rook_func;
typedef struct rook_allocator rook_allocator;

typedef struct rook_closure
{
	int refcount;
	rook_obj func;
	rook_obj* captured_values;
} rook_closure;

void rook_closure_init(rook_closure* c, rook_obj func, rook_obj* captured_values);
void rook_closure_free(rook_closure* c, rook_allocator* a);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_CLOSURE
