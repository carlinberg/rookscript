#include "rook_array.h"
#include <utility/rook_malloc.h>
#include "rook_object.h"


#include <stdlib.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

static int round_to_8(int number)
{
	if (number <= 8)
		return 8;

	int mod = number % 8;
	if (mod)
		return number + (8 - mod);

	return number;
}

void rook_array_init(rook_array* array, int capacity)
{
	assert(capacity >= 0);

	array->refcount = 1;

	if (capacity > 0)
		array->array = ROOK_ALLOC(capacity * sizeof(*array->array));
	else
		array->array = 0;

	array->size = 0;
	array->capacity = capacity;
}

void rook_array_free(rook_array* array, rook_allocator* a)
{
	for (int i = 0; i < array->size; ++i)
	{
		rook_obj_free(a, &array->array[i]);
	}

	ROOK_FREE(array->array);
	array->array = 0;
	array->size = 0;
	array->capacity = 0;
}

void rook_array_push(rook_array* array, rook_obj element)
{
	if (array->array == 0)
	{
		assert(array->capacity == 0);
		array->capacity = 8;
		array->array = ROOK_ALLOC(array->capacity * sizeof(*array->array));
		assert(array->array);
	}
	else if (array->size == array->capacity)
	{
		array->capacity = array->capacity < 8 ? 8 : round_to_8(array->capacity * 2);
		void* new_array = ROOK_REALLOC(array->array, array->capacity * sizeof(*array->array));
		assert(new_array);
		array->array = new_array;
	}

	array->array[array->size++] = element;
}

void rook_array_push_many(rook_array* array, rook_obj* elements, int count)
{
	if (array->array == 0)
	{
		assert(array->capacity == 0);
		array->capacity = count;
		array->array = ROOK_ALLOC(array->capacity * sizeof(*array->array));
		assert(array->array);
	}

	if (array->size + count > array->capacity)
	{
		int min_capacity = round_to_8(array->size + count);
		array->capacity = min_capacity < array->capacity * 2 ? array->capacity * 2 : min_capacity;
		void* new_array = ROOK_REALLOC(array->array, array->capacity * sizeof(*array->array));
		assert(new_array);
		array->array = new_array;
	}

	for (rook_obj* e = elements; e < elements + count; ++e)
	{
		array->array[array->size++] = *e;
	}
}

void rook_array_fill(rook_array* array, rook_obj element, int count)
{
	if (array->array == 0)
	{
		assert(array->capacity == 0);
		array->capacity = count;
		array->array = ROOK_ALLOC(array->capacity * sizeof(*array->array));
		assert(array->array);
	}

	if (array->size + count > array->capacity)
	{
		int min_capacity = round_to_8(array->size + count);
		array->capacity = min_capacity < array->capacity * 2 ? array->capacity * 2 : min_capacity;
		void* new_array = ROOK_REALLOC(array->array, array->capacity * sizeof(*array->array));
		assert(new_array);
		array->array = new_array;
	}

	for (int i = 0; i < count; ++i)
	{
		rook_obj_incref(&element);
		array->array[array->size++] = element;
	}
}

void rook_array_insert(rook_array* array, int index, rook_obj* element)
{
	if (array->array == 0)
	{
		assert(index == 0);
		assert(array->capacity == 0);
		array->capacity = 8;
		array->array = ROOK_ALLOC(array->capacity * sizeof(*array->array));
		assert(array->array);

		*array->array = *element;
		array->size = 1;
		return;
	}

	if (array->size == array->capacity)
	{
		array->capacity = array->capacity < 8 ? 8 : round_to_8(array->capacity * 2);
		void* new_array = ROOK_REALLOC(array->array, array->capacity);
		assert(new_array);
		array->array = new_array;
	}

	for (int i = array->size; i > index; --i)
	{
		array->array[i] = array->array[i - 1];
	}

	array->array[index] = *element;
	++array->size;
}

void rook_array_erase(rook_array* array, int index, rook_allocator* a)
{
	assert(array->array);
	assert(array->size);
	assert(index >= 0 && index < array->size);

	rook_obj_free(a, array->array + index);

	for (int i = index; i < array->size - 1; ++i)
	{
		array->array[i] = array->array[i + 1];
	}
	--array->size;
}

void rook_array_clear(rook_array* array, rook_allocator* a)
{
	for (int i = 0; i < array->size; ++i)
	{
		rook_obj_free(a, &array->array[i]);
	}

	array->size = 0;
}

static int partition(rook_obj* A, int p, int q, int(*comparer)(void*, rook_obj*, rook_obj*), void* compare_state)
{
	int i = p;
	int j;

	rook_obj temp;

	for (j = p + 1; j < q; j++)
	{
		if (comparer(compare_state, &A[p], &A[j]))
		{
			i = i + 1;
			temp = A[i];
			A[i] = A[j];
			A[j] = temp;
		}
	}

	temp = A[i];
	A[i] = A[p];
	A[p] = temp;

	return i;
}

static void quicksort(rook_obj* a, int start, int end, int(*comparer)(void*, rook_obj*, rook_obj*), void* compare_state)
{
	int r;
	if (start < end)
	{
		r = partition(a, start, end, comparer, compare_state);
		quicksort(a, start, r, comparer, compare_state);
		quicksort(a, r + 1, end, comparer, compare_state);
	}
}

void rook_array_sort(rook_array* a, int(*comparer)(void*, rook_obj*, rook_obj*), void* compare_state)
{
	quicksort(a->array, 0, a->size, comparer, compare_state);
}

#ifdef __cplusplus
}
#endif // __cplusplus
