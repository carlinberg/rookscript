#include "rook_map.h"
#include <utility/rook_malloc.h>
#include <utility/rook_number_types.h>
#include <utility/rook_string.h>

#include "rook_object.h"


#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

static u32 rook_hash_fnv1a(const char* str, int length)
{
	u32 hash = 2166136261;

	for (const char* end = str + length; str < end; ++str)
	{
		hash ^= *str;
		hash *= 16777619;
	}

	return hash;
}

typedef struct rook_map_entry
{
	const char* key;
	int key_length;
	rook_obj value;
} rook_map_entry;

static rook_map_entry* rook_map_find_entry(rook_map_entry* entries, int capacity, const char* key, int key_length);
static void rook_map_grow(rook_map* map, int capacity);

void rook_map_init(rook_map* map)
{
	map->refcount = 1;
	map->entries = 0;
	map->size = 0;
	map->capacity = 0;
}

void rook_map_init_ex(rook_map* map, int capacity)
{
	rook_map_init(map);
	rook_map_grow(map, capacity);
}

void rook_map_free(rook_map* map, rook_allocator* a)
{
	for (int i = 0; i < map->capacity; ++i)
	{
		rook_map_entry* entry = map->entries + i;
		if (entry->key == 0 || entry->key == (void*)1)
			continue;

		ROOK_FREE((char*)entry->key);
		rook_obj_free(a, &entry->value);
	}

	ROOK_FREE(map->entries);
	map->entries = 0;
	map->size = 0;
	map->capacity = 0;
}

int rook_map_set(rook_map* map, const char* key, int key_length, rook_obj value, rook_allocator* a)
{
	assert(key_length);

	if (map->size * 4 + 1 > map->capacity * 3)
	{
		int capacity = map->capacity < 8 ? 8 : map->capacity * 2;
		rook_map_grow(map, capacity);
	}

	rook_map_entry* entry = rook_map_find_entry(map->entries, map->capacity, key, key_length);

	int is_empty = entry->key == 0;
	int is_tombstone = entry->key == (void*)1;

	if (is_empty)
	{
		++map->size;
	}
	else if (!is_tombstone)
	{
		rook_obj_free(a, &entry->value);
	}

	if (is_empty || is_tombstone)
	{
		char* new_key = ROOK_ALLOC((size_t)key_length + 1);
		assert(new_key);
		memcpy(new_key, key, key_length);
		new_key[key_length] = '\0';
		entry->key = new_key;
		entry->key_length = key_length;
	}

	entry->value = value;

	return is_empty || is_tombstone;
}

int rook_map_tryset(rook_map* map, const char* key, int key_length, rook_obj value, rook_allocator* a)
{
	rook_map_entry* entry = rook_map_find_entry(map->entries, map->capacity, key, key_length);

	if (entry->key == 0 || entry->key == (void*)1)
		return 0;

	rook_obj_free(a, &entry->value);
	entry->value = value;

	return 1;
}

int rook_map_get_shallow(rook_map* map, const char* key, int key_length, rook_obj* value)
{
	if (map->entries == 0)
		return 0;

	rook_map_entry* entry = rook_map_find_entry(map->entries, map->capacity, key, key_length);
	if (entry->key == 0 || entry->key == (const char*)1)
		return 0;

	if (value)
		*value = entry->value;

	return 1;
}

int rook_map_get(rook_map* map, const char* key, int key_length, rook_obj* value)
{
	rook_obj base = { 0 };
	rook_cstring base_name = rook_cstring_from_literal("base");

	while (1)
	{
		if (rook_map_get_shallow(map, key, key_length, value))
			return 1;

		if (!rook_map_get_shallow(map, base_name.chars, base_name.length, &base) || !rook_obj_ismap(base))
			break;
		
		map = base.as.map;
	}
	
	return 0;
}

void rook_map_erase(rook_map* map, const char* key, int key_length, rook_allocator* a)
{
	if (map->entries == 0 || map->capacity == 0)
		return;

	rook_map_entry* entry = rook_map_find_entry(map->entries, map->capacity, key, key_length);
	if (entry->key == 0 || entry->key == (void*)1)
		return;

	ROOK_FREE((char*)entry->key);
	rook_obj_free(a, &entry->value);

	entry->key = (void*)1;
}

rook_map_entry* rook_map_find_entry(rook_map_entry* entries, int capacity, const char* key, int key_length)
{
	u32 hash = rook_hash_fnv1a(key, key_length);
	u32 index = hash % capacity;

	rook_map_entry* tombstone = 0;

	while (1)
	{
		rook_map_entry* entry = entries + index;

		if (entry->key == 0)
			return tombstone ? tombstone : entry;
		else if (entry->key == (void*)1)
			tombstone = tombstone ? tombstone : entry;
		else if (strncmp(entry->key, key, key_length) == 0)
			return entry;

		if (++index == (u32)capacity)
			index = 0;
	}
}

void rook_map_grow(rook_map* map, int capacity)
{
	rook_map_entry* entries = ROOK_ALLOC(sizeof(*entries) * capacity);
	assert(entries);
	memset(entries, 0, sizeof(*entries) * capacity);

	map->size = 0;

	for (int i = 0; i < map->capacity; ++i)
	{
		rook_map_entry* old_entry = map->entries + i;
		if (old_entry->key == 0 || old_entry->key == (void*)1)
			continue;

		rook_map_entry* new_entry = rook_map_find_entry(entries, capacity, old_entry->key, old_entry->key_length);

		new_entry->key = old_entry->key;
		new_entry->key_length = old_entry->key_length;
		new_entry->value = old_entry->value;

		++map->size;
	}

	ROOK_FREE(map->entries);

	map->entries = entries;
	map->capacity = capacity;
}

void rook_map_clear(rook_map* map, rook_allocator* a)
{
	for (int i = 0; i < map->capacity; ++i)
	{
		rook_map_entry* entry = map->entries + i;
		if (entry->key == 0 || entry->key == (void*)1)
			continue;

		ROOK_FREE((char*)entry->key);
		rook_obj_free(a, &entry->value);
	}

	memset(map->entries, 0, sizeof(*map->entries) * map->capacity);
	map->size = 0;
}

int rook_map_entry_exists(rook_map* map, int raw_index)
{
	const void* key = map->entries[raw_index].key;
	return key != 0 && key != (void*)1;
}

rook_cstring rook_map_get_key(rook_map* map, int raw_index)
{
	assert(raw_index >= 0 && raw_index < map->capacity);
	rook_cstring key;
	rook_cstring_init(&key, map->entries[raw_index].key, map->entries[raw_index].key_length);
	return key;
}

rook_obj rook_map_get_value(rook_map* map, int raw_index)
{
	assert(raw_index >= 0 && raw_index < map->capacity);
	return map->entries[raw_index].value;
}

#ifdef __cplusplus
}
#endif // __cplusplus
