#include "rook_vm.h"

#include <utility/rook_malloc.h>
#include <utility/rook_file.h>
#include <utility/rook_string.h>
#include <utility/stb_sb.h>

#include <core/compiler/rook_lexer.h>
#include <core/compiler/rook_variable_resolver.h>

#include "rook_func.h"
#include "rook_closure.h"
#include "rook_bound_func.h"


#include <assert.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define SET_CURRENT_CODE(v, n, c) do { (v)->current_file = (n); (v)->current_code = (c); } while (0)
#define SET_CURRENT_CODEs(v, n, c) do { (v)->current_file = rook_cstring_from_string(n); (v)->current_code = rook_cstring_from_string(c); } while (0)
#define CLEAR_CURRENT_CODE(v) do { rook_cstring_init(&vm->current_file, 0, 0); rook_cstring_init(&vm->current_code, 0, 0); } while (0)

static int read_file(rook_string* directories, rook_cstring path, rook_string* content)
{
	int i, result;
	rook_string full_path;

	if (rook_read_file(path, content))
		return 1;

	for (i = 0; i < sb_count(directories); ++i)
	{
		full_path.length = directories[i].length + path.length;
		full_path.chars = ROOK_ALLOC(full_path.length + 1);
		
		memcpy(full_path.chars, directories[i].chars, directories[i].length);
		memcpy(full_path.chars + directories[i].length, path.chars, path.length);

		full_path.chars[full_path.length] = '\0';

		result = rook_read_file(rook_cstring_from_string(full_path), content);

		rook_string_free(&full_path);

		if (result)
			return result;
	}

	return 0;
}

static rook_bytecode_chunk compile_chunk(rook_vm* vm, rook_cstring file, rook_cstring code)
{
	int success = 0;
	rook_parser parser;
	rook_bytecode_generator generator;
	rook_bytecode_chunk chunk;

	rook_parser_init(&parser, file, code, &vm->errors);
	rook_bytecode_generator_init(&generator, &vm->runtime_allocator, file, code, &vm->errors, 0);
	rook_bytecode_chunk_init(&chunk);

	if (rook_lex(code.chars, &parser))
	{
		if (rook_parse(&parser))
		{
			if (rook_variable_resolver_run(&parser))
			{
				if (rook_bytecode_generator_run(&generator, parser.stmts, sb_count(parser.stmts), &chunk))
				{
					success = 1;
				}
				else
				{
					rook_errorlist_add_literal(&vm->errors, "Bytecode generation error", file, code, 0, 0);
				}
			}
			else
			{
				rook_errorlist_add_literal(&vm->errors, "Variable resolving error", file, code, 0, 0);
			}
		}
		else
		{
			rook_errorlist_add_literal(&vm->errors, "Parse error", file, code, 0, 0);
		}
	}
	else
	{
		rook_errorlist_add_literal(&vm->errors, "Lex error", file, code, 0, 0);
	}

	rook_bytecode_generator_free(&generator);
	rook_parser_free(&parser);

	if (!success)
		rook_bytecode_chunk_free(&chunk, &vm->runtime_allocator);

	return chunk;
}

void rook_vm_init(rook_vm* vm)
{
	vm->errors = 0;
	vm->directories = 0;

	rook_allocator_init(&vm->runtime_allocator);
	rook_bytecode_interpreter_init(&vm->interpreter, &vm->runtime_allocator, &vm->errors, vm);

	CLEAR_CURRENT_CODE(vm);
}

void rook_vm_free(rook_vm* vm)
{
	int i;

	rook_bytecode_interpreter_free(&vm->interpreter);
	rook_allocator_free(&vm->runtime_allocator);

	for (i = 0; i < sb_count(vm->directories); ++i)
		rook_string_free(&vm->directories[i]);

	sb_free(vm->directories);
	vm->directories = 0;

	for (i = 0; i < sb_count(vm->errors); ++i)
		rook_error_free(&vm->errors[i]);

	sb_free(vm->errors);
	vm->errors = 0;

	CLEAR_CURRENT_CODE(vm);
}

void rook_vm_add_directory(rook_vm* vm, rook_cstring directory)
{
	rook_string d;
	rook_string_initc(&d, directory);
	sb_push(vm->directories, d);
}

void rook_vm_runtime_error(rook_vm* vm, rook_cstring msg)
{
	rook_string error;
	rook_string_initc(&error, msg);
	rook_vm_runtime_error_ex(vm, error);
}

void rook_vm_runtime_error_ex(rook_vm* vm, rook_string msg)
{
	rook_error error;
	error.msg = msg;
	error.line = vm->interpreter.current_line;
	error.column = vm->interpreter.current_column;
	rook_string_initc(&error.file, vm->current_file);
	rook_string_initc(&error.code, vm->current_code);
	sb_push(vm->errors, error);
}

void rook_vm_clear_errors(rook_vm* vm)
{
	if (vm->errors)
	{
		for (int i = 0; i < sb_count(vm->errors); ++i)
			rook_error_free(&vm->errors[i]);

		stb__sbn(vm->errors) = 0;
	}
}

rook_local_watch* rook_vm_getlocals(rook_vm* vm)
{
	rook_bytecode_chunk* chunk = sb_last(vm->interpreter.chunk_stack);
	int ip_number = (int)(uptr)(vm->interpreter.ip - chunk->code);
	int stack_base = sb_last(vm->interpreter.callframe_stack_base_stack);
	rook_local_watch* result = 0;
	rook_local_watch r;
	rook_local_info info;
	int i;

	for (i = 0; i < sb_count(chunk->local_infos); ++i)
	{
		info = chunk->local_infos[i];

		if (info.pc_start <= ip_number && info.pc_end >= ip_number)
		{
			r.name = rook_cstring_from_string(info.name);
			r.stack_idx = info.stack_idx + stack_base;
			sb_push(result, r);
		}
	}

	return result;
}

rook_obj rook_vm_compile_string(rook_vm* vm, rook_string code, rook_cstring name)
{
	rook_bytecode_chunk chunk = compile_chunk(vm, name, rook_cstring_from_string(code));
	if (chunk.code == 0)
		return rook_vm_create_null();

	rook_obj program = rook_obj_create(&vm->runtime_allocator, rook_obj_type_func);
	rook_func_init(program.as.func, 0, 0, 0, chunk, name, code);
	return program;
}

rook_obj rook_vm_compile_file(rook_vm* vm, rook_cstring path)
{
	rook_string content;
	if (!read_file(vm->directories, path, &content))
	{
		rook_errorlist_add_errorf(&vm->errors, path, rook_cstring_from_string(content), 0, 0, "Compile error file not found: %S", path);
		return rook_vm_create_null();
	}

	return rook_vm_compile_string(vm, content, path);
}

int rook_vm_run_string_args(rook_vm* vm, rook_cstring code, rook_cstring name, int argc, rook_obj* args)
{
	int i, res;
	rook_bytecode_chunk chunk = compile_chunk(vm, name, code);
	if (chunk.code == 0)
		return -1;

	if (!rook_env_push_null(&vm->interpreter.env))
		return -1;

	for (i = 0; i < argc; ++i)
	{
		if (!rook_env_push_obj(&vm->interpreter.env, args[i]))
			return -1;
	}

	SET_CURRENT_CODE(vm, name, code);	
	res = rook_bytecode_interpreter_run(vm, &vm->interpreter, &chunk, argc + 1);
	CLEAR_CURRENT_CODE(vm);

	rook_bytecode_chunk_free(&chunk, &vm->runtime_allocator);
	return res;
}

static rook_obj* as_closure(rook_obj* obj)
{
	while (1)
	{
		if (rook_obj_isclosure(*obj))
			return obj;
		
		if (rook_obj_isbound_func(*obj))
			obj = &obj->as.bound_func->func;
		else
			return 0;
	}
}

static rook_obj* as_prototype(rook_obj* obj)
{
	while (1)
	{
		if (rook_obj_isfunc(*obj) || rook_obj_isclosure(*obj) || rook_obj_iscfunc(*obj))
			return obj;
		
		if (rook_obj_isbound_func(*obj))
			obj = &obj->as.bound_func->func;
		else
			return 0;
	}
}

int rook_vm_run_obj_args(rook_vm* vm, rook_obj obj, int argc, rook_obj* args)
{
	rook_obj* closure = as_closure(&obj);
	rook_obj* prototype;
	rook_obj* rets = 0;
	rook_obj this_;
	rook_env* env = &vm->interpreter.env;
	int retc, i;

	if (closure)
	{
		if (!rook_env_push_obj(env, *closure))
			return -1;

		rook_obj_incref(closure);
	}

	if (rook_obj_isbound_func(obj))
	{
		this_ = obj.as.bound_func->this_;

		if (!rook_env_push_obj(env, this_))
			return -1;

		rook_obj_incref(&this_);
	}
	else
	{
		this_ = rook_obj_create_null();

		if (!rook_env_push_obj(env, this_))
			return -1;
	}

	if (!rook_obj_iscfunc(obj))
	{
		for (i = 0; i < argc; ++i)
		{
			if (!rook_env_push_obj(env, args[i]))
				return -1;
		}
	}

	prototype = as_prototype(&obj);
	if (!prototype)
	{
		rook_vm_runtime_errorf(vm, "Tried calling object of type %s, which is not callable.", rook_obj_type_names[rook_obj_get_type(obj)]);
		return -1;
	}

	switch (rook_obj_get_type(*prototype))
	{
	case rook_obj_type_func:
		if (prototype->as.func->parameter_count != -1 && prototype->as.func->parameter_count != argc)
		{
			rook_vm_runtime_errorf(vm, "Function '%s' expects %d arguments, got %d", prototype->as.func->name, prototype->as.func->parameter_count, argc);
			return -1;
		}

		SET_CURRENT_CODEs(vm, prototype->as.func->name, prototype->as.func->code);
		retc = rook_bytecode_interpreter_run(vm, &vm->interpreter, &prototype->as.func->chunk, argc + 1);
		CLEAR_CURRENT_CODE(vm);
		break;
	case rook_obj_type_closure:
		if (prototype->as.closure->func.as.func->parameter_count != -1 && prototype->as.closure->func.as.func->parameter_count != argc)
		{
			rook_vm_runtime_errorf(vm, "Function '%s' expects %d arguments, got %d", prototype->as.closure->func.as.func->name, prototype->as.closure->func.as.func->parameter_count, argc);
			return -1;
		}

		SET_CURRENT_CODEs(vm, prototype->as.closure->func.as.func->name, prototype->as.closure->func.as.func->code);
		retc = rook_bytecode_interpreter_run(vm, &vm->interpreter, &prototype->as.closure->func.as.func->chunk, argc + 1);
		CLEAR_CURRENT_CODE(vm);
		break;
	case rook_obj_type_cfunc:
		{
			if (prototype->as.cfunc->parameter_count != -1 && prototype->as.cfunc->parameter_count != argc)
			{
				rook_vm_runtime_errorf(vm, "Function '%s' expects %d arguments, got %d", prototype->as.cfunc->name, prototype->as.cfunc->parameter_count, argc);
				return -1;
			}

			retc = prototype->as.cfunc->delegate(vm, prototype->as.cfunc->userdata, &this_, args, argc, &rets);
			if (retc == -1)
				return -1;

			if (closure && !rook_env_pop(env))
				return -1;

			for (i = 0; i < retc; ++i)
				rook_env_push_obj(env, rets[i]);

			return retc;
		}
	default:
		assert(0 && "Shouldn't happen");
		retc = -1;
		break;
	}

	if (closure && retc != -1)
	{
		if (retc > 0)
		{
			for (i = 0; i < retc; ++i)
				sb_push(rets, rook_vm_stack_get(vm, retc - i - 1));

			rook_env_shrink_stack(env, retc);
		}
		
		rook_env_pop(env);

		if (retc > 0)
		{
			for (i = 0; i < retc; ++i)
				rook_env_push_obj(env, rets[i]);

			sb_free(rets);
		}
	}
	
	return retc;
}

int rook_vm_run_file_args(rook_vm* vm, rook_cstring path, int argc, rook_obj* args)
{
	rook_string content;
	if (!read_file(vm->directories, path, &content))
	{
		rook_errorlist_add_errorf(&vm->errors, path, vm->current_code, 0, 0, "Compile error file not found: %S", path);
		return -1;
	}

	int res = rook_vm_run_string_args(vm, rook_cstring_from_string(content), path, argc, args);
	rook_string_free(&content);
	return res;
}

void rook_vm_stack_push(rook_vm* vm, rook_obj obj)
{
	rook_env_push_obj(&vm->interpreter.env, obj);
}

rook_obj rook_vm_stack_get(rook_vm* vm, int idx)
{
	rook_obj* obj = rook_env_stack_get(&vm->interpreter.env, idx);
	if (obj)
		return *obj;
	return rook_vm_create_null();
}

int rook_vm_stack_size(rook_vm* vm)
{
	return rook_env_stack_size(&vm->interpreter.env);
}

void rook_vm_stack_pop(rook_vm* vm, int how_many)
{
	rook_env_pop_many(&vm->interpreter.env, how_many);
}

void rook_vm_register_global(rook_vm* vm, rook_cstring name, rook_obj obj)
{
	rook_env_register_global(&vm->interpreter.env, name, obj);
}

int rook_vm_get_global(rook_vm* vm, rook_cstring name, rook_obj* obj)
{
	return rook_env_get_global(&vm->interpreter.env, name, obj);
}

int rook_vm_set_global(rook_vm* vm, rook_cstring name, rook_obj obj)
{
	return rook_env_set_global(&vm->interpreter.env, name, obj);
}

void rook_vm_register_extension(rook_vm* vm, rook_obj_type type, rook_cstring name, rook_delegate_type func)
{
	rook_obj* map = &vm->interpreter.extension_funcs[type];
	rook_obj func_obj = rook_obj_create_cfunc(&vm->runtime_allocator, func, name);
	rook_map_set(map->as.map, name.chars, name.length, func_obj, &vm->runtime_allocator);
}

int rook_vm_register_tag(rook_vm* vm, rook_cstring name)
{
	rook_obj new_map = rook_vm_create_map(vm, 0);
	int id = sb_count(vm->interpreter.tag_names);
	sb_push(vm->interpreter.tag_names, rook_string_from_cstring(name));
	sb_push(vm->interpreter.tag_extension_funcs, new_map);
	return id;
}

int rook_vm_register_tag_extension(rook_vm* vm, int tag, rook_cstring func_name, rook_obj func)
{
	if (tag < 0 || tag >= sb_count(vm->interpreter.tag_names))
	{
		rook_errorlist_add_errorf(&vm->errors, vm->current_file, vm->current_code, 0, 0, "Invalid tag (%d) passed to %s", tag, __FUNCTION__);
		return 0;
	}

	rook_map_set(vm->interpreter.tag_extension_funcs[tag].as.map, func_name.chars, func_name.length, func, &vm->runtime_allocator);
	return 1;
}

rook_obj rook_vm_create_null(void)
{
	return rook_obj_create_null();
}

rook_obj rook_vm_create_boolean(int b)
{
	return rook_obj_create_boolean(b);
}

rook_obj rook_vm_create_number(rook_float n)
{
	return rook_obj_create_number(n);
}

rook_obj rook_vm_create_userdata(void* u)
{
	return rook_obj_create_userdata(u);
}

rook_obj rook_vm_create_string(rook_vm* vm, rook_cstring s)
{
	return rook_obj_create_string(&vm->runtime_allocator, s);
}

rook_obj rook_vm_create_empty_string(rook_vm* vm)
{
	return rook_obj_create_string(&vm->runtime_allocator, rook_cstring_from_literal(""));
}

rook_obj rook_vm_create_array(rook_vm* vm, int capacity)
{
	return rook_obj_create_array(&vm->runtime_allocator, capacity);
}

rook_obj rook_vm_create_map(rook_vm* vm, int capacity)
{
	return rook_obj_create_map(&vm->runtime_allocator, capacity);
}

rook_obj rook_vm_create_cfunc(rook_vm* vm, rook_delegate_type func, rook_cstring name)
{
	return rook_obj_create_cfunc(&vm->runtime_allocator, func, name);
}

void rook_vm_free_obj(rook_vm* vm, rook_obj* obj)
{
	rook_obj_free(&vm->runtime_allocator, obj);
}

#ifdef __cplusplus
}
#endif // __cplusplus
