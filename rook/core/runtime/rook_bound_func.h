#ifndef ROOK_BOUND_FUNC
#define ROOK_BOUND_FUNC

#include "rook_object.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_bound_func
{
	int refcount;
	rook_obj func;
	rook_obj this_;
} rook_bound_func;

void rook_bound_func_init(rook_bound_func* bf, rook_obj f, rook_obj t);
void rook_bound_func_free(rook_bound_func* bf, rook_allocator* a);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_BOUND_FUNC
