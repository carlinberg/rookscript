#include "rook_object.h"

#include <utility/rook_string.h>
#include <utility/rook_malloc.h>
#include <utility/rook_allocators.h>

#include "rook_obj_string.h"
#include "rook_array.h"
#include "rook_map.h"
#include "rook_func.h"
#include "rook_cfunc.h"
#include "rook_closure.h"
#include "rook_bound_func.h"


#include <assert.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

rook_obj rook_obj_create(rook_allocator* a, rook_obj_type t)
{
	rook_obj obj = { 0 };
	obj.type_and_tag = t;

	switch (t)
	{
	case rook_obj_type_null:
		break;
	case rook_obj_type_boolean:
		obj.as.boolean = 0;
		break;
	case rook_obj_type_number:
		obj.as.number = 0;
		break;
	case rook_obj_type_string:
		obj.as.string = rook_allocator_alloc(a, sizeof(*obj.as.string));
		rook_obj_string_init(obj.as.string);
		break;
	case rook_obj_type_userdata:
		obj.as.userdata = 0;
		break;
	case rook_obj_type_array:
		obj.as.array = rook_allocator_alloc(a, sizeof(*obj.as.array));
		rook_array_init(obj.as.array, 0);
		break;
	case rook_obj_type_map:
		obj.as.map = rook_allocator_alloc(a, sizeof(*obj.as.map));
		rook_map_init(obj.as.map);
		break;
	case rook_obj_type_func:
		obj.as.func = rook_allocator_alloc(a, sizeof(*obj.as.func));
		break;
	case rook_obj_type_cfunc:
		obj.as.cfunc = rook_allocator_alloc(a, sizeof(*obj.as.cfunc));
		break;
	case rook_obj_type_closure:
		obj.as.closure = rook_allocator_alloc(a, sizeof(*obj.as.closure));
		break;
	case rook_obj_type_bound_func:
		obj.as.bound_func = rook_allocator_alloc(a, sizeof(*obj.as.bound_func));
		break;
	default:
		assert(0);
		break;
	}

	return obj;
}

void rook_obj_incref(rook_obj* obj)
{
	switch (rook_obj_get_type(*obj))
	{
	case rook_obj_type_string:
	case rook_obj_type_array:
	case rook_obj_type_map:
	case rook_obj_type_func:
	case rook_obj_type_closure:
	case rook_obj_type_cfunc:
	case rook_obj_type_bound_func:
		++*obj->as.refcount;
		break;
	}
}

void rook_obj_free(rook_allocator* a, rook_obj* obj)
{
	switch (rook_obj_get_type(*obj))
	{
	case rook_obj_type_string:
		if (!--*obj->as.refcount)
		{
			rook_obj_string_free(obj->as.string);
			rook_allocator_free_obj(a, obj->as.string, sizeof(*obj->as.string));
		}
		break;
	case rook_obj_type_array:
		if (!--*obj->as.refcount)
		{
			rook_array_free(obj->as.array, a);
			rook_allocator_free_obj(a, obj->as.array, sizeof(*obj->as.array));
		}
		break;
	case rook_obj_type_map:
		if (!--*obj->as.refcount)
		{
			rook_map_free(obj->as.map, a);
			rook_allocator_free_obj(a, obj->as.map, sizeof(*obj->as.map));
		}
		break;
	case rook_obj_type_func:
		if (!--*obj->as.refcount)
		{
			rook_func_free(a, obj->as.func);
			rook_allocator_free_obj(a, obj->as.func, sizeof(*obj->as.func));
		}
		break;
	case rook_obj_type_cfunc:
		if (!--*obj->as.refcount)
		{
			rook_cfunk_free(obj->as.cfunc);
			rook_allocator_free_obj(a, obj->as.cfunc, sizeof(*obj->as.cfunc));
		}
		break;
	case rook_obj_type_closure:
		if (!--*obj->as.refcount)
		{
			rook_closure_free(obj->as.closure, a);
			rook_allocator_free_obj(a, obj->as.closure, sizeof(*obj->as.closure));
		}
		break;
	case rook_obj_type_bound_func:
		if (!-- * obj->as.refcount)
		{
			rook_bound_func_free(obj->as.bound_func, a);
			rook_allocator_free_obj(a, obj->as.bound_func, sizeof(*obj->as.bound_func));
		}
		break;
	}

	obj->type_and_tag = rook_obj_type_null;
	obj->as.userdata = 0;
}

rook_obj rook_obj_create_null(void)
{
	rook_obj obj;
	obj.type_and_tag = rook_obj_type_null;
	return obj;
}

rook_obj rook_obj_create_boolean(int b)
{
	rook_obj obj;
	obj.type_and_tag = rook_obj_type_boolean;
	obj.as.boolean = b;
	return obj;
}

rook_obj rook_obj_create_number(rook_float n)
{
	rook_obj obj;
	obj.type_and_tag = rook_obj_type_number;
	obj.as.number = n;
	return obj;
}

rook_obj rook_obj_create_userdata(void* ud)
{
	rook_obj obj;
	obj.type_and_tag = rook_obj_type_userdata;
	obj.as.userdata = ud;
	return obj;
}

rook_obj rook_obj_create_string(rook_allocator* a, rook_cstring s)
{
	rook_obj obj = rook_obj_create(a, rook_obj_type_string);
	rook_obj_string_init_ex(obj.as.string, s.chars, s.length);
	return obj;
}

rook_obj rook_obj_create_empty_string(rook_allocator* a)
{
	rook_obj obj = rook_obj_create(a, rook_obj_type_string);
	rook_obj_string_init(obj.as.string);
	return obj;
}

rook_obj rook_obj_create_array(rook_allocator* a, int c)
{
	rook_obj obj = rook_obj_create(a, rook_obj_type_array);
	rook_array_init(obj.as.array, c);
	return obj;
}

rook_obj rook_obj_create_map(rook_allocator* a, int c)
{
	rook_obj obj = rook_obj_create(a, rook_obj_type_map);
	
	if (c)
		rook_map_init_ex(obj.as.map, c);
	else
		rook_map_init(obj.as.map);

	return obj;
}

rook_obj rook_obj_create_cfunc(rook_allocator* a, rook_delegate_type d, rook_cstring n)
{
	rook_obj obj = rook_obj_create(a, rook_obj_type_cfunc);
	rook_cfunc_init(obj.as.cfunc, d, n);
	return obj;
}

rook_obj rook_obj_create_closure(rook_allocator* a, rook_obj f, rook_obj* cv)
{
	rook_obj obj = rook_obj_create(a, rook_obj_type_closure);
	rook_closure_init(obj.as.closure, f, cv);
	return obj;
}

rook_obj rook_obj_create_bound_func(rook_allocator* a, rook_obj f, rook_obj t)
{
	rook_obj obj = rook_obj_create(a, rook_obj_type_bound_func);
	rook_bound_func_init(obj.as.bound_func, f, t);
	return obj;
}

rook_cstring rook_obj_get_func_name(rook_obj f)
{
	switch (rook_obj_get_type(f))
	{
	case rook_obj_type_func:
		return rook_cstring_from_string(f.as.func->name);
	case rook_obj_type_closure:
		return rook_cstring_from_string(f.as.closure->func.as.func->name);
	case rook_obj_type_cfunc:
		return rook_cstring_from_string(f.as.cfunc->name);
	case rook_obj_type_bound_func:
		return rook_obj_get_func_name(f.as.bound_func->func);
	default:
		return rook_cstring_from_literal("");
	}
}

int rook_obj_get_parameter_count(rook_obj f)
{
	switch (rook_obj_get_type(f))
	{
	case rook_obj_type_func:
		return f.as.func->parameter_count;
	case rook_obj_type_closure:
		return f.as.closure->func.as.func->parameter_count;
	case rook_obj_type_cfunc:
		return f.as.cfunc->parameter_count;
	case rook_obj_type_bound_func:
		return rook_obj_get_parameter_count(f.as.bound_func->func);
	default:
		return -1;
	}
}

rook_string* rook_obj_get_parameter_names(rook_obj f)
{
	switch (rook_obj_get_type(f))
	{
	case rook_obj_type_cfunc:
		return f.as.cfunc->parameter_names;
	case rook_obj_type_func:
		return f.as.func->parameter_names;
	case rook_obj_type_closure:
		return f.as.closure->func.as.func->parameter_names;
	case rook_obj_type_bound_func:
		return rook_obj_get_parameter_names(f.as.bound_func->func);
	default:
		return 0;
	}
}

rook_string* rook_obj_get_parameter_types(rook_obj f)
{
	switch (rook_obj_get_type(f))
	{
	case rook_obj_type_cfunc:
		return f.as.cfunc->parameter_types;
	case rook_obj_type_func:
		return f.as.func->parameter_types;
	case rook_obj_type_closure:
		return f.as.closure->func.as.func->parameter_types;
	case rook_obj_type_bound_func:
		return rook_obj_get_parameter_types(f.as.bound_func->func);
	default:
		return 0;
	}
}

rook_func* rook_obj_get_prototype(rook_obj f)
{
	switch (rook_obj_get_type(f))
	{
	case rook_obj_type_func:
		return f.as.func;
	case rook_obj_type_closure:
		return f.as.closure->func.as.func;
	case rook_obj_type_bound_func:
		return rook_obj_get_prototype(f.as.bound_func->func);
	default:
		return 0;
	}
}

int rook_obj_get_member(rook_obj obj, rook_cstring key, rook_obj* value, rook_allocator* a)
{
	if (!rook_obj_ismap(obj))
		return 0;
	if (!rook_map_get(obj.as.map, key.chars, key.length, value))
		return 0;

	if (rook_obj_iscallable(*value))
	{
		rook_obj_incref(&obj);
		rook_obj_incref(value);
		*value = rook_obj_create_bound_func(a, *value, obj);
	}

	rook_obj_incref(value);

	return 1;
}

static void rook_obj_write_parameter(char* buffer, int buffer_length, int* obj_str_length, rook_cstring parameter_name, rook_cstring parameter_type)
{
	*obj_str_length += parameter_name.length;
	if (*obj_str_length <= buffer_length)
		memcpy(buffer + *obj_str_length - parameter_name.length, parameter_name.chars, parameter_name.length);

	if (parameter_type.length)
	{
		*obj_str_length += 3;
		if (*obj_str_length <= buffer_length)
		{
			buffer[*obj_str_length - 3] = ' ';
			buffer[*obj_str_length - 2] = ':';
			buffer[*obj_str_length - 1] = ' ';
		}

		*obj_str_length += parameter_type.length;
		if (*obj_str_length <= buffer_length)
			memcpy(buffer + *obj_str_length - parameter_type.length, parameter_type.chars, parameter_type.length);
	}
}

int rook_obj_to_string(rook_obj obj, char* buffer, int buffer_length, int* obj_str_length)
{
	int i = 0, element_str_length = 0, parameter_count = 0;
	rook_cstring name = { 0 };
	rook_string* parameter_names = 0;
	rook_string* parameter_types = 0;

	switch (rook_obj_get_type(obj))
	{
	case rook_obj_type_null:
		*obj_str_length = 4;
		if (buffer_length < 4)
			return 0;

		memcpy(buffer, "null", 4);
		return 1;
	case rook_obj_type_boolean:
		if (obj.as.boolean)
			*obj_str_length = 4;
		else
			*obj_str_length = 5;
		
		if (buffer_length < *obj_str_length)
			return 0;

		if (obj.as.boolean)
			memcpy(buffer, "true", 4);
		else
			memcpy(buffer, "false", 5);
		return 1;
	case rook_obj_type_number:
		*obj_str_length = rook_string_try_append_number(buffer, buffer_length, obj.as.number);
		return *obj_str_length <= buffer_length;
	case rook_obj_type_string:
		*obj_str_length = obj.as.string->length;
		if (*obj_str_length > buffer_length)
			return 0;
		memcpy(buffer, obj.as.string->chars, *obj_str_length);
		return 1;
	case rook_obj_type_userdata:
		*obj_str_length = 2 + rook_string_try_append_integer(buffer + 2, buffer_length - 2, (i64)obj.as.userdata);
		if (*obj_str_length > buffer_length)
			return 0;
		buffer[0] = '0';
		buffer[1] = 'x';
		return 1;
	case rook_obj_type_array:
		if (obj.as.array->size == 0)
		{
			*obj_str_length = 2;
			if (*obj_str_length <= buffer_length)
			{
				buffer[0] = '[';
				buffer[1] = ']';
			}
		}
		else
		{
			*obj_str_length = 2;
			if (*obj_str_length <= buffer_length)
			{
				buffer[0] = '[';
				buffer[1] = ' ';
			}

			for (i = 0; i < obj.as.array->size - 1; ++i)
			{
				rook_obj_to_string(obj.as.array->array[i], buffer + *obj_str_length, buffer_length - *obj_str_length, &element_str_length);
				*obj_str_length += element_str_length;

				if (*obj_str_length + 2 <= buffer_length)
				{
					buffer[*obj_str_length] = ',';
					buffer[*obj_str_length + 1] = ' ';
				}
				
				*obj_str_length += 2;
			}

			rook_obj_to_string(obj.as.array->array[i], buffer + *obj_str_length, buffer_length - *obj_str_length, &element_str_length);
			*obj_str_length += element_str_length;

			*obj_str_length += 2;

			if (*obj_str_length <= buffer_length)
			{
				buffer[*obj_str_length] = ' ';
				buffer[*obj_str_length + 1] = ']';
			}
		}
		return *obj_str_length <= buffer_length;
	case rook_obj_type_map:
		if (obj.as.map->size == 0)
		{
			*obj_str_length = 2;
			if (*obj_str_length <= buffer_length)
			{
				buffer[0] = '{';
				buffer[1] = '}';
			}
		}
		else
		{
			*obj_str_length = 2;
			if (*obj_str_length <= buffer_length)
			{
				buffer[0] = '{';
				buffer[1] = ' ';
			}

			for (i = 0; i < obj.as.map->capacity; ++i)
			{
				if (rook_map_entry_exists(obj.as.map, i))
				{
					name = rook_map_get_key(obj.as.map, i);
					*obj_str_length += name.length;

					if (*obj_str_length <= buffer_length)
					{
						memcpy(buffer + *obj_str_length, name.chars, name.length);
					}

					*obj_str_length += 3;

					if (*obj_str_length <= buffer_length)
					{
						buffer[*obj_str_length - 3] = ' ';
						buffer[*obj_str_length - 2] = '=';
						buffer[*obj_str_length - 1] = ' ';
					}

					rook_obj_to_string(rook_map_get_value(obj.as.map, i), buffer + *obj_str_length, buffer_length - *obj_str_length, &element_str_length);
					*obj_str_length += element_str_length;
					*obj_str_length += 2;

					if (*obj_str_length <= buffer_length)
					{
						buffer[*obj_str_length - 2] = ',';
						buffer[*obj_str_length - 1] = ' ';
					}
				}
			}

			*obj_str_length += 2;

			if (*obj_str_length <= buffer_length)
			{
				buffer[*obj_str_length - 2] = ' ';
				buffer[*obj_str_length - 1] = '}';
			}
		}
		return *obj_str_length <= buffer_length;
	case rook_obj_type_func:
	case rook_obj_type_closure:
	case rook_obj_type_bound_func:
	case rook_obj_type_cfunc:
		name = rook_obj_get_func_name(obj);
		if (name.length == 0)
			name = rook_cstring_from_literal("<anonymous func>");
		*obj_str_length = name.length;
		
		if (*obj_str_length <= buffer_length)
			memcpy(buffer, name.chars, *obj_str_length);

		parameter_count = rook_obj_get_parameter_count(obj);
		parameter_names = rook_obj_get_parameter_names(obj);
		parameter_types = rook_obj_get_parameter_types(obj);

		if (parameter_names)
		{
			*obj_str_length += 4;
			if (*obj_str_length <= buffer_length)
			{
				buffer[*obj_str_length - 4] = ' ';
				buffer[*obj_str_length - 3] = ':';
				buffer[*obj_str_length - 2] = ' ';
				buffer[*obj_str_length - 1] = '(';
			}

			for (i = 0; i < parameter_count - 1; ++i)
			{
				rook_obj_write_parameter(buffer, buffer_length, obj_str_length, rook_cstring_from_string(parameter_names[i]), parameter_types ? rook_cstring_from_string(parameter_types[i]) : rook_cstring_from_literal(""));

				*obj_str_length += 2;
				if (*obj_str_length <= buffer_length)
				{
					buffer[*obj_str_length - 2] = ',';
					buffer[*obj_str_length - 1] = ' ';
				}
			}

			rook_obj_write_parameter(buffer, buffer_length, obj_str_length, rook_cstring_from_string(parameter_names[i]), parameter_types ? rook_cstring_from_string(parameter_types[i]) : rook_cstring_from_literal(""));

			*obj_str_length += 1;
			if (*obj_str_length <= buffer_length)
				buffer[*obj_str_length - 1] = ')';
		}
		else if (parameter_count > 0)
		{
			*obj_str_length += 2;
			if (*obj_str_length <= buffer_length)
			{
				buffer[*obj_str_length - 2] = ' ';
				buffer[*obj_str_length - 1] = '(';
			}

			*obj_str_length += rook_string_try_append_integer(buffer + *obj_str_length, buffer_length - *obj_str_length, parameter_count);

			*obj_str_length += 1;
			if (*obj_str_length <= buffer_length)
				buffer[*obj_str_length - 1] = ')';
		}
		else if (parameter_count == 0)
		{
			rook_cstring void_string = rook_cstring_from_literal("void");

			*obj_str_length += 2;
			if (*obj_str_length <= buffer_length)
			{
				buffer[*obj_str_length - 2] = ' ';
				buffer[*obj_str_length - 1] = '(';
			}

			*obj_str_length += void_string.length;
			if (*obj_str_length <= buffer_length)
				memcpy(buffer + *obj_str_length - void_string.length, void_string.chars, void_string.length);

			*obj_str_length += 1;
			if (*obj_str_length <= buffer_length)
				buffer[*obj_str_length - 1] = ')';
		}

		return *obj_str_length <= buffer_length;
	default:
		assert(0);
		return 0;
	}
}

rook_obj rook_obj_clone(rook_allocator* a, rook_obj obj)
{
	int i;
	rook_obj container;
	rook_cstring key;

	switch (rook_obj_get_type(obj))
	{
	case rook_obj_type_func:
	case rook_obj_type_closure:
	case rook_obj_type_cfunc:
	case rook_obj_type_bound_func:
		rook_obj_incref(&obj);

	case rook_obj_type_null:
	case rook_obj_type_boolean:
	case rook_obj_type_number:
	case rook_obj_type_userdata:
		return obj;

	case rook_obj_type_string:
		return rook_obj_create_string(a, rook_cstring_from_chars_ex(obj.as.string->chars, obj.as.string->length));

	case rook_obj_type_array:
		container = rook_obj_create_array(a, obj.as.array->size);
		container.as.array->size = obj.as.array->size;
		for (i = 0; i < obj.as.array->size; ++i)
			container.as.array->array[i] = rook_obj_clone(a, obj.as.array->array[i]);
		return container;

	case rook_obj_type_map:
		container = rook_obj_create_map(a, obj.as.map->capacity);
		for (i = 0; i < obj.as.map->capacity; ++i)
		{
			if (rook_map_entry_exists(obj.as.map, i))
			{
				key = rook_map_get_key(obj.as.map, i);
				rook_map_set(container.as.map, key.chars, key.length, rook_obj_clone(a, rook_map_get_value(obj.as.map, i)), a);
			}
		}
		return container;

	default:
		assert(0 && "wtf");
		return rook_obj_create_null();
	}
}

int rook_obj_equal(rook_obj left, rook_obj right)
{
	if (rook_obj_get_type(left) != rook_obj_get_type(right))
		return 0;

	switch (rook_obj_get_type(left))
	{
	case rook_obj_type_null:
		return 1;
	case rook_obj_type_boolean:
		return left.as.boolean == right.as.boolean;
	case rook_obj_type_number:
		return left.as.number == right.as.number;
	case rook_obj_type_string:
		return rook_obj_string_equals(left.as.string, right.as.string);
	case rook_obj_type_userdata:
	case rook_obj_type_array:
	case rook_obj_type_map:
	case rook_obj_type_func:
	case rook_obj_type_closure:
	case rook_obj_type_bound_func:
		return left.as.userdata == right.as.userdata;
	case rook_obj_type_cfunc:
		return left.as.cfunc->delegate == right.as.cfunc->delegate && left.as.cfunc->userdata == right.as.cfunc->userdata;
	default:
		assert(0);
		return 0;
	}
}

int rook_obj_istruthy(rook_obj* obj)
{
	switch (rook_obj_get_type(*obj))
	{
	case rook_obj_type_null:
		return 0;
	case rook_obj_type_boolean:
		return obj->as.boolean;
	case rook_obj_type_number:
		return obj->as.number != 0.0;
	}

	return 1;
}

const char* rook_obj_type_names[] = {
	"null",
	"boolean",
	"number",
	"string",
	"userdata",
	"array",
	"map",
	"function",
	"function",
	"function",
	"function"
};

#ifdef __cplusplus
}
#endif // __cplusplus
