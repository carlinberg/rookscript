#ifndef ROOK_BYTECODE_INTERPRETER
#define ROOK_BYTECODE_INTERPRETER

#include <utility/rook_allocators.h>

#include "rook_call_info.h"
#include "rook_env.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef	struct rook_bytecode_chunk rook_bytecode_chunk;
typedef struct rook_error rook_error;

typedef struct rook_bytecode_interpreter
{
	rook_env env;
	int current_line;
	int current_column;
	int* callframe_stack_base_stack;

	u8* ip;
	rook_bytecode_chunk** chunk_stack;

#ifdef ROOK_DEBUG
	rook_call_info* current_call_info;
#endif // ROOK_DEBUG

	rook_allocator* runtime_allocator;
	rook_error** errors;
	rook_obj extension_funcs[rook_obj_type_length];
	rook_string* tag_names;
	rook_obj* tag_extension_funcs;
} rook_bytecode_interpreter;

void rook_bytecode_interpreter_init(rook_bytecode_interpreter* i, rook_allocator* runtime_allocator, rook_error** errors, rook_vm* vm);
void rook_bytecode_interpreter_free(rook_bytecode_interpreter* i);
int rook_bytecode_interpreter_run(rook_vm* vm, rook_bytecode_interpreter* i, rook_bytecode_chunk* c, int arg_count);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_BYTECODE_INTERPRETER
