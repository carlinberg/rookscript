#ifndef ROOK_ARRAY
#define ROOK_ARRAY

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_obj rook_obj;
typedef struct rook_allocator rook_allocator;

typedef struct rook_array
{
	int refcount;
	rook_obj* array;
	int size;
	int capacity;
} rook_array;

void rook_array_init(rook_array* array, int capacity);
void rook_array_free(rook_array* array, rook_allocator* a);

void rook_array_push(rook_array* array, rook_obj element);
void rook_array_push_many(rook_array* array, rook_obj* elements, int count);
void rook_array_fill(rook_array* array, rook_obj element, int count);
void rook_array_insert(rook_array* array, int index, rook_obj* element);
void rook_array_erase(rook_array* array, int index, rook_allocator* a);
void rook_array_clear(rook_array* array, rook_allocator* a);
void rook_array_sort(rook_array* a, int(*comparer)(void*, rook_obj*, rook_obj*), void* compare_state);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_ARRAY
