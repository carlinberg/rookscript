#include "rook_obj_string.h"

#include <utility/rook_malloc.h>
#include <utility/rook_allocators.h>

#include "rook_object.h"


#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_obj_string_init(rook_obj_string* str)
{
	str->refcount = 1;
	str->chars = 0;
	str->length = 0;
	str->capacity = 0;
}

void rook_obj_string_init_ex(rook_obj_string* str, const char* chars, int length)
{
	rook_obj_string_allocate(str, length);
	memcpy(str->chars, chars, length);
	str->chars[length] = '\0';
}

void rook_obj_string_allocate(rook_obj_string* str, int length)
{
	str->refcount = 1;
	str->length = length;
	str->capacity = length + 1;
	str->chars = ROOK_ALLOC(str->capacity);
	assert(str->chars);
}

void rook_obj_string_free(rook_obj_string* str)
{
	ROOK_FREE(str->chars);
	rook_obj_string_init(str);
}

void rook_obj_string_append(rook_obj_string* left, rook_obj_string* right)
{
	int new_capacity = left->length + right->length + 1;
	if (left->capacity < new_capacity)
	{
		char* new_chars = ROOK_REALLOC(left->chars, new_capacity);
		assert(new_chars);
		left->chars = new_chars;
		left->capacity = new_capacity;
	}

	memcpy(left->chars + left->length, right->chars, right->length);
	left->length += right->length;
	left->chars[left->length] = '\0';
}

int rook_obj_string_insert(rook_obj_string* str, int where, rook_obj_string* value)
{
	int i, j;
	int new_capacity = str->length + value->length + 1;

	if (value->length == 0)
		return 1;
	if (where < 0 || where >= str->length)
		return 0;

	new_capacity = str->length + value->length + 1;
	if (str->capacity < new_capacity)
	{
		char* new_chars = ROOK_REALLOC(str->chars, new_capacity);
		assert(new_chars);
		str->chars = new_chars;
		str->capacity = new_capacity;
	}

	for (i = str->length + value->length - 1; i >= where + value->length; --i)
		str->chars[i] = str->chars[i - value->length];

	for (i = where, j = 0; j < value->length; ++i, ++j)
		str->chars[i] = value->chars[j];

	str->length += value->length;
	str->chars[str->length] = '\0';

	return 1;
}

void rook_obj_string_clone(rook_obj_string* in, rook_obj_string* out)
{
	out->refcount = 1;
	out->length = in->length;
	out->capacity = in->length + 1;
	out->chars = ROOK_ALLOC(out->capacity);
	assert(out->chars);
	memcpy(out->chars, in->chars, in->length);
	out->chars[in->length] = '\0';
}

int rook_obj_string_equals(rook_obj_string* left, rook_obj_string* right)
{
	return left->length == right->length && memcmp(left->chars, right->chars, left->length) == 0;
}

#ifdef __cplusplus
}
#endif // __cplusplus
