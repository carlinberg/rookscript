#include "rook_bound_func.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_bound_func_init(rook_bound_func* bf, rook_obj f, rook_obj t)
{
	bf->refcount = 1;
	bf->func = f;
	bf->this_ = t;
}

void rook_bound_func_free(rook_bound_func* bf, rook_allocator* a)
{
	rook_obj_free(a, &bf->func);
	rook_obj_free(a, &bf->this_);

	bf->func = rook_obj_create_null();
	bf->this_ = rook_obj_create_null();
}

#ifdef __cplusplus
}
#endif // __cplusplus
