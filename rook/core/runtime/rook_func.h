#ifndef ROOK_FUNCTION
#define ROOK_FUNCTION

#include <utility/rook_string.h>
#include "rook_bytecode_chunk.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_allocator rook_allocator;
typedef struct rook_obj rook_obj;
typedef struct rook_stmt_base rook_stmt_base;
typedef struct rook_token rook_token;
typedef struct rook_error rook_error;

typedef struct rook_func
{
	int refcount;

	rook_bytecode_chunk chunk;

	int parameter_count;
	rook_string* parameter_names;
	rook_string* parameter_types;
	rook_string name;
	rook_string code;
} rook_func;

void rook_func_init_empty(rook_func* func);
void rook_func_init(rook_func* func, rook_token** pn, rook_token** pt, int pc, rook_bytecode_chunk c, rook_cstring name, rook_string code);
void rook_func_free(rook_allocator* a, rook_func* func);

int rook_func_serialize(rook_func* func, rook_string* result, rook_error** errors);
int rook_func_deserialize(rook_string* func_data, rook_func* result, rook_error** errors, rook_allocator* a);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_FUNCTION
