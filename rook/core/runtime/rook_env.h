#ifndef ROOK_ENV
#define ROOK_ENV

#include <utility/rook_settings.h>
#include <utility/rook_allocators.h>
#include <utility/stb_sb.h>

#include "rook_object.h"
#include "rook_map.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define ROOK_MAX_STACK 512

typedef struct rook_scope rook_scope;
typedef struct rook_obj rook_obj;
typedef struct rook_error rook_error;
typedef struct rook_callframe rook_callframe;
typedef struct rook_vm rook_vm;

typedef struct rook_env
{
	rook_obj stack[ROOK_MAX_STACK];
	rook_obj* stack_end;

	rook_map globals;

	rook_allocator* runtime_allocator;
	rook_vm* vm;
} rook_env;

void rook_env_init(rook_env* env, rook_allocator* runtime_allocator, rook_vm* vm);
void rook_env_free(rook_env* env);

rook_obj* rook_env_stack_get(rook_env* env, int idx);
rook_obj* rook_env_stack_top(rook_env* env);
int rook_env_stack_size(rook_env* env);

void rook_env_register_global(rook_env* env, rook_cstring name, rook_obj obj);
int rook_env_get_global(rook_env* env, rook_cstring name, rook_obj* obj);
int rook_env_set_global(rook_env* env, rook_cstring name, rook_obj obj);

int rook_env_push_obj(rook_env* env, rook_obj obj);
rook_obj* rook_env_push_empty(rook_env* env);
int rook_env_push_null(rook_env* env);
int rook_env_push_many_null(rook_env* env, int how_many);
int rook_env_pop(rook_env* env);
int rook_env_pop_many(rook_env* env, int how_many);
int rook_env_shrink_stack(rook_env* env, int how_many);

#define rook_env_push_result(r, o) rook_env_push_result_impl(&(r), (o))
void rook_env_push_result_impl(rook_obj** results, rook_obj object);
void rook_env_free_result_array(rook_obj* results);

#ifdef ROOK_DEBUG
#define rook_env_init_call_info_base(e, c) (e)->current_call_info = (c)
#else
#define rook_env_init_call_info_base(e, c) ((void)0)
#endif // ROOK_DEBUG

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_ENV
