#ifndef ROOK_HASHMAP
#define ROOK_HASHMAP

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_obj rook_obj;
typedef struct rook_map_entry rook_map_entry;
typedef struct rook_cstring rook_cstring;
typedef struct rook_allocator rook_allocator;

typedef struct rook_map
{
	int refcount;
	rook_map_entry* entries;
	int size;
	int capacity;
} rook_map;

void rook_map_init(rook_map* map);
void rook_map_init_ex(rook_map* map, int capacity);

void rook_map_free(rook_map* map, rook_allocator* a);

int rook_map_set(rook_map* map, const char* key, int key_length, rook_obj value, rook_allocator* a);
#define rook_map_setl(m, k, v, a) rook_map_set((m), (k), sizeof(k) - 1, (v), (a))
int rook_map_tryset(rook_map* map, const char* key, int key_length, rook_obj value, rook_allocator* a);

int rook_map_get_shallow(rook_map* map, const char* key, int key_length, rook_obj* value);
int rook_map_get(rook_map* map, const char* key, int key_length, rook_obj* value);
#define rook_map_getl(m, k, v) rook_map_get((m), (k), sizeof(k) - 1, (v))
#define rook_map_get_shallowl(m, k, v) rook_map_get_shallow((m), (k), sizeof(k) - 1, (v))

void rook_map_erase(rook_map* map, const char* key, int key_length, rook_allocator* a);
void rook_map_clear(rook_map* map, rook_allocator* a);

int rook_map_entry_exists(rook_map* map, int raw_index);
rook_cstring rook_map_get_key(rook_map* map, int raw_index);
rook_obj rook_map_get_value(rook_map* map, int raw_index);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_map
