#ifndef ROOK_SETTINGS
#define ROOK_SETTINGS

//define ROOK_DEBUG to enable callstack, lineinfo etc
#define ROOK_DEBUG

//change rook_float to whatever you want as your underlying floating point type
#define ROOK_FLOAT_SIZE 32
typedef float rook_float;

#endif // !ROOK_SETTINGS
