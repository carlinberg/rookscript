#include "stb_sb.h"

#include <stdlib.h>
#include <assert.h>

#include "rook_malloc.h"

#ifdef __cplusplus
}
#endif // __cplusplus

void* stb__sbgrowf_rook(void* arr, int increment, int itemsize, const char* file, int line)
{
	assert(increment > 0);
	assert(itemsize > 0);

	int dbl_cur = arr ? 2 * stb__sbm(arr) : 0;
	int min_needed = stb_sb_count(arr) + increment;
	int m = dbl_cur > min_needed ? dbl_cur : min_needed;
#ifdef ROOK_CHECK_MEMORY
	int* p = (int*)rook_realloc(arr ? stb__sbraw(arr) : 0, (size_t)itemsize * m + sizeof(int) * 2, file, line);
#else
	int* p = (int*)ROOK_REALLOC(arr ? stb__sbraw(arr) : 0, (size_t)itemsize * m + sizeof(int) * 2);
#endif // ROOK_CHECK_MEMORY
	if (p) {
		if (!arr)
			p[1] = 0;
		p[0] = m;
		return p + 2;
	}
	else {
		assert(0);
		exit(-1);
	}
}

void stb__free_rook(void* ptr)
{
	ROOK_FREE(ptr);
}

#ifdef __cplusplus
}
#endif // __cplusplus
