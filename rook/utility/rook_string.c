#include "rook_string.h"

#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "rook_malloc.h"
#include "rook_number_types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_string_init(rook_string* str, const char* chars)
{
	if (chars)
	{
		str->length = (int)strlen(chars);
		str->chars = ROOK_ALLOC((size_t)str->length + 1);
		assert(str->chars);
		memcpy(str->chars, chars, str->length);
		str->chars[str->length] = '\0';
	}
	else
	{
		str->chars = 0;
		str->length = 0;
	}
}

void rook_string_init_ex(rook_string* str, const char* chars, int length)
{
	if (chars && length)
	{
		str->length = length;
		str->chars = ROOK_ALLOC((size_t)str->length + 1);
		assert(str->chars);
		memcpy(str->chars, chars, str->length);
		str->chars[str->length] = '\0';
	}
	else
	{
		str->chars = 0;
		str->length = 0;
	}
}

void rook_string_initc(rook_string* str, rook_cstring cstr)
{
	rook_string_init_ex(str, cstr.chars, cstr.length);
}

void rook_string_allocate(rook_string* str, int length)
{
	assert(length);

	str->length = length;
	str->chars = ROOK_ALLOC((size_t)str->length + 1);
	assert(str->chars);
	memset(str->chars, 0, str->length + 1);
}

void rook_string_free(rook_string* str)
{
	ROOK_FREE(str->chars);
	str->chars = 0;
	str->length = 0;
}

rook_string rook_string_copy(rook_string* from)
{
	rook_string new_str;
	new_str.length = from->length;
	new_str.chars = ROOK_ALLOC((size_t)new_str.length + 1);
	assert(new_str.chars);
	memcpy(new_str.chars, from->chars, new_str.length);
	new_str.chars[new_str.length] = '\0';
	return new_str;
}

void rook_string_append(rook_string* to, rook_string* from)
{
	rook_cstring f;
	rook_cstring_init(&f, from->chars, from->length);
	rook_string_appendc(to, f);
}

void rook_string_appendc(rook_string* to, rook_cstring from)
{
	int new_length = to->length + from.length;
	char* new_chars = ROOK_REALLOC(to->chars, (size_t)new_length + 1);
	assert(new_chars);
	to->chars = new_chars;
	memcpy(to->chars + to->length, from.chars, from.length);
	to->chars[new_length] = '\0';
	to->length = new_length;
}

void rook_string_append_chars(rook_string* to, const char* from, int length)
{
	rook_cstring str;
	rook_cstring_init(&str, from, length);
	rook_string_appendc(to, str);
}

void rook_string_append_char(rook_string* to, char c)
{
	rook_string_append_chars(to, &c, 1);
}

void rook_string_append_char_ex(rook_string* to, char c, int count)
{
	int new_length = to->length + count;
	char* new_chars = ROOK_REALLOC(to->chars, (size_t)new_length + 1);
	assert(new_chars);
	to->chars = new_chars;
	memset(to->chars + to->length, c, count);
	to->chars[new_length] = '\0';
	to->length = new_length;

}

void rook_string_append_number(rook_string* to, rook_float number)
{
	i64 base = (i64)floorf(number);
	rook_float decimals = number - (rook_float)base;

	char const digit[] = "0123456789";

	if (base < 0)
	{
		rook_string_append_literal(to, "-");
		base *= -1;
	}

	char* p;
	i64 shifter = base;
	do
	{
		rook_string_append_literal(to, "\0");
		shifter = shifter / 10;
		p = to->chars + to->length;
	} while (shifter);

	do
	{
		*--p = digit[base % 10];
		base = base / 10;
	} while (base);

	if (decimals > (rook_float)0.000)
	{
		rook_string_append_literal(to, ".");

		base = (i64)(decimals * 1000);
		shifter = base;

		do
		{
			rook_string_append_literal(to, "\0");
			shifter = shifter / 10;
			p = to->chars + to->length;
		} while (shifter);

		do
		{
			*--p = digit[base % 10];
			base = base / 10;
		} while (base);
	}
}

void rook_string_append_integer(rook_string* to, i64 integer)
{
	char const digit[] = "0123456789";

	if (integer < 0)
	{
		rook_string_append_literal(to, "-");
		integer *= -1;
	}

	char* p;
	i64 shifter = integer;
	do
	{
		rook_string_append_literal(to, "\0");
		shifter = shifter / 10;
		p = to->chars + to->length;
	} while (shifter);

	do
	{
		*--p = digit[integer % 10];
		integer = integer / 10;
	} while (integer);
}

int rook_string_equals_impl(rook_cstring* left, rook_cstring* right)
{
	return left->length == right->length && memcmp(left->chars, right->chars, left->length) == 0;
}

int rook_string_try_append_number(char* to, int to_length, rook_float number)
{
	i64 base = (i64)floorf(number);
	rook_float decimals = number - (rook_float)base;

	int length = rook_string_try_append_integer(to, to_length, base);

	if (decimals > (rook_float)0.000)
	{
		if (length < to_length)
			to[length] = '.';
		++length;

		length += rook_string_try_append_integer(to + length, to_length - length, (i64)(decimals * (rook_float)1000));
	}

	return length;
}

int rook_string_try_append_integer(char* to, int to_length, i64 integer)
{
	char const digit[] = "0123456789";
	int length = 0;

	if (integer < 0)
	{
		if (length < to_length)
			to[length++] = '-';
		integer *= -1;
	}

	char* p = 0;
	i64 shifter = integer;
	do
	{
		shifter = shifter / 10;
		if (length++ <= to_length)
			p = to + length;
	} while (shifter);

	if (length <= to_length)
	{
		do
		{
			*--p = digit[integer % 10];
			integer = integer / 10;
		} while (integer);
	}

	return length;
}

rook_float rook_float_from_string(rook_cstring s)
{
	const char* str = s.chars;
	int length = s.length;
	int start = 0;
	rook_float number = 0;
	rook_float sign = 1;
	if (*str == '-')
	{
		sign = -1;
		++str;
	}

	for (; *str >= '0' && *str <= '9' && start < length; ++str, ++start)
	{
		number = number * 10 + *str - '0';
	}

	if (*str == '.' && start++ < length)
	{
		rook_float divider = 1.0;

		for (; *str >= '0' && *str <= '9' && start < length; ++str, ++start)
		{
			number += ((long long)*str - '0') / (divider /= (rook_float)10);
		}
	}

	return number * sign;
}

rook_string rook_string_from_chars(const char* chars)
{
	rook_string str;
	rook_string_init(&str, chars);
	return str;
}

rook_string rook_string_from_chars_ex(const char* chars, int length)
{
	rook_string str;
	rook_string_init_ex(&str, chars, length);
	return str;
}

rook_string rook_string_from_format(const char* fmt, ...)
{
	va_list ap;
	const char* it;
	const char* temp;
	rook_string str = { 0, 0 };

	va_start(ap, fmt);
	for (it = fmt; *it != '\0'; ++it)
	{
		if (*it == '%')
		{
			switch (*++it)
			{
			case 'd':
				rook_string_append_integer(&str, va_arg(ap, int));
				break;
			case 'f':
				rook_string_append_number(&str, (rook_float)va_arg(ap, double));
				break;
			case 's':
				temp = va_arg(ap, const char*);
				rook_string_append_chars(&str, temp, (int)strlen(temp));
				break;
			case 'S':
				rook_string_appendc(&str, va_arg(ap, rook_cstring));
				break;
			default:
				rook_string_append_literal(&str, "%");
				rook_string_append_chars(&str, it, 1);
				break;
			}
		}
		else
		{
			rook_string_append_chars(&str, it, 1);
		}
	}
	va_end(ap);

	return str;
}

rook_cstring rook_cstring_from_chars(const char* chars)
{
	return rook_cstring_from_chars_ex(chars, (int)strlen(chars));
}

rook_cstring rook_cstring_from_chars_ex(const char* chars, int length)
{
	rook_cstring str;
	rook_cstring_init(&str, chars, length);
	return str;
}

#ifdef __cplusplus
}
#endif // __cplusplus
