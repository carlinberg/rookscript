#include "rook_error.h"
#include "stb_sb.h"

#include <stdarg.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_error_init(rook_error* error, const char* chars, int length, rook_cstring file, rook_cstring code, int line, int column)
{
	rook_string_init_ex(&error->msg, chars, length);
	rook_string_initc(&error->file, file);
	rook_string_initc(&error->code, code);
	error->line = line;
	error->column = column;
}

void rook_error_free(rook_error* error)
{
	rook_string_free(&error->msg);
	rook_string_free(&error->file);
	rook_string_free(&error->code);
}

void rook_errorlist_add_error(rook_error** errors, const char* msg, int length, rook_cstring file, rook_cstring code, int line, int column)
{
	rook_error error;
	rook_error_init(&error, msg, length, file, code, line, column);
	sb_push(*errors, error);
}

void rook_errorlist_add_errorf(rook_error** errors, rook_cstring file, rook_cstring code, int line, int column, const char* fmt, ...)
{
	va_list ap;
	const char* it = fmt;
	rook_error error;

	rook_error_init(&error, 0, 0, file, code, line, column);

	va_start(ap, fmt);
	for (; *it != '\0'; ++it)
	{
		if (*it == '%')
		{
			switch (*++it)
			{
			case 'c':
				rook_string_append_char(&error.msg, (char)va_arg(ap, int));
				break;
			case 'd':
				rook_string_append_number(&error.msg, (rook_float)va_arg(ap, int));
				break;
			case 'f':
				rook_string_append_number(&error.msg, (rook_float)va_arg(ap, double));
				break;
			case 's':
				{
					const char* temp = va_arg(ap, const char*);
					rook_string_append_chars(&error.msg, temp, (int)strlen(temp));
				}
				break;
			case 'S':
					rook_string_appendc(&error.msg, va_arg(ap, rook_cstring));
				break;
			default:
				rook_string_append_literal(&error.msg, "%");
				rook_string_append_chars(&error.msg, it, 1);
				break;
			}
		}
		else
		{
			rook_string_append_chars(&error.msg, it, 1);
		}
	}
	va_end(ap);

	sb_push(*errors, error);
}

rook_string rook_error_show_error_in_code(rook_error* error)
{
	int i;
	int current_line = 1;
	int found = 0;
	const char* line_start = 0;
	const char* line_end = 0;
	rook_string result;

	for (i = 0; i < error->code.length; ++i)
	{
		if (!found)
		{
			if (current_line == error->line)
			{
				found = 1;
				line_start = error->code.chars + i;
			}
			else if (error->code.chars[i] == '\n')
			{
				++current_line;
			}
		}
		else if (found && error->code.chars[i] == '\n')
		{
			line_end = error->code.chars + i;
			break;
		}
	}

	if (line_start && line_end)
	{
		rook_string_init_ex(&result, line_start, (int)(line_end - line_start));
		rook_string_append_char(&result, '\n');
		rook_string_append_char_ex(&result, '-', error->column);
		rook_string_append_char(&result, '^');
	}
	else
	{
		rook_string_init_literal(&result, "No line info");
	}

	rook_string_append_char(&result, '\n');

	return result;
}


#ifdef __cplusplus
}
#endif // __cplusplus
