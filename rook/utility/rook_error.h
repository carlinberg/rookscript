#ifndef ROOK_ERROR
#define ROOK_ERROR

#include "rook_string.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_error
{
	rook_string msg;
	rook_string file;
	rook_string code;
	int line;
	int column;
} rook_error;

void rook_error_init(rook_error* error, const char* chars, int length, rook_cstring file, rook_cstring code, int line, int column);
void rook_error_free(rook_error* error);

void rook_errorlist_add_error(rook_error** errors, const char* msg, int length, rook_cstring file, rook_cstring code, int line, int column);
void rook_errorlist_add_errorf(rook_error** errors, rook_cstring file, rook_cstring code, int line, int column, const char* fmt, ...);
#define rook_errorlist_add_literal(e, msg, f, co, l, c) rook_errorlist_add_error((e), (msg), sizeof(msg) - 1, (f), (co), (l), (c))

rook_string rook_error_show_error_in_code(rook_error* error);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_ERROR
