#ifndef ROOK_FILE
#define ROOK_FILE

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_string rook_string;
typedef struct rook_cstring rook_cstring;

int rook_read_file(rook_cstring path, rook_string* content);
int rook_write_file(rook_cstring path, rook_cstring content);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_FILE
