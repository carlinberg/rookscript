#ifndef ROOK_STRING
#define ROOK_STRING

#include "rook_settings.h"
#include "rook_number_types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_string
{
	char* chars;
	int length;
} rook_string;

typedef struct rook_cstring
{
	const char* chars;
	int length;
} rook_cstring;

void rook_string_init(rook_string* str, const char* chars);
void rook_string_init_ex(rook_string* str, const char* chars, int length);
void rook_string_initc(rook_string* str, rook_cstring cstr);
void rook_string_allocate(rook_string* str, int length);
void rook_string_free(rook_string* str);

rook_string rook_string_copy(rook_string* from);
void rook_string_append(rook_string* to, rook_string* from);
void rook_string_appendc(rook_string* to, rook_cstring from);
void rook_string_append_chars(rook_string* to, const char* from, int length);
void rook_string_append_char(rook_string* to, char c);
void rook_string_append_char_ex(rook_string* to, char c, int count);
void rook_string_append_number(rook_string* to, rook_float number);
void rook_string_append_integer(rook_string* to, i64 integer);
int rook_string_equals_impl(rook_cstring* left, rook_cstring* right);

int rook_string_try_append_number(char* to, int to_length, rook_float number);
int rook_string_try_append_integer(char* to, int to_length, i64 integer);

rook_float rook_float_from_string(rook_cstring str);

rook_string rook_string_from_chars(const char* chars);
rook_string rook_string_from_chars_ex(const char* chars, int length);
rook_string rook_string_from_format(const char* fmt, ...);
#define rook_string_from_cstring(cstr) rook_string_from_chars_ex((cstr).chars, (cstr).length)
#define rook_string_from_literal(l) rook_string_from_chars_ex((l), sizeof(l) - 1)

rook_cstring rook_cstring_from_chars(const char* chars);
rook_cstring rook_cstring_from_chars_ex(const char* chars, int length);
#define rook_cstring_from_literal(l) rook_cstring_from_chars_ex(l, sizeof(l) - 1)
#define rook_cstring_from_string(l) (*(rook_cstring*)&l)

#define rook_string_equals(l, r) rook_string_equals_impl((rook_cstring*)(l), (rook_cstring*)(r))

#define rook_string_init_literal(s, l) rook_string_init_ex((s), (l), sizeof(l) - 1)
#define rook_cstring_init(s, c, l) do { (s)->chars = c; (s)->length = l; } while(0)
#define rook_cstring_init_literal(s, l) do { (s)->chars = (l); (s)->length = sizeof(l) - 1; } while(0)
#define rook_string_append_literal(s, l) rook_string_append_chars((s), (l), sizeof(l) - 1)

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_STRING
