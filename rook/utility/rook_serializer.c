#include "rook_serializer.h"
#include <utility/rook_string.h>
#include <utility/rook_malloc.h>
#include <core/runtime/rook_object.h>
#include <core/runtime/rook_obj_string.h>
#include <core/runtime/rook_array.h>

#include <string.h>
#include <assert.h>

void rook_serializer_write(rook_serializer* s, void* obj, int bytes)
{
    if (!s->buffer || s->current + bytes > s->buffer + s->length)
    {
        int current_idx = (int)(s->current - s->buffer);

        int new_length = s->buffer ? s->length * 2 : 8;
        if (s->length + bytes > new_length)
            new_length = s->length + bytes;
        s->buffer = ROOK_REALLOC(s->buffer, new_length);
        s->current = s->buffer + current_idx;
        s->length = new_length;
    }
    
    if (bytes > 0)
        memcpy(s->current, obj, bytes);
    s->current += bytes;
}

void rook_serializer_free_write(rook_serializer* s)
{
    ROOK_FREE(s->buffer);
	s->func = 0;
	s->buffer = 0;
	s->current = 0;
	s->length = 0;
}

void rook_serializer_read(rook_serializer* s, void* obj, int bytes)
{
    assert(s->buffer && s->current + bytes <= s->buffer + s->length);
    if (!s->buffer || s->current + bytes > s->buffer + s->length)
        return;
    
    if (bytes > 0)
        memcpy(obj, s->current, bytes);
    s->current += bytes;
}

void rook_serializer_init_write(rook_serializer* s)
{
	s->func = rook_serializer_write;
	s->buffer = 0;
	s->current = 0;
	s->length = 0;
}

void rook_serializer_init_read(rook_serializer* s, char* buffer, int bytes)
{
	s->func = rook_serializer_read;
	s->buffer = buffer;
	s->current = buffer;
	s->length = bytes;
}

void rook_serialize_obj(rook_serializer* s, rook_obj* obj, rook_allocator* a)
{
	int length = 0;

    rook_serialize(s, &obj->type_and_tag);

    switch (rook_obj_get_type(*obj))
    {
	case rook_obj_type_null:
        obj->as.userdata = 0;
        break;
	case rook_obj_type_boolean:
        rook_serialize(s, &obj->as.boolean);
        break;
	case rook_obj_type_number:
        rook_serialize(s, &obj->as.number);
        break;
	case rook_obj_type_string:
        rook_serialize(s, &length);
        if (rook_serializer_is_reader(s))
        {
            *obj = rook_obj_create_empty_string(a);
            rook_obj_string_allocate(obj->as.string, length);
        }
        rook_serialize_ex(s, obj->as.string->chars, length);
        break;
	case rook_obj_type_userdata:
        rook_serialize(s, &obj->as.userdata);
        break;
	case rook_obj_type_array:
        rook_serialize(s, &length);
        if (rook_serializer_is_reader(s))
        {
            *obj = rook_obj_create_array(a, length);
            rook_array_fill(obj->as.array, rook_obj_create_null(), length);
        }
        
        for (int i = 0; i < length; ++i)
        {
            rook_obj* element = &obj->as.array->array[i];
            rook_serialize_obj(s, element, a);
        }
        break;
	case rook_obj_type_map:
        assert(0 && "Can't use 'map' in rook_serializer");
        break;
	case rook_obj_type_func:
        assert(0 && "Can't use 'func' in rook_serializer");
        break;
	case rook_obj_type_closure:
        assert(0 && "Can't use 'closure' in rook_serializer");
        break;
	case rook_obj_type_cfunc:
        assert(0 && "Can't use 'cfunc' in rook_serializer");
        break;
	case rook_obj_type_bound_func:
        assert(0 && "Can't use 'bound_func' in rook_serializer");
        break;
    default:
        assert(0 && "rook_obj has invalid type in rook_serializer");
        break;
    }
}

rook_cstring rook_serializer_to_cstr(rook_serializer* s)
{
    int length = rook_serializer_is_reader(s) ? s->length : (int)(s->current - s->buffer);
    return rook_cstring_from_chars_ex(s->buffer, length);
}

rook_string rook_serializer_to_str(rook_serializer* s)
{
    int length = rook_serializer_is_reader(s) ? s->length : (int)(s->current - s->buffer);
    return rook_string_from_chars_ex(s->buffer, length);
}
