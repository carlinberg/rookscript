#ifndef ROOK_NUMBER_TYPES
#define ROOK_NUMBER_TYPES

typedef short i16;
typedef int i32;
typedef long long i64;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

#if defined(_WIN64) || defined(__x86_64__)
typedef unsigned long long uptr;
#else
typedef unsigned int uptr;
#endif // defined(_WIN64)

#define I16_MAX 32767

#endif // !ROOK_NUMBER_TYPES
