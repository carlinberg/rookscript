#ifndef ROOK_ALLOCATORS
#define ROOK_ALLOCATORS

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_memory_list rook_memory_list;

typedef struct rook_allocator
{
	rook_memory_list* memory;
	rook_memory_list* memory_last;
} rook_allocator;

void rook_allocator_init(rook_allocator* a);
void rook_allocator_free(rook_allocator* a);
void* rook_allocator_alloc(rook_allocator* a, int bytes);
void rook_allocator_free_obj(rook_allocator* a, void* obj, int bytes);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_ALLOCATORS
