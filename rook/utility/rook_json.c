#include "rook_json.h"
#include "rook_string.h"
#include "rook_error.h"
#include "rook_malloc.h"
#include "stb_sb.h"

#include <core/runtime/rook_object.h>
#include <core/runtime/rook_map.h>
#include <core/runtime/rook_array.h>
#include <core/runtime/rook_obj_string.h>

#define _CRT_SECURE_NO_WARNINGS


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define TAB_COLUMN_WIDTH 4

static int map_tojson(rook_map* map, rook_string* json);
static int array_tojson(rook_array* array, rook_string* json);

static int value_tojson(rook_obj obj, rook_string* json)
{
	char buffer[1024] = { 0 };
	char* func_str = 0;
	int length = 0;

	switch (rook_obj_get_type(obj))
	{
	case rook_obj_type_null:
		rook_string_append_literal(json, "null");
		return 1;
	case rook_obj_type_boolean:
		if (obj.as.boolean)
			rook_string_append_literal(json, "true");
		else
			rook_string_append_literal(json, "false");
		return 1;
	case rook_obj_type_number:
		rook_string_append_number(json, obj.as.number);
		return 1;
	case rook_obj_type_string:
		rook_string_append_char(json, '"');
		rook_string_append_chars(json, obj.as.string->chars, obj.as.string->length);
		rook_string_append_char(json, '"');
		return 1;
	case rook_obj_type_userdata:
		rook_string_append_number(json, (rook_float)(long long)obj.as.userdata);
		return 1;
	case rook_obj_type_array:
		return array_tojson(obj.as.array, json);
	case rook_obj_type_map:
		return map_tojson(obj.as.map, json);
	case rook_obj_type_func:
	case rook_obj_type_cfunc:
	case rook_obj_type_bound_func:
	case rook_obj_type_closure:
		func_str = buffer;
		if (!rook_obj_to_string(obj, buffer, sizeof(buffer), &length))
		{
			func_str = ROOK_ALLOC(length);
			if (!rook_obj_to_string(obj, buffer, sizeof(buffer), &length))
			{
				ROOK_FREE(func_str);
				return 0;
			}
		}

		rook_string_append_char(json, '"');
		rook_string_append_chars(json, func_str, length);
		rook_string_append_char(json, '"');

		if (func_str != buffer)
			ROOK_FREE(func_str);

		return 1;
	default:
		rook_string_append_literal(json, "\"[");
		rook_string_appendc(json, rook_cstring_from_chars(rook_obj_type_names[rook_obj_get_type(obj)]));
		rook_string_append_literal(json, "]\"");
		return 1;
	}
}

static int map_tojson(rook_map* map, rook_string* json)
{
	rook_string_append_literal(json, "{");

	if (map->size)
	{
		for (int i = 0; i < map->capacity; ++i)
		{
			if (rook_map_entry_exists(map, i))
			{
				rook_string_append_literal(json, "\"");
				rook_string_appendc(json, rook_map_get_key(map, i));
				rook_string_append_literal(json, "\"");
				rook_string_append_literal(json, ":");
				if (!value_tojson(rook_map_get_value(map, i), json))
					return 0;
				rook_string_append_literal(json, ",");
			}
		}
		
		json->chars[json->length - 1] = '}';
	}
	else
	{
		rook_string_append_literal(json, "}");
	}
	
	return 1;
}

static int array_tojson(rook_array* array, rook_string* json)
{
	rook_string_append_literal(json, "[");

	if (array->size)
	{
		for (int i = 0; i < array->size; ++i)
		{
			if (!value_tojson(array->array[i], json))
				return 0;
			rook_string_append_literal(json, ",");
		}

		json->chars[json->length - 1] = ']';
	}
	else
	{
		rook_string_append_literal(json, "]");
	}

	return 1;
}

rook_string rook_tojson(rook_obj obj)
{
	rook_string json;
	rook_string_init(&json, 0);

	if (rook_obj_ismap(obj))
	{
		if (!map_tojson(obj.as.map, &json))
		{
			rook_string_free(&json);
		}
	}

	return json;
}

typedef struct 
{
	const char* json_it;
	const char* json_end;

	int line;
	int column;

	rook_allocator* alloc;
	rook_error** errors;
} parse_state;

static int skip_whitespace(parse_state* state);
static int advance_if(parse_state* state, char value);
static void add_error_impl(parse_state* state, const char* msg, int length);
#define add_error(s, m) add_error_impl((s), (m), sizeof(m) - 1)

static rook_obj parse_value(parse_state* state);
static rook_obj parse_object(parse_state* state);
static rook_obj parse_array(parse_state* state);
static rook_cstring parse_string(parse_state* state);
static rook_obj parse_number(parse_state* state);

rook_obj rook_fromjson(rook_cstring json, rook_allocator* alloc, rook_error** errors)
{
	parse_state state;

	state.json_it = json.chars;
	state.json_end = json.chars + json.length;
	state.line = 1;
	state.column = 1;
	state.alloc = alloc;
	state.errors = errors;

	if (!skip_whitespace(&state))
	{
		add_error(&state, "Empty json string");
		return rook_obj_create_null();
	}

	if (*state.json_it++ != '{')
	{
		add_error(&state, "No root object declared");
		return rook_obj_create_null();
	}

	return parse_object(&state);
}


static int skip_whitespace(parse_state* state)
{
	while (state->json_it < state->json_end)
	{
		switch (*state->json_it)
		{
		case ' ':
			++state->column;
			break;
		case '\t':
			state->column += TAB_COLUMN_WIDTH;
			break;
		case '\n':
			state->line += 1;
			state->column = 0;
			break;
		case '\r':
			break;
		default:
			return 1;
		}

		++state->json_it;
	}

	return 0;
}

static int advance_if(parse_state* state, char value)
{
	if (state->json_it < state->json_end && *state->json_it == value)
	{
		++state->json_it;
		return 1;
	}

	return 0;
}

static void add_error_impl(parse_state* state, const char* msg, int length)
{
	rook_errorlist_add_error(state->errors, msg, length, rook_cstring_from_literal("json"), rook_cstring_from_literal(""), state->line, state->column);
}

static rook_obj parse_value(parse_state* state)
{
	if (!skip_whitespace(state))
	{
		add_error(state, "Expected key in object or object delimiter '}'");
		return rook_obj_create_null();
	}

	switch (*state->json_it)
	{
	case '{':
		++state->json_it;
		return parse_object(state);
		break;
	case '[':
		++state->json_it;
		return parse_array(state);
	case '"':
		++state->json_it;
		return rook_obj_create_string(state->alloc, parse_string(state));
	case 'n':
		++state->json_it;
		if (state->json_end - state->json_it < 4 || strncmp(state->json_it, "ull", 3) != 0)
			add_error(state, "Unknown identifier");
		else
			state->json_it += 3;
		return rook_obj_create_null();
	case 't':
		++state->json_it;
		if (state->json_end - state->json_it < 4 || strncmp(state->json_it, "rue", 3) != 0)
			add_error(state, "Unknown identifier");
		else
			state->json_it += 3;
		return rook_obj_create_boolean(1);
	case 'f':
		++state->json_it;
		if (state->json_end - state->json_it < 4 || strncmp(state->json_it, "alse", 4) != 0)
			add_error(state, "Unknown identifier");
		else
			state->json_it += 4;
		return rook_obj_create_boolean(0);
	case '+':
		++state->json_it;
		if (*state->json_it < '0' || *state->json_it > '9')
		{
			add_error(state, "Unexpected character while parsing number");
			return rook_obj_create_null();
		}
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		return parse_number(state);
	case '-':
		++state->json_it;
		if (*state->json_it < '0' || *state->json_it > '9')
		{
			add_error(state, "Unexpected character while parsing number");
			return rook_obj_create_null();
		}
		else
		{
			rook_obj number = parse_number(state);
			number.as.number = -number.as.number;
			return number;
		}
	}

	add_error(state, "Expected value");
	return rook_obj_create_null();
}

static rook_obj parse_object(parse_state* state)
{
	rook_obj obj;

	if (!skip_whitespace(state) || (*state->json_it != '"' && *state->json_it != '}'))
	{
		add_error(state, "Expected key in object or object delimiter '}'");
		return rook_obj_create_null();
	}

	obj = rook_obj_create_map(state->alloc, 0);

	if (advance_if(state, '}'))
	{
		return obj;
	}
	else
	{
		do
		{
			if (!skip_whitespace(state) || !advance_if(state, '"'))
				add_error(state, "Expected key in object");

			rook_cstring key = parse_string(state);

			if (!skip_whitespace(state) || !advance_if(state, ':'))
				add_error(state, "Expected ':' after key in object");

			rook_obj value = parse_value(state);

			if (!skip_whitespace(state))
				add_error(state, "Expected ',' or '}' after value in object");

			rook_map_set(obj.as.map, key.chars, key.length, value, state->alloc);
		}
		while (sb_count(*state->errors) == 0 && advance_if(state, ','));
	}

	if (sb_count(*state->errors) == 0 && !advance_if(state, '}'))
		add_error(state, "Expected '}' at end of object or ',' for next pair");

	return obj;
}

static rook_obj parse_array(parse_state* state)
{
	rook_obj array;

	if (!skip_whitespace(state))
	{
		add_error(state, "Expected value in array or array delimiter ']'");
		return rook_obj_create_null();
	}

	array = rook_obj_create_array(state->alloc, 0);

	if (advance_if(state, ']'))
	{
		return array;
	}
	else
	{
		do
		{
			if (!skip_whitespace(state))
				add_error(state, "Expected value in array");

			rook_obj value = parse_value(state);

			if (!skip_whitespace(state))
				add_error(state, "Expected ',' or ']' after value in array");

			rook_array_push(array.as.array, value);
		}
		while (sb_count(*state->errors) == 0 && advance_if(state, ','));
	}

	if (sb_count(*state->errors) == 0 && !advance_if(state, ']'))
		add_error(state, "Expected '}' at end of object or ',' for next pair");

	return array;
}

static rook_cstring parse_string(parse_state* state)
{
	const char* key_start = state->json_it;
	int key_length = 0;
	
	while (*state->json_it != '"' && state->json_it < state->json_end)
		++state->json_it, ++key_length;

	if (state->json_it >= state->json_end)
		add_error(state, "Unexpected end of file while parsing string");
	else
		++state->json_it;

	return rook_cstring_from_chars_ex(key_start, key_length);
}

static rook_obj parse_number(parse_state* state)
{
	long long integer = 0;
	rook_float divider = (rook_float)1;
	rook_float number;

	for (; *state->json_it >= '0' && *state->json_it <= '9'; ++state->json_it)
	{
		integer = integer * 10 + *state->json_it - '0';
	}

	if (!advance_if(state, '.'))
	{
		return rook_obj_create_number((rook_float)integer);
	}

	number = (rook_float)integer;

	for (; *state->json_it >= '0' && *state->json_it <= '9'; ++state->json_it)
	{
		number += (*state->json_it - '0') * (divider /= (rook_float)10);
	}

	return rook_obj_create_number(number);
}

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus
