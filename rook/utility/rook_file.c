#include "rook_file.h"

#include "rook_string.h"
#include "rook_malloc.h"

#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

static FILE* openfile(const char* p, const char* m)
{
#ifdef _WIN32
	FILE* f = 0;
	fopen_s(&f, p, m);
	return f;
#else
	return fopen(p, m);
#endif // _WIN32
}

int rook_read_file(rook_cstring path, rook_string* content)
{
	char p[FILENAME_MAX];
	char* c = 0;
	FILE* f;

	if (path.length >= FILENAME_MAX)
		return 0;

	memcpy(p, path.chars, path.length);
	p[path.length] = '\0';
	
	f = openfile(p, "rb");
	if (!f)
		return 0;

	fseek(f, 0, SEEK_END);
	int bytes = ftell(f);
	rewind(f);

	c = ROOK_ALLOC((size_t)bytes + 1);

	if (fread(c, 1, (size_t)bytes, f) != (size_t)bytes)
	{
		ROOK_FREE(c);
		fclose(f);
		return 0;
	}

	c[bytes] = '\0';

	content->chars = c;
	content->length = bytes;

	fclose(f);
	return 1;
}

int rook_write_file(rook_cstring path, rook_cstring content)
{
	char p[FILENAME_MAX];
	FILE* f;

	if (path.length >= FILENAME_MAX)
		return 0;

	if (content.length <= 0)
		return 0;

	memcpy(p, path.chars, path.length);
	p[path.length] = '\0';

	f = openfile(p, "wb");
	if (!f)
		return 0;

	if (fwrite(content.chars, 1, content.length, f) != (size_t)content.length)
	{
		fclose(f);
		return 0;
	}

	fclose(f);

	return 1;
}

#ifdef __cplusplus
}
#endif // __cplusplus
