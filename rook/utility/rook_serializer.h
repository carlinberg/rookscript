#ifndef ROOK_SERIALIZER
#define ROOK_SERIALIZER

typedef struct rook_string rook_string;
typedef struct rook_cstring rook_cstring;
typedef struct rook_obj rook_obj;
typedef struct rook_allocator rook_allocator;

typedef struct rook_serializer
{
	void(*func)(struct rook_serializer*, void*, int);

	char* buffer;
	char* current;
	int length;
} rook_serializer;

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void rook_serializer_write(rook_serializer* s, void* obj, int bytes);
void rook_serializer_read(rook_serializer* s, void* obj, int bytes);
void rook_serializer_init_write(rook_serializer* s);
void rook_serializer_free_write(rook_serializer* s);
void rook_serializer_init_read(rook_serializer* s, char* buffer, int bytes);
void rook_serialize_obj(rook_serializer* s, rook_obj* obj, rook_allocator* a);
rook_cstring rook_serializer_to_cstr(rook_serializer* s);
rook_string rook_serializer_to_str(rook_serializer* s);

#ifdef __cplusplus
}
#endif // _cplusplus

#define rook_serializer_is_reader(s) ((s)->func == rook_serializer_read)
#define rook_serializer_length(s) ((int)((s)->buffer_current - (s)->buffer_begin))
#define rook_serializer_capacity(s) ((int)((s)->buffer_end - (s)->buffer_begin))
#define rook_serialize_ex(s, o, b) (s)->func((s), (o), (b))
#define rook_serialize(s, o) rook_serialize_ex((s), (o), sizeof(*(o)))

#endif // !ROOK_SERIALIZER
