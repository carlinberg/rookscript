#include "rook_malloc.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <Windows.h>
#include <malloc.h>
#endif // _WIN32

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define HAWK_MAX_ALLOC_DATAS 4096

typedef struct rookalloc_data
{
	void* data;
	size_t bytes;
	const char* file;
	int line;
} rookalloc_data;

static rookalloc_data rookalloc_datas[HAWK_MAX_ALLOC_DATAS];
static rookalloc_data* rookalloc_data_next = rookalloc_datas;

void* rook_alloc(size_t bytes, const char* file, int line)
{
	assert(rookalloc_data_next - rookalloc_datas < HAWK_MAX_ALLOC_DATAS);

	rookalloc_data* data = rookalloc_data_next++;

	data->data = malloc(bytes);
	data->bytes = bytes;
	data->file = file;
	data->line = line;

	return data->data;
}

void* rook_realloc(void* ptr, size_t bytes, const char* file, int line)
{
	if (ptr)
	{
		for (rookalloc_data* it = rookalloc_datas; it < rookalloc_data_next; ++it)
		{
			if (it->data == ptr)
			{
				void* new_ptr = realloc(ptr, bytes);
				assert(new_ptr);
				it->data = new_ptr;
				return new_ptr;
			}
		}

		assert(0 && "calling rook_realloc on unknown ptr");
	}

	return rook_alloc(bytes, file, line);
}

void rook_free(void* ptr)
{
	if (!ptr)
		return;

	for (rookalloc_data* it = rookalloc_datas; it < rookalloc_data_next; ++it)
	{
		if (it->data == ptr)
		{
			free(ptr);
			*it = *--rookalloc_data_next;
			return;
		}
	}

	assert(0 && "calling rook_free on unknown ptr");
}

size_t rookstring_strip_directories(const char* path, char* buffer, size_t buffer_length)
{
	assert(path);
	assert(buffer);
	assert(buffer_length);

	if (!*path)
	{
		*buffer = '\0';
		return 0;
	}

	const char* path_begin = path;
	size_t path_length = 0;

	for (; *path; ++path, ++path_length)
	{
		if (*path == '/')
			path_begin = ++path;
		else if (*path == '\\')
			path_begin = ++path;
	}

	if (path_begin != path)
		path_length = path - path_begin;

	if (buffer_length > path_length)
		buffer_length = path_length;

	memcpy(buffer, path_begin, buffer_length);
	buffer[buffer_length] = '\0';

	return buffer_length;
}

void rook_report_memory_leaks()
{
#ifdef _WIN32
	FILE* fp;
	fopen_s(&fp, "memory_leeks.txt", "w");
#else
	FILE* fp = fopen("memory_leeks.txt", "w");
#endif

	if (!fp)
		return;

	if (rookalloc_datas == rookalloc_data_next)
	{
		fprintf(fp, "Memory leaks: 0");
		fclose(fp);
		return;
	}

	printf("Memory leaks: %d\n\n", (int)(rookalloc_data_next - rookalloc_datas));
	fprintf(fp, "Memory leaks: %d\n\n", (int)(rookalloc_data_next - rookalloc_datas));

	for (rookalloc_data* it = rookalloc_datas; it < rookalloc_data_next; ++it)
	{
		char buffer[32];
		rookstring_strip_directories(it->file, buffer, sizeof(buffer));
		printf("Bytes: %d. Line: %d. File: %s\n", (int)it->bytes, it->line, buffer);
		fprintf(fp, "Bytes: %d. Line: %d. File: %s\n", (int)it->bytes, it->line, buffer);
	}

	fclose(fp);

#ifdef _WIN32
	char buffer2[32];
	sprintf_s(buffer2, sizeof(buffer2), "\nMemory leaks: %d", (int)(rookalloc_data_next - rookalloc_datas));
	MessageBoxA(0, buffer2, "Memory leaks", MB_OK);
#endif // _WIN32
}

#ifdef __cplusplus
}
#endif // __cplusplus
