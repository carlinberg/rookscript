#ifndef ROOK_MALLOC
#define ROOK_MALLOC

#define ROOK_CHECK_MEMORY

#ifdef ROOK_CHECK_MEMORY

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus
#include "stddef.h"

void* rook_alloc(size_t bytes, const char* file, int line);
void* rook_realloc(void* ptr, size_t bytes, const char* file, int line);
void rook_free(void* ptr);
void rook_report_memory_leaks();

#ifdef __cplusplus
}
#endif // __cplusplus

#define ROOK_ALLOC(b) rook_alloc(b, __FILE__, __LINE__)
#define ROOK_REALLOC(p, b) rook_realloc(p, b, __FILE__, __LINE__)
#define ROOK_FREE(p) rook_free(p)

#else

#include <stdlib.h>

#define ROOK_ALLOC(b) malloc(b)
#define ROOK_REALLOC(p, b) realloc(p, b)
#define ROOK_FREE(p) free(p)

#endif //ROOK_CHECK_MEMORY

#endif // !ROOK_MALLOC
