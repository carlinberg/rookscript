#include "rook_allocators.h"
#include "rook_malloc.h"

#include <string.h>
#include <assert.h>


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define MEMORY_BYTE_COUNT 512

typedef struct rook_memory_list
{
	int bytes_used;
	int bytes_freed;
	struct rook_memory_list* next;
	char data[1];
} rook_memory_list;

static int round_to_8(int number)
{
	if (number <= 8)
		return 8;

	int mod = number % 8;
	if (mod)
		return number + (8 - mod);

	return number;
}

#define round_to(nr, round) ((nr) <= (round) ? (round) : (nr) % (round) ? (nr) + ((round) - (nr) % (round)) : (nr)) 

void rook_allocator_init(rook_allocator* a)
{
	a->memory = 0;
	a->memory_last = 0;
}

void rook_allocator_free(rook_allocator* a)
{
	rook_memory_list* it = a->memory;
	while (it)
	{
		rook_memory_list* next = it->next;
		ROOK_FREE(it);
		it = next;
	}

	a->memory = 0;
	a->memory_last = 0;
}

void* rook_allocator_alloc(rook_allocator* a, int bytes)
{
	int bytes_to_allocate = MEMORY_BYTE_COUNT;

	bytes = round_to_8(bytes);

	if (bytes > MEMORY_BYTE_COUNT)
		bytes_to_allocate = bytes;

	if (a->memory == 0)
	{
		a->memory = ROOK_ALLOC(offsetof(rook_memory_list, data) + bytes_to_allocate);
		assert(a->memory);
		a->memory_last = a->memory;
		a->memory_last->bytes_used = 0;
		a->memory_last->bytes_freed = 0;
		a->memory_last->next = 0;
	}
	else if (a->memory_last->bytes_used + bytes > bytes_to_allocate)
	{
		a->memory_last->next = ROOK_ALLOC(offsetof(rook_memory_list, data) + bytes_to_allocate);
		assert(a->memory_last->next);
		a->memory_last = a->memory_last->next;
		a->memory_last->bytes_used = 0;
		a->memory_last->bytes_freed = 0;
		a->memory_last->next = 0;
	}

	void* new_memory = a->memory_last->data + a->memory_last->bytes_used;
	a->memory_last->bytes_used += bytes;
	memset(new_memory, 0, bytes);
	return new_memory;
}

void rook_allocator_free_obj(rook_allocator* a, void* obj, int bytes)
{
	bytes = round_to_8(bytes);

	rook_memory_list* previous_link = 0;
	rook_memory_list* link = a->memory;

	while (link)
	{
		if ((char*)obj >= link->data && (char*)obj < link->data + link->bytes_used)
		{
			if (!link->next && (char*)obj == link->data + link->bytes_used - bytes)
			{
				link->bytes_used -= bytes;
			}
			else
			{
				link->bytes_freed += bytes;
				if (link->bytes_freed == link->bytes_used)
				{
					if (link->next == 0)
					{
						link->bytes_used = 0;
						link->bytes_freed = 0;
					}
					else
					{
						if (previous_link)
						{
							previous_link->next = link->next;
						}
						else
						{
							assert(link == a->memory);
							a->memory = link->next;
						}

						ROOK_FREE(link);
					}
				}
			}

			return;
		}

		previous_link = link;
		link = link->next;
	}

	assert(0 && "unreachable");
}

#ifdef __cplusplus
}
#endif // __cplusplus
