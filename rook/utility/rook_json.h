#ifndef ROOK_JSON
#define ROOK_JSON

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_obj rook_obj;
typedef struct rook_string rook_string;
typedef struct rook_cstring rook_cstring;
typedef struct rook_error rook_error;
typedef struct rook_allocator rook_allocator;

rook_string rook_tojson(rook_obj obj);
rook_obj rook_fromjson(rook_cstring json, rook_allocator* alloc, rook_error** errors);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_JSON
