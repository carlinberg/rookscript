#include "rook_standard_library.h"

#include <utility/rook_file.h>
#include <utility/rook_error.h>
#include <utility/rook_json.h>

#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_object.h>
#include <core/runtime/rook_obj_string.h>
#include <core/runtime/rook_array.h>
#include <core/runtime/rook_map.h>
#include <core/runtime/rook_func.h>

#include <stdio.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define add_error(vm, l, c, fmt, ...) rook_errorlist_add_errorf(&(vm)->errors, (l), (c), (fmt), __VA_ARGS__), -1

static void print_value(rook_obj obj);

static int print(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int error(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int assert_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int get_callstack(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int type(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int tostring(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int tonumber(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int clone(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int dofile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int dostring(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int compile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int get_global(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int set_global(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int add_global(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int clock_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int readfile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int writefile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int tojson(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int fromjson(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int getlocals(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int serialize_func(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int deserialize_func(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int register_tag(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int register_tag_extension(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int create_tagged_obj(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int get_tag(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);

void rook_lib_register_general(rook_vm* vm)
{
	rook_lib_register_func(print);
	rook_lib_register_func(error);
	rook_lib_register_func_ex(assert_, "assert");
	rook_lib_register_func(get_callstack);
	rook_lib_register_func(type);
	rook_lib_register_func(tostring);
	rook_lib_register_func(tonumber);
	rook_lib_register_func(clone);
	rook_lib_register_func(dofile);
	rook_lib_register_func(dostring);
	rook_lib_register_func(compile);
	rook_lib_register_func(get_global);
	rook_lib_register_func(set_global);
	rook_lib_register_func(add_global);
	rook_lib_register_func_ex(clock_, "clock");
	rook_lib_register_func(readfile);
	rook_lib_register_func(writefile);
	rook_lib_register_func(tojson);
	rook_lib_register_func(fromjson);
	rook_lib_register_func(getlocals);
	rook_lib_register_func(serialize_func);
	rook_lib_register_func(deserialize_func);
	rook_lib_register_func(register_tag);
	rook_lib_register_func(register_tag_extension);
	rook_lib_register_func(create_tagged_obj);
	rook_lib_register_func(get_tag);
}

static void print_value(rook_obj obj)
{
	switch (rook_obj_get_type(obj))
	{
	case rook_obj_type_null:
		printf("null");
		break;
	case rook_obj_type_boolean:
		if (obj.as.boolean)
			printf("true");
		else
			printf("false");
		break;
	case rook_obj_type_number:
		printf("%f", (float)obj.as.number);
		break;
	case rook_obj_type_string:
		printf("%s", obj.as.string->chars);
		break;
	case rook_obj_type_userdata:
		printf("[Userdata] %p", obj.as.userdata);
		break;
	case rook_obj_type_array:
		if (obj.as.array->size == 0)
		{
			printf("[]");
		}
		else
		{
			int i = 0;
			printf("[ ");
			for (; i < obj.as.array->size - 1; ++i)
			{
				print_value(obj.as.array->array[i]);
				printf(", ");
			}
			print_value(obj.as.array->array[i]);
			printf(" ]");
		}
		break;
	case rook_obj_type_map:
		if (obj.as.map->size == 0)
		{
			printf("{}");
		}
		else
		{
			int i = 0;
			int s = 0;
			printf("{ ");
			for (; i < obj.as.map->capacity && s < obj.as.map->size; ++i)
			{
				if (rook_map_entry_exists(obj.as.map, i))
				{
					printf("%s: ", rook_map_get_key(obj.as.map, i).chars);
					print_value(rook_map_get_value(obj.as.map, i));

					if (s < obj.as.map->size - 1)
						printf(", ");
					else
						break;
					
					++s;
				}
			}

			printf(" }");
		}
		break;
	case rook_obj_type_func:
		printf("[Function]");
		break;
	case rook_obj_type_cfunc:
		printf("[C function]");
		break;
	case rook_obj_type_bound_func:
		printf("[Bound func]");
		break;
	case rook_obj_type_closure:
		printf("[Closure]");
		break;
	default:
		printf("[Error type]");
		break;
	}
}

static int print(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'print' expects at least 1 argument, got ", argc), -1;

	for (int i = 0; i < argc; ++i)
	{
		print_value(args[i]);
		printf(" ");
	}

	printf("\n");
	return 0;
}

static int error(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc > 0)
	{
		rook_error error;
		error.line = vm->interpreter.current_line;
		error.column = vm->interpreter.current_column;

		print(vm, userdata, this_, args, argc, rets);

		rook_string_init(&error.msg, 0);

		for (int i = 0; i < argc; ++i)
		{
			switch (rook_obj_get_type(args[i]))
			{
			case rook_obj_type_null:
				rook_string_append_literal(&error.msg, "null");
				break;
			case rook_obj_type_boolean:
				if (args[i].as.boolean)
					rook_string_append_literal(&error.msg, "true");
				else
					rook_string_append_literal(&error.msg, "false");
				break;
			case rook_obj_type_number:
				rook_string_append_number(&error.msg, args[i].as.number);
				break;
			case rook_obj_type_string:
				rook_string_append_chars(&error.msg, args[i].as.string->chars, args[i].as.string->length);
				break;
			default:
				rook_string_append_literal(&error.msg, "[");
				rook_string_appendc(&error.msg, rook_cstring_from_chars(rook_obj_type_names[rook_obj_get_type(args[i])]));
				rook_string_append_literal(&error.msg, "]");
				break;
			}

			rook_string_append_char(&error.msg, ' ');
		}

		sb_push(vm->errors, error);
	}

	return -1;
}

static int assert_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function assert expects at least 1 argument."), -1;

	if (rook_obj_isfalsey(&args[0]))
		return error(vm, userdata, this_, args + 1, argc - 1, rets);

	rook_env_push_result(*rets, args[0]);
	return 1;
}

static int get_callstack(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
#ifdef ROOK_DEBUG
	rook_call_info* ci = vm->interpreter.current_call_info;
	rook_obj ret;
	rook_string str;

	rook_string_init(&str, 0);

	while (ci)
	{
		rook_string_append_literal(&str, "[");
		rook_string_appendc(&str, ci->file);
		rook_string_append_literal(&str, "] ");
		rook_string_appendc(&str, ci->function);
		rook_string_append_literal(&str, ": ");
		rook_string_append_integer(&str, ci->call_site_line);
		rook_string_append_literal(&str, "\n");

		ci = ci->previous;
	}

	ret = rook_vm_create_string(vm, rook_cstring_from_string(str));
	rook_env_push_result(*rets, ret);
	rook_string_free(&str);

#else

	rook_obj ret = rook_vm_create_string(vm, rook_cstring_from_literal("get_callstack only available in debug mode"));
	rook_env_push_result(*rets, ret);

#endif // ROOK_DEBUG
	
	return 1;
}

static int type(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function type expects 1 argument."), -1;

	rook_obj type_name = rook_vm_create_string(vm, rook_cstring_from_chars(rook_obj_type_names[rook_obj_get_type(args[0])]));

	rook_env_push_result(*rets, type_name);
	return 1;
}

static int tostring(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj str = { 0 };
	char buffer[1024] = { 0 };
	int length = 0;

	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function tostring expects 1 argument."), -1;

	if (!rook_obj_to_string(args[0], buffer, sizeof(buffer), &length))
	{
		 str = rook_vm_create_string(vm, rook_cstring_from_literal(""));
		 rook_obj_string_allocate(str.as.string, length);

		 if (!rook_obj_to_string(args[0], str.as.string->chars, str.as.string->length, &length))
			 return rook_vm_runtime_errorf(vm, "%s", "Unknown error in tostring, not able to stringify object."), -1;

		 str.as.string->chars[str.as.string->length] = '\0';
	}
	else
	{
		str = rook_vm_create_string(vm, rook_cstring_from_chars_ex(buffer, length));
	}

	rook_env_push_result(*rets, str);
	return 1;
}

static int tonumber(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function tonumber expects 1 argument."), -1;

	rook_obj number;

	switch (rook_obj_get_type(args[0]))
	{
	case rook_obj_type_boolean:
		number = rook_obj_create_number((rook_float)args[0].as.boolean);
		break;
	case rook_obj_type_number:
		number = args[0];
		break;
	case rook_obj_type_string:
		number = rook_obj_create_number(rook_float_from_string(rook_cstring_from_chars_ex(args[0].as.string->chars, args[0].as.string->length)));
		break;
	case rook_obj_type_userdata:
		number = rook_obj_create_number((rook_float)(size_t)args[0].as.userdata);
		break;
	default:
		return -1;
	}

	rook_env_push_result(*rets, number);
	return 1;
}

static int clone(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj c;

	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'clone' expects 1 argument (obj to clone)."), -1;

	c = rook_obj_clone(&vm->runtime_allocator, args[0]);

	rook_env_push_result(*rets, c);

	return 1;
}

static int dofile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_cstring path;
	int result_count;
	int stack_base = rook_env_stack_size(&vm->interpreter.env);
	int stack_top;

	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'dofile' expects 1 argument (file path)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'dofile' expects argument of type string (file path)."), -1;

	rook_cstring_init(&path, args[0].as.string->chars, args[0].as.string->length);

	rook_cstring prev_file = vm->current_file;
	rook_cstring prev_code = vm->current_code;

	result_count = rook_vm_run_file(vm, path);
	if (result_count == -1)
		return -1;

	vm->current_file = prev_file;
	vm->current_code = prev_code;

	stack_top = rook_env_stack_size(&vm->interpreter.env);

	for (int i = 0; i < result_count; ++i)
	{
		int idx = stack_top - result_count + i;
		rook_obj result = vm->interpreter.env.stack[idx];
		rook_obj_incref(&result);
		rook_env_push_result(*rets, result);
	}

	rook_env_shrink_stack(&vm->interpreter.env, result_count);
	rook_env_pop_many(&vm->interpreter.env, rook_env_stack_size(&vm->interpreter.env) - stack_base);

	return result_count;
}

static int dostring(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_cstring code;
	int result_count;
	int stack_base = rook_env_stack_size(&vm->interpreter.env);
	int stack_top;

	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'dostring' expects 1 argument (code string)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'dostring' expects argument of type string (code string), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_cstring_init(&code, args[0].as.string->chars, args[0].as.string->length);

	rook_cstring prev_file = vm->current_file;
	rook_cstring prev_code = vm->current_code;

	result_count = rook_vm_run_string(vm, code, code);
	if (result_count == -1)
		return -1;

	vm->current_file = prev_file;
	vm->current_code = prev_code;

	stack_top = rook_env_stack_size(&vm->interpreter.env);

	for (int i = 0; i < result_count; ++i)
	{
		int idx = stack_top - result_count + i;
		rook_obj result = vm->interpreter.env.stack[idx];
		rook_obj_incref(&result);
		rook_env_push_result(*rets, result);
	}

	rook_env_shrink_stack(&vm->interpreter.env, result_count);
	rook_env_pop_many(&vm->interpreter.env, rook_env_stack_size(&vm->interpreter.env) - stack_base);

	return result_count;
}

static int compile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_string code;
	rook_obj program;

	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'parse' expects 1 argument (code string)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'parse' expects argument of type string (code string), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_string_init_ex(&code, args[0].as.string->chars, args[0].as.string->length);

	program = rook_vm_compile_string(vm, code, rook_cstring_from_string(code));
	if (!rook_obj_isfunc(program))
		return -1;

	rook_env_push_result(*rets, program);

	return 1;
}

static int get_global(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj global;
	rook_cstring name;

	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'get_global' expects 1 argument (name of global)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'get_global' expects argument of type string (name of global), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_cstring_init(&name, args[0].as.string->chars, args[0].as.string->length);

	if (!rook_vm_get_global(vm, name, &global))
		return rook_vm_runtime_errorf(vm, "%s%S", "Could not find global with name", name), -1;

	rook_obj_incref(&global);
	rook_env_push_result(*rets, global);

	return 1;
}

static int set_global(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_cstring name;

	if (argc < 2)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'set_global' expects 2 argument (name and new value of global)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'set_global' expects first argument of type string (name of global), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_cstring_init(&name, args[0].as.string->chars, args[0].as.string->length);

	if (!rook_vm_set_global(vm, name, args[1]))
		return rook_vm_runtime_errorf(vm, "%s%S", "Could not find global with name", name), -1;
	
	rook_obj_incref(&args[1]);

	return 0;
}

static int add_global(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_cstring name;

	if (argc < 2)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'add_global' expects 2 argument (name and new value of global)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'add_global' expects first argument of type string (name of global), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_cstring_init(&name, args[0].as.string->chars, args[0].as.string->length);

	rook_obj_incref(&args[1]);
	rook_vm_register_global(vm, name, args[1]);

	return 0;
}

static int clock_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_float t = (rook_float)clock() / (rook_float)CLOCKS_PER_SEC;
	rook_env_push_result(*rets, rook_vm_create_number(t));
	return 1;
}

static int readfile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_string content;
	rook_obj res;
	
	if (argc != 1)
		return rook_vm_runtime_error(vm, rook_cstring_from_literal("Function 'readfile' expects 1 argument (path).")), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "Function 'readfile' expects argument of type string (path), got argument of type %s", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	if (!rook_read_file(rook_cstring_from_chars_ex(args[0].as.string->chars, args[0].as.string->length), &content))
		return rook_vm_runtime_errorf(vm, "Error in 'readfile', couldn't open file '%s'", args[0].as.string->chars), -1;

	res = rook_vm_create_string(vm, rook_cstring_from_string(content));
	rook_string_free(&content);

	rook_env_push_result(*rets, res);
	return 1;
}

static int writefile(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_cstring content;

	if (argc != 2)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'writefile' expects 2 arguments (path and new content of file)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'writefile' expects first argument of type string (path), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;
	if (!rook_obj_isstring(args[1]))
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'writefile' expects second argument of type string (file content), got argument of type", rook_obj_type_names[rook_obj_get_type(args[1])]), -1;

	content = rook_cstring_from_chars_ex(args[1].as.string->chars, args[1].as.string->length);

	if (!rook_write_file(rook_cstring_from_chars_ex(args[0].as.string->chars, args[0].as.string->length), content))
		return rook_vm_runtime_errorf(vm, "Error in 'writefile', couldn't write to file '%s'", args[0].as.string->chars), -1;

	return 0;
}

static int tojson(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_string json_str;
	rook_obj res;

	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'tojson' expects 1 argument (obj)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_map)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'tojson' expects argument of type map (obj), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	json_str = rook_tojson(args[0]);

	res = rook_vm_create_string(vm, rook_cstring_from_string(json_str));
	rook_string_free(&json_str);

	rook_env_push_result(*rets, res);
	return 1;
}

static int fromjson(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj res;

	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s", "Function 'fromjson' expects 1 argument (obj)."), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'fromjson' expects argument of type string (json), got argument of type", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_vm_clear_errors(vm);

	res = rook_fromjson(rook_cstring_from_chars_ex(args[0].as.string->chars, args[0].as.string->length), &vm->runtime_allocator, &vm->errors);

	if (sb_count(vm->errors) > 0)
		return -1;

	rook_env_push_result(*rets, res);
	return 1;
}

static int getlocals(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_bytecode_chunk* chunk = sb_last(vm->interpreter.chunk_stack);
	int ip_number = (int)(uptr)(vm->interpreter.ip - chunk->code);
	rook_obj result = rook_vm_create_map(vm, 0);
	rook_obj obj;

	for (int i = 0; i < sb_count(chunk->local_infos); ++i)
	{
		if (chunk->local_infos[i].pc_start <= ip_number && chunk->local_infos[i].pc_end >= ip_number)
		{
			obj = vm->interpreter.env.stack[chunk->local_infos[i].stack_idx + sb_last(vm->interpreter.callframe_stack_base_stack)];
			rook_obj_incref(&obj);
			rook_map_set(result.as.map, chunk->local_infos[i].name.chars, chunk->local_infos[i].name.length, obj, &vm->runtime_allocator);
		}
	}

	rook_env_push_result(*rets, result);
	return 1;
}

static int serialize_func(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "Function 'serialize_func' expects 1 argument (of type func), got %d.", argc), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_func)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'serialize_func' expects argument of type func, got argument of type ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_string result = { 0 };
	if (!rook_func_serialize(args[0].as.func, &result, &vm->errors))
		return rook_vm_runtime_errorl(vm, "Function 'serialize_func' failed"), -1;

	rook_obj result_obj = rook_vm_create_string(vm, rook_cstring_from_string(result));
	rook_string_free(&result);
	rook_env_push_result(*rets, result_obj);
	return 1;
}

static int deserialize_func(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "Function 'deserialize_func' expects 1 argument (of type string), got %d.", argc), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'serialize_func' expects argument of type string (func_data), got argument of type ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_string func_data = { 0 };
	func_data.chars = args[0].as.string->chars;
	func_data.length = args[0].as.string->length;

	rook_obj func_obj = rook_obj_create(&vm->runtime_allocator, rook_obj_type_func);
	rook_func_init_empty(func_obj.as.func);

	if (!rook_func_deserialize(&func_data, func_obj.as.func, &vm->errors, &vm->runtime_allocator))
		return rook_vm_runtime_errorl(vm, "Function 'deserialize_func' failed"), -1;

	rook_env_push_result(*rets, func_obj);
	return 1;
}

static int register_tag(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "Function 'register_tag' expects 1 argument (of type string), got %d.", argc), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'register_tag' expects argument of type string (json), got argument of type ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	int tag = rook_vm_register_tag(vm, rook_cstring_from_chars_ex(args[0].as.string->chars, args[0].as.string->length));

	rook_obj res = rook_vm_create_number((rook_float)tag);
	rook_env_push_result(*rets, res);

	return 1;
}

static int register_tag_extension(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 3)
		return rook_vm_runtime_errorf(vm, "Function 'register_tag_extension' expects 3 arguments (1st of type 'number', 2nd of type 'string', 3rd or type 'function'), got %d.", argc), -1;
	if (rook_obj_get_type(args[0]) != rook_obj_type_number)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'register_tag_extension' expects 1st argument of type 'number', got argument of type ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;
	if (rook_obj_get_type(args[1]) != rook_obj_type_string)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'register_tag_extension' expects 2nd argument of type 'string', got argument of type ", rook_obj_type_names[rook_obj_get_type(args[1])]), -1;
	if (!rook_obj_iscallable(args[2]))
		return rook_vm_runtime_errorf(vm, "Function 'register_tag_extension' expects 3rd argument of type 'function', got argument of type '%s'", rook_obj_type_names[rook_obj_get_type(args[2])]), -1;

	int tag = (int)args[0].as.number;
	if (tag < 0 || tag >= sb_count(vm->interpreter.tag_names))
		return rook_vm_runtime_errorf(vm, "Invalid tag (%d) passed to 'register_tag_extension'", tag), -1;

	rook_obj_string* func_name = args[1].as.string;
	rook_obj func = args[2];

	rook_obj_incref(&func);

	rook_vm_register_tag_extension(vm, tag, rook_cstring_from_chars_ex(func_name->chars, func_name->length), func);

	return 0;
}

static int create_tagged_obj(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 2)
		return rook_vm_runtime_errorf(vm, "Function 'create_tagged_obj' expects 2 argument (first of any type, second of type 'number'), got %d.", argc), -1;
	if (rook_obj_get_type(args[1]) != rook_obj_type_number)
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'create_tagged_obj' expects 2nd argument of type 'number', got argument of type ", rook_obj_type_names[rook_obj_get_type(args[1])]), -1;

	int tag = (int)args[1].as.number;
	rook_obj_set_tag(args[0], tag);
	rook_obj_incref(&args[0]);
	rook_env_push_result(*rets, args[0]);

	return 1;
}

static int get_tag(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "Function 'get_tag' expects 1 argument (of any type), got %d.", argc), -1;

	int tag = rook_obj_get_tag(args[0]);
	rook_obj res = rook_vm_create_number((rook_float)tag);
	rook_env_push_result(*rets, res);

	return 1;
}
