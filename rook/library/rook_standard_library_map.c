#include "rook_standard_library.h"

#include <utility/rook_error.h>

#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_object.h>
#include <core/runtime/rook_map.h>
#include <core/runtime/rook_obj_string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define add_error(vm, l, c, fmt, ...) rook_errorlist_add_errorf(&(vm)->errors, (l), (c), (fmt), __VA_ARGS__), -1

static int count(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int erase(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int find(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int haskey(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int hasvalue(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int clear(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int empty(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);

void rook_lib_register_map(rook_vm* vm)
{
	rook_lib_register_extension(count, rook_obj_type_map);
	rook_lib_register_extension(erase, rook_obj_type_map);
	rook_lib_register_extension(find, rook_obj_type_map);
	rook_lib_register_extension(haskey, rook_obj_type_map);
	rook_lib_register_extension(hasvalue, rook_obj_type_map);
	rook_lib_register_extension(clear, rook_obj_type_map);
	rook_lib_register_extension(empty, rook_obj_type_map);
}

static int count(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_env_push_result(*rets, rook_vm_create_number((rook_float)this_->as.map->size));
	return 1;
}

static int erase(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'map.erase' expects 1 argument (key), got ", argc), -1;
	if (!rook_obj_isstring(args[0]))
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'map.erase' expects argument (key) to be string, got ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_map_erase(this_->as.map, args[0].as.string->chars, args[0].as.string->length, &vm->runtime_allocator);

	return 0;
}

static int find(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj value;

	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'map.find' expects 1 argument (key), got ", argc), -1;
	if (!rook_obj_isstring(args[0]))
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'map.find' expects argument (key) to be string, got ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	if (!rook_map_get(this_->as.map, args[0].as.string->chars, args[0].as.string->length, &value))
		value = rook_vm_create_null();

	rook_env_push_result(*rets, value);

	return 1;
}

static int haskey(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj value;

	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'map.haskey' expects 1 argument (key), got ", argc), -1;
	if (!rook_obj_isstring(args[0]))
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'map.haskey' expects argument (key) to be string, got ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	value.as.boolean = rook_map_get(this_->as.map, args[0].as.string->chars, args[0].as.string->length, 0);
	value.type_and_tag = rook_obj_type_boolean;

	rook_env_push_result(*rets, value);

	return 1;
}

static int hasvalue(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj value = rook_vm_create_boolean(0);

	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'map.hasvalue' expects 1 argument (value), got ", argc), -1;

	for (int i = 0; i < this_->as.map->capacity; ++i)
	{
		if (rook_map_entry_exists(this_->as.map, i))
		{
			rook_obj v = rook_map_get_value(this_->as.map, i);
			if (rook_obj_equal(args[0], v))
			{
				value.as.boolean = 1;
				break;
			}
		}
	}

	rook_env_push_result(*rets, value);

	return 1;
}

static int clear(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_map_clear(this_->as.map, &vm->runtime_allocator);
	return 0;
}

static int empty(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_env_push_result(*rets, rook_vm_create_boolean(this_->as.map->size == 0));
	return 1;
}

#ifdef __cplusplus
}
#endif // __cplusplus
