#include "rook_standard_library.h"

#include <utility/rook_error.h>

#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_object.h>
#include <core/runtime/rook_array.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

static int push(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int insert(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int erase(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int count(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int sort(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int clear(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int empty(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);

void rook_lib_register_array(rook_vm* vm)
{
	rook_lib_register_extension(push, rook_obj_type_array);
	rook_lib_register_extension(insert, rook_obj_type_array);
	rook_lib_register_extension(erase, rook_obj_type_array);
	rook_lib_register_extension(count, rook_obj_type_array);
	rook_lib_register_extension(sort, rook_obj_type_array);
	rook_lib_register_extension(clear, rook_obj_type_array);
	rook_lib_register_extension(empty, rook_obj_type_array);
}

static int push(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc < 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'array.push' expects at least 1 argument, got ", argc), -1;

	for (int i = 0; i < argc; ++i)
		rook_obj_incref(&args[i]);

	rook_array_push_many(this_->as.array, args, argc);

	return 0;
}

static int insert(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 2)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'array.insert' expects 2 arguments (index and new element), got ", argc), -1;
	if (!rook_obj_isnumber(args[0]))
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'array.insert' expects first argument (index) to be number, got ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_obj_incref(&args[1]);
	rook_array_insert(this_->as.array, (int)args[0].as.number, &args[1]);

	return 0;
}

static int erase(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'array.erase' expects 1 argument (index), got ", argc), -1;
	if (!rook_obj_isnumber(args[0]))
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'array.erase' expects argument (index) to be number, got ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	rook_array_erase(this_->as.array, (int)args[0].as.number, &vm->runtime_allocator);

	return 0;
}

static int count(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_env_push_result(*rets, rook_vm_create_number((rook_float)this_->as.array->size));
	return 1;
}

typedef struct
{
	rook_vm* vm;
	rook_obj* func;
} compare_state;

static int comparer(void* s, rook_obj* l, rook_obj* r)
{
	compare_state* cs = (compare_state*)s;
	rook_env* env = &cs->vm->interpreter.env;
	int stack_size = rook_env_stack_size(env);

	rook_obj args[2] = { *l, *r };

	int results = rook_vm_run_obj_args(cs->vm, *cs->func, 2, args);
	if (results < 1)
		return rook_vm_runtime_errorf(cs->vm, "%s", "Comparer in 'array.sort' must return one result (boolean)"), -1;

	int result = rook_obj_istruthy(&env->stack[stack_size]);
	rook_env_pop_many(env, rook_env_stack_size(env) - stack_size);
	return result;
}

static int sort(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 1)
		return rook_vm_runtime_errorf(vm, "%s%d", "Function 'array.sort' expects 1 argument (comparer), got ", argc), -1;
	if (!rook_obj_iscallable(args[0]))
		return rook_vm_runtime_errorf(vm, "%s%s", "Function 'array.sort' expects argument (comparer) to be function, got ", rook_obj_type_names[rook_obj_get_type(args[0])]), -1;

	compare_state state;
	state.vm = vm;
	state.func = &args[0];
	rook_array_sort(this_->as.array, comparer, &state);

	if (sb_count(vm->errors) > 0)
		return -1;

	return 0;
}

static int clear(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_array_clear(this_->as.array, &vm->runtime_allocator);
	return 0;
}

static int empty(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_env_push_result(*rets, rook_vm_create_boolean(this_->as.array->size == 0));
	return 1;
}

#ifdef __cplusplus
}
#endif // __cplusplus
