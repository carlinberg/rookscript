#ifndef ROOK_STANDARD_LIBRARY
#define ROOK_STANDARD_LIBRARY

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct rook_vm rook_vm;

void rook_lib_register_general(rook_vm* vm);
void rook_lib_register_string(rook_vm* vm);
void rook_lib_register_array(rook_vm* vm);
void rook_lib_register_map(rook_vm* vm);
void rook_lib_register_math(rook_vm* vm);

#define rook_lib_register_func(f) rook_vm_register_global(vm, rook_cstring_from_literal(#f), rook_vm_create_cfunc(vm, f, rook_cstring_from_literal(#f)))
#define rook_lib_register_func_ex(f, n) rook_vm_register_global(vm, rook_cstring_from_literal(n), rook_vm_create_cfunc(vm, f, rook_cstring_from_literal(n)))
#define rook_lib_register_extension(f, t) rook_vm_register_extension(vm, t, rook_cstring_from_literal(#f), f)

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !ROOK_STANDARD_LIBRARY
