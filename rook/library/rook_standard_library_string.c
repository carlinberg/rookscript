#include "rook_standard_library.h"

#include <utility/rook_number_types.h>
#include <utility/rook_error.h>
#include <utility/rook_malloc.h>

#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_object.h>
#include <core/runtime/rook_array.h>
#include <core/runtime/rook_obj_string.h>

#include <string.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define add_error(vm, fmt, ...) rook_vm_runtime_errorf((vm), (fmt), __VA_ARGS__), -1

static int substring(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int reverse(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int empty(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int clone(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int find(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int rfind(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int findall(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int contains(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int remove(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int insert(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int replace(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int join(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int split(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int startswith(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int endswith(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int toupper(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int tolower(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int length(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);

void rook_lib_register_string(rook_vm* vm)
{
	rook_lib_register_extension(substring, rook_obj_type_string);
	rook_lib_register_extension(reverse, rook_obj_type_string);
	rook_lib_register_extension(empty, rook_obj_type_string);
	rook_lib_register_extension(clone, rook_obj_type_string);
	rook_lib_register_extension(find, rook_obj_type_string);
	rook_lib_register_extension(rfind, rook_obj_type_string);
	rook_lib_register_extension(findall, rook_obj_type_string);
	rook_lib_register_extension(contains, rook_obj_type_string);
	rook_lib_register_extension(remove, rook_obj_type_string);
	rook_lib_register_extension(insert, rook_obj_type_string);
	rook_lib_register_extension(replace, rook_obj_type_string);
	rook_lib_register_extension(join, rook_obj_type_array);
	rook_lib_register_extension(split, rook_obj_type_string);
	rook_lib_register_extension(startswith, rook_obj_type_string);
	rook_lib_register_extension(endswith, rook_obj_type_string);
	rook_lib_register_extension(toupper, rook_obj_type_string);
	rook_lib_register_extension(tolower, rook_obj_type_string);
	rook_lib_register_extension(length, rook_obj_type_string);
}

static int substring(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	int start;
	int length;
	rook_obj new_str;

	if (argc < 1)
		return add_error(vm, "string.substring expects at least 1 argument (start index), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "string.substring expects 1st argument (start index) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
	if (argc > 1 && !rook_obj_isnumber(args[1]))
		return add_error(vm, "string.substring expects 2nd argument (length) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[1])]);

	start = (int)args[0].as.number;
	length = argc > 1 ? (int)args[1].as.number : this_->as.string->length - start;

	if (start < 0 || start >= this_->as.string->length || start + length > this_->as.string->length)
		return add_error(vm, "string.substring indices out of range (start: %d length: %d)", start, length);

	new_str = rook_vm_create_string(vm, rook_cstring_from_chars_ex(this_->as.string->chars + start, length));

	rook_env_push_result(*rets, new_str);
	return 1;
}

static int reverse(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	int i;
	int j;
	rook_obj new_str;

	new_str = rook_obj_create(&vm->runtime_allocator, rook_obj_type_string);
	rook_obj_string_allocate(new_str.as.string, this_->as.string->length);

	for (i = this_->as.string->length - 1, j = 0; i >= 0; --i, ++j)
		new_str.as.string->chars[j] = this_->as.string->chars[i];

	new_str.as.string->chars[new_str.as.string->length] = '\0';

	rook_env_push_result(*rets, new_str);
	return 1;
}

static int empty(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_env_push_result(*rets, rook_vm_create_boolean(this_->as.string->length == 0));
	return 1;
}

static int clone(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj new_str = rook_vm_create_string(vm, rook_cstring_from_chars_ex(this_->as.string->chars, this_->as.string->length));
	rook_env_push_result(*rets, new_str);
	return 1;
}

static int find(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	const char* me;
	int length;
	const char* other;
	int other_length;
	int i;
	int skip = 0;
	i16 table[256];

	if (argc != 1)
		return add_error(vm, "string.find expects 1 argument (string to find), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.find expects 1st argument (string to find) to be string , got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	me = this_->as.string->chars;
	length = this_->as.string->length;
	other = args[0].as.string->chars;
	other_length = args[0].as.string->length;

	if (length > I16_MAX || other_length > I16_MAX)
		return add_error(vm, "string.find only supports strings of max size %d, got %d and %d", I16_MAX, length, other_length);

	for (i = 0; i < 256; ++i)
		table[i] = (i16)other_length;

	for (i = 0; i < other_length; ++i)
		table[other[i]] = (i16)(other_length - 1 - i);

	while (length - skip >= other_length)
	{
		i = other_length - 1;

		while (me[skip + i] == other[i])
		{
			if (i == 0)
			{
				rook_env_push_result(*rets, rook_vm_create_number((rook_float)skip));
				return 1;
			}

			--i;
		}
		
		skip = skip + table[me[skip + other_length - 1]];
	}

	rook_env_push_result(*rets, rook_vm_create_number((rook_float)-1));
	return 1;
}

static int rfind(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	const char* me;
	int length;
	const char* other;
	int other_length;
	int i;
	int skip;
	i16 table[256];

	if (argc != 1)
		return add_error(vm, "string.rfind expects 1 argument (string to find), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.rfind expects 1st argument (string to find) to be string , got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	me = this_->as.string->chars;
	length = this_->as.string->length;
	other = args[0].as.string->chars;
	other_length = args[0].as.string->length;

	if (length > I16_MAX || other_length > I16_MAX)
		return add_error(vm, "string.rfind only supports strings of max size %d, got %d and %d", I16_MAX, length, other_length);

	for (i = 0; i < 256; ++i)
		table[i] = (i16)other_length;

	for (i = 0; i < other_length; ++i)
		table[other[i]] = (i16)(other_length - 1 - i);

	skip = length - other_length;

	while (length >= other_length + skip)
	{
		i = other_length - 1;

		while (me[skip + i] == other[i])
		{
			if (i == 0)
			{
				rook_env_push_result(*rets, rook_vm_create_number((rook_float)skip));
				return 1;
			}

			--i;
		}

		skip -= table[me[skip + other_length - 1]];
	}

	rook_env_push_result(*rets, rook_vm_create_number((rook_float)-1));
	return 1;
}

static int findall(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	const char* me;
	int length;
	const char* other;
	int other_length;
	int i;
	int skip = 0;
	rook_obj indices;
	i16 table[256];

	if (argc != 1)
		return add_error(vm, "string.findall expects 1 argument (string to find), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.findall expects 1st argument (string to find) to be string , got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	me = this_->as.string->chars;
	length = this_->as.string->length;
	other = args[0].as.string->chars;
	other_length = args[0].as.string->length;

	if (length > I16_MAX || other_length > I16_MAX)
		return add_error(vm, "string.findall only supports strings of max size %d, got %d and %d", I16_MAX, length, other_length);

	indices = rook_vm_create_array(vm, 0);

	for (i = 0; i < 256; ++i)
		table[i] = (i16)other_length;

	for (i = 0; i < other_length; ++i)
		table[other[i]] = (i16)(other_length - 1 - i);

	while (length - skip >= other_length)
	{
		i = other_length - 1;

		while (me[skip + i] == other[i])
		{
			if (i == 0)
			{
				rook_array_push(indices.as.array, rook_vm_create_number((rook_float)skip));
				++skip;
				break;
			}

			--i;
		}

		skip++;
		//skip += table[me[skip + other_length - 1]];
	}

	rook_env_push_result(*rets, indices);
	return 1;
}

static int contains(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	const char* me;
	int length;
	const char* other;
	int other_length;
	int result;

	if (argc != 1)
		return add_error(vm, "string.contains expects 1 argument (string to find), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.contains expects 1st argument (string to find) to be string , got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	me = this_->as.string->chars;
	length = this_->as.string->length;
	other = args[0].as.string->chars;
	other_length = args[0].as.string->length;

	if (length > I16_MAX || other_length > I16_MAX)
		return add_error(vm, "string.contains only supports strings of max size %d, got %d and %d", I16_MAX, length, other_length);

	if (find(vm, userdata, this_, args, argc, rets) == -1)
		return -1;

	result = (*rets)[0].as.number != (rook_float)-1;
	(*rets)[0] = rook_vm_create_boolean(result);
	return 1;
}

static int remove(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	const char* me;
	int length;
	int other_length;
	int i;
	int j;
	int k;
	rook_obj indices;
	rook_obj new_string;

	if (argc != 1)
		return add_error(vm, "string.remove expects 1 argument (string to remove), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.remove expects 1st argument (string to remove) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	me = this_->as.string->chars;
	length = this_->as.string->length;
	other_length = args[0].as.string->length;

	if (length > I16_MAX || other_length > I16_MAX)
		return add_error(vm, "string.remove only supports strings of max size %d, got %d and %d", I16_MAX, length, other_length);

	if (other_length == 0)
	{
		new_string = rook_vm_create_string(vm, rook_cstring_from_chars_ex(me, length));
		rook_env_push_result(*rets, new_string);
	}
	else
	{
		if (findall(vm, userdata, this_, args, argc, rets) == -1)
			return -1;

		indices = (*rets)[0];

		if (indices.as.array->size == 0)
		{
			new_string = rook_vm_create_string(vm, rook_cstring_from_chars_ex(me, length));
		}
		else
		{
			new_string = rook_obj_create(&vm->runtime_allocator, rook_obj_type_string);
			rook_obj_string_allocate(new_string.as.string, length - indices.as.array->size * other_length);

			rook_array_push(indices.as.array, rook_vm_create_number((rook_float)length));

			for (i = 0, j = 0, k = 0; i < indices.as.array->size; ++i)
			{
				for (; j < (int)indices.as.array->array[i].as.number; ++j, ++k)
				{
					new_string.as.string->chars[k] = me[j];
				}

				j += other_length;
			}

			new_string.as.string->chars[k] = '\0';
		}

		rook_obj_free(&vm->runtime_allocator, &indices);
		(*rets)[0] = new_string;
	}

	return 1;
}

static int insert(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc != 2)
		return add_error(vm, "string.insert expects 2 arguments (place to insert (number) and string to insert), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "string.insert expects 1st argument (place to insert) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
	if (!rook_obj_isstring(args[1]))
		return add_error(vm, "string.insert expects 2nd argument (string to insert) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[1])]);

	if (!rook_obj_string_insert(this_->as.string, (int)args[0].as.number, args[1].as.string))
		return add_error(vm, "string.insert index out of range. Index: %d, string length: %d", (int)args[0].as.number, args[1].as.string->length);

	return 0;
}

static int replace(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	const char* me;
	const char* insert;
	int length;
	int remove_length;
	int insert_length;
	int i;
	int j;
	int k;
	int l;
	rook_obj indices;
	rook_obj new_string;

	if (argc != 2)
		return add_error(vm, "string.replace expects 2 arguments (string to replace and string to be replaced with), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.replace expects 1st argument (string to replace) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
	if (!rook_obj_isstring(args[1]))
		return add_error(vm, "string.replace expects 2nd argument (string to be replaced with) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[1])]);

	me = this_->as.string->chars;
	length = this_->as.string->length;
	
	remove_length = args[0].as.string->length;

	insert_length = args[1].as.string->length;
	insert = args[1].as.string->chars;

	if (length > I16_MAX || remove_length > I16_MAX)
		return add_error(vm, "string.remove only supports strings of max size %d, got %d and %d", I16_MAX, length, remove_length);

	if (remove_length == 0)
	{
		new_string = rook_vm_create_string(vm, rook_cstring_from_chars_ex(me, length));
		rook_env_push_result(*rets, new_string);
	}
	else
	{
		if (findall(vm, userdata, this_, args, 1, rets) == -1) // Use 1 here since find all will be angry otherwise
			return -1;

		indices = (*rets)[0];

		if (indices.as.array->size == 0)
		{
			new_string = rook_vm_create_string(vm, rook_cstring_from_chars_ex(me, length));
		}
		else
		{
			new_string = rook_obj_create(&vm->runtime_allocator, rook_obj_type_string);

			rook_obj_string_allocate(new_string.as.string, length - (indices.as.array->size * remove_length) + (indices.as.array->size * insert_length));
			rook_array_push(indices.as.array, rook_vm_create_number((rook_float)length));

			for (i = 0, j = 0, k = 0; i < indices.as.array->size; ++i)
			{
				for (; j < (int)indices.as.array->array[i].as.number; ++j, ++k)
				{
					new_string.as.string->chars[k] = me[j];
				}

				if(j < length)
				{
					for(l = 0; l < insert_length;++l, ++k)
						new_string.as.string->chars[k] = insert[l];
				}
				
				j += remove_length;
			}

			new_string.as.string->chars[k] = '\0';
		}

		rook_obj_free(&vm->runtime_allocator, &indices);
		(*rets)[0] = new_string;
	}

	return 1;
}

static int join(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	char buffer[2048];
	char* element;
	int element_length = 0;
	rook_obj joined;
	char delimiter = ',';

	if (argc >= 1)
	{
		if (!rook_obj_isstring(args[0]))
			return add_error(vm, "array.join expects 1st optional argument (delimiter) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
		if (args[0].as.string->length != 1)
			return add_error(vm, "string.join only accepts one character as delimiter, a string of length 1. Got string of length %d", args[0].as.string->length);

		delimiter = args[0].as.string->chars[0];
	}

	joined = rook_obj_create(&vm->runtime_allocator, rook_obj_type_string);

	for (int i = 0; i < this_->as.array->size; ++i)
	{
		element = buffer;
		if (!rook_obj_to_string(this_->as.array->array[i], buffer, sizeof(buffer), &element_length))
		{
			element = ROOK_ALLOC(element_length);
			if (!rook_obj_to_string(this_->as.array->array[i], element, element_length, &element_length))
			{
				ROOK_FREE(element);
				rook_obj_free(&vm->runtime_allocator, &joined);
				return add_error(vm, "Unknown error in %s", "rook_obj_to_string");
			}
		}

		int new_length = joined.as.string->length + element_length + (i == this_->as.array->size - 1 ? 0 : 1);
		if (joined.as.string->capacity < new_length)
		{
			int new_capacity = joined.as.string->capacity * 2;
			if (new_capacity < new_length + 1)
				new_capacity = new_length + 1;
			char* new_chars = ROOK_REALLOC(joined.as.string->chars, new_capacity);
			assert(new_chars);
			joined.as.string->chars = new_chars;
			joined.as.string->capacity = new_capacity;
		}

		memcpy(joined.as.string->chars + joined.as.string->length, element, element_length);
		joined.as.string->length = new_length;

		if (element_length > sizeof(buffer))
			ROOK_FREE(element);

		if (i < this_->as.array->size - 1)
			joined.as.string->chars[joined.as.string->length - 1] = delimiter;
	}
	
	if (joined.as.string->chars)
		joined.as.string->chars[joined.as.string->length] = '\0';

	rook_env_push_result(*rets, joined);
	return 1;
}

static int split(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj result;
	rook_string next;
	int i;
	char delimiter;

	if (argc != 1)
		return add_error(vm, "string.split expects 1 argument (delimiter (string)), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.split expects 1st argument (delimiter) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
	if (args[0].as.string->length != 1)
		return add_error(vm, "string.split only accepts one character as delimiter, a string of length 1. Got string of length %d", args[0].as.string->length);

	delimiter = args[0].as.string->chars[0];
	result = rook_vm_create_array(vm, 0);
	rook_string_init(&next, 0);

	for (i = 0; i < this_->as.string->length; ++i)
	{
		if (this_->as.string->chars[i] == delimiter)
		{
			rook_array_push(result.as.array, rook_vm_create_string(vm, rook_cstring_from_string(next)));
			rook_string_free(&next);
		}
		else
		{
			rook_string_append_char(&next, this_->as.string->chars[i]);
		}
	}

	rook_array_push(result.as.array, rook_vm_create_string(vm, rook_cstring_from_string(next)));
	rook_string_free(&next);

	rook_env_push_result(*rets, result);
	return 1;
}

static int startswith(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	int result;

	if (argc != 1)
		return add_error(vm, "string.startswith expects 1 argument (string to start with), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.startswith expects argument (string to start with) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	result = this_->as.string->length >= args[0].as.string->length &&
		strncmp(this_->as.string->chars, args[0].as.string->chars, args[0].as.string->length) == 0;

	rook_env_push_result(*rets, rook_vm_create_boolean(result));
	return 1;
}

static int endswith(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	int result, tl, al;

	if (argc != 1)
		return add_error(vm, "string.endswith expects 1 argument (string to start with), got %d", argc);
	if (!rook_obj_isstring(args[0]))
		return add_error(vm, "string.endswith expects argument (string to start with) to be string, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	tl = this_->as.string->length;
	al = args[0].as.string->length;
	
	result = tl >= al && strncmp(this_->as.string->chars + tl - al, args[0].as.string->chars, al) == 0;

	rook_env_push_result(*rets, rook_vm_create_boolean(result));
	return 1;
}

static int toupper(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	int i;
	rook_obj new_str;

	new_str = rook_obj_create(&vm->runtime_allocator, rook_obj_type_string);
	rook_obj_string_allocate(new_str.as.string, this_->as.string->length);

	for (i = 0; i < this_->as.string->length; ++i)
	{
		char c = this_->as.string->chars[i];
		if (c >= 'a' && c <= 'z')
			c -= 'a' - 'A';

		new_str.as.string->chars[i] = c;
	}

	new_str.as.string->chars[new_str.as.string->length] = '\0';

	rook_env_push_result(*rets, new_str);
	return 1;
}

static int tolower(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	int i;
	rook_obj new_str;

	new_str = rook_obj_create(&vm->runtime_allocator, rook_obj_type_string);
	rook_obj_string_allocate(new_str.as.string, this_->as.string->length);

	for (i = 0; i < this_->as.string->length; ++i)
	{
		char c = this_->as.string->chars[i];
		if (c >= 'A' && c <= 'Z')
			c += 'a' - 'A';

		new_str.as.string->chars[i] = c;
	}

	new_str.as.string->chars[new_str.as.string->length] = '\0';

	rook_env_push_result(*rets, new_str);
	return 1;
}

static int length(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	rook_obj length_obj;
	length_obj = rook_obj_create(&vm->runtime_allocator, rook_obj_type_number);
	length_obj.as.number = (rook_float)this_->as.string->length;
	rook_env_push_result(*rets, length_obj);
	return 1;
}

#ifdef __cplusplus
}
#endif // __cplusplus
