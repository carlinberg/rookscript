#include "rook_standard_library.h"

#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_env.h>

#include <math.h>

#if ROOK_FLOAT_SIZE == 32
#define floor floorf
#define ceil ceilf
#define fmax fmaxf
#define fmin fminf
#define fabs fabsf
#define sin sinf
#define cos cosf
#define tan tanf
#define acos acosf
#define asin asinf
#define atan atanf
#define atan2 atan2f
#define log	logf
#define log10 log10f
#define sqrt sqrtf
#endif

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define add_error(vm, fmt, ...) rook_vm_runtime_errorf(vm, (fmt), __VA_ARGS__), -1
#define rook_add_math_func(f) rook_map_set(math.as.map, #f, sizeof(#f) - 2, rook_vm_create_cfunc(vm, f, rook_cstring_from_chars_ex(#f, sizeof(#f) - 2)), &vm->runtime_allocator)

static int floor_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int ceil_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int max_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int min_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int abs_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int sin_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int cos_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int tan_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int acos_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int asin_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int atan_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int atan2_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int log_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int log10_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);
static int sqrt_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets);

void rook_lib_register_math(rook_vm* vm)
{
	rook_obj math = rook_vm_create_map(vm, 20);
	rook_vm_register_global(vm, rook_cstring_from_literal("math"), math);

	rook_add_math_func(floor_);
	rook_add_math_func(ceil_);
	rook_add_math_func(max_);
	rook_add_math_func(min_);
	rook_add_math_func(abs_);
	rook_add_math_func(sin_);
	rook_add_math_func(cos_);
	rook_add_math_func(tan_);
	rook_add_math_func(acos_);
	rook_add_math_func(asin_);
	rook_add_math_func(atan_);
	rook_add_math_func(atan2_);
	rook_add_math_func(log_);
	rook_add_math_func(log10_);
	rook_add_math_func(sqrt_);
}

static int floor_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0)
		return add_error(vm, "floor expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "floor expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
		
	rook_obj number = rook_vm_create_number(floor(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int ceil_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "ceil expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "ceil expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(ceil(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int max_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc < 2)
		return add_error(vm, "max expects at least 2 arguments (x, y), got %d", argc);

	for (int i = 0; i < argc; ++i)
	{
		if (!rook_obj_isnumber(args[i]))
			return add_error(vm, "max expects %d%s argument (x) to be number, got %s", i, (i + 1 == 1) ? "st" : (i + 1 == 2) ? "nd" : (i + 1 == 3) ? "rd" : "th", rook_obj_type_names[rook_obj_get_type(args[i])]);
	}

	rook_float x = args[0].as.number;
	for (int i = 1; i < argc; ++i)
	{
		x = fmax(x, args[i].as.number);
	}

	rook_obj number = rook_vm_create_number(x);
	rook_env_push_result(*rets, number);
	return 1;
}

static int min_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc < 2)
		return add_error(vm, "min expects at least 2 arguments (x, y), got %d", argc);
		
	for (int i = 0; i < argc; ++i)
	{
		if (!rook_obj_isnumber(args[i]))
			return add_error(vm, "min expects %d%s argument (x) to be number, got %s", i, (i+1 == 1) ? "st" : (i+1==2) ? "nd" : (i + 1 == 3) ? "rd" : "th" ,rook_obj_type_names[rook_obj_get_type(args[i])]);
	}

	rook_float x = args[0].as.number;
	for (int i = 1; i < argc; ++i)
	{
		x = fmin(x, args[i].as.number);
	}

	rook_obj number = rook_vm_create_number(x);
	rook_env_push_result(*rets, number);
	return 1;
}

static int abs_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "abs expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "abs expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(fabs(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int sin_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "sin expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "sin expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(sin(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int cos_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "cos expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "cos expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(cos(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int tan_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "tan expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "tan expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(tan(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int acos_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "acos expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "acos expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(acos(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int asin_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "asin expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "asin expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(asin(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int atan_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "atan expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "atan expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(atan(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int atan2_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 2)
		return add_error(vm, "min expects 2 arguments (x, y), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "min expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
	if (!rook_obj_isnumber(args[1]))
		return add_error(vm, "min expects 2nd argument (y) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[1])]);

	rook_obj number = rook_vm_create_number(atan2(args[1].as.number, args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int log_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "log expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "log expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);
		
	rook_obj number = rook_vm_create_number(log(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int log10_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "log10 expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "log10 expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(log10(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

static int sqrt_(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (argc == 0 || argc > 1)
		return add_error(vm, "sqrt expects 1 argument (x), got %d", argc);
	if (!rook_obj_isnumber(args[0]))
		return add_error(vm, "sqrt expects 1st argument (x) to be number, got %s", rook_obj_type_names[rook_obj_get_type(args[0])]);

	rook_obj number = rook_vm_create_number(sqrt(args[0].as.number));
	rook_env_push_result(*rets, number);
	return 1;
}

#ifdef __cplusplus
}
#endif // __cplusplus
