#ifndef ROOK_BYTECODE_INTERPRETER_TEST
#define ROOK_BYTECODE_INTERPRETER_TEST

struct rook_test_context;

extern void(*rook_bytecode_interpreter_tests[])(struct rook_test_context*);

#endif // !ROOK_BYTECODE_INTERPRETER_TEST
