#include "rook_variable_resolver_test.h"



#include "rook_test_context.h"
#include <utility/stb_sb.h>
#include <utility/rook_error.h>

#include <core/compiler/rook_lexer.h>
#include <core/compiler/rook_parser.h>
#include <core/compiler/rook_variable_resolver.h>

int lex_and_parse(const char* code, struct rook_parser* parser, struct rook_test_context* ctx);

void rook_test_declarations(struct rook_test_context* ctx)
{
	const char* code =
		"local a = 0;"					"\n"
		"local b = 0;"					"\n"
		""								"\n"
		"local f = () -> {"				"\n"
		"    local c = 1;"				"\n"
		"    local d = 2;"				"\n"
		"    print(a);"					"\n"
		"    print(b);"					"\n"
		"    print(c);"					"\n"
		"    print(d);"					"\n"
		"};"							"\n"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_variable_resolver_run(&parser);

	rook_parser_free(&parser);
}

void(*rook_variable_resolver_tests[2])(struct rook_test_context*) =
{
	rook_test_declarations,
	0
};
