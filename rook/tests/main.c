#include "rook_test_context.h"

#include "rook_lexer_test.h"
#include "rook_parser_test.h"
#include "rook_variable_resolver_test.h"
#include "rook_bytecode_interpreter_test.h"
#include "rook_library_test.h"

#include <utility/rook_malloc.h>

int main()
{
	rook_test_context ctx;
	rook_test_context_init(&ctx);

	for (rook_test_func* test = rook_lexer_tests; *test; ++test)
	{
		(*test)(&ctx);
	}

	for (rook_test_func* test = rook_parser_tests; *test; ++test)
	{
		(*test)(&ctx);
	}

	for (rook_test_func* test = rook_variable_resolver_tests; *test; ++test)
	{
		(*test)(&ctx);
	}

	for (rook_test_func* test = rook_bytecode_interpreter_tests; *test; ++test)
	{
		(*test)(&ctx);
	}

	for (rook_test_func* test = rook_library_tests; *test; ++test)
	{
		(*test)(&ctx);
	}

	rook_display_errors(&ctx);
	rook_save_errors_to_disk(&ctx);

	rook_test_context_free(&ctx);

	rook_report_memory_leaks();

	return 0;
}
