#ifndef ROOK_LEXER_TEST
#define ROOK_LEXER_TEST

struct rook_test_context;

extern void(*rook_lexer_tests[])(struct rook_test_context*);

#endif // !ROOK_LEXER_TEST
