#include "rook_test_context.h"

#include <stdio.h>

#include <utility/stb_sb.h>

#ifdef _WIN32
#include <Windows.h>
#define SPRINTF(d, fmt, ...) sprintf_s((d), sizeof(d) - 1, (fmt), __VA_ARGS__)
#else
#define SPRINTF(d, fmt, ...) sprintf((d), (fmt), __VA_ARGS__)
#endif // _WIN32

typedef struct rook_test_info
{
	const char* msg;
	const char* file;
	const char* func;
	int line;
} rook_test_info;

void rook_test_context_init(rook_test_context* ctx)
{
	ctx->succeeded_tests = 0;
	ctx->failed_tests = 0;
}

void rook_test_context_free(rook_test_context* ctx)
{
	sb_free(ctx->succeeded_tests);
	sb_free(ctx->failed_tests);
}

int rook_test(rook_test_context* ctx, int cond, const char* msg, const char* file, const char* func, int line)
{
	rook_test_info test_info;
	test_info.msg = msg;
	test_info.file = file;
	test_info.func = func;
	test_info.line = line;

	if (cond)
	{
		sb_push(ctx->succeeded_tests, test_info);
	}
	else
	{
		sb_push(ctx->failed_tests, test_info);
	}

	return cond;
}

void rook_display_errors(rook_test_context* ctx)
{
	if (sb_count(ctx->failed_tests) > 0)
	{
		for (int i = 0; i < sb_count(ctx->failed_tests); ++i)
		{
			printf("File: %s Function: %s Line: %d\nError: %s\n\n", ctx->failed_tests[i].file, ctx->failed_tests[i].func, ctx->failed_tests[i].line, ctx->failed_tests[i].msg);
		}

#ifdef _WIN32
		system("pause");
#endif
	}
}

void rook_save_errors_to_disk(rook_test_context* ctx)
{
	const char* date = __DATE__;
	const char* time = __TIME__;


	char testResult[8192];
	int charsWritten = SPRINTF(testResult, "Tests run %s %s\n\nSuccessful tests: %d\n\nFailed tests (%d):\n", date, time, sb_count(ctx->succeeded_tests), sb_count(ctx->failed_tests));
	if (charsWritten == -1 || charsWritten >= sizeof(testResult))
		return;

	for (int i = 0; i < sb_count(ctx->failed_tests); ++i)
	{
		int written = SPRINTF(testResult + charsWritten, "%s: %d\n", ctx->failed_tests[i].func, ctx->failed_tests[i].line);
		if (written == -1)
			return;

		charsWritten += written;
	}
	
	if (charsWritten < 0 || charsWritten >= sizeof(testResult) - 1)
		return;

	testResult[charsWritten] = '\0';

#ifdef _WIN32
	MessageBoxA(0, testResult, "Test result", MB_OK);
#else
	printf("%s\n", testResult);
#endif // _WIN32
}
