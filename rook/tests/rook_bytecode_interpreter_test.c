#include "rook_bytecode_interpreter_test.h"


#include <string.h>

#include "rook_test_context.h"
#include <utility/stb_sb.h>
#include <utility/rook_error.h>

#include <core/compiler/rook_lexer.h>
#include <core/compiler/rook_parser.h>
#include <core/compiler/rook_variable_resolver.h>
#include <core/compiler/rook_bytecode_generator.h>

#include <core/runtime/rook_bytecode_chunk.h>
#include <core/runtime/rook_bytecode_interpreter.h>
#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_object.h>
#include <core/runtime/rook_obj_string.h>
#include <core/runtime/rook_array.h>
#include <core/runtime/rook_cfunc.h>

#include <library/rook_standard_library.h>

#include <stdio.h>

static int global_print_function(rook_vm* vm, void* ud, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	for (int i = 0; i < argc; ++i)
	{
		switch (rook_obj_get_type(args[i]))
		{
		case rook_obj_type_null:
			printf("null");
			break;
		case rook_obj_type_boolean:
			if (args[i].as.boolean)
				printf("true");
			else
				printf("false");
			break;
		case rook_obj_type_number:
			printf("%f", (float)args[i].as.number);
			break;
		case rook_obj_type_string:
			printf("%s", args[i].as.string->chars);
			break;
		case rook_obj_type_userdata:
			printf("[Userdata]");
			break;
		case rook_obj_type_array:
			printf("[Array]");
			break;
		case rook_obj_type_map:
			printf("[Map]");
			break;
		case rook_obj_type_func:
			printf("[Function]");
			break;
		case rook_obj_type_cfunc:
			printf("[C function]");
			break;
		default:
			printf("Error type");
			break;
		}

		printf(" ");
	}

	printf("\n");
	return 0;
}

static void test_interpret_simle_logic(struct rook_test_context* ctx)
{
	rook_allocator alloc;
	rook_parser parser;
	rook_bytecode_generator generator;
	rook_bytecode_interpreter interpreter;
	rook_bytecode_chunk chunk;
	rook_error* errors = 0;
	rook_bytecode_chunk_init(&chunk);
	rook_allocator_init(&alloc);

	rook_cstring code = rook_cstring_from_literal(
		"if (1)"		"\n"
		"    3 + 5;"	"\n"
		"else"			"\n"
		"    3 + 6;"	"\n"
		"return;");

	rook_parser_init(&parser, code, code, &errors);
	rook_bytecode_interpreter_init(&interpreter, &alloc, &errors, 0);
	rook_bytecode_generator_init(&generator, &alloc, code, code, &errors, 0);

	if (!rook_lex(code.chars, &parser))
	{
		test_ctx_add_error(ctx, "Failed to lex code");
		return;
	}

	if (!rook_parse(&parser))
	{
		test_ctx_add_error(ctx, "Failed to parse code");
		return;
	}

	if (!rook_bytecode_generator_run(&generator, parser.stmts, sb_count(parser.stmts), &chunk))
	{
		test_ctx_add_error(ctx, "Failed to generate bytecode");
		return;
	}

	rook_bytecode_interpreter_run(0, &interpreter, &chunk, 0);

	assert_are_equal(ctx, 0, rook_env_stack_size(&interpreter.env), "After script that returns nothing is run, stack should be empty");

	rook_parser_free(&parser);
	rook_bytecode_chunk_free(&chunk, &alloc);
	rook_bytecode_interpreter_free(&interpreter);
	rook_bytecode_generator_free(&generator);
	rook_allocator_free(&alloc);
}

static void test_interpret_call_cfunction(struct rook_test_context* ctx)
{
	rook_allocator alloc;
	rook_parser parser;
	rook_bytecode_generator generator;
	rook_bytecode_interpreter interpreter;
	rook_bytecode_chunk chunk;
	rook_error* errors = 0;
	rook_bytecode_chunk_init(&chunk);
	rook_allocator_init(&alloc);

	rook_cstring code = rook_cstring_from_literal(
		"if (1)"				"\n"
		"    print(3 + 5);"		"\n"
		"return;");

	rook_parser_init(&parser, code, code, &errors);
	rook_bytecode_interpreter_init(&interpreter, &alloc, &errors, 0);
	rook_bytecode_generator_init(&generator, &alloc, code, code, &errors, 0);

	if (!rook_lex(code.chars, &parser))
	{
		test_ctx_add_error(ctx, "Failed to lex code");
		return;
	}

	if (!rook_parse(&parser))
	{
		test_ctx_add_error(ctx, "Failed to parse code");
		return;
	}

	rook_variable_resolver_run(&parser);

	if (!rook_bytecode_generator_run(&generator, parser.stmts, sb_count(parser.stmts), &chunk))
	{
		test_ctx_add_error(ctx, "Failed to generate bytecode");
		return;
	}

	rook_obj cfunk_obj = rook_obj_create(interpreter.runtime_allocator, rook_obj_type_cfunc);
	rook_cfunc_init(cfunk_obj.as.cfunc, global_print_function, rook_cstring_from_literal("print"));
	rook_env_register_global(&interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_bytecode_interpreter_run(0, &interpreter, &chunk, 0);

	assert_are_equal(ctx, 0, rook_env_stack_size(&interpreter.env), "After script that returns nothing is run, stack should be empty");

	rook_parser_free(&parser);
	rook_bytecode_chunk_free(&chunk, &alloc);
	rook_bytecode_interpreter_free(&interpreter);
	rook_bytecode_generator_free(&generator);
	rook_allocator_free(&alloc);
}

static void test_interpret_decl_func(struct rook_test_context* ctx)
{
	rook_allocator alloc;
	rook_parser parser;
	rook_bytecode_generator generator;
	rook_bytecode_interpreter interpreter;
	rook_bytecode_chunk chunk;
	rook_error* errors = 0;
	rook_bytecode_chunk_init(&chunk);
	rook_allocator_init(&alloc);

	rook_cstring code = rook_cstring_from_literal(
		"() -> {"						"\n"
		"    print('Hello world!');"	"\n"
		"    print(2 + 4 + 1);"			"\n"
		"}();");

	rook_parser_init(&parser, code, code, &errors);
	rook_bytecode_interpreter_init(&interpreter, &alloc, &errors, 0);
	rook_bytecode_generator_init(&generator, &alloc, code, code, &errors, 0);

	if (!rook_lex(code.chars, &parser))
	{
		test_ctx_add_error(ctx, "Failed to lex code");
		return;
	}

	if (!rook_parse(&parser))
	{
		test_ctx_add_error(ctx, "Failed to parse code");
		return;
	}

	rook_variable_resolver_run(&parser);

	if (!rook_bytecode_generator_run(&generator, parser.stmts, sb_count(parser.stmts), &chunk))
	{
		test_ctx_add_error(ctx, "Failed to generate bytecode");
		return;
	}

	rook_obj cfunk_obj = rook_obj_create(interpreter.runtime_allocator, rook_obj_type_cfunc);
	rook_cfunc_init(cfunk_obj.as.cfunc, global_print_function, rook_cstring_from_literal("print"));
	rook_env_register_global(&interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_bytecode_interpreter_run(0, &interpreter, &chunk, 0);

	assert_are_equal(ctx, 0, rook_env_stack_size(&interpreter.env), "After script that returns nothing is run, stack should be empty");

	rook_parser_free(&parser);
	rook_bytecode_chunk_free(&chunk, &alloc);
	rook_bytecode_interpreter_free(&interpreter);
	rook_bytecode_generator_free(&generator);
	rook_allocator_free(&alloc);
}

static void test_interpret_local(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local f = () -> {"				"\n"
		"    print('Hello world!');"	"\n"
		"    print(2 + 4 + 1);"			"\n"
		"};"							"\n"
		""								"\n"
		"f();");

	rook_cstring code2 = rook_cstring_from_literal(
		"local a = 0;"					"\n"
		"local b = 234;"				"\n"
		""								"\n"
		"local f = () -> {"				"\n"
		"	local c = 34;"				"\n"
		"	local d = 2412;"			"\n"
		""								"\n"
		"	print(c, d);"				"\n"
		""								"\n"
		"	local f2 = () -> {"			"\n"
		"		local e = 5342;"		"\n"
		"		print(e);"	 			"\n"
		"	};"	 						"\n"
		"	f2();"						"\n"
		"};"	 						"\n"
		""	 							"\n"
		"print(a, b); "					"\n"
		"f(); ");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("print"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_captured_values(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local a = 0;"					"\n"
		"local b = 0;"					"\n"
		""								"\n"
		"local f = () -> {"				"\n"
		"    local c = 1;"				"\n"
		"    local d = 2;"				"\n"
		"    print(a);"					"\n"
		"    print(b);"					"\n"
		"    print(c);"					"\n"
		"    print(d);"					"\n"
		"};"							"\n"
		"f();");

	rook_cstring code2 = rook_cstring_from_literal(
		"local a = 1;"					"\n"
		"local b = 2;"					"\n"
		""								"\n"
		"local f = () -> {"				"\n"
		"	local x = 10;"				"\n"
		"	local y = 20;"				"\n"
		"	local f2 = () -> {"			"\n"
		"		print(a, b);"			"\n"
		"	};"							"\n"
		""								"\n"
		"	f2();"						"\n"
		"};"							"\n"
		""								"\n"
		"f(); ");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("print"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_globals(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"global gNumber = 5132;");

	rook_cstring code2 = rook_cstring_from_literal(
		"print('gNumber: ', gNumber);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_return_values(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local f = () -> {"		"\n"
		"    return 43;"		"\n"
		"};"					"\n"
		""						"\n"
		"local a = f();"		"\n"
		""						"\n"
		"print(a);");

	rook_cstring code2 = rook_cstring_from_literal(
		"local f = () -> {"		"\n"
		"    return 65, 43;"	"\n"
		"};"					"\n"
		""						"\n"
		"local a = f();"		"\n"
		""						"\n"
		"print(a);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_return_values_and_captures(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local f1 = () -> {"		"\n"
		"	local a = 53;"			"\n"
		"	"						"\n"
		"	local f2 = () -> {"		"\n"
		"		print(a);"			"\n"
		"	};"						"\n"
		"	"						"\n"
		"	return f2;"				"\n"
		"};"						"\n"
		""							"\n"
		"local c2 = f1();"			"\n"
		""							"\n"
		"print(c2());");

	rook_cstring code2 = rook_cstring_from_literal(
		"local f3 = () -> {"			"\n"
		"	local a = 54;"				"\n"
		""								"\n"
		"	local f4 = () -> {"			"\n"
		"		local f5 = () -> {"		"\n"
		"			print(a);"			"\n"
		"		};"						"\n"
		"		return f5;"				"\n"
		"	};"							"\n"
		""								"\n"
		"	return f4;"					"\n"
		"};"							"\n"
		""								"\n"
		"local c4 = f3();"				"\n"
		"local c5 = c4();"				"\n"
		""								"\n"
		"print(c5());");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_args(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local f = (arg) -> {"	"\n"
		"    return arg + arg;"	"\n"
		"};"					"\n"
		""						"\n"
		"local a = f(3);"		"\n"
		""						"\n"
		"print(a);");

	rook_cstring code2 = rook_cstring_from_literal(
		"local f = (arg) -> {"	"\n"
		"    print(arg);"		"\n"
		"    return arg * arg;"	"\n"
		"};"					"\n"
		""						"\n"
		"local a = f(5);"		"\n"
		""						"\n"
		"print(a);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_array_decl(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local a1 = [];"
		"local a2 = [ 0, 2, 4 ];");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_map_decl(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local a1 = {};"
		"local a2 = { a = 0, b = 2, c = 'hej' };");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_assignment(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local l = {};"			"\n"
		"global g = 234;"		"\n"
		"l = 23;"				"\n"
		"g = 43;"				"\n"
		"print(l, g);");

	rook_cstring code2 = rook_cstring_from_literal(
		"local l = 32;"			"\n"
		"local f = () -> {"		"\n"
		"    print(l);"			"\n"
		"    l = 23;"			"\n"
		"    print(l);"			"\n"
		"};"					"\n"
		"f();");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_map_set(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local a2 = { a = 0, b = 2, c = 'hej' };"	"\n"
		"a2.a = 32;"								"\n"
		"a2.b = 31;"								"\n"
		"print(a2.a, a2.b, a2.c);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_map_subscripting(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local a = { a = 0, b = 2, c = 'hej' };"	"\n"
		"a['a'] = 32;"								"\n"
		"a['b'] = 31;"								"\n"
		"print(a['a'], a['b'], a['c']);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_array_subscripting(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local a = [ 0, 2, 'hej' ];"				"\n"
		"a[0] = 32;"								"\n"
		"a[1] = 31;"								"\n"
		"a[2] = 30;"								"\n"
		"print(a[0], a[1], a[2]);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_compound_exprs(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"print((2 + 4) * 3);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_while(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local i = 0;"		"\n"
		"while (i < 3)"		"\n"
		"	i = i + 1;"		"\n"
		"print(i);");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_unary(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local i = 0;"		"\n"
		"while (i < 3)"		"\n"
		"	++i;"			"\n"
		"print(i);");

	rook_cstring code2 = rook_cstring_from_literal(
		"global i = 0;"		"\n"
		"while (i < 4)"		"\n"
		"	++i;"			"\n"
		"print(i++);"		"\n"//4
		"print(i--);"		"\n"//5
		"print(i--);"		"\n"//4
		"print(i++);");			//3

	rook_cstring code3 = rook_cstring_from_literal(
		"print(-4);"		"\n"
		"print(!4);"		"\n"
		"print(!true);"		"\n"
		"print(-(-7));"		"\n"
		);

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code3, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_logical(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local i = 0;"		"\n"
		"while (i < 3)"		"\n"
		"	++i;"			"\n"
		"print(i == 3 && true);");

	rook_cstring code2 = rook_cstring_from_literal(
		"print(false || 'Helloo');");

	rook_cstring code3 = rook_cstring_from_literal(
		"print('Yes' || 'Helloo');");

	rook_cstring code4 = rook_cstring_from_literal(
		"print(0 || 'Helloo');");

	rook_cstring code5 = rook_cstring_from_literal(
		"print(false && 'Helloo');");

	rook_cstring code6 = rook_cstring_from_literal(
		"print('Yes' && 'Helloo');");

	rook_cstring code7 = rook_cstring_from_literal(
		"print(0 && 'Helloo');");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code3, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code4, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code5, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code6, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code7, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_memberfuncs(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local dog = {"									"\n"
		"	name = '',"									"\n"
		"	"											"\n"
		"	bark = () -> {"								"\n"
		"		print('my name is ' + this.name);"		"\n"
		"	},"											"\n"
		"	"											"\n"
		"	barkTo = (other) -> {"						"\n"
		"		print('hello ' + other);"				"\n"
		"	}"											"\n"
		"};"											"\n"
		""												"\n"
		"dog.name = 'Doglas';"							"\n"
		""												"\n"
		"dog.bark();"									"\n"
		""												"\n"
		"dog.barkTo('Catty');");

	rook_cstring code2 = rook_cstring_from_literal(
		"local dog = {"									"\n"
		"	name = '',"									"\n"
		"	"											"\n"
		"	bark = () -> {"								"\n"
		"		print('my name is ' + this.name);"		"\n"
		"	},"											"\n"
		"	"											"\n"
		"	barkTo = (other) -> {"						"\n"
		"		print('hello ' + other);"				"\n"
		"	}"											"\n"
		"};"											"\n"
		""												"\n"
		"dog.name = 'Doglas';"							"\n"
		""												"\n"
		"dog['bark']();"								"\n"
		""												"\n"
		"dog['barkTo']('Catty');");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static int array_push(rook_vm* vm, void* ud, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	if (!rook_obj_isarray(*this_))
		return -1;
	if (argc != 1)
		return -1;

	rook_obj_incref(args);
	rook_array_push(this_->as.array, *args);
	return 0;
}

static void test_interpret_extension_funcs(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local dogs = [];"									"\n"
		"dogs.push_ext('Mr Dog');"							"\n"
		"dogs.push_ext('Mrs Dog');"							"\n"
		"dogs.push_ext('Ms Dog');");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, global_print_function, rook_cstring_from_literal("pring"));
	rook_env_register_global(&vm.interpreter.env, rook_cstring_from_literal("print"), cfunk_obj);

	rook_vm_register_extension(&vm, rook_obj_type_array, rook_cstring_from_literal("push_ext"), array_push);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_for(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local dogs = [];"									"\n"
		"dogs.push('Mr Dog');"								"\n"
		"dogs.push('Mrs Dog');"								"\n"
		"dogs.push('Ms Dog');"								"\n"
		""													"\n"
		"for (local i = 0; i < dogs.count(); ++i) {"		"\n"
		"	print('dogs[', i, ']: ', dogs[i]);"				"\n"
		"}");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_lib_register_general(&vm);
	rook_lib_register_array(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_foreach(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local dogs = [];"								"\n"
		"dogs.push('Mr Dog');"							"\n"
		"dogs.push('Mrs Dog');"							"\n"
		"dogs.push('Ms Dog');"							"\n"
		""												"\n"
		"for (dog : dogs) {"							"\n"
		"	print('dog: ', dog);"						"\n"
		"}"												"\n"
		""												"\n"
		"for (i, dog : dogs) {"							"\n"
		"	print('dog (', i, '): ', dog);"				"\n"
		"}");

	rook_cstring code2 = rook_cstring_from_literal(
		"local dog = {"									"\n"
		"	name = '',"									"\n"
		"	"											"\n"
		"	bark = () -> {"								"\n"
		"		print('my name is ' + this.name);"		"\n"
		"	},"											"\n"
		"	"											"\n"
		"	barkTo = (other) -> {"						"\n"
		"		print('hello ' + other);"				"\n"
		"	}"											"\n"
		"};"											"\n"
		""												"\n"
		"dog.name = 'Doglas';"							"\n"
		""												"\n"
		"for (key, member : dog) {"						"\n"
		"	print('\"', key, '\": ', member);"			"\n"
		"}");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_lib_register_general(&vm);
	rook_lib_register_array(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_empty_block(rook_test_context* ctx)
{
	//rook_vm vm;
	//rook_vm_init(&vm);

	//rook_lib_register_general(&vm);

	//rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	//rook_vm_free(&vm);
}

static void test_interpret_comp_assign(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local i = 0; print(i); i += 3; print(i);"			"\n"
		"local dog = {"										"\n"
		"	age = 1,"										"\n"
		"	"												"\n"
		"	bark = () -> {"									"\n"
		"		print('my age is ' + tostring(this.age));"	"\n"
		"	}"												"\n"
		"};"												"\n"
		""													"\n"
		"dog.age += 7;"										"\n"
		"dog['age'] += 2;"									"\n"
		"dog.bark();"										"\n"
		"local dogList = [ dog, dog.age ];"					"\n"
		"dogList[1] += 3;"									"\n"
		"print(dogList[1]);"								"\n"
		"");

	rook_vm vm;
	rook_vm_init(&vm);

	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_multiple_decl(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local i, j = 0;"						"\n"
		"print('i, j:', i, j);"					"\n"
		"local a, b = 3, 'hey';"				"\n"
		"print('a, b:', a, b);"					"\n"
	);

	rook_cstring code2 = rook_cstring_from_literal(
		"local getNameAndAge = () -> {					\n"
		"	return 'Carl', 26;							\n"
		"};												\n"
		"												\n"
		"local name, age = getNameAndAge();				\n"
		"												\n"
		"print('name, age:', name, age);				\n"
		"print('getNameAndAge():', getNameAndAge());	\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_multiple_assignment(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local getNameAndAge = () -> {					\n"
		"	return 'Carl', 26;							\n"
		"};												\n"
		"												\n"
		"local name;									\n"
		"local age;										\n"
		"												\n"
		"name, age = getNameAndAge();					\n"
		"												\n"
		"print('name, age:', name, age);				\n"
		"print('getNameAndAge():', getNameAndAge());	\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_captured_values_in_top_block(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local capturee = 'test';	\n"
		"							\n"
		"local obj = {				\n"
		"	closure = () -> {		\n"
		"		capturee = 3;		\n"
		"		print(capturee);	\n"
		"	}						\n"
		"};							\n"
		"							\n"
		"return obj;				\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	int res = rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	if (!assert_are_equal(ctx, res, 1, "Expected 1 result"))
		return;

	rook_obj obj = rook_vm_stack_get(&vm, -1);
	rook_obj_incref(&obj);
	if (!assert_are_equal(ctx, rook_obj_get_type(obj), rook_obj_type_map, "Expected closure returned"))
		return;

	rook_vm_stack_pop(&vm, 1);
	
	rook_obj closure;
	assert_is_true(ctx, rook_obj_get_member(obj, rook_cstring_from_literal("closure"), &closure, &vm.runtime_allocator), "Failed to get closure from obj");
	assert_are_equal(ctx, rook_obj_get_type(closure), rook_obj_type_bound_func, "closure is not of type closure");

	rook_vm_run_obj(&vm, closure);

	rook_vm_free(&vm);
}

static void test_interpret_tag_extensions(rook_test_context* ctx)
{
	rook_cstring code1 = rook_cstring_from_literal(
		"global dogs = [];"									"\n");

	rook_cstring code2 = rook_cstring_from_literal(
		"dogs.push_ext('Mr Dog');"							"\n"
		"dogs.push_ext('Mrs Dog');"							"\n"
		"dogs.push_ext('Ms Dog');");

	rook_vm vm = { 0 };
	rook_vm_init(&vm);

	rook_obj cfunk_obj = rook_vm_create_cfunc(&vm, array_push, rook_cstring_from_literal("push_ext"));

	int dog_tag = rook_vm_register_tag(&vm, rook_cstring_from_literal("Dog"));

	rook_vm_register_tag_extension(&vm, dog_tag, rook_cstring_from_literal("push_ext"), cfunk_obj);

	rook_vm_run_string(&vm, code1, rook_cstring_from_literal(__FUNCTION__));

	rook_obj global_dogs = { 0 };
	if (rook_vm_get_global(&vm, rook_cstring_from_literal("dogs"), &global_dogs))
	{
		rook_obj_incref(&global_dogs);
		rook_obj_set_tag(global_dogs, dog_tag);
		rook_vm_set_global(&vm, rook_cstring_from_literal("dogs"), global_dogs);
	}
	else
	{
		test_ctx_add_error(ctx, "Failed to get global 'dogs'");
	}

	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_global_self_assignment(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"global dogs = [];"									"\n"
		"dogs = dogs;"										"\n");

	rook_vm vm = { 0 };
	rook_vm_init(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_obj global_dogs = { 0 };
	if (rook_vm_get_global(&vm, rook_cstring_from_literal("dogs"), &global_dogs))
	{
		assert_are_equal(ctx, *global_dogs.as.refcount, 1, "Globals must be assignable to themselves");
		rook_obj_incref(&global_dogs);
		rook_vm_set_global(&vm, rook_cstring_from_literal("dogs"), global_dogs);
		assert_are_equal(ctx, *global_dogs.as.refcount, 1, "Globals must be assignable to themselves");
	}
	else
	{
		test_ctx_add_error(ctx, "Failed to get global 'dogs'");
	}

	rook_vm_free(&vm);
}

static int test_cfunc_func(rook_vm* vm, void* userdata, rook_obj* this_, rook_obj* args, int argc, rook_obj** rets)
{
	return 0;
}

static void test_interpret_cfunc_parameters(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"print(tostring(test_cfunc));");

	rook_cfunc_parameter parameters[] = {
		{ rook_cstring_from_literal("first"), rook_cstring_from_literal("string") },
		{ rook_cstring_from_literal("sec"), rook_cstring_from_literal("number") },
		{ rook_cstring_from_literal("third"), rook_cstring_from_literal("fake_type") }
	};

	rook_vm vm = { 0 };
	rook_vm_init(&vm);

	rook_lib_register_general(&vm);

	rook_obj test_cfunc = rook_vm_create_cfunc(&vm, test_cfunc_func, rook_cstring_from_literal("test_cfunc"));
	rook_cfunc_set_parameters(test_cfunc.as.cfunc, parameters, sizeof(parameters) / sizeof(parameters[0]));

	rook_vm_register_global(&vm, rook_cstring_from_literal("test_cfunc"), test_cfunc);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_func_parameter_types(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local testfunc = (first : string, second : number, third: any) -> { };"	"\n"
		"print(tostring(testfunc));");

	rook_vm vm = { 0 };
	rook_vm_init(&vm);

	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_negation(rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local o = { x = false }\n"
		"print(\"false?\", o.x);\n"
		"print(\"true (!false)?\", !o.x);\n"
		"print(\"![].empty():\", ![].empty());");

	rook_vm vm = { 0 };
	rook_vm_init(&vm);

	rook_lib_register_general(&vm);
	rook_lib_register_array(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

void(*rook_bytecode_interpreter_tests[])(rook_test_context*) =
{
	test_interpret_simle_logic,
	test_interpret_call_cfunction,
	test_interpret_decl_func,
	test_interpret_local,
	test_interpret_captured_values,
	test_interpret_globals,
	test_interpret_return_values,
	test_interpret_return_values_and_captures,
	test_interpret_args,
	test_interpret_array_decl,
	test_interpret_map_decl,
	test_interpret_assignment,
	test_interpret_map_set,
	test_interpret_map_subscripting,
	test_interpret_array_subscripting,
	test_interpret_compound_exprs,
	test_interpret_while,
	test_interpret_unary,
	test_interpret_logical,
	test_interpret_memberfuncs,
	test_interpret_extension_funcs,
	test_interpret_for,
	test_interpret_foreach,
	test_interpret_empty_block,
	test_interpret_comp_assign,
	test_interpret_multiple_decl,
	test_interpret_multiple_assignment,
	test_interpret_captured_values_in_top_block,
	test_interpret_tag_extensions,
	test_interpret_global_self_assignment,
	test_interpret_cfunc_parameters,
	test_interpret_func_parameter_types,
	test_interpret_negation,
	0
};
