#ifndef ROOK_LIBRARY_TEST
#define ROOK_LIBRARY_TEST

struct rook_test_context;

extern void(*rook_library_tests[])(struct rook_test_context*);

#endif // !ROOK_LIBRARY_TEST
