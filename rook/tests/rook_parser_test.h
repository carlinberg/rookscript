#ifndef ROOK_PARSER_TEST
#define ROOK_PARSER_TEST

struct rook_test_context;

extern void(*rook_parser_tests[])(struct rook_test_context*);

#endif // !ROOK_PARSER_TEST
