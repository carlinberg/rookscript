#ifndef ROOK_TEST_CONTEXT
#define ROOK_TEST_CONTEXT

typedef struct rook_test_context
{
	struct rook_test_info* succeeded_tests;
	struct rook_test_info* failed_tests;
} rook_test_context;

typedef void(*rook_test_func)(rook_test_context*);

void rook_test_context_init(rook_test_context* ctx);
void rook_test_context_free(rook_test_context* ctx);

int rook_test(rook_test_context* ctx, int cond, const char* msg, const char* file, const char* func, int line);
void rook_display_errors(rook_test_context* ctx);
void rook_save_errors_to_disk(rook_test_context* ctx);

#define test_ctx_add_error(ctx, message) rook_test(ctx, 0, message, __FILE__, __FUNCTION__, __LINE__)
#define assert_are_equal(ctx, left, right, message) rook_test(ctx, (left) == (right), message, __FILE__, __FUNCTION__, __LINE__)
#define assert_are_not_equal(ctx, left, right, message) rook_test(ctx, (left) != (right), message, __FILE__, __FUNCTION__, __LINE__)
#define assert_is_true(ctx, condition, message) rook_test(ctx, condition, message, __FILE__, __FUNCTION__, __LINE__)
#define assert_is_false(ctx, condition, message) rook_test(ctx, !(condition), message, __FILE__, __FUNCTION__, __LINE__)

#endif // !ROOK_TEST_CONTEXT
