#include "stb_sb.h"

#include <stdlib.h>
#include <assert.h>

void* stb__sbgrowf_rooktest(void* arr, int increment, int itemsize)
{
	assert(increment > 0);
	assert(itemsize > 0);

	int dbl_cur = arr ? 2 * stb__sbm(arr) : 0;
	int min_needed = stb_sb_count(arr) + increment;
	int m = dbl_cur > min_needed ? dbl_cur : min_needed;
	int* p = (int*)realloc(arr ? stb__sbraw(arr) : 0, itemsize * m + sizeof(int) * 2);
	if (p) {
		if (!arr)
			p[1] = 0;
		p[0] = m;
		return p + 2;
	}
	else {
		assert(0);
		exit(-1);
	}
}

void stb__free_rooktest(void* ptr)
{
	free(ptr);
}
