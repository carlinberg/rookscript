#include "rook_parser_test.h"

#include "rook_test_context.h"

#include <utility/stb_sb.h>
#include <utility/rook_error.h>

#include <core/compiler/rook_lexer.h>
#include <core/compiler/rook_parser.h>
#include <core/compiler/rook_token.h>
#include <core/compiler/rook_token_type.h>
#include <core/compiler/rook_stmt.h>

int lex_and_parse(const char* code, struct rook_parser* parser, struct rook_test_context* ctx)
{
	rook_error* error;

	if (!rook_lex(code, parser))
	{
		test_ctx_add_error(ctx, "Failed to lex code");
		for (error = *parser->errors; error < *parser->errors + sb_count(*parser->errors); ++error)
		{
			test_ctx_add_error(ctx, error->msg.chars);
		}

		return 0;
	}
	if (!rook_parse(parser))
	{
		test_ctx_add_error(ctx, "Failed to parse code");
		for (error = *parser->errors; error < *parser->errors + sb_count(*parser->errors); ++error)
		{
			test_ctx_add_error(ctx, error->msg.chars);
		}

		return 0;
	}

	return 1;
}

static void test_for_each(struct rook_test_context* ctx)
{
	const char* code =
		"for (apa : apor) print(apa);"
		""
		"for (i, apa : apor)"
		"	if (i > 0)"
		"		print(apa);"
		""
		"for (i, apa : apor)"
		"{"
		"	print(i, apa);"
		"}"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_for(struct rook_test_context* ctx)
{
	const char* code =
		"for (local i = 0; i < 10; ++i)"
		"{"
		"	print(i);"
		"}"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_return(struct rook_test_context* ctx)
{
	const char* code =
		"return \"good\";"
		"return multiple, values;"
		"half = (number) -> number / 2;"
		"sort(array, (left, right) -> left < right);"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_block(struct rook_test_context* ctx)
{
	const char* code =
		"local outside;"
		"{"
		"	local inside;"
		"}"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_func_decl(struct rook_test_context* ctx)
{
	const char* code =
		"local empty = () -> {};"
		"local full = () -> { local hej = \"world\"; };"
		"local with_arg = (arg) -> { print(arg); };"
		"local with_args = (arg1, arg2) -> { print(arg + arg2); };"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_map(struct rook_test_context* ctx)
{
	const char* code =
		"local empty = {};"
		"local full = { hej = \"world\" };"
		"local full2 = { one = 1, two = 2, thre = 3 };"
		"local full3 = { one = 1, t = full2, thr = empty, works = \"yes works\" };"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_array(struct rook_test_context* ctx)
{
	const char* code =
		"local empty = [];"
		"local full = [ 1 ];"
		"local full2 = [ 1, 2, 5 ];"
		"local full3 = [ 1, full2, empty, \"yes works\" ];"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void rook_test_parser(struct rook_test_context* ctx)
{
	const char* code =
		"local anka = \"Kalle\";"
		"global wealth = null;"
		"//wealth = {};\n"
		""
		"wealth();"
		"print(\"Hello\", \" world!\");"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_braceless_func(struct rook_test_context* ctx)
{
	const char* code =
		"local anka = () -> 'Kalle';"
		"local ankas = () -> ('Kalle', 'Joakim');";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_incr_decr(struct rook_test_context* ctx)
{
	const char* code =
		"local i = 0;"
		"local j = i++;"
		"print('++j', ++j);"
		"print('j++', j++);"
		"print('j--', j--);"
		"print('--j', --j);";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

static void test_attributes(struct rook_test_context* ctx)
{
	const char* code =
		"['Test Attribute']"
		"local i = 0;"
		""
		//"local t = {"
		//"    ['Member attr']"
		//"    m = 'hey'"
		//"};"
		"";

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, rook_cstring_from_chars(code), rook_cstring_from_chars(code), &errors);

	if (!lex_and_parse(code, &parser, ctx))
		return;

	if (sb_count(parser.stmts) == 1)
	{
		rook_stmt_base* stmt_with_attr = parser.stmts[0];

		if (stmt_with_attr->type == rook_stmt_type_global ||
			stmt_with_attr->type == rook_stmt_type_local)
		{
			rook_stmt_assignment* decl = (rook_stmt_assignment*)stmt_with_attr;
			
			rook_cstring test_attribute_constant;
			rook_cstring_init_literal(&test_attribute_constant, "Test Attribute");
			if (decl->attr_count == 1 && rook_string_equals(&decl->attrs[0], &test_attribute_constant))
			{
				//yes it works!!!
			}
			else
			{
				test_ctx_add_error(ctx, "Expected one attribute on the declaration");
			}
		}
		else
		{
			test_ctx_add_error(ctx, "Expected a declaration");
		}
	}
	else
	{
		test_ctx_add_error(ctx, "Expected one statement");
	}

	rook_parser_free(&parser);
}

static void test_declare_without_semicolon(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local i = 0;"
		"local o = {};"
		"local o2 = {}"
		"local a = [];"
		"local a2 = []");

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, code, code, &errors);

	if (!lex_and_parse(code.chars, &parser, ctx))
		return;

	rook_parser_free(&parser);
}

void(*rook_parser_tests[])(struct rook_test_context*) =
{
	test_array,
	rook_test_parser,
	test_map,
	test_func_decl,
	test_block,
	test_return,
	test_for,
	test_for_each,
	test_braceless_func,
	test_incr_decr,
	test_attributes,
	test_declare_without_semicolon,
	0
};
