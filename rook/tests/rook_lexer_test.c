#include "rook_lexer_test.h"



#include "rook_test_context.h"

#include <utility/stb_sb.h>
#include <utility/rook_error.h>

#include <core/compiler/rook_lexer.h>
#include <core/compiler/rook_parser.h>
#include <core/compiler/rook_token.h>
#include <core/compiler/rook_token_type.h>

static enum rook_token_type token_types[] =
{
	rook_token_type_identifier,
	rook_token_type_null,
	rook_token_type_true,
	rook_token_type_false,
	rook_token_type_number,
	rook_token_type_number,
	rook_token_type_string,

	rook_token_type_assignment,
	rook_token_type_colon,
	rook_token_type_semicolon,
	rook_token_type_comma,
	rook_token_type_dot,
	rook_token_type_arrow,

	rook_token_type_left_par,
	rook_token_type_right_par,

	rook_token_type_left_square,
	rook_token_type_right_square,

	rook_token_type_left_curly,
	rook_token_type_right_curly,

	rook_token_type_add,
	rook_token_type_sub,
	rook_token_type_star,
	rook_token_type_div,
	rook_token_type_modulo,

	rook_token_type_add_assign,
	rook_token_type_sub_assign,
	rook_token_type_star_assign,
	rook_token_type_div_assign,
	rook_token_type_modulo_assign,
	rook_token_type_increment,
	rook_token_type_decrement,

	rook_token_type_equal,
	rook_token_type_not_eq,
	rook_token_type_less,
	rook_token_type_less_eq,
	rook_token_type_greater,
	rook_token_type_greater_eq,

	rook_token_type_not,
	rook_token_type_and,
	rook_token_type_or,

	rook_token_type_global,
	rook_token_type_local,
	rook_token_type_const,
	rook_token_type_if,
	rook_token_type_else,
	rook_token_type_while,
	rook_token_type_for,
	rook_token_type_case,
	rook_token_type_break,
	rook_token_type_continue,
	rook_token_type_defer,
	rook_token_type_return,
};

static void rook_test_lexer(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"apa null true false 34 34.5 \"Carl\" "
		"= : ; , . -> ( ) [ ] { } + - * / % "
		"+= -= *= /= %= ++ -- "
		"== != < <= > >= ! && || "
		"global local const if else while for "
		"case break continue defer return");

	rook_error* errors = 0;
	rook_parser parser;
	rook_parser_init(&parser, code, code, &errors);

	if (!rook_lex(code.chars, &parser))
	{
		rook_error* error;

		test_ctx_add_error(ctx, "Failed to lex code");
		for (error = *parser.errors; error < *parser.errors + sb_count(*parser.errors); ++error)
		{
			test_ctx_add_error(ctx, error->msg.chars);
		}
	}
	else
	{
		int i;

		for (i = 0; i < parser.token_count && i < sizeof(token_types) / sizeof(*token_types); ++i)
		{
			assert_are_equal(ctx, token_types[i], parser.tokens[i].type, "Wrong type lexed");
		}
	}

	rook_parser_free(&parser);
}

void(*rook_lexer_tests[2])(struct rook_test_context*) =
{
	rook_test_lexer,
	0
};
