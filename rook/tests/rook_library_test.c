#include "rook_library_test.h"
#include "rook_test_context.h"

#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_obj_string.h>
#include <core/runtime/rook_array.h>
#include <core/runtime/rook_func.h>

#include <library/rook_standard_library.h>

#include <stdio.h>
#include <string.h>

static void test_array_sort(struct rook_test_context* ctx)
{
	const char* code =
		"local a = [ 3, 5, 1, 0, 2, 8, 6 ];"		"\n"
		"local i = 0;"								"\n"
		"while (i < a.count())"						"\n"
		"{"											"\n"
		"    print(a[i]);"							"\n"
		"    ++i;"									"\n"
		"}"											"\n"
		"a.sort((l, r) -> l > r);"					"\n"
		"i = 0;"									"\n"
		"while (i < a.count())"						"\n"
		"{"											"\n"
		"    print(a[i]);"							"\n"
		"    ++i;"									"\n"
		"}"											"\n"
		"";

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);
	rook_lib_register_array(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_readfile(struct rook_test_context* ctx)
{
	const char* code =
		"local content = readfile('test.txt');";

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_writefile(struct rook_test_context* ctx)
{
	const char* code =
		"local content = writefile('test.txt', 'Hello mr File!');";

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_dofile(struct rook_test_context* ctx)
{
	const char* code =
		"dofile('test.rs');";

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);
	rook_lib_register_array(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_fromjson(struct rook_test_context* ctx)
{
	const char* code =
		"local obj = {"									"\n"
		"	nully = null,"								"\n"
		"	booleani = true,"							"\n"
		"	booleani2 = false,"							"\n"
		"	num = 234179.342,"							"\n"
		"	str = \"Hello!\","							"\n"
		"	arr = ["									"\n"
		"		\"test\","								"\n"
		"		234,"									"\n"
		"		false"									"\n"
		"	],"											"\n"
		"	mappy = {"									"\n"
		"		error = \"No errors\","					"\n"
		"		howmany = 0,"							"\n"
		"		func = (a1:number, a2) -> (a1 + 12),"	"\n"
		"		good = true"							"\n"
		"	}"											"\n"
		"};"											"\n"
		""												"\n"
		"print(obj);"									"\n"
		"local json = tojson(obj);"						"\n"
		"print(json);"									"\n"
		"obj = fromjson(json);"							"\n"
		"print(obj);"									"\n"
		;
	
	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_string(struct rook_test_context* ctx)
{
	const char* code =
		"local str1 = \"long string with stuff\";"						"\n"
		"local str2 = str1.substring(5, 8);"							"\n"
		""																"\n"
		"print(\"str1.empty(): \", str1.empty());"						"\n"
		"print(\"str2.empty(): \", str2.empty());"						"\n"
		"print(\"str1: \", str1);"										"\n"
		"print(\"str2: \", str2);"										"\n"
		"print(\"str 2.substring(3): \", str2.substring(3));"			"\n"
		"print(\"str2.reverse(): \", str2.reverse());"					"\n"
		"print(\"str1.reverse(): \", str1.reverse());"					"\n"
		"print(\"str1.remove('st'): \", str1.remove('st'));"			"\n"
		"print(\"'harry'.find('r'): \", 'harry'.find('r'));"			"\n"
		"print(\"'harry'.rfind('r'): \", 'harry'.rfind('r'));"			"\n"
		"print(\"'harry'.findall('r'): \", 'harry'.findall('r'));"		"\n"
		"print(\"'harry'.contains('r'): \", 'harry'.contains('r'));"	"\n"
		//"print(\"'harry'.replace('rr'): \", 'harry'.replace('w'));"		"\n"
		//"print(\"'harry'.replace('r'): \", 'harry'.replace('w'));"		"\n"
		//"print(\"'harry'.replace('ar'): \", 'harry'.replace('e'));"		"\n"
		;

	const char* code2 =
		"local str1 = \"fix this sing\";"					"\n"
		"print(\"str1 before insert: \", str1);"			"\n"
		"str1.insert(10, 'tr');"							"\n"
		"print(\"str1 after insert: \", str1);"				"\n"
		;

	const char* code3 =
		"print(\"'harry'.endswith('r'): \", 'harry'.endswith('r'));"		"\n"
		"print(\"'harry'.endswith('y'): \", 'harry'.endswith('y'));"		"\n"
		"print(\"'harry'.startswith('r'): \", 'harry'.startswith('r'));"	"\n"
		"print(\"'harry'.startswith('h'): \", 'harry'.startswith('h'));"	"\n"
		"print(\"'Harry'.toupper(): \", 'Harry'.toupper());"				"\n"
		"print(\"'Harry'.tolower(): \", 'Harry'.tolower());"				"\n"
		;

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);
	rook_lib_register_string(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, rook_cstring_from_chars(code2), rook_cstring_from_literal(__FUNCTION__));
	rook_vm_run_string(&vm, rook_cstring_from_chars(code3), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_math(struct rook_test_context* ctx)
{
	const char* code =
		"print(\"math.max(2, 12): \", math.max(2, 12));"									"\n"
		"print(\"math.max(2, 12, 43, 12, 233, 3): \", math.max(2, 12, 43, 12, 233, 3));"	"\n"
		;

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);
	rook_lib_register_math(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_get_callstack(struct rook_test_context* ctx)
{
	const char* code =
		"local func1 = () ->		""\n"
		"{							""\n"
		"	return get_callstack();	""\n"
		"};							""\n"
		"							""\n"
		"local func2 = () ->		""\n"
		"{							""\n"
		"	return func1();			""\n"
		"};							""\n"
		"							""\n"
		"local func3 = () ->		""\n"
		"{							""\n"
		"	return func2();			""\n"
		"};							""\n"
		"							""\n"
		"local func4 = () ->		""\n"
		"{							""\n"
		"	return func3();			""\n"
		"};							""\n"
		"							""\n"
		"print(func4());";


	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_split_join(struct rook_test_context* ctx)
{
	const char* code =
		"local name = 'Carl';			\n"
		"								\n"
		"local a = [					\n"
		"	true,						\n"
		"	'True',						\n"
		"	234,						\n"
		"	66.34,						\n"
		"	name						\n"
		"];								\n"
		"								\n"
		"local s = a.join(',');			\n"
		"print('s:', s);				\n"
		"print('s2:', s.split(',').join(';'));	\n"
		;

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);
	rook_lib_register_string(&vm);

	rook_vm_run_string(&vm, rook_cstring_from_chars(code), rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_clone(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local name = 'Test Testsson';							\n"
		"														\n"
		"local names = [										\n"
		"	'Testerino',										\n"
		"	'Testy McTesty',									\n"
		"	'Testana Testanavic'								\n"
		"];														\n"
		"														\n"
		"print('name:', name);									\n"
		"local name_clone = clone(name);						\n"
		"print('name_clone:', name_clone);						\n"
		"														\n"
		"local names_clone = clone(names);						\n"
		"names_clone[1] = 'Testy MacTesty';						\n"
		"														\n"
		"print('names:', names, 'names_clone:', names_clone);	\n")
		;

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_getlocals(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local outerst = 'yes';					\n"
		"										\n"
		"{										\n"
		"	local inScope = 'yesy';				\n"
		"	local f1 = (p1) -> {				\n"
		"		local inFunc = 'no';			\n"
		"										\n"
		"		local locals = getlocals();		\n"
		"		print('\nlocals in f1:');		\n"
		"		for (ln, lv : locals)			\n"
		"		{								\n"
		"			print(ln, ':', lv);			\n"
		"		}								\n"
		"										\n"
		"		locals = getlocals();			\n"
		"		print('\nlocals in f1 2:');	\n"
		"		for (ln, lv : locals)			\n"
		"		{								\n"
		"			print(ln, ':', lv);			\n"
		"		}								\n"
		"	};									\n"
		"										\n"
		"	f1('argument');						\n"
		"										\n"
		"	local locals = getlocals();			\n"
		"	print('\nlocals in scope:');		\n"
		"	for (ln, lv : locals)				\n"
		"	{									\n"
		"		print(ln, ':', lv);				\n"
		"	}									\n"
		"}										\n"
		"										\n"
		"local locals = getlocals();			\n"
		"print('\nlocals in outer scope:');	\n"
		"for (ln, lv : locals)					\n"
		"{										\n"
		"	print(ln, ':', lv);					\n"
		"}										\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_tag_extensions_library(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local print_sound = () -> print(this.sound);			\n"
		"local dog_tag = register_tag('dog');					\n"
		"register_tag_extension(dog_tag, 'talk', print_sound);	\n"
		"local my_dog = create_tagged_obj({}, dog_tag);			\n"
		"my_dog.sound = 'woof';									\n"
		"my_dog.talk();											\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_func_tostring(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local func_without = () -> print('nothing');			\n"
		"local func_with1 = (first) -> print(first);			\n"
		"local func_with2 = (first, sec) -> print(first, sec);	\n"
		"\n"
		"print(tostring(func_without));							\n"
		"print(tostring(func_with1));							\n"
		"print(tostring(func_with2));							\n"
		"print(tostring(assert));								\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));

	rook_vm_free(&vm);
}

static void test_interpret_serialize_func(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local add = (a, b) -> { return a + b; };"		"\n"
		"return add;"									"\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	if (!assert_are_equal(ctx, sb_count(vm.errors), 0, "Failed to run script"))
		return;

	rook_obj obj = rook_vm_stack_get(&vm, -1);
	rook_obj_incref(&obj);
	if (!assert_are_equal(ctx, rook_obj_get_type(obj), rook_obj_type_func, "Expected raw func returned"))
		return;

	rook_vm_stack_pop(&vm, 1);
	
	rook_string func_data = { 0 };
	assert_is_true(ctx, rook_func_serialize(obj.as.func, &func_data, &vm.errors), "Failed to serialize func");
	
	rook_obj deserialized_func = rook_obj_create(&vm.runtime_allocator, rook_obj_type_func);
	assert_is_true(ctx, rook_func_deserialize(&func_data, deserialized_func.as.func, &vm.errors, &vm.runtime_allocator), "Failed to deserialize func");

	rook_obj args[2] = {
		rook_obj_create_number(2),
		rook_obj_create_number(3)
	};
	int result = rook_vm_run_obj_args(&vm, deserialized_func, 2, args);	
	if (!assert_are_equal(ctx, 1, result, "Expected one result from run_obj_args"))
		return;

	rook_obj sum = rook_vm_stack_get(&vm, -1);
	rook_obj_incref(&sum);
	if (!assert_are_equal(ctx, rook_obj_get_type(sum), rook_obj_type_number, "Expected number returned"))
		return;

	rook_vm_stack_pop(&vm, 1);

	if (!assert_are_equal(ctx, sum.as.number, 5, "Expected sum 5"))
		return;

	rook_vm_free(&vm);
}

static void test_interpret_serialize_func2(struct rook_test_context* ctx)
{
	rook_cstring code = rook_cstring_from_literal(
		"local add = (a, b) -> { return a + b; };"		"\n"
		"return serialize_func(add);"					"\n"
	);

	rook_vm vm;
	rook_vm_init(&vm);
	rook_lib_register_general(&vm);

	rook_vm_run_string(&vm, code, rook_cstring_from_literal(__FUNCTION__));
	if (!assert_are_equal(ctx, sb_count(vm.errors), 0, "Failed to run script"))
		return;

	rook_obj func_data = rook_vm_stack_get(&vm, -1);
	rook_obj_incref(&func_data);
	if (!assert_are_equal(ctx, rook_obj_get_type(func_data), rook_obj_type_string, "Expected string of func data returned"))
		return;

	//rook_vm_stack_pop(&vm, 1);

	rook_vm_register_global(&vm, rook_cstring_from_literal("add_serialized"), func_data);
	
	rook_cstring code2 = rook_cstring_from_literal(
		"local add = deserialize_func(add_serialized);"		"\n"
		"return add(2, 3);"									"\n"
	);
	
	rook_vm_run_string(&vm, code2, rook_cstring_from_literal(__FUNCTION__));
	if (!assert_are_equal(ctx, sb_count(vm.errors), 0, "Failed to run second script"))
		return;

	rook_obj sum = rook_vm_stack_get(&vm, -1);
	rook_obj_incref(&sum);
	if (!assert_are_equal(ctx, rook_obj_get_type(sum), rook_obj_type_number, "Expected number returned"))
		return;

	rook_vm_stack_pop(&vm, 1);

	if (!assert_are_equal(ctx, sum.as.number, 5, "Expected sum 5"))
		return;

	rook_vm_free(&vm);
}

void(*rook_library_tests[])(struct rook_test_context*) = {
	test_array_sort,
	test_readfile,
	test_writefile,
	test_dofile,
	test_fromjson,
	test_string,
	test_math,
	test_get_callstack,
	test_split_join,
	test_clone,
	test_getlocals,
	test_interpret_tag_extensions_library,
	test_func_tostring,
	test_interpret_serialize_func,
	test_interpret_serialize_func2,
	0
};
