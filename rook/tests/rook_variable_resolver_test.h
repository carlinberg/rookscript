#ifndef ROOK_VARIABLE_RESOLVER_TEST
#define ROOK_VARIABLE_RESOLVER_TEST

struct rook_test_context;

extern void(*rook_variable_resolver_tests[])(struct rook_test_context*);

#endif // !ROOK_VARIABLE_RESOLVER_TEST
