#include <utility/stb_sb.h>

#include <core/runtime/rook_vm.h>
#include <core/runtime/rook_obj_string.h>
#include <core/runtime/rook_array.h>
#include <core/runtime/rook_map.h>

#include <library/rook_standard_library.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

static char* current_line = 0;


void get_line();
void display_error(rook_vm* vm);
void display_result(rook_vm* vm, int ret_count);
void print_value(rook_obj obj);
rook_string get_code_line(rook_cstring code, int line);

int main()
{
	int ret_count;
	rook_vm vm;

	rook_vm_init(&vm);
	rook_lib_register_general(&vm);
	rook_lib_register_string(&vm);
	rook_lib_register_array(&vm);
	rook_lib_register_map(&vm);
	rook_lib_register_math(&vm);

	while (1)
	{
		get_line();

		if (current_line == 0 || *current_line == '\0')
			break;
		if (strcmp(current_line, "quit\n") == 0)
			break;

		ret_count = rook_vm_run_string(&vm, rook_cstring_from_chars(current_line), rook_cstring_from_literal("cmd line"));
		if (ret_count < 0)
			display_error(&vm);
		else if (ret_count > 0)
			display_result(&vm, ret_count);
	}

	return 0;
}

void get_line()
{
	char c;
	char p1 = '\0';
	char p2 = '\0';
	int i;

	sb_clear(current_line);

	printf(">> ");

	while (1)
	{
		c = (char)fgetc(stdin);
		if (c == EOF)
			break;

		sb_push(current_line, c);

		if (c == '\n' && (p1 != '-' || p2 != '-'))
			break;

		p2 = p1;
		p1 = c;
	}

	sb_push(current_line, '\0');

	for (i = 0; current_line[i]; ++i)
	{
		if (strncmp(current_line + i, "--\n", 3) == 0)
		{
			current_line[i] = ' ';
			current_line[i + 1] = ' ';
		}
	}
}

void display_error(rook_vm* vm)
{
	int i;
	rook_string code_line;

	for (i = 0; i < sb_count(vm->errors); ++i)
	{
		code_line = rook_error_show_error_in_code(&vm->errors[i]);

		printf("Error in '%s' on line %d: %s\n", vm->errors[i].file.chars, vm->errors[i].line, vm->errors[i].msg.chars);
		printf("%s\n", code_line.chars);

		rook_string_free(&code_line);
	}

	sb_clear(vm->errors);
	rook_vm_stack_pop(vm, rook_vm_stack_size(vm));
}

void display_result(rook_vm* vm, int ret_count)
{
	int i, l;
	rook_obj obj;
	char buffer[2048];

	for (i = 0; i < ret_count; ++i)
	{
		obj = rook_vm_stack_get(vm, i);

		if (rook_obj_to_string(obj, buffer, sizeof(buffer), &l))
		{
			buffer[l] = '\0';
			printf("%s\n", buffer);
		}
	}

	rook_vm_stack_pop(vm, ret_count);

	printf("\n");
}
