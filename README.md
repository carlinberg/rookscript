# Rook

Simplistic C-like dynamic scripting language inspired by Lua, Squirrel (and Javascript). The main goal of the language is to have a simple syntax and for integrating with C and C++ to be very easy and pain free.


## Building
To build, run build.bat (on windows) or build.sh (on linux (and mac?)). The script takes two parameters, configuration (debug/release) and platform (x86/x64). An example is "build.bat d x64". Check .vscode/tasks.json to see more.
To be able to run build.bat, the parent folder of vcvarsall.bat must be added to PATH. It is usually in a place similar to one of these paths:
- C:\Program Files\Microsoft Visual Studio\2022\Professional\VC\Auxiliary\Build\vcvarsall.bat
- C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat

## Tests
Unfortunately the tests are pretty bad. They are more "testing out the features" than "testing that the features work". One day I would like to create better and more organized tests. However using the language for real purposes is obviously the best form of testing, and is therefore the priority.